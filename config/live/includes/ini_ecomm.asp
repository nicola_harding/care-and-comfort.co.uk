<!--#include virtual="/includes/ini_common.asp" -->
<%
'// SWITCH USED TO SHOW/HIDE PRODUCT CODES ON RELATIONSHIP CREATION FORMS
const SHOW_PRCODES_ONSEL = true 
const SHOW_SZCODES_ONSEL = true 

const SEARCH_RESULTS_PER_PAGE = 2
const SEARCH_ORDER_BY_PROD_NAME = ""
const SEARCH_ORDER_BY_PROD_PRICE = ""

dim DELIVERY_AVAILABILITY_MSG   : DELIVERY_AVAILABILITY_MSG = "<p class=""red_text""><strong>Apologies, internationally this product has availability limitations.</strong></p><p><strong>Weight and / or size dimensions create shipping difficulties.</strong></p>"
dim DELIVERY_MSG_UK             : DELIVERY_MSG_UK = "<p>Postage &amp; Packaging will be charged at flat rate of {0} for all UK mainland destinations.</p>"
dim DELIVERY_MSG_EURO           : DELIVERY_MSG_EURO = "<p>Postage &amp; Packaging will be charged at flat rate of {0} for all European destinations.</p>"
dim DELIVERY_MSG_INT            : DELIVERY_MSG_INT = "<p>Postage &amp; Packaging will be charged at flat rate of {0} for all International destinations.</p>"
dim DELIVERY_COST_UK            : DELIVERY_COST_UK = cdbl(0)
dim DELIVERY_COST_EURO          : DELIVERY_COST_EURO = cdbl(7.95)
dim DELIVERY_COST_USA           : DELIVERY_COST_USA = cdbl(10.00)

dim MSG_HELP_DONT_WORRY, DEFAULT_EXCEEDED_MSG, DEFAULT_UNIT_IMG_PRICE

MSG_HELP_DONT_WORRY = "<p>Remember, you do not need to worry about sending us your photographs right now. " _
                      &  "We shall contact you once your order is complete to help you with collating and sending your pictures to us.</p>" 


const ERR_MSG_GEN = "<p>Apologies, an error has occurred, the site administrator has been notified. <br/>Please <a href=""/contact/"" title=""contact us immediately to report this problem"">contact us</a> immediately to report this problem or <a href=""/contact/"" title=""contact us immediately to discuss offline ordering"">discuss offline ordering</a> with our friendly team.</p>"
const ERR_MSG_NONE = "<p>No products were found.</p>"

dim ECOM_CUR_SYMBOL         : ECOM_CUR_SYMBOL = "&pound;"
dim ECOM_CUR_SPLITTER1      : ECOM_CUR_SPLITTER1 = "|"
dim ECOM_CUR_SPLITTER2      : ECOM_CUR_SPLITTER2 = "/"

' Used for nixs protx functions frig
dim ECOM_CUR_ISO_EURO       : ECOM_CUR_ISO_EURO = "EUR "
dim ECOM_CUR_ISO_USA        : ECOM_CUR_ISO_USA = "USD "
dim ECOM_CUR_ISO_POUND      : ECOM_CUR_ISO_POUND = "GBP "

const NAV_CAT_STOCK_PHOTO_ID            = 3

const COLLECTION_ART_OF_TRAVEL          = 2
const COLLECTION_NOTICEBOARD            = 3
const COLLECTION_BLINDS                 = 4
const COLLECTION_WALLPAPER              = 5
const COLLECTION_TILES_CERAMIC          = 6
const COLLECTION_GARDEN_ART             = 7

' This Frig is used to aid photobook duplicate ordering. the code will look for this word in the notes field as an indicatyor of which price usage applies
const PRODUCT_TYPE_PHOTOBOOK_FRIG_NOTE  = "Duplicate"

' These are used for 001_Collection purchasing process only
const PRODUCT_TYPE_COL_MLD_BLK  = 2 	
const PRODUCT_TYPE_COL_FR_AL_BR = 33	
const PRODUCT_TYPE_COL_WD_WHT   = 34	
const PRODUCT_TYPE_COL_FR_AL    = 35	
const PRODUCT_TYPE_COL_WD_BLK   = 36	
const PRODUCT_TYPE_COL_CAN      = 37	
const PRODUCT_TYPE_COL_AC_BX    = 38	
const PRODUCT_TYPE_COL_AC_BX_F  = 39	
const PRODUCT_TYPE_COL_AC_DIG   = 40	
const PRODUCT_TYPE_COL_P_GTY    = 41	
const PRODUCT_TYPE_COL_MIR_BLK  = 42	
const PRODUCT_TYPE_COL_P_STK    = 43	
const PRODUCT_TYPE_COL_MIR_NAT  = 47	
                                       



'// lets define some global constants for the different areas //
const SHOW_OPTS = 1 '// show the options screen at all? //
const POST_OPTS = 0 '// no del options //
const VCHR_OPTS = 0 '// accepts vouchers //
const VAT_OPTS  = 0  '// standard //
const VAT_ON_ALL= 1 '// all products incur VAT in full //
const WRAP_OPTS = 0 '// none //
const DEL_OPTS  = 1 '// none //
const GIFT_OPTS = 1 '// none //
const PAY_METH  = 1  '// enc email //
'// Image upload and resizing definitions use 0 where image isn't used //
const IMG_RESIZE_DFT = 0  '// Resize uploads by default? //
const IMG_PRESERVE_R = 0  '// Preserve Image Aspect Ratio By Default //
'// Canvas Background Colours //
const IMG_CANVAS_CLR = &HFFFFFF '// white - hex value &HRRGGBB //

dim aIMG_CANVAS_BG
aIMG_CANVAS_BG = array( _
array("default",IMG_CANVAS_CLR, "#FFFFFF"), _
array("white",&HFFFFFF, "#FFFFFF"), _
array("black",&H000000, "#000000"), _
array("grey" ,&HF3F3F2, "#F3F3F2") _
)





'// lets define some global constants for the different areas //
const BSKT_CMS_ID = 18
const EMAIL_WRAPLEN = 67
const SUMMARY_LENGTH = 220

const SHOP_NAME = "Care and Comfort"
const SHOP_POST_ADDRESS = ""
const SHOP_TEL_NO = "01702-530847"


const BSKT_HDR_CLR          = "#E64396" ' done
const BSKT_PROMO_BGD_CLR    = "#848482" ' done
const BSKT_HDR_CLR_PRINT    = "#FFFFFF"
const BSKT_ROW_CLR          = "#FFFFFF"
const BSKT_BGD_CLR          = "#FFFFFF"
const MAX_LAST_ITEMS        = 2
const DFLT_THUMBNAIL        = "clearpix.gif"

const ECOM_IMAGE_DESIGN_DIR = "/images/blocks/"
const ECOM_IMAGE_BUTTON_DIR = "/images/blocks/"
const ECOM_IMAGE_DIR = "/ecomm/graphics/"
const ECOM_FILE_DIR = "/ecomm/files/"
const ECOMM_IMAGE_UNAVAILABLE_TH = "Image_unavailable_th.GIF"
const ECOMM_IMAGE_UNAVAILABLE_MN = "Image_unavailable_mn.GIF"

'// Product Shots  //
const IMG_SZ_PR_TH_W = 195
const IMG_SZ_PR_TH_H = 95
const IMG_SZ_PR_MN_W = 133
const IMG_SZ_PR_MN_H = 133
const IMG_SZ_PR_LG_W = 224
const IMG_SZ_PR_LG_H = 224
'// Manufacturer Shots //
const IMG_SZ_MF_TH_W = 0
const IMG_SZ_MF_TH_H = 0
const IMG_SZ_MF_MN_W = 0
const IMG_SZ_MF_MN_H = 0
'// Range Shots    //
const IMG_SZ_RN_TH_W = 133
const IMG_SZ_RN_TH_H = 133
const IMG_SZ_RN_MN_W = 0
const IMG_SZ_RN_MN_H = 0
'// Group Shots    //
const IMG_SZ_GR_TH_W = 133
const IMG_SZ_GR_TH_H = 100
const IMG_SZ_GR_MN_W = 0
const IMG_SZ_GR_MN_H = 0
'// Category Shots //
const IMG_SZ_CA_TH_W = 133
const IMG_SZ_CA_TH_H = 100
const IMG_SZ_CA_MN_W = 472
const IMG_SZ_CA_MN_H = 192
'// Product Type Shots //
const IMG_SZ_PT_TH_W = 133
const IMG_SZ_PT_TH_H = 100
const IMG_SZ_PT_MN_W = 0
const IMG_SZ_PT_MN_H = 0
'// Collection Shots  //
const IMG_SZ_CL_TH_W = 0
const IMG_SZ_CL_TH_H = 0
const IMG_SZ_CL_MN_W = 0
const IMG_SZ_CL_MN_H = 0
const IMG_SZ_CL_LG_W = 0
const IMG_SZ_CL_LG_H = 0

' Care and Comfort specific constants
Const CAT_MATTRESSES_ID = 12
Const CAT_BEDS_ID = 16
'Const BED_COLLECTION_ID = 13
Const BED_TOPPERS_ID = 13
Const CAT_PILLOWS_ID = 15

'Care and comfort content pages

Const TOPPERS_CONTENT_ID = 84
Const MATTRESSES_CONTENT_ID = 85
Const PILLOWS_CONTENT_ID = 86
Const COVERS_CONTENT_ID = 87

'// db DSNs //
dim DSN_NEWS_DB, DSN_ECOM_DB, IMAGE_DIRECTORY, FILE_DIRECTORY
dim CHECKOUT_URL, SHOP_TANDC_URL

' // Nix has moved the following to ini_common as the can be used across the website
' // EML_SHOPFROMEMAIL, EML_SHOPFROMNAME, EML_SHOPTOEMAIL, EML_SHOPTOEMAIL
dim EML_SHOPFROMEMAIL_ECOMM, EML_SHOPTOEMAIL_ECOMM
const EML_PASSWORDSUBJECT = "Your login details for www.care-and-comfort.com"

dim PROTX_SITE_URL, PROTX_VENDOR_EMAIL
const PROTX_CLIENT_NAME = "careandcomfort"
'const PROTX_CLIENT_NAME = "www.55max.com"


' // Temporary import variables
Dim DSN_ECOMM_LEGACY
Dim IMAGES_DIR_ROOT
Dim IMAGES_DIR_ROOT_NEW


' Live / pre-live env
if IsEnvironmentLive(sHostHeader) then
    'response.Write "live"
    IMAGE_DIRECTORY = "E:\webs\care-and-comfort.co.uk\www\ecomm\graphics\"
    FILE_DIRECTORY = "E:\webs\care-and-comfort.co.uk\www\ecomm\files\"
    
    '// temporary overrides for db testing //
    'DSN_NEWS_DB 	= "Provider=SQLOLEDB; Data Source=192.168.1.133; Network Library=DBMSSOCN; Initial Catalog=CareandComfortEcommV3;User ID=CareandComfortEcommV3User;Password=Fj6w9@82W;"
    'DSN_ECOM_DB 	= "Provider=SQLOLEDB; Data Source=192.168.1.133; Network Library=DBMSSOCN; Initial Catalog=CareandComfortEcommV3;User ID=CareandComfortEcommV3User;Password=Fj6w9@82W;"
       

DSN_NEWS_DB  = "Provider=SQLNCLI11;SERVER=192.168.1.69;Database=CareandComfortEcommV3;Integrated Security=SSPI;DataTypeCompatibility=80;"
DSN_ECOM_DB  = "Provider=SQLNCLI11;SERVER=192.168.1.69;Database=CareandComfortEcommV3;Integrated Security=SSPI;DataTypeCompatibility=80;"


    ' Live ecomm url settings
    CHECKOUT_URL        = "https://www.care-and-comfort.co.uk/shoppingbasket/checkout.asp"
    SHOP_TANDC_URL      = "http://www.care-and-comfort.co.uk/termsandconditions"
    
    PROTX_SITE_URL              =  "http://www.care-and-comfort.co.uk"
    PROTX_VENDOR_EMAIL          = "info@care-and-comfort.co.uk" 
    
    EML_SHOPFROMEMAIL_ECOMM     = "info@care-and-comfort.co.uk"
    EML_SHOPTOEMAIL_ECOMM       = "info@care-and-comfort.co.uk"
else
    if IsEnvironmentPreLive(sHostHeader) then
        'response.Write "pre-live"
        IMAGE_DIRECTORY = "E:\webs\care-and-comfort.co.uk\www\ecomm\graphics\"
        FILE_DIRECTORY  = "E:\webs\care-and-comfort.co.uk\www\ecomm\ecomm\files\"
        
        '// temporary overrides for db testing //
		'DSN_NEWS_DB 	= "Provider=SQLOLEDB; Data Source=192.168.1.133; Network Library=DBMSSOCN; Initial Catalog=CareandComfortEcommV3;User ID=CareandComfortEcommV3User;Password=Fj6w9@82W;"
		'DSN_ECOM_DB 	= "Provider=SQLOLEDB; Data Source=192.168.1.133; Network Library=DBMSSOCN; Initial Catalog=CareandComfortEcommV3;User ID=CareandComfortEcommV3User;Password=Fj6w9@82W;"
        

DSN_NEWS_DB  = "Provider=SQLNCLI11;SERVER=192.168.1.69;Database=CareandComfortEcommV3;Integrated Security=SSPI;DataTypeCompatibility=80;"
DSN_ECOM_DB  = "Provider=SQLNCLI11;SERVER=192.168.1.69;Database=CareandComfortEcommV3;Integrated Security=SSPI;DataTypeCompatibility=80;"

        ' Live ecomm url settings
        CHECKOUT_URL      = "http://careandcomfort.nvisage.uk.com/shoppingbasket/checkout.asp"
        SHOP_TANDC_URL    = "http://careandcomfort.nvisage.uk.com/termsandconditions"
        
        PROTX_SITE_URL              = "http://careandcomfort.nvisage.uk.com"   
        PROTX_VENDOR_EMAIL          = DEVELOPER_EMAIL1     
        
        EML_SHOPFROMEMAIL_ECOMM     = DEVELOPER_EMAIL1
        EML_SHOPTOEMAIL_ECOMM       = DEVELOPER_EMAIL1
        
           ' DSN_ECOMM_LEGACY = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=D:\webs\55max.com\DATA\55MAX.mdb"
        IMAGES_DIR_ROOT = "E:\webs\care-and-comfort.co.uk\www\ecomm\htdocs\"
        IMAGES_DIR_ROOT_NEW = "E:\webs\care-and-comfort.co.uk\www\ecomm\graphics\"
    
	else
        ' dev env    
    
		if IsEnvironmentDev(sHostHeader) then
            'response.Write "dev"
            IMAGE_DIRECTORY = "E:\webs\careandcomfort\www\ecomm\graphics\"
            FILE_DIRECTORY  = "E:\webs\careandcomfort\www\ecomm\ecomm\files\"
            
           'DSN_NEWS_DB 	= "Provider=SQLOLEDB; Data Source=192.168.1.133; Network Library=DBMSSOCN; Initial Catalog=CareandComfortEcommV3;User ID=CareandComfortEcommV3User;Password=Fj6w9@82W;"
		  ' DSN_ECOM_DB 	= "Provider=SQLOLEDB; Data Source=192.168.1.133; Network Library=DBMSSOCN; Initial Catalog=CareandComfortEcommV3;User ID=CareandComfortEcommV3User;Password=Fj6w9@82W;"
		
DSN_NEWS_DB  = "Provider=SQLNCLI11;SERVER=192.168.1.69;Database=CareandComfortEcommV3;Integrated Security=SSPI;DataTypeCompatibility=80;"
DSN_ECOM_DB  = "Provider=SQLNCLI11;SERVER=192.168.1.69;Database=CareandComfortEcommV3;Integrated Security=SSPI;DataTypeCompatibility=80;"		
         
            ' Dev ecomm settings
            CHECKOUT_URL      = "http://careandcomfort/shoppingbasket/checkout.asp"
            SHOP_TANDC_URL    = "http://careandcomfort/termsandconditions"
            
            PROTX_SITE_URL              = "http://careandcomfort"
            PROTX_VENDOR_EMAIL          = DEVELOPER_EMAIL1
            
            EML_SHOPFROMEMAIL_ECOMM     = DEVELOPER_EMAIL1
            EML_SHOPTOEMAIL_ECOMM       = DEVELOPER_EMAIL1
            
            'DSN_ECOMM_LEGACY = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=C:\inetpub\wwwroot\www.55max.com\DATA\55MAX.mdb"
            IMAGES_DIR_ROOT = "D:\webs\careandcomfort\www\ecomm\htdocs\"
            IMAGES_DIR_ROOT_NEW = "D:\webs\careandcomfort\www\ecomm\graphics\"
            
        else
            ' // technically not possible so write an annoying message for someone to pic up on this logic error
            response.Write "Critical: Environment has not been detected correctly. Site settings will not be initialised correctly"
        end if
    end if 
end if


'Response.Write DSN_ECOM_DB
%>