<%
'// Installation Version ID
const INST_ID = 45

'// lets define some global constants //
const EMAIL_DEBUG = false
const SMTP_REMOTE_HOST = "localhost"
const ADMIN_URL = "/admin"

const EML_SHOPFROMEMAIL = "info@care-and-comfort.co.uk"
const EML_SHOPFROMNAME = "Care & Comfort Products Limited"
const EML_SHOPTOEMAIL = "info@care-and-comfort.co.uk"
const EML_SHOPTONAME = "Care & Comfort Products Limited"
const NVISAGE_IP = "84.12.51.209"
const DEVELOPER_EMAIL1 = "orders@nvisage.co.uk"
'const DEVELOPER_EMAIL1 = "nicola.harding@nvisage.uk.com"
'const DEVELOPER_EMAIL2 = "ryan.moynan@nvisage.uk.com"
'const DEVELOPER_EMAIL3 = "dru.moore@nvisage.co.uk"
'const DEVELOPER_EMAIL4 = "martin.janes@nvisage.co.uk"

const BREADCRUMB_STRING_VARIABLE_SITE_AREA_SEPARATOR = "<>"
const BREADCRUMB_STRING_VARIABLE_HYPERLINK_SEPARATOR = "|"
const BREADCRUMB_DISPLAY_SITE_AREA_SEPARATOR = " - "
const BREADCRUMB_DISPLAY_INTRODUCTION_TEXT = "You're in: "
const BREADCRUMB_DISPLAY_HYPERLINK_TITLE_TEXT = "Navigate to: "
const BR_SITEAREA = 0
const BR_SITEAREA_LINK = 1
Dim REMOTE_HOST

CONST SANDBOX = FALSE
dim B_PARTNERAREA : B_PARTNERAREA = FALSE

dim DATA_DIR : DATA_DIR	= replace(Server.MapPath("/") & "\", "\www\", "\data")

'// file paths & db DSNs - Common //
Dim DSN_ADMIN_USERS, SITE_URL, sHostHeader, sFormsEmailTo
sHostHeader = lcase(REQUEST.SERVERVARIABLES("SERVER_NAME"))

'// DSN for Local Partners area
'DSN_PARTNERS = "Provider=SQLOLEDB; Data Source=(local); Network Library=DBMSSOCN; Initial Catalog=Commercialise;User ID=CommercialiseUser;Password=m1nky487lappy;"


' Live 
if IsEnvironmentLive(sHostHeader) then
    ' Nix says: Def correct on wallace. not sure about big betty
    DSN_ADMIN_USERS = "Provider=SQLOLEDB; Data Source=(local); Network Library=DBMSSOCN; Initial Catalog=NVadminUsers;User ID=NVadminUsers;Password=wa11abee;"
    REMOTE_HOST = REQUEST.SERVERVARIABLES("REMOTE_HOST")
	sFormsEmailTo   = EML_SHOPTOEMAIL   
    SITE_URL        = "http://www.care-and-comfort.co.uk"
else
    ' Pre-live env
    if IsEnvironmentPreLive(sHostHeader) then
        ' Nix says: Def correct on wallace. not sure about big betty
        DSN_ADMIN_USERS = "Provider=SQLOLEDB; Data Source=(local); Network Library=DBMSSOCN; Initial Catalog=NVadminUsers;User ID=NVadminUsers;Password=wa11abee;"
         REMOTE_HOST = REQUEST.SERVERVARIABLES("REMOTE_HOST")
        sFormsEmailTo   = DEVELOPER_EMAIL1 
        SITE_URL        = "http://careandcomfort.nvisage.uk.com"
    else
        ' dev env    
        if IsEnvironmentDev(sHostHeader) then
            'old mini dru
	    'DSN_ADMIN_USERS = "DSN=NVAdminUsers;UID=NVAdminUsers;PWD=mint44mash;"
            DSN_ADMIN_USERS = "Provider=SQLOLEDB; Data Source=(local); Network Library=DBMSSOCN; Initial Catalog=NVadminUsers;User ID=NVadminUsers;Password=wa11abee;"
                        
			 REMOTE_HOST = "84.12.51.209"

            sFormsEmailTo   = DEVELOPER_EMAIL1 
            SITE_URL = "http://careandcomfort"
        else
            ' // technically not possible so write an annoying message for someone to pic up on this logic error
            response.Write "Critical: Environment has not been detected correctly. Site settings will not be initialised correctly"
        end if
    end if  
end if

 
function IsEnvironmentLive(sHostHeader)
    dim blnConfirmation : blnConfirmation = false
    ' Live env
    if instr(sHostHeader, "www") or instr(sHostHeader, ".com") or instr(sHostHeader, ".co.uk") or instr(sHostHeader, ".org") then
        if instr(sHostHeader, "nvisage.uk.com") then
            blnConfirmation = false
        else
            blnConfirmation = true
        end if
    end if
    IsEnvironmentLive = blnConfirmation
end function

function IsEnvironmentPreLive(sHostHeader)
    dim blnConfirmation : blnConfirmation = false
    ' Pre-Live env
    if instr(sHostHeader, "nvisage.uk.com") then
        blnConfirmation = true
    end if
    IsEnvironmentPreLive = blnConfirmation
end function

function IsEnvironmentDev(sHostHeader)
    dim blnConfirmation : blnConfirmation = false
    ' Live env
	
    if (not instr(sHostHeader, "nvisage.uk.com")) and (not instr(sHostHeader, "www")) and (not instr(sHostHeader, ".com")) and (not instr(sHostHeader, ".org")) then
        blnConfirmation = true
    end if
    IsEnvironmentDev = blnConfirmation
end function
%>