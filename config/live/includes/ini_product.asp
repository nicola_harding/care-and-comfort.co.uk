<%
' // Picks up querystring and sets values into nCatID --> nPrdID
call parseParms(request.querystring,"",nCatID,nRngID,nGrpID,nColID,nPrdID)

dim aProduct    : aProduct=""
dim aTypeFields : aTypeFields=""
dim aSizeFields : aSizeFields=""
dim g_sName
dim g_sShortDescription 
dim g_sLongDescription 
dim g_sFurtherInfo
dim g_sMiscInfo1
dim g_sMiscInfo2
dim g_nSize
dim g_sSize
dim g_sImgThumb
dim g_sImgMain
dim g_sImgLarge
dim g_blnSpecialRqrmntTxt
dim g_nPricePrinting
dim g_n55Maximum          
dim g_sMsgMaxExceeded     
dim g_nProductTypeId    : g_nProductTypeId = 0
dim g_nProdMfId         : g_nProdMfId = 0

dim sPrIDandQtySngl
dim aProdQtySngl : aProdQtySngl = null
Dim nParentId : nParentId = 0

' Some pages require a child product id to be selected, followed by provision of how many images the customer would like to supply to the client shop
' if we are on the image provision step. find out the selected child option. so that we can display pricing info etc

'response.Write "nPrdID=" & nPrdID

select case g_sCurrentPage
     case "/product/product_image.asp", "/product/001_frame.asp" 
     
        sPrIDandQtySngl = request("option")
        
        if len("" & sPrIDandQtySngl) then
            if instr(sPrIDandQtySngl, "x") then
                aProdQtySngl    = split(sPrIDandQtySngl, "x") 
                nParentId       = nPrdID              
                nPrdID          = aProdQtySngl(0) 
                nQty            = aProdQtySngl(1)                                
                
                if (nPrdID = 0) then
                    response.redirect "./error.asp?err=code1"
                else
                    nErr  = loadProduct(nParentId,aParent) 
                    if nErr > 0 or isNull(aParent) then
                        response.redirect "./error.asp?err=code2"
                    end if  
                end if
            else
                response.redirect "./error.asp?err=code3"
            end if
        end if
end select 

'response.Write "nPrdID=" & nPrdID
'response.Write "nParentId=" & nParentId
    
if nPrdID > 0 then    
   ' // Load product - method is from ecommscripts virtual directory
   nErr  = loadProduct(nPrdID,aProduct) 
   if nErr > 0 or isNull(aProduct) then
        response.redirect("?" & buildParms("",nCatID,nRngID,nGrpID,nColID,nPrdID))
   else 
        ' if a parent array is provided - use its informational text and imagery, not the child item"s  
        if not isNull(aParent) and isarray(aParent) then
            g_sName               = para_text(aParent(PR_NAME, 0))
            g_sShortDescription   = para_text(aParent(PR_DESCSHORT, 0))
            g_sLongDescription    = para_text(aParent(PR_DESCLONG, 0))
            g_sFurtherInfo        = para_text(aParent(PR_FURTHERINFO, 0))
            g_sMiscInfo1          = para_text(aParent(PR_MISC1, 0))
            g_sMiscInfo2          = para_text(aParent(PR_MISC2, 0))
            g_sImgThumb           = aParent(PR_IMAGETHUMB, 0)
            g_sImgMain            = aParent(PR_IMAGEMAIN, 0)
            g_sImgLarge           = aParent(PR_IMAGELARGE, 0)
            g_nProductTypeId      = aParent(PR_KEYFPTID, 0)
            g_nProdMfId           = aParent(PR_KEYFMFID, 0)  
        else
            g_sName               = para_text(aProduct(PR_NAME, 0))
            g_sShortDescription   = para_text(aProduct(PR_DESCSHORT, 0))
            g_sLongDescription    = para_text(aProduct(PR_DESCLONG, 0))
            g_sFurtherInfo        = para_text(aProduct(PR_FURTHERINFO, 0))
            g_sMiscInfo1          = para_text(aProduct(PR_MISC1, 0))
            g_sMiscInfo2          = para_text(aProduct(PR_MISC2, 0))
            g_sImgThumb           = aProduct(PR_IMAGETHUMB, 0)
            g_sImgMain            = aProduct(PR_IMAGEMAIN, 0)
            g_sImgLarge           = aProduct(PR_IMAGELARGE, 0)
            g_nProductTypeId      = aProduct(PR_KEYFPTID, 0)
            g_nProdMfId           = aProduct(PR_KEYFMFID, 0)
        end if        
        g_nSize               = para_text(aProduct(PR_KEYFSZID, 0))        
            
        if len(g_sImgThumb) = 0 then
            g_sImgThumb = ECOM_IMAGE_DIR & ECOMM_IMAGE_UNAVAILABLE_TH
        else
            g_sImgThumb = ECOM_IMAGE_DIR & g_sImgThumb
        end if
        if len(g_sImgMain) = 0 then
            g_sImgMain = ECOM_IMAGE_DIR & ECOMM_IMAGE_UNAVAILABLE_MN
        else
            g_sImgMain = ECOM_IMAGE_DIR & g_sImgMain
        end if       
      
        if len(g_sImgLarge) > 0 then
            g_sImgLarge = ECOM_IMAGE_DIR & g_sImgLarge
        end if
               
        if len(g_sLongDescription) > 0 then
            g_sLongDescription = "<p>" & g_sLongDescription & "</p>"
        end if
        if len(g_sMiscInfo2) > 0 then
            g_sMiscInfo2 = "<p>" & g_sMiscInfo2 & "</p>"
        end if

        ' [nix] thinks this preview var will disappear very quickly
        'if g_nSize > 0 then 
        '    ' // Get all products under selected range
        '    nErr  = loadSizeCategory(g_nSize,aSizeFields)
        '    if nErr = 0 and (not isNull(aSizeFields)) then
        '        if len("" & aSizeFields(SZ_CODE, 0)) > 0 then
        '            g_sSize = aSizeFields(SZ_CODE, 0) & "(" & aSizeFields(SZ_NAME, 0) & ")"
        '        end if
        '        'sSize = aTypeFields(SZ_CODE, 0) 
        '        'sSize = aTypeFields(SZ_NAME, 0)
        '    end if
        'end if
    end if  ' nErr = 0
end if      ' nPrdID > 0
%>