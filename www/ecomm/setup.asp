<%' {Any /{cmsscripts or ecommscripts folder}/setup.asp file will always need the matching {ini_..}.asp include statement, paired with it%>
<!--#include virtual="/includes/ini_ecomm.asp"-->
<!--#include virtual="/admin/ecommscripts/setup.asp"-->

<%
dim sCurFldr:sCurFldr=""
'dim nCatID:nCatID=0
dim sQManuf:sQManuf = "" 
dim nRngID:nRngID=0
dim nGrpID:nGrpID=0
dim nColID:nColID=0
dim nPrdID:nPrdID=0

' // Picks up querystring and sets values into nCatID --> nPrdID
call parseParms(request.querystring,"",sQManuf,nRngID,nGrpID,nColID,nPrdID)

const   PROMOTION_CODE = "asdis"
dim     PROMOTION_DATE_START : PROMOTION_DATE_START = cdate("23/11/2006")
dim     PROMOTION_DATE_END   : PROMOTION_DATE_END = dateadd("d", 10, now()) ' // This means the promotion will never expire


'function buildParms(sCurFldr,nCatID,nRngID,nGrpID,nColID,nPrdID)
'  dim sParms:sParms=""
'  dim sC:sC=","
' 
'  sParms = "" & nPrdID & sC & nColID & sC & nGrpID & sC & nRngID & sC & nCatID
'  buildParms = sParms
'end function

function buildParms(sCurFldr,nCatID,nRngID,nGrpID,nColID,nPrdID)
  dim sParms:sParms=""
  dim sC:sC=","
  sParms = "" & nPrdID & sC & nColID & sC & nGrpID & sC & nRngID & sC & nCatID
  buildParms = sParms
end function

function parseParms(sParms,sCurFldr,nCatID,nRngID,nGrpID,nColID,nPrdID)
  dim nErr:nErr=0
  dim sC:sC=","
  dim aTmp:aTmp=null
  dim nIdx:nIdx=0
  dim nCt:nCt=0
  nCatID=0
  nRngID=0
  nGrpID=0
  nColID=0
  nPrdID=0
  if len("" & sParms) > 0 then
    if instr("" & sParms, sC) then
        aTmp = split(sParms,sC)
        nCt = UBound(aTmp)
          
        for nIdx = nCt to 0 step -1
            if isInt(aTmp(nIdx)) = true then
              select case nIdx
                case 0
                  nPrdID = CLng("0" & aTmp(nIdx))             
                case 1
                  nColID = CLng("0" & aTmp(nIdx))
                case 2
                  nGrpID = CLng("0" & aTmp(nIdx))
                case 3
                  nRngID = CLng("0" & aTmp(nIdx))
                case 4
                  nCatID = CLng("0" & aTmp(nIdx))
                case else
                  '
              end select
            end if
        next
    else
        if IsInt(sParms) then nPrdID = CLng("0" & sParms)
    end if
  end if
  parseParms = nErr
end function


'// load records from db //
function loadGroups(aFields)
	dim nErr:nErr = 0
	dim sSQL
	sSQL = "SELECT GR.* FROM tblGroup AS GR WHERE GR.keyFinID=" & INST_ID & ";"
	'response.Write "sSQl=" & sSQl 
	nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aFields)
	loadGroups = nErr
end function

'// load records from db //
function loadManufacturers(aFields)
	dim nErr:nErr = 0
	dim sSQL
	sSQL = "SELECT * FROM tblManufacturer WHERE keyFinID=" & INST_ID & " ORDER BY mfName;"
	nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aFields)
	loadManufacturers = nErr
end function

function getCartItemsProductsByCartID(nCtID, aFields)
dim nErr:nErr = 0
dim sSQL  
   
    if nCtID > 0 then    
        sSQL = "SELECT PR.*, CI.ciQuantity, LT.ltText, CI.ciID, PT.ptName, NT.ntID, NT.ntText, SZ.szCode + ' (' + SZ.szName + ')', MF.mfName " 
        sSQL = sSQL & " FROM tblCartItem AS CI LEFT JOIN tblProduct AS PR ON CI.keyFprID=PR.prID"
        sSQL = sSQL & " LEFT OUTER JOIN tblLeadTime AS LT ON PR.keyFltID=LT.ltID"
        sSQL = sSQL & " LEFT JOIN tblProductType AS PT ON PR.keyFptID = PT.ptID"
        sSQL = sSQL & " LEFT OUTER JOIN tblNote as NT ON CI.keyFntID = NT.ntID"
        sSQL = sSQL & " LEFT OUTER JOIN tblSizeCategory as SZ ON PR.keyFszID = SZ.szID"
        sSQL = sSQL & " LEFT OUTER JOIN tblManufacturer as MF ON PR.keyFmfID = MF.mfID"
       
        sSQL = sSQL & " WHERE CI.keyFctID=" & nCtID 
        sSQL = sSQL & " AND CI.keyFinID=" & INST_ID 
        sSQL = sSQL & " AND PR.keyFinID=" & INST_ID 
        sSQL = sSQL & " AND ( LT.keyFinID IS NULL OR LT.keyFinID=" & INST_ID & ")"
        sSQL = sSQL & " AND PR.prDisplay=1 AND PR.prOnSale=1"
        sSQl = sSQL & " ORDER BY CI.ciID ASC;"
        	'response.Write "sSQl=" & sSQl 
        nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aFields)        
        if nErr > 0 then
            aFields = null     
        end if
	else
		nErr = 1	'// no id supplied //
		aFields = null
	end if
	getCartItemsProductsByCartID = nErr 
end function

function loadCollectionProducts(clID, aFields)
dim nErr:nErr=0
dim sSQL:sSQL=""
    if clID > 0 then
        sSQL = sSQL & "SELECT PR.* FROM tblProduct AS PR LEFT JOIN tblProductToCollection AS P2C ON PR.prID=P2C.keyFprID" 
        sSQL = sSQL & " WHERE PR.prDisplay=1 AND PR.prOnSale=1 AND PR.prIsChild=0 AND P2C.keyFclID=" & clID & " AND P2C.keyFinID=" & INST_ID & " AND PR.keyFinID=" & INST_ID & ""
        nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aFields)
        if nErr > 0 then
            aFields = null
        end if
    end if
    loadCollectionProducts = nErr
end function

function loadRangeProducts(nRngID,aFields)
	dim nErr:nErr = 0
	dim sSQL
	if nRngID > 0 then
		sSQL = "SELECT PR.* FROM (tblProduct AS PR INNER JOIN tblProductToRange AS P2R ON PR.prID = P2R.keyFprID) WHERE PR.prDisplay=1 AND PR.prOnSale=1 AND P2R.keyFrnID=" & nRngID & " AND PR.keyFinID=" & INST_ID & " ORDER BY P2R.p2rDisplay;"
		'RESPONSE.WRITE sSQL
		nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aFields)
	else
		nErr = 1	'// no id supplied //
		aFields = null
	end if
	loadRangeProducts = nErr
end function


function loadProductsByTypeId(nTypeID, aFields)
    dim nErr:nErr = 0
    dim sSQL   
    if nTypeID > 0 then
        sSQL = "SELECT PR.* FROM tblProduct AS PR WHERE PR.prDisplay=1 AND PR.prOnSale=1 AND PR.keyFptID=" & nTypeID & " AND PR.keyFinID=" & INST_ID & ";"
        'RESPONSE.WRITE sSQL
        nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aFields)
        if nErr > 0 then
            aFields = null
        end if
	else
		nErr = 1	'// no id supplied //
		aFields = null
	end if
	loadProductsByTypeId = nErr    
end function

' // Shopping cart helpers in case the querystring loses the range and category ids
' // Get range id from Product ID
function loadProductRange(nPrdID, aFields)
    dim nErr:nErr = 0
    dim sSQL   
    if nPrdID > 0 then
        sSQL = "SELECT RN.* FROM (tblRange AS RN INNER JOIN tblProductToRange AS P2R ON RN.rnID = P2R.keyFrnID) WHERE P2R.keyFprID=" & nPrdID & " AND RN.keyFinID=" & INST_ID & ";"
		nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aFields)
        if nErr > 0 then
            aFields = null
        end if
	else
		nErr = 1	'// no id supplied //
		aFields = null
	end if
	loadProductRange = nErr    
end function

' // Get Cat id from Range ID
function loadRangeCategory(nRangeID)
    sSQL = "SELECT R2C.keyFcaID FROM tblRangeToCategory AS R2C WHERE R2C.keyFrnID=" & nRangeID
    nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aCatRange)
    if nErr = 0 then
	    loadRangeCategory = aCatRange(0,0)
    else
	    loadRangeCategory = 0
    end if
end function

' // Get Cat id from Range ID
function loadAllParentProducts(aFields)
    sSQL = "SELECT P.prId, P.prName FROM tblProduct AS P WHERE P.prIsParent = 1 AND P.prDisplay=1 AND P.prOnSale=1 AND P.keyFinID=" & INST_ID & " ORDER BY P.prName;"
    nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aFields)
    loadAllParentProducts = nErr
end function

function loadRelatedProducts(prID, aFields)
dim nErr:nErr=0
dim sSQL:sSQL=""
    if prID > 0 then
        sSQL = sSQL & "SELECT PR.* FROM tblProduct AS PR LEFT JOIN tblProductToProduct AS P2P ON PR.prID=P2P.keyFp1ID" 
        sSQL = sSQL & " WHERE PR.prDisplay=1 AND PR.prOnSale=1 AND PR.prIsChild=0 AND P2P.keyFp2ID=" & prID & " AND P2P.keyFinID=" & INST_ID & " AND PR.keyFinID=" & INST_ID & ""
        sSQL = sSQL & " UNION "
        sSQL = sSQL & "SELECT PR.* FROM tblProduct AS PR LEFT JOIN tblProductToProduct AS P2P ON PR.prID=P2P.keyFp2ID" 
        sSQL = sSQL & " WHERE PR.prDisplay=1 AND PR.prOnSale=1 AND AND PR.prIsChild=0 AND P2P.keyFp1ID=" & prID & " AND P2P.keyFinID=" & INST_ID & " AND PR.keyFinID=" & INST_ID & ""
        nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aFields)
        if nErr > 0 then
            aFields = null
        end if
    end if
    loadRelatedProducts = nErr
end function


function buildPageTitleAndMetas(sQS,sCurFldr,nCatID,nRngID,nGrpID,nColID,nPrdID)
    dim DFLT_META_DESC:DFLT_META_DESC="data logging systems, data logging equipment, easyview, PLM software, glm, GLM"
    dim DFLT_META_KEYS:DFLT_META_KEYS="Solutions data logging, temperature dataloggers, humidity loggers, datalogger software, loggers, glm, GLM, Tinytag"
    dim nErr:nErr=0
    dim sTmp:sTmp=""
    dim sTitle:sTitle=SHOP_NAME
    dim sMetaDesc:sMetaDesc=""
    dim sMetaKeys:sMetaKeys=""
    dim aProd:aProd=null
    if nPrdID > 0 then
        nErr = loadProduct(nPrdID, aProd)
        if nErr = 0 then
          if len("" & aProd(PR_METATITLE, 0)) > 0 then
            sTitle = aProd(PR_METATITLE, 0) & " - " & sTitle
          else
            sTitle = aProd(PR_NAME, 0) & " - " & sTitle
          end if
          if len("" & aProd(PR_METADESC, 0)) > 0 then
            sMetaDesc = aProd(PR_METADESC, 0)
          elseif len("" & aProd(PR_DESCLONG, 0)) > 0 then
            sMetaDesc = stripTags(aProd(PR_DESCLONG, 0))
          else
            sMetaDesc = DFLT_META_DESC
          end if
          if len("" & aProd(PR_KEYWORDS, 0)) > 0 then
            sMetaKeys = aProd(PR_KEYWORDS, 0)
          elseif len("" & aProd(PR_DESCLONG, 0)) > 0 then
            sMetaKeys = replace(stripTags(aProd(PR_DESCLONG, 0)), " ", ",")
          else
            sMetaKeys = DFLT_META_KEYS
          end if
        else
          sMetaDesc = DFLT_META_DESC
          sMetaKeys = DFLT_META_KEYS
        end if
        aProd = null
    else
        sMetaDesc = DFLT_META_DESC
        sMetaKeys = DFLT_META_KEYS
    end if
    sTmp = sTmp & "<title>" & sTitle & "</title>" & vbcrlf
    sTmp = sTmp & "<meta http-equiv=""Content-Language"" content=""en-gb"" />" & vbcrlf
    sTmp = sTmp & "<meta http-equiv=""Content-Type"" content=""text/html; charset=windows-1252"" />" & vbcrlf
    sTmp = sTmp & "<meta http-equiv=""Content-Type"" content=""text/html; charset=iso-8859-1"" />" & vbcrlf
    sTmp = sTmp & "<meta name=""author"" content=""NVisage (http://www.nvisage.co.uk)"" />" & vbcrlf
    sTmp = sTmp & "<meta name=""description"" content=""" & replace(sMetaDesc,"""","") & """ />" & vbcrlf
    sTmp = sTmp & "<meta name=""keywords"" content=""" &  replace(sMetaKeys,"""","") & """ />" & vbcrlf
    buildPageTitleAndMetas = sTmp
end function


function stripTags(strIn)
    '// okay we need to replace every '<tag>' & '</tag>' with '' //
    '// might as well use regular expressions //
    '// Microsoft Visual Basic Regular Expressions 5.5 project reference //
    Dim MyReg
    Set MyReg = New RegExp
    MyReg.IgnoreCase = True
    MyReg.Global = True
    MyReg.Pattern = "<(.|\n)+?>"
    StripTags = MyReg.Replace("" & strIn, "")
End function

function para_text(strTxt)
	strTxt = REPLACE("" & strTxt,vbcrlf,"&nbsp;<br />")
	para_text = strTxt
end function

function update_Stock(nPrID,sPrdStock)
    Dim sWhere

    if nPrID > 0 AND sPrdStock > 0 then
    sSQL = "UPDATE tblProduct SET prStockQty=" & (sPrdStock - 1) & " WHERE prID=" & nPrID
    call sqlExec(DSN_ECOM_DB, sSQL)
	    update_Stock = 0
    else
	    update_Stock = 1
    end if
End function


function addNote(sNote)
    dim aNote(2,2)
	if len(sNote) > 0 then
        aNote(0,0) = NT_ID
        aNote(1,0) = sNote
        aNote(2,0) = INST_ID
        nErr = saveNote(aNote)
        addNote = aNote(0,0)
	end if
end function


'// function for retreiving postage options //
function getPostOptions(nCtID, sPostOpt, nNoteID, sGiftMsg, nNote2ID, sDelMsg, bPartShip)
  dim nErr:nErr=0
  'sPostOpt  = "" 'session("POSTOPT")
  'sGiftMsg  = "" 'session("GIFTMSG")
  'sDelMsg   = "" 'session("DELMSG")  
  
  nNoteID   = session("NOTE1ID")
  nNote2ID  = 0 'session("NOTE2ID")
  bPartShip = 0 'session("PARTSHIP")
  getPostOptions = nErr
end function


'// function for storing postage options //
function setPostOptions(nCtID, sPostOpt, nNoteID, sGiftMsg, nNote2ID, sDelMsg, bPartShip)
  dim nErr:nErr=0
  'session("POSTOPT")  = sPostOpt
  'session("DELMSG")   = sDelMsg
  'session("GIFTMSG")  = sGiftMsg
  'session("PARTSHIP") = bPartShip
  
  session("NOTE2ID")  = nNote2ID  
  session("NOTE1ID")  = nNoteID  
  setPostOptions = nErr
end function


function sendPasswordEmail(aMember)
  dim nErr:nErr=0
  dim sName:sName = ""
  dim sPWD:sPWD = ""
  dim sMsg:sMsg=""
  dim sEmail:sEmail=""
	if (not isNull(aMember)) And (isArray(aMember)) then
		sPWD = "" & aMember(ME_PWD, 0)
    sEmail = "" & aMember(ME_EMAIL, 0)
		if len("" & aMember(ME_FIRSTNAME, 0)) > 0 then sName = sName & aMember(ME_FIRSTNAME, 0) & " "
		if len("" & aMember(ME_SURNAME, 0)) > 0 then sName = sName & aMember(ME_SURNAME, 0)
		if len("" & sName) = 0 then sName = "Member"
		if len(sPWD) > 0 Then
			sMsg = "Dear " & sName & vbcrlf _
				& vbcrlf & "Your member number for the " & SHOP_NAME & " web site is: " & sPWD & vbcrlf & vbcrlf _
				& "If you have any queries please email " & EML_SHOPFROMEMAIL & vbcrlf & vbcrlf _
				& SHOP_NAME & vbcrlf & SITE_URL & vbcrlf
			sendPasswordEmail = SendMailEnc(sEmail, sName, EML_SHOPFROMEMAIL, EML_SHOPFROMNAME, EML_PASSWORDSUBJECT, sMsg, "", "", false, false)
    else
      nErr = 2  '// bad output parms //
		end if
  else
    nErr = 1  '// bad input parms //
	end if
  sendPasswordEmail = nErr
end function

function processVoucher(nMeID, sVchr, sMsg)
  dim nErr:nErr=0
  dim dtNow:dtNow=now()
  dim dtStart:dtStart=null
  dim dtEnd:dtEnd=null
  dim sDesc:sDesc=""
  dim nDisc:nDisc=0
  nErr=1
  if len("" & sVchr) < 5 then
    nErr = 2  '// bad input parms //
    sMsg = "&nbsp;<br /><b>Sorry the voucher you entered is not valid.</b>" _
         & "<br />Please check that you have entered the details correctly.<br />"
  else
    select case sVchr
      case "ECAT1315"
        '// 15% off everything from jan to feb14th 2006 //
        dtStart = CDate("01/01/2006 00:00:01")
        dtEnd   = CDate("14/02/2006 23:59:59")
        if (dtStart > dtNow) then
          nErr = 1
          sMsg = "&nbsp;<br /><b>Sorry the voucher you entered is not currently valid.</b>" _
             & "<br />"
        elseif (dtEnd < dtNow) then
          nErr = 1
          sMsg = "&nbsp;<br /><b>Sorry the voucher you entered has expired.</b>" _
             & "<br />"
        else
          nErr = 0
          sDesc = "EW catalogue 15% off promotion."
          nDisc = 15
          session("VCHRCODE") = sVchr
          session("VCHRDESC") = sDesc
          session("VCHRDISC") = nDisc
        end if
      case else
        nErr = 1
        sMsg = "&nbsp;<br /><b>Sorry the voucher you entered is not valid.</b>" _
             & "<br />Please check that you have entered the details correctly.<br />"
    end select
  end if
  processVoucher = nErr
end function


function showLastItemViewed(nID, sLink)
  dim nErr:nErr=0
  dim sTmp:sTmp = ""
  dim aProd:aProd=null
  sTmp = sTmp & "<img src=""/ecomm/buttons/button_last_item_viewed.gif"" alt=""Last item viewed"" width=""115"" height=""19"" hspace=""0"" vspace=""0"" border=""0"" class=""padding_top"">" & vbcrlf
  if nID > 0 then
    nErr = getRowsBySQL(DSN_ECOM_DB, "SELECT PR.* FROM tblProduct AS PR WHERE PR.keyFinID=" & INST_ID & " AND PR.prID=" & nID & ";", aProd)
    if nErr = 0 then
      sTmp = sTmp & "<a href=""" & sLink & """><img src=""" & displayImage(aProd(PR_IMAGEMAIN, 0)) & """ alt=""" & aProd(PR_NAME, 0) & """ width=""75"" height=""75"" hspace=""0"" vspace=""0"" border=""0"" class=""padding_top""></a>" & vbcrlf
      aProd = null
    end if
  end if
  showLastItemViewed = sTmp
end function


function makeAddress(sTitle,sFirstName,sSurName,sAdd1,sAdd2,sAdd3,sAdd4,sAdd5,sPostCode)
	dim sTmp:sTmp = ""
	sTmp = replace(trim(sTitle & " " & sFirstName & " " & sSurName), "  ", " ") & vbcrlf
	if len("" & sAdd1) then	sTmp = sTmp & sAdd1 & vbcrlf
	if len("" & sAdd2) then	sTmp = sTmp & sAdd2 & vbcrlf
	if len("" & sAdd3) then	sTmp = sTmp & sAdd3 & vbcrlf
	if len("" & sAdd4) then	sTmp = sTmp & sAdd4 & vbcrlf
	if len("" & sAdd5) then	sTmp = sTmp & sAdd5 & vbcrlf
	if len("" & sPostCode) then	sTmp = sTmp & sPostCode & vbcrlf
	makeAddress = sTmp
end function

function getRelatedProducts(prID, aFields)
  dim nErr:nErr=0
  dim sSQL:sSQL=""
  if prID > 0 then
    sSQL = sSQL & "SELECT PR.* FROM tblProduct AS PR LEFT JOIN tblProductToProduct AS P2P ON PR.prID=P2P.keyFp1ID" 
    sSQL = sSQL & " WHERE PR.prDisplay=1 AND PR.prIsChild=0 AND P2P.keyFp2ID=" & prID & " AND P2P.keyFinID=" & INST_ID & " AND PR.keyFinID=" & INST_ID & ""
    sSQL = sSQL & " UNION "
    sSQL = sSQL & "SELECT PR.* FROM tblProduct AS PR LEFT JOIN tblProductToProduct AS P2P ON PR.prID=P2P.keyFp2ID" 
    sSQL = sSQL & " WHERE PR.prDisplay=1 AND PR.prIsChild=0 AND P2P.keyFp1ID=" & prID & " AND P2P.keyFinID=" & INST_ID & " AND PR.keyFinID=" & INST_ID & ""
    nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aFields)
    if nErr > 0 then
        aFields = null
    end if
   end if
  getRelatedProducts = nErr
end function

%>