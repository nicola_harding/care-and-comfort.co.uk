<%
'// DEVELOPER NOTE: [hardin] Monday 19th February:
' // This file had the EU countries reflected within
' // This file had the channel islands: Gurnsey and Jersey added. Theyre included as zone 9 UK delivery areas but have "no vat" configured


' [hardin] Nix fudge is to make non EU european countries = zone 3

'// Pass in the country code
'// returns the country name as a string
private function xxgetCountry(sCode)
Dim sCntry
sCntry = ""
Select Case sCode
Case "XX-1-0-ab"
sCntry = "Abu Dhabi"
Case "AF-1-0-AF"
sCntry = "Afghanistan"
Case "XX-2-0-aj"
sCntry = "Ajman"
Case "AL-3-0-AL"        ' "AL-0-0-AL"
sCntry = "Albania"
Case "DZ-1-0-DZ"
sCntry = "Algeria"
Case "AD-0-0-AD"
sCntry = "Andorra"
Case "AO-1-0-AO"
sCntry = "Angola"
Case "AI-1-0-AI"
sCntry = "Anguilla"
Case "AG-1-0-AG"
sCntry = "Antigua & Barbuda"
Case "AS-1-0-as"
sCntry = "American Samoa"
Case "AR-1-0-AR"
sCntry = "Argentina"
Case "AM-3-0-AM"        ' "AM-0-0-AM"
sCntry = "Armenia"
Case "AW-1-0-AW"
sCntry = "Aruba"
Case "XX-1-0-ac"
sCntry = "Ascension"
Case "AU-2-0-AU"
sCntry = "Australia"
Case "AT-0-1-AT"
sCntry = "Austria (EU)"
Case "AZ-3-0-AZ"        ' "AZ-0-0-AZ"
sCntry = "Azerbaijan"
Case "XX-0-1-ao"        ' "XX-2-1-ao" 
sCntry = "Azores (EU)"       
Case "BS-1-0-BS"
sCntry = "Bahamas"
Case "BH-1-0-BH"
sCntry = "Bahrain"
Case "XX-0-1-bi"
sCntry = "Balearic Islands (EU)"
Case "BD-1-0-BD"
sCntry = "Bangladesh"
Case "BB-1-0-BB"
sCntry = "Barbados"
Case "BY-0-0-BY"
sCntry = "Belarus"
Case "XX-2-0-bl"
sCntry = "Belau (Palau)"
Case "BE-0-1-BE"
sCntry = "Belgium (EU)"
Case "BZ-1-1-BZ"
sCntry = "Belize"
Case "BJ-1-0-BJ"
sCntry = "Benin"
Case "BM-1-0-BM"
sCntry = "Bermuda"
Case "BT-1-0-BT"
sCntry = "Bhutan"
Case "BO-1-0-BO"
sCntry = "Bolivia"
Case "BA-3-0-BA"        ' "BA-0-0-BA"
sCntry = "Bosnia-Herzegovina"
Case "BW-1-0-BW"
sCntry = "Botswana"
Case "BR-1-0-BR"
sCntry = "Brazil"
Case "IO-1-0-IO"
sCntry = "British Indian Ocean Territory"
Case "BN-1-0-BN"
sCntry = "Brunei Darussalam"
Case "XX-1-0-bv"
sCntry = "British Virgin Islands"
Case "BG-0-1-BG"
sCntry = "Bulgaria (EU)"
Case "BF-1-0-BF"
sCntry = "Burkina Faso"
Case "XX-2-0-BU"
sCntry = "Burma"
Case "BI-1-0-BI"
sCntry = "Burundi"
Case "KH-1-0-KH"
sCntry = "Cambodia"
Case "CM-1-0-CM"
sCntry = "Cameroon"
Case "CA-1-0-CA"
sCntry = "Canada"
Case "XX-0-0-ci"
sCntry = "Canary Islands"
Case "CV-1-0-CV"
sCntry = "Cape Verde"
Case "KY-1-0-KY"
sCntry = "Cayman Islands"
Case "CF-1-0-CF"
sCntry = "Central African Rep."
Case "TD-1-0-TD"
sCntry = "Chad"
Case "CL-1-0-CL"
sCntry = "Chile"
Case "CN-2-0-CN"
sCntry = "China (People's Republic)"
Case "CX-1-0-CX"
sCntry = "Christmas Island"
Case "CC-1-0-CC"
sCntry = "Cocos Island (Keeling Island)"
Case "CO-1-0-CO"
sCntry = "Colombia"
Case "KM-1-0-KM"
sCntry = "Comoros"
Case "CG-1-0-CG"
sCntry = "Congo"
Case "CD-1-0-cd"
sCntry = "Congo Dem. Rep."
Case "CK-2-0-co"            ' "CK-0-0-co"
sCntry = "Cook Islands"
Case "XX-0-1-cr"
sCntry = "Corsica (EU)"
Case "CR-1-0-CR"
sCntry = "Costa Rica"
Case "CI-1-0-CI"
sCntry = "Cote d'Ivoire"
Case "HR-3-0-HR"            ' "HR-0-0-HR"
sCntry = "Croatia"
Case "CU-1-0-CU"
sCntry = "Cuba"
Case "XX-1-0-cu"
sCntry = "Curacao"
Case "CY-3-0-CY"            ' "CY-0-0-CY"
sCntry = "Cyprus"
Case "CY-0-1-CY"
sCntry = "Cyprus (EU)"
Case "CZ-0-1-CZ"
sCntry = "Czech Republic (EU)"
Case "DK-3-1-DK"            ' "DK-0-1-DK"
sCntry = "Denmark"          ' "Denmark (EU)"
Case "DJ-1-0-DJ"
sCntry = "Djibouti"
Case "DM-1-0-DM"
sCntry = "Dominica"
Case "DO-1-0-DO"
sCntry = "Dominican Republic"
Case "XX-1-0-du"
sCntry = "Dubai"
Case "TP-2-0-TP"
sCntry = "East Timor"
Case "EC-1-0-EC"
sCntry = "Ecuador"
Case "EG-1-0-EG"
sCntry = "Egypt"
Case "SV-1-0-SV"
sCntry = "El Salvador"
Case "XX-1-0-GQ"
sCntry = "Equatorial Guinea"
Case "XX-1-0-ER"
sCntry = "Eritrea"
Case "XX-0-1-EE"
sCntry = "Estonia (EU)"
Case "XX-1-0-ET"
sCntry = "Ethiopia"
Case "FK-1-0-FK"
sCntry = "Falkland Islands"
Case "FO-0-0-FO"
sCntry = "Faroe Islands"
Case "FJ-2-0-FJ"
sCntry = "Fiji"
Case "FI-0-1-FI"
sCntry = "Finland (EU)"
Case "FR-0-1-FR"
sCntry = "France (EU)"
Case "GF-1-0-GF"          
sCntry = "French Guiana"
Case "PF-2-0-PF"
sCntry = "French Polynesia"
Case "TF-2-0-fs"
sCntry = "French Southern & Antarctic Territories"
Case "XX-1-0-fw"
sCntry = "French West Indies"
Case "XX-1-0-fu"
sCntry = "Fujairah"
Case "GA-1-0-GA"
sCntry = "Gabon"
Case "GM-1-0-GM"
sCntry = "Gambia"
Case "XX-2-0-ga"
sCntry = "Gaza & KhanYunis"
Case "GE-3-0-GE"                ' "GE-0-0-GE"
sCntry = "Georgia"
Case "DE-0-1-DE"
sCntry = "Germany (EU)"
Case "GH-1-0-GH"
sCntry = "Ghana"
Case "GI-0-0-GI"
sCntry = "Gibraltar"
Case "GR-0-1-GR"
sCntry = "Greece (EU)"
Case "GL-3-0-GL"                ' "GL-0-0-GL"
sCntry = "Greenland"
Case "GD-1-0-GD"
sCntry = "Grenada"
Case "GU-2-0-GU"
sCntry = "Guam"
Case "GT-1-0-GT"
sCntry = "Guatemala"
' Nix addition: Channel islands - part of uk delivery (zone = -9-), but exzempt from VAT (-0-)
Case "GG-9-0-GG"
sCntry = "Guernsey (Channel Islands)(UK)"
Case "GN-1-0-GN"
sCntry = "Guinea"
Case "GW-1-0-GW"
sCntry = "Guinea-Bissau"
Case "GY-1-0-GY"
sCntry = "Guyana"
Case "HT-1-0-HT"
sCntry = "Haiti"
Case "HN-1-0-HN"
sCntry = "Honduras"
Case "HK-1-0-HK"
sCntry = "Hong Kong"
Case "HU-0-1-HU"
sCntry = "Hungary (EU)"
Case "IS-3-0-IS"            ' "IS-0-0-IS"
sCntry = "Iceland"
Case "IN-1-0-IN"
sCntry = "India"
Case "ID-1-0-ID"
sCntry = "Indonesia"
Case "IR-1-0-IR"
sCntry = "Iran"
Case "IQ-1-0-IQ"
sCntry = "Iraq"
Case "IR-0-1-IE"
sCntry = "Irish Republic (EU)"
Case "IL-1-0-IL"
sCntry = "Israel"
Case "IT-0-1-IT"
sCntry = "Italy (EU)"
Case "JM-1-0-JM"
sCntry = "Jamaica"
Case "JP-2-0-JP"
sCntry = "Japan"
' Nix addition: Channel islands - part of uk delivery (zone = -9-), but exzempt from VAT (-0-)
Case "JE-9-0-JE"
sCntry = "Jersey (Channel Islands)(UK)"
Case "JO-1-0-JO"
sCntry = "Jordan"
Case "KZ-3-0-KZ"        ' "KZ-0-0-KZ"
sCntry = "Kazakhstan"
Case "KE-1-0-KE"
sCntry = "Kenya"
Case "XX-2-0-ki"
sCntry = "Kirghizstan"
Case "KI-2-0-KI"
sCntry = "Kiribati"
Case "KP-2-0-KP"
sCntry = "Korea (Democratic People's Republic)"
Case "KR-2-0-KR"
sCntry = "Korea (Republic of)"
Case "KW-1-0-KW"
sCntry = "Kuwait"
Case "XX-1-0-XX"
sCntry = "Laos"
Case "LA-1-0-LA"
sCntry = "Lao People's Democratic Republic"
Case "LV-3-1-LV"        ' "LV-0-1-LV"
sCntry = "Latvia"       ' "Latvia (EU)"
Case "LB-1-0-LB"
sCntry = "Lebanon"
Case "LS-1-0-LS"
sCntry = "Lesotho"
Case "LR-1-0-LR"
sCntry = "Liberia"
Case "LY-1-0-LY"
sCntry = "Libyan Arab Jamahiriya"
Case "LI-0-0-LI"
sCntry = "Liechtenstein"
Case "LY-0-1-LT"
sCntry = "Lithuania (EU)"
Case "LU-0-1-LU"
sCntry = "Luxembourg (EU)"
Case "MO-1-0-MO"
sCntry = "Macao"
Case "MK-3-0-MK"            ' "MK-0-0-MK"
sCntry = "Macedonia (Former Yugoslav Rep. of)"
Case "MG-1-0-MG"
sCntry = "Madagascar"
Case "XX-0-1-ma"            ' "XX-2-1-ma"
sCntry = "Madeira (EU)"
Case "MW-1-0-MW"
sCntry = "Malawi"
Case "MY-1-0-MY"
sCntry = "Malaysia"
Case "MV-1-0-MV"
sCntry = "Maldives"
Case "ML-1-0-ML"
sCntry = "Mali"
Case "MT-0-1-MT"
sCntry = "Malta (EU)"
Case "MH-2-0-MH"
sCntry = "Marshall Islands"
Case "MR-1-0-MR"
sCntry = "Mauritania"
Case "MU-1-0-MU"
sCntry = "Mauritius"
Case "MU-0-0-my"            ' "MU-1-0-my"
sCntry = "Mayotte"
Case "MX-1-0-MX"
sCntry = "Mexico"
Case "FM-2-0-fm"
sCntry = "Micronesia (Federated States)"
Case "MD-3-0-md"            ' "MD-0-0-md"
sCntry = "Moldova, Republic of"
Case "MC-0-1-mc"
sCntry = "Monaco (EU)"
Case "MN-2-0-mn"
sCntry = "Mongolia"
Case "XX-0-0-mt"            ' "XX-2-0-mt"
sCntry = "Montenegro"
Case "MS-1-0-MS"
sCntry = "Montserrat"
Case "MA-1-0-MA"
sCntry = "Morocco"
Case "MZ-1-0-MZ"
sCntry = "Mozambique"
Case "MM-1-0-MM"
sCntry = "Myanmar (formerly Burma)"
Case "NA-1-0-NA"
sCntry = "Namibia"
Case "NR-2-0-NR"
sCntry = "Nauru Island"
Case "NP-1-0-NP"
sCntry = "Nepal"
Case "AN-1-0-AN"
sCntry = "Netherland Antilles"
Case "NL-0-1-NL"
sCntry = "Netherlands (EU)"
Case "NC-2-0-NC"
sCntry = "New Caledonia"
Case "NZ-2-0-NZ"
sCntry = "New Zealand"
Case "XX-2-0-nz"
sCntry = "New Zealand Island Territories"
Case "NI-1-0-NI"
sCntry = "Nicaragua"
Case "NE-1-0-NE"
sCntry = "Niger Republic"
Case "NG-1-0-NG"
sCntry = "Nigeria"
Case "NF-2-0-NF"
sCntry = "Norfolk Island"
Case "MP-2-0-MP"
sCntry = "Northern Mariana Islands"
Case "NO-3-0-NO"        ' "NO-0-0-NO"
sCntry = "Norway"
Case "OM-1-0-OM"
sCntry = "Oman"
Case "PK-1-0-PK"
sCntry = "Pakistan"
Case "XX-2-0-pa"
sCntry = "Palau"
Case "XX-2-0-pt"
sCntry = "Palestinian Terrritory, occupied"
Case "PA-1-0-PA"
sCntry = "Panama"
Case "PG-2-0-PG"
sCntry = "Papua New Guinea"
Case "PY-1-0-PY"
sCntry = "Paraguay"
Case "PE-1-0-PE"
sCntry = "Peru"
Case "PH-2-0-PH"
sCntry = "Philippines"
Case "PN-2-0-PN"
sCntry = "Pitcairn Island"
Case "PL-0-1-PL"
sCntry = "Poland (EU)"
Case "PT-0-1-PT"
sCntry = "Portugal (EU)"
Case "PR-1-0-PR"
sCntry = "Puerto Rico"
Case "QA-1-0-QA"
sCntry = "Qatar"
Case "XX-2-0-rk"
sCntry = "Ras al Khaimah"
Case "RE-1-0-RE"
sCntry = "Reunion"
Case "RO-0-1-RO"
sCntry = "Romania (EU)"
Case "RU-3-0-RU"            ' "RU-0-0-RU"
sCntry = "Russia Federation"
Case "RW-1-0-RW"
sCntry = "Rwanda"
Case "XX-2-0-KN"
sCntry = "Saint Kitts and Nevis"
Case "XX-2-0-VC"
sCntry = "Saint Vincent and the Grenadines"
Case "XX-2-0-sa"
sCntry = "Sabah"
Case "WS-2-0-WS"
sCntry = "Samoa (American)"
Case "SM-0-1-SM"
sCntry = "San Marino (EU)"
Case "ST-1-0-ST"
sCntry = "Sao Tome and Principe"
Case "XX-2-0-sr"
sCntry = "Sarawak"
Case "XX-2-0-so"
sCntry = "Sao Tome and the Grenadines"
Case "SA-1-0-SA"
sCntry = "Saudi Arabia"
Case "SN-1-0-SN"
sCntry = "Senegal"
Case "XX-2-0-se"
sCntry = "Serbia"
Case "SC-1-0-SC"
sCntry = "Seychelles"
Case "XX-2-0-sh"
sCntry = "Sharjah"
Case "SL-1-0-SL"
sCntry = "Sierra Leone"
Case "SG-1-0-SG"
sCntry = "Singapore"
Case "SK-0-1-SK"
sCntry = "Slovakia (Slovak Republic) (EU)"
Case "SI-0-1-SI"
sCntry = "Slovenia (EU)"
Case "SB-2-0-SB"
sCntry = "Solomon Islands"
Case "SO-1-0-SO"
sCntry = "Somalia"
Case "ZA-1-0-ZA"
sCntry = "South Africa"
Case "GS-1-0-sg"
sCntry = "South Georgia"
Case "XX-2-0-ss"
sCntry = "South Sandwich Islands"
Case "ES-0-1-ES"
sCntry = "Spain (EU)"
Case "XX-2-0-sn"
sCntry = "Spanish North African Territories"
Case "XX-2-0-sp"
sCntry = "Spitzbergen"
Case "LK-1-0-LK"
sCntry = "Sri Lanka"
Case "SH-1-0-sh"
sCntry = "St Helena"
Case "LC-1-0-LC"
sCntry = "St Lucia"
Case "PM-1-0-sp"
sCntry = "St Pierre & Miquelon"
Case "VC-1-0-VC"
sCntry = "St Vincent & Grenadines"
Case "SD-1-0-SD"
sCntry = "Sudan"
Case "SR-1-0-SR"
sCntry = "Suriname"
Case "SJ-1-0-sv"
sCntry = "Svaland and Jan Mayen Islands"
Case "SZ-1-0-SZ"
sCntry = "Swaziland"
Case "CH-3-0-CH"        ' "CH-0-0-CH"
sCntry = "Switzerland"
Case "SE-3-1-SE"        ' "SE-0-1-SE"
sCntry = "Sweden"       ' "Sweden (EU)"
Case "SY-1-0-SY"
sCntry = "Syrian Arab Republic"
Case "TW-2-0-TW"
sCntry = "Taiwan"
Case "TJ-3-0-TJ"        ' "TJ-0-0-TJ"
sCntry = "Tajikistan"
Case "TZ-1-0-TZ"
sCntry = "Tanzania, United Republic of"
Case "TH-1-0-TH"
sCntry = "Thailand"
Case "XX-2-0-ti"
sCntry = "Tibet"
Case "TG-1-0-TG"
sCntry = "Togo"
Case "TO-2-0-TO"
sCntry = "Tonga"
Case "TT-1-0-TT"
sCntry = "Trinidad & Tobago"
Case "XX-2-0-tr"
sCntry = "Tristan da Cunha"
Case "TN-1-0-TN"
sCntry = "Tunisia"
Case "TR-3-0-TR"    ' "TR-0-0-TR"
sCntry = "Turkey"
Case "TM-3-0-TM"    ' "TM-0-0-TM"
sCntry = "Turkmenistan"
Case "TC-1-0-TC"
sCntry = "Turks & Caicos Islands"
Case "TV-2-0-TV"
sCntry = "Tuvalu"
Case "UG-1-0-UG"
sCntry = "Uganda"
Case "UA-3-0-UA"    '"UA-0-0-UA"
sCntry = "Ukraine"
Case "XX-2-0-uq"
sCntry = "Umm al Qaiwan"
Case "AE-1-0-AE"
sCntry = "United Arab Emirates"
Case "UY-1-0-UY"
sCntry = "Uruguay"
Case "GB-9-1-UK"
sCntry = "United Kingdom"
Case "US-1-0-US"
sCntry = "USA"
Case "UZ-3-0-UZ"    ' "UZ-0-0-UZ"
sCntry = "Uzbekistan"
Case "VU-2-0-VU"
sCntry = "Vanuatu"
Case "XX-0-0-VA"
sCntry = "Vatican City State"
Case "VE-1-0-VE"
sCntry = "Venezuela"
Case "VN-1-0-VN"
sCntry = "Vietnam"
Case "VG-2-0-VG"
sCntry = "Virgin Islands (British)"
Case "VI-1-0-VI"
sCntry = "Virgin Islands (USA)"
Case "XX-2-0-wi"
sCntry = "Wake Island"
Case "WF-2-0-WF"
sCntry = "Wallis & Futuna Islands"
Case "XX-2-0-ws"
sCntry = "Western Sahara"
Case "XX-2-0-we"
sCntry = "Western Samoa"
Case "YE-1-0-YE"
sCntry = "Yemen"
Case "ZM-1-0-ZM"
sCntry = "Zambia"
Case "XX-2-0-ZR"
sCntry = "Zaire"
Case "ZW-1-0-ZW"
sCntry = "Zimbabwe"
End Select
xxgetCountry = sCntry
end function



'// Pass in the name of the code and you are returned the country"s name
private function getCountry(strCode)
  dim sSQL:sSQL=""
  sSQL = "SELECT ctyName FROM tblCountry WHERE ctyCode='" & strCode & "';"
  'response.Write "sSQL=" & sSQL
  nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aCountry)
  if nErr = 0 then
    getCountry = aCountry(0, 0) 
  else
    getCountry = ""
  end if
end function

'// Pass in the name of the control eg "Country"
private function CountrySelectorPayment(strObjectName, strCode)
  dim sSQL:sSQL=""
  sSQL = "SELECT ctyCode, ctyName FROM tblCountry WHERE ctyActiveForPay=true ORDER BY ctyOrder ASC, ctyName ASC;"
  CountrySelectorPayment = CountrySelectorGeneric(sSQL, strObjectName, strCode) 
end function

private function CountrySelectorDelivery(strObjectName, strCode)
  dim sSQL:sSQL=""
  sSQL = "SELECT ctyCode, ctyName FROM tblCountry WHERE ctyActiveForDel=1 ORDER BY ctyOrder ASC, ctyName ASC;"
  CountrySelectorDelivery = CountrySelectorGeneric(sSQL, strObjectName, strCode) 
end function

private function CountrySelectorGeneric(sSQL, strObjectName, strCode)
  dim sOut:sOut=""
  dim nErr:nErr=0
  dim nIdx:nIdx=0
  dim aCounts:aCounts=null
  sOut = "<select name=""" & strObjectName & """><option value=""""></option>"
  nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aCounts)
  if nErr = 0 then
    for nIdx = 0 to UBound(aCounts, 2)
      sOut = sOut & vbcrlf & "<option value=""" & aCounts(0, nIdx) & """"
      if strCode = aCounts(0, nIdx) then
        sOut = sOut & " selected"
      end if
      sOut = sOut & ">" & aCounts(1, nIdx) & "</option>"
    next
  end if
  sOut = sOut & "</select>"
  CountrySelectorGeneric = sOut
end function

private function CountrySelector(strObjectName, strCode)
  CountrySelector = CountrySelectorPayment(strObjectName, strCode)
end function


'// Pass in the name of the control eg "Country"
'// returns HTML to display a country selection list
private function xxCountrySelector(strObjectName)
xxCountrySelector="<select id=""" & strObjectName & """ name=""" & strObjectName & """ class=""regForm"">" & _
"<option value=""""></option>" & _
"<option value=""GB-9-1-UK"">United Kingdom</option>" & _
"<option value=""XX-1-0-ab"">Abu Dhabi</option>" & _
"<option value=""AF-1-0-AF"">Afghanistan</option>" & _
"<option value=""XX-2-0-aj"">Ajman</option>" & _
"<option value=""AL-3-0-AL"">Albania</option>" & _
"<option value=""DZ-1-0-DZ"">Algeria</option>" & _
"<option value=""AD-0-0-AD"">Andorra</option>" & _
"<option value=""AO-1-0-AO"">Angola</option>" & _
"<option value=""AI-1-0-AI"">Anguilla</option>" & _
"<option value=""AG-1-0-AG"">Antigua & Barbuda</option>" & _
"<option value=""AS-1-0-as"">American Samoa</option>" & _
"<option value=""AR-1-0-AR"">Argentina</option>" & _
"<option value=""AM-3-0-AM"">Armenia</option>" & _
"<option value=""AW-1-0-AW"">Aruba</option>" & _
"<option value=""XX-1-0-ac"">Ascension</option>" & _
"<option value=""AU-2-0-AU"">Australia</option>" & _
"<option value=""AT-0-1-AT"">Austria (EU)</option>" & _
"<option value=""AZ-3-0-AZ"">Azerbaijan</option>" & _
"<option value=""XX-0-1-ao"">Azores</option>" & _
"<option value=""BS-1-0-BS"">Bahamas</option>" & _
"<option value=""BH-1-0-BH"">Bahrain</option>" & _
"<option value=""XX-0-1-bi"">Balearic Islands (EU)</option>" & _
"<option value=""BD-1-0-BD"">Bangladesh</option>" & _
"<option value=""BB-1-0-BB"">Barbados</option>" & _
"<option value=""BY-0-0-BY"">Belarus</option>" & _
"<option value=""XX-2-0-bl"">Belau (Palau)</option>" & _
"<option value=""BE-0-1-BE"">Belgium (EU)</option>" & _
"<option value=""BZ-1-1-BZ"">Belize</option>" & _
"<option value=""BJ-1-0-BJ"">Benin</option>" & _
"<option value=""BM-1-0-BM"">Bermuda</option>" & _
"<option value=""BT-1-0-BT"">Bhutan</option>" & _
"<option value=""BO-1-0-BO"">Bolivia</option>" & _
"<option value=""BA-0-0-BA"">Bosnia-Herzegovina</option>" & _
"<option value=""BW-1-0-BW"">Botswana</option>" & _
"<option value=""BR-1-0-BR"">Brazil</option>" & _
"<option value=""IO-1-0-IO"">British Indian Ocean Territory</option>" & _
"<option value=""BN-1-0-BN"">Brunei Darussalam</option>" & _
"<option value=""XX-1-0-bv"">British Virgin Islands</option>" & _
"<option value=""BG-0-1-BG"">Bulgaria (EU)</option>" & _
"<option value=""BF-1-0-BF"">Burkina Faso</option>" & _
"<option value=""XX-2-0-BU"">Burma</option>" & _
"<option value=""BI-1-0-BI"">Burundi</option>" & _
"<option value=""KH-1-0-KH"">Cambodia</option>" & _
"<option value=""CM-1-0-CM"">Cameroon</option>" & _
"<option value=""CA-1-0-CA"">Canada</option>" & _
"<option value=""XX-0-0-ci"">Canary Islands</option>" & _
"<option value=""CV-1-0-CV"">Cape Verde</option>" & _
"<option value=""KY-1-0-KY"">Cayman Islands</option>" & _
"<option value=""CF-1-0-CF"">Central African Rep.</option>" & _
"<option value=""TD-1-0-TD"">Chad</option>" & _
"<option value=""CL-1-0-CL"">Chile</option>" & _
"<option value=""CN-2-0-CN"">China (People's Republic)</option>" & _
"<option value=""CX-1-0-CX"">Christmas Island</option>" & _
"<option value=""CC-1-0-CC"">Cocos Island (Keeling Island)</option>" & _
"<option value=""CO-1-0-CO"">Colombia</option>" & _
"<option value=""KM-1-0-KM"">Comoros</option>" & _
"<option value=""CG-1-0-CG"">Congo</option>" & _
"<option value=""CD-1-0-cd"">Congo Dem. Rep.</option>" & _
"<option value=""CK-2-0-co"">Cook Islands</option>" & _
"<option value=""XX-0-1-cr"">Corsica (EU)</option>" & _
"<option value=""CR-1-0-CR"">Costa Rica</option>" & _
"<option value=""CI-1-0-CI"">Cote d'Ivoire</option>" & _
"<option value=""HR-3-0-HR"">Croatia</option>" & _
"<option value=""CU-1-0-CU"">Cuba</option>" & _
"<option value=""XX-1-0-cu"">Curacao</option>" & _
"<option value=""CY-3-0-CY"">Cyprus</option>" & _
"<option value=""CY-0-1-CY"">Cyprus (EU)</option>" & _
"<option value=""CZ-0-1-CZ"">Czech Republic (EU)</option>" & _
"<option value=""DK-3-1-DK"">Denmark</option>" & _
"<option value=""DJ-1-0-DJ"">Djibouti</option>" & _
"<option value=""DM-1-0-DM"">Dominica</option>" & _
"<option value=""DO-1-0-DO"">Dominican Republic</option>" & _
"<option value=""XX-1-0-du"">Dubai</option>" & _
"<option value=""TP-2-0-TP"">East Timor</option>" & _
"<option value=""EC-1-0-EC"">Ecuador</option>" & _
"<option value=""EG-1-0-EG"">Egypt</option>" & _
"<option value=""SV-1-0-SV"">El Salvador</option>" & _
"<option value=""XX-1-0-GQ"">Equatorial Guinea</option>" & _
"<option value=""XX-1-0-ER"">Eritrea</option>" & _
"<option value=""XX-0-1-EE"">Estonia (EU)</option>" & _
"<option value=""XX-1-0-ET"">Ethiopia</option>" & _
"<option value=""FK-1-0-FK"">Falkland Islands</option>" & _
"<option value=""FO-0-0-FO"">Faroe Islands</option>" & _
"<option value=""FJ-2-0-FJ"">Fiji</option>" & _
"<option value=""FI-0-1-FI"">Finland (EU)</option>" & _
"<option value=""FR-0-1-FR"">France (EU)</option>" & _
"<option value=""GF-1-0-GF"">French Guiana</option>" & _
"<option value=""PF-2-0-PF"">French Polynesia</option>" & _
"<option value=""TF-2-0-fs"">French Southern & Antarctic Territories</option>" & _
"<option value=""XX-1-0-fw"">French West Indies</option>" & _
"<option value=""XX-1-0-fu"">Fujairah</option>" & _
"<option value=""GA-1-0-GA"">Gabon</option>" & _
"<option value=""GM-1-0-GM"">Gambia</option>" & _
"<option value=""XX-2-0-ga"">Gaza & KhanYunis</option>" & _
"<option value=""GE-3-0-GE"">Georgia</option>" & _
"<option value=""DE-0-1-DE"">Germany (EU)</option>" & _
"<option value=""GH-1-0-GH"">Ghana</option>" & _
"<option value=""GI-0-0-GI"">Gibraltar</option>" & _
"<option value=""GR-0-1-GR"">Greece (EU)</option>" & _
"<option value=""GL-3-0-GL"">Greenland</option>" & _
"<option value=""GD-1-0-GD"">Grenada</option>" & _
"<option value=""GU-2-0-GU"">Guam</option>" & _
"<option value=""GT-1-0-GT"">Guatemala</option>" & _
"<option value=""GG-9-0-GG"">Guernsey (Channel Islands)(UK)</option>" & _ 
"<option value=""GN-1-0-GN"">Guinea</option>" & _
"<option value=""GW-1-0-GW"">Guinea-Bissau</option>" & _
"<option value=""GY-1-0-GY"">Guyana</option>" & _
"<option value=""HT-1-0-HT"">Haiti</option>" & _
"<option value=""HN-1-0-HN"">Honduras</option>" & _
"<option value=""HK-1-0-HK"">Hong Kong</option>" & _
"<option value=""HU-0-1-HU"">Hungary (EU)</option>" & _
"<option value=""IS-3-0-IS"">Iceland</option>" & _
"<option value=""IN-1-0-IN"">India</option>" & _
"<option value=""ID-1-0-ID"">Indonesia</option>" & _
"<option value=""IR-1-0-IR"">Iran</option>" & _
"<option value=""IQ-1-0-IQ"">Iraq</option>" & _
"<option value=""IR-0-1-IE"">Irish Republic (EU)</option>" & _
"<option value=""IL-1-0-IL"">Israel</option>" & _
"<option value=""IT-0-1-IT"">Italy (EU)</option>" & _
"<option value=""JM-1-0-JM"">Jamaica</option>" & _
"<option value=""JP-2-0-JP"">Japan</option>" & _
"<option value=""JE-9-0-JE"">Jersey (Channel Islands)(UK)</option>" & _
"<option value=""JO-1-0-JO"">Jordan</option>" & _
"<option value=""KZ-3-0-KZ"">Kazakhstan</option>" & _
"<option value=""KE-1-0-KE"">Kenya</option>" & _
"<option value=""XX-2-0-ki"">Kirghizstan</option>" & _
"<option value=""KI-2-0-KI"">Kiribati</option>" & _
"<option value=""KP-2-0-KP"">Korea (Democratic People's Republic)</option>" & _
"<option value=""KR-2-0-KR"">Korea (Republic of)</option>" & _
"<option value=""KW-1-0-KW"">Kuwait</option>" & _
"<option value=""XX-1-0-XX"">Laos</option>" & _
"<option value=""LA-1-0-LA"">Lao People's Democratic Republic</option>" & _
"<option value=""LV-3-1-LV"">Latvia (EU)</option>" & _
"<option value=""LB-1-0-LB"">Lebanon</option>" & _
"<option value=""LS-1-0-LS"">Lesotho</option>" & _
"<option value=""LR-1-0-LR"">Liberia</option>" & _
"<option value=""LY-1-0-LY"">Libyan Arab Jamahiriya</option>" & _
"<option value=""LI-0-0-LI"">Liechtenstein</option>" & _
"<option value=""LY-0-1-LT"">Lithuania (EU)</option>" & _
"<option value=""LU-0-1-LU"">Luxembourg (EU)</option>" & _
"<option value=""MO-1-0-MO"">Macao</option>" & _
"<option value=""MK-0-0-MK"">Macedonia (Former Yugoslav Rep. of)</option>" & _
"<option value=""MG-1-0-MG"">Madagascar</option>" & _
"<option value=""XX-0-1-ma"">Madeira (EU)</option>" & _
"<option value=""MW-1-0-MW"">Malawi</option>" & _
"<option value=""MY-1-0-MY"">Malaysia</option>" & _
"<option value=""MV-1-0-MV"">Maldives</option>" & _
"<option value=""ML-1-0-ML"">Mali</option>" & _
"<option value=""MT-0-1-MT"">Malta (EU)</option>" & _
"<option value=""MH-2-0-MH"">Marshall Islands</option>" & _
"<option value=""MR-1-0-MR"">Mauritania</option>" & _
"<option value=""MU-1-0-MU"">Mauritius</option>" & _
"<option value=""MU-0-0-my"">Mayotte</option>" & _
"<option value=""MX-1-0-MX"">Mexico</option>" & _
"<option value=""FM-2-0-fm"">Micronesia (Federated States)</option>" & _
"<option value=""MD-0-0-md"">Moldova, Republic of</option>" & _
"<option value=""MC-0-1-mc"">Monaco (EU)</option>" & _
"<option value=""MN-2-0-mn"">Mongolia</option>" & _
"<option value=""XX-0-0-mt"">Montenegro</option>" & _
"<option value=""MS-1-0-MS"">Montserrat</option>" & _
"<option value=""MA-1-0-MA"">Morocco</option>" & _
"<option value=""MZ-1-0-MZ"">Mozambique</option>" & _
"<option value=""MM-1-0-MM"">Myanmar (formerly Burma)</option>" & _
"<option value=""NA-1-0-NA"">Namibia</option>" & _
"<option value=""NR-2-0-NR"">Nauru Island</option>" & _
"<option value=""NP-1-0-NP"">Nepal</option>" & _
"<option value=""AN-1-0-AN"">Netherland Antilles</option>" & _
"<option value=""NL-0-1-NL"">Netherlands (EU)</option>" & _
"<option value=""NC-2-0-NC"">New Caledonia</option>" & _
"<option value=""NZ-2-0-NZ"">New Zealand</option>" & _
"<option value=""XX-2-0-nz"">New Zealand Island Territories</option>" & _
"<option value=""NI-1-0-NI"">Nicaragua</option>" & _
"<option value=""NE-1-0-NE"">Niger Republic</option>" & _
"<option value=""NG-1-0-NG"">Nigeria</option>" & _
"<option value=""NF-2-0-NF"">Norfolk Island</option>" & _
"<option value=""MP-2-0-MP"">Northern Mariana Islands</option>" & _
"<option value=""NO-3-0-NO"">Norway</option>" & _
"<option value=""OM-1-0-OM"">Oman</option>" & _
"<option value=""PK-1-0-PK"">Pakistan</option>" & _
"<option value=""XX-2-0-pa"">Palau</option>" & _
"<option value=""XX-2-0-pt"">Palestinian Terrritory, occupied</option>" & _
"<option value=""PA-1-0-PA"">Panama</option>" & _
"<option value=""PG-2-0-PG"">Papua New Guinea</option>" & _
"<option value=""PY-1-0-PY"">Paraguay</option>" & _
"<option value=""PE-1-0-PE"">Peru</option>" & _
"<option value=""PH-2-0-PH"">Philippines</option>" & _
"<option value=""PN-2-0-PN"">Pitcairn Island</option>" & _
"<option value=""PL-0-1-PL"">Poland (EU)</option>" & _
"<option value=""PT-0-1-PT"">Portugal (EU)</option>" & _
"<option value=""PR-1-0-PR"">Puerto Rico</option>" & _
"<option value=""QA-1-0-QA"">Qatar</option>" & _
"<option value=""XX-2-0-rk"">Ras al Khaimah</option>" & _
"<option value=""RE-1-0-RE"">Reunion</option>" & _
"<option value=""RO-0-1-RO"">Romania (EU)</option>" & _
"<option value=""RU-0-0-RU"">Russia Federation</option>" & _
"<option value=""RW-1-0-RW"">Rwanda</option>" & _
"<option value=""XX-2-0-KN"">Saint Kitts and Nevis</option>" & _
"<option value=""XX-2-0-VC"">Saint Vincent and the Grenadines</option>" & _
"<option value=""XX-2-0-sa"">Sabah</option>" & _
"<option value=""WS-2-0-WS"">Samoa (American)</option>" & _
"<option value=""SM-0-1-SM"">San Marino (EU)</option>" & _
"<option value=""ST-1-0-ST"">Sao Tome and Principe</option>" & _
"<option value=""XX-2-0-sr"">Sarawak</option>" & _
"<option value=""XX-2-0-so"">Sao Tome and the Grenadines</option>" & _
"<option value=""SA-1-0-SA"">Saudi Arabia</option>" & _
"<option value=""SN-1-0-SN"">Senegal</option>" & _
"<option value=""XX-2-0-se"">Serbia</option>" & _
"<option value=""SC-1-0-SC"">Seychelles</option>" & _
"<option value=""XX-2-0-sh"">Sharjah</option>" & _
"<option value=""SL-1-0-SL"">Sierra Leone</option>" & _
"<option value=""SG-1-0-SG"">Singapore</option>" & _
"<option value=""SK-0-1-SK"">Slovakia (Slovak Republic) (EU)</option>" & _
"<option value=""SI-0-1-SI"">Slovenia (EU)</option>" & _
"<option value=""SB-2-0-SB"">Solomon Islands</option>" & _
"<option value=""SO-1-0-SO"">Somalia</option>" & _
"<option value=""ZA-1-0-ZA"">South Africa</option>" & _
"<option value=""GS-1-0-sg"">South Georgia</option>" & _
"<option value=""XX-2-0-ss"">South Sandwich Islands</option>" & _
"<option value=""ES-0-1-ES"">Spain (EU)</option>" & _
"<option value=""XX-2-0-sn"">Spanish North African Territories</option>" & _
"<option value=""XX-2-0-sp"">Spitzbergen</option>" & _
"<option value=""LK-1-0-LK"">Sri Lanka</option>" & _
"<option value=""SH-1-0-sh"">St Helena</option>" & _
"<option value=""LC-1-0-LC"">St Lucia</option>" & _
"<option value=""PM-1-0-sp"">St Pierre & Miquelon</option>" & _
"<option value=""VC-1-0-VC"">St Vincent & Grenadines</option>" & _
"<option value=""SD-1-0-SD"">Sudan</option>" & _
"<option value=""SR-1-0-SR"">Suriname</option>" & _
"<option value=""SJ-1-0-sv"">Svaland and Jan Mayen Islands</option>" & _
"<option value=""SZ-1-0-SZ"">Swaziland</option>" & _
"<option value=""CH-3-0-CH"">Switzerland</option>" & _
"<option value=""SE-3-1-SE"">Sweden</option>" & _
"<option value=""SY-1-0-SY"">Syrian Arab Republic</option>" & _
"<option value=""TW-2-0-TW"">Taiwan</option>" & _
"<option value=""TJ-3-0-TJ"">Tajikistan</option>" & _
"<option value=""TZ-1-0-TZ"">Tanzania, United Republic of</option>" & _
"<option value=""TH-1-0-TH"">Thailand</option>" & _
"<option value=""XX-2-0-ti"">Tibet</option>" & _
"<option value=""TG-1-0-TG"">Togo</option>" & _
"<option value=""TO-2-0-TO"">Tonga</option>" & _
"<option value=""TT-1-0-TT"">Trinidad & Tobago</option>" & _
"<option value=""XX-2-0-tr"">Tristan da Cunha</option>" & _
"<option value=""TN-1-0-TN"">Tunisia</option>" & _
"<option value=""TR-3-0-TR"">Turkey</option>" & _
"<option value=""TM-3-0-TM"">Turkmenistan</option>" & _
"<option value=""TC-1-0-TC"">Turks & Caicos Islands</option>" & _
"<option value=""TV-2-0-TV"">Tuvalu</option>" & _
"<option value=""UG-1-0-UG"">Uganda</option>" & _
"<option value=""UA-3-0-UA"">Ukraine</option>" & _
"<option value=""XX-2-0-uq"">Umm al Qaiwan</option>" & _
"<option value=""AE-1-0-AE"">United Arab Emirates</option>" & _
"<option value=""GB-9-1-UK"">United Kingdom</option>" & _
"<option value=""UY-1-0-UY"">Uruguay</option>" & _
"<option value=""US-1-0-US"">USA</option>" & _
"<option value=""UZ-3-0-UZ"">Uzbekistan</option>" & _
"<option value=""VU-2-0-VU"">Vanuatu</option>" & _
"<option value=""XX-0-0-VA"">Vatican City State</option>" & _
"<option value=""VE-1-0-VE"">Venezuela</option>" & _
"<option value=""VN-1-0-VN"">Vietnam</option>" & _
"<option value=""VG-2-0-VG"">Virgin Islands (British)</option>" & _
"<option value=""VI-1-0-VI"">Virgin Islands (USA)</option>" & _
"<option value=""XX-2-0-wi"">Wake Island</option>" & _
"<option value=""WF-2-0-WF"">Wallis & Futuna Islands</option>" & _
"<option value=""XX-2-0-ws"">Western Sahara</option>" & _
"<option value=""XX-2-0-we"">Western Samoa</option>" & _
"<option value=""YE-1-0-YE"">Yemen</option>" & _
"<option value=""ZM-1-0-ZM"">Zambia</option>" & _
"<option value=""XX-2-0-ZR"">Zaire</option>" & _
"<option value=""ZW-1-0-ZW"">Zimbabwe</option>" & _
"</select>"
end function

function javaSetDropDown(sForm)
	dim sTmp:sTmp = ""
	sTmp = vbcrlf & "<script language=""JavaScript""><!--" & vbcrlf _
		& "function setDropDown(sName, sVal){" & vbcrlf _
		& "var oDD = document.forms[""" & sForm & """].elements[sName];" & vbcrlf _
		& "for (i=0;i<oDD.length;i++)	if (oDD.options[i].value==sVal)	oDD.selectedIndex=i;" & vbcrlf _
		& "}" & vbcrlf _
		& "//--></script>" & vbcrlf
	javaSetDropDown = sTmp
end function
%>

