<!--#include virtual="/includes/careandcomfortAPI.asp"-->
<!--#include virtual="/ecomm/setup.asp"-->
<!--#include virtual="/cms/setup.asp"-->
<!--#include virtual="/includes/dates.asp" -->





		


<!--#include virtual="/includes/doctype.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
     <%=GetDefaultEcommMetaTags("Basket - " & SHOP_NAME)%>  
    <!--#include virtual="/includes/mainCSS.asp" -->
	  <!--#include virtual="/includes/javascript.asp" -->
</head>

<!--#include virtual="/includes/templateStart.asp" -->
<!--#include virtual="/includes/templateTopNav.asp" -->
<!--#include virtual="/includes/templateLeftNav.asp" -->


<!--#include virtual="/includes/templateBeginMainContent.asp" -->
	
<!-- Begin Content -->
    
   <div id="breadcrumb">
		You are in: <span class="coloured">Shopping basket</span>
	</div>
    
	<br/>		
	<p><strong>Sorry but the shopping cart is offline until the all the site's components are put in place. We hope to get this completed by the end of business on Monday 20th August .</strong></p>	
	<br/>
	<p>We are still able to take orders directly over the phone or via email until this takes place.</p>
	
	<p>For any questions or technical help please ring Martin Ashmore - Mobile 07791-605722 Mon - Sat 9am until 9pm and Sun 1pm until 5pm.</p>

	<p>Or send us an email at <a href="mailto:careandcomfort@btinternet.com" title="email us">careandcomfort@btinternet.com</a>.</p>
	
	<p>Thank you , the Care and Comfort team.</p>

<!-- End Content -->
<!--#include virtual="/includes/templateEndMainContent.asp" -->

<!--#include virtual="/includes/templateRightColumn.asp" -->

<!--#include virtual="/includes/templateFooter.asp" -->
<!--#include virtual="/includes/templateEnd.asp" -->


