<!--#include virtual="/includes/careandcomfortAPI.asp"-->
<!--#include virtual="/ecomm/setup.asp"-->
<%
'// cms content id setting for 1st content item that will hold the meta tags
Dim nCntId 


call parseParms(request.querystring,sCurFldr,nCatID,nRngID,nGrpID,nColID,nPrdID)

Select Case nCatID
		
		Case CAT_MATTRESSES_ID
			nCntId = MATTRESSES_CONTENT_ID
			sBreathEasy = "mattresses"
		
		Case BED_TOPPERS_ID
			nCntId = TOPPERS_CONTENT_ID
			sBreathEasy = "toppers"

		Case CAT_PILLOWS_ID
			nCntId = PILLOWS_CONTENT_ID
			sBreathEasy = "pillows"

		Case CAT_MATTRESSES_ID
			nCntId = MATTRESSES_CONTENT_ID

	End Select

%>
<!--#include virtual="/cms/setup.asp"-->
<!--#include virtual="/includes/doctype.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <%=getCMSMetaData(nCntId,"","","")%> 
    <!--#include virtual="/includes/mainCSS.asp" -->
	  <!--#include virtual="/includes/javascript.asp" -->
</head>

<!--#include virtual="/includes/templateStart.asp" -->
<!--#include virtual="/includes/templateTopNav.asp" -->
<!--#include virtual="/includes/templateLeftNav.asp" -->


<!--#include virtual="/includes/templateBeginMainContent.asp" -->
<!-- Begin Content -->	

<%

sSQL = "SELECT * FROM tblCategory WHERE (tblCategory.keyFinID = " & INST_ID & ") AND (tblCategory.caName <> '') AND caID = " & nCatID & ";"

	
		

nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aRangeMeta)

If nErr = 0 Then
	
%>

	<img src="/ecomm/graphics/<%=aRangeMeta(CA_IMAGEMAIN,0)%>"   alt="<%=aRangeMeta(CA_IMAGEMAIN,0)%> range"/>


	<div id="breadcrumb">
		You are in: <span class="coloured"><%=aRangeMeta(CA_NAME,0)%></span>
	</div>
	
	<h1><%=aRangeMeta(CA_NAME,0)%></h1>
	

	<%=nvCmsGetContentHTML(nCntId)%> 

<%

IF CInt(nCatID) <> CInt(CAT_PILLOWS_ID) Then

	' can assume that an array of sub categories has already been populated
	'if on this page already
	sSQL2 = "SELECT  tblRange.* FROM tblRangeToCategory INNER JOIN " & _
			" tblRange ON tblRangeToCategory.keyFrnID = tblRange.rnID " & _
			"WHERE ( tblRangeToCategory.keyFcaID = " & nTempCatId & ") AND tblRange.keyFinID = " & INST_ID & " ORDER BY tblRangeToCategory.r2cDisplay;"
									
	
	nSubRangeErr = getRowsBySQL(DSN_ECOM_DB, sSQL2, aSubRanges)

	if nSubRangeErr = 0 then

		for intJ = 0 to ubound(aSubRanges,2)
		%>

		<div class="sub_range">
							
				<img class="thumbnail" src="/ecomm/graphics/<%=aSubRanges(CA_IMAGETHUMB,intJ)%>" width="195" height="95" alt="<%=aSubRanges(CA_NAME,intJ)%> thumb"/>

				<div class="content">
					
					<h3><%=aSubRanges(CA_NAME,intJ)%></h3>

					<p><%=aSubRanges(CA_DESCRIPTION,intJ)%></p>
					
					<div class="buttons">

						<%
						sProductSql = "SELECT  tblProduct.* FROM  tblProductToRange INNER JOIN " & _
						"tblProduct ON tblProductToRange.keyFprID = tblProduct.prID " & _
						"WHERE (tblProduct.prOnSale = 1) AND (tblProduct.prDisplay = 1) AND (tblProductToRange.keyFrnID = " & aSubRanges(CA_ID,intJ) & ") AND (tblProduct.keyFinID = " & INST_ID & ") ORDER BY prId,prName,prCode;"

						'Response.Write sProductSql
						nErr = getRowsBySQL(DSN_ECOM_DB, sProductSql, aChildProducts)

						If nErr = 0 Then
							 for intK = 0 to ubound(aChildProducts,2)
								
								%><a href="/products/details/?<%=buildParms("",nCatID,nRngID,0,0,aChildProducts(PR_ID,intK))%>"><%
								If intK = 0 Then
								%><img src="/images/buttons/btn_british_sizes.gif" alt="British sizes" width="100" height="17"/><%
								Else
								%><img src="/images/buttons/btn_euro_sizes.gif" alt="Euro sizes" width="86" height="17"/><%
								END If
								%></a>
								<%
							 Next


						End If
						%>
					</div>
			
			</div>

			<div class="clear"></div>
		</div>
	<%
	 Next


	End If

Else
	sChildProductsSQL = "SELECT tblProduct.* FROM tblRangeToCategory INNER JOIN tblProductToRange ON " & _
							"tblRangeToCategory.keyFrnID = tblProductToRange.keyFrnID INNER JOIN tblProduct ON " & _
							"tblProductToRange.keyFprID = tblProduct.prID " & _
							"WHERE (tblProduct.prOnSale = 1) AND (tblProduct.prDisplay = 1) AND (tblRangeToCategory.keyFcaID = " & nCatID & ") AND(tblRangeToCategory.keyFinID = " & INST_ID & ") ORDER By tblProductToRange.p2rDisplay ASC;"
								
								
	nChildProductErr = getRowsBySQL(DSN_ECOM_DB, sChildProductsSQL, aChildProducts)
	
	Dim aProductSingleDimension(48) 

	If nChildProductErr = 0 Then
	
		for intJ = 0 to ubound(aChildProducts,2)

			 ' Put all the fields of the current array row into an 1 dimensional array array for the bespoke code to use
			for nFieldIdx = 0 to ubound(aChildProducts, 1)
			   ' Need this to stop population of aProduct when all the product db fields are retrieved from the aCartItems datasat
			   if nFieldIdx <= ubound(aProductSingleDimension) then
					aProductSingleDimension(nFieldIdx) = aChildProducts(nFieldIdx,intJ)
			   end if
			next                        
			' Retrieve price for display and store it in a hidden field for the product_final.asp step to use
			nPriceErr = getAppropriatePrice(aProductSingleDimension, nOutputPrice, nOutputVAT, nOriginalPrice, nOriginalVAT) 
			
			%>

			<div class="sub_range">
								
					
					
					<img class="thumbnail" src="/ecomm/graphics/<%=aChildProducts(PR_IMAGEMAIN,intJ)%>" width="195" height="95" alt="<%=aChildProducts(PR_NAME,intJ)%> thumb"/>

					<div class="content">
						
						<h3><%=aChildProducts(PR_NAME,intJ)%></h3>

						<p><%=aChildProducts(PR_FURTHERINFO,intJ)%></p>
						
						<p class="price"><strong><%
						
						If CDbl(nOutputPrice) <  CDbl(nOriginalPrice) Then
							Response.Write "<span class=""strike"">" & getPriceForexHTML(nOriginalPrice,2) & "</span><br/>"		
						End IF				
	
						Response.Write getPriceForexHTML(nOutputPrice,2)%></strong></p>

						<div class="button_buy">
							<form method="post" action="/shoppingbasket/buy.asp?<%=buildParms("",nCatID,nRngID,0,0,aChildProducts(PR_ID,intJ))%>">
						
						<input type="hidden" name="prID" value="<%=aChildProducts(PR_ID,intJ)%>"/>
						<input type="hidden" name="qty" value="1"/>

						<input type="image" style="float:right;margin:8px 0px;" name="buy" src="/images/buttons/btn_buy.gif"/>
					</form>
						</div>

					</div>

					<div class="clear"></div>

			</div>
			<%
		Next
	End If

End If

End If




%>
<!--#include virtual="/includes/contentfooter.asp" -->

<!-- End Content -->
<!--#include virtual="/includes/templateEndMainContent.asp" -->


<!--#include virtual="/includes/templateRightColumn.asp" -->


<!--#include virtual="/includes/templateFooter.asp" -->
<!--#include virtual="/includes/templateEnd.asp" -->