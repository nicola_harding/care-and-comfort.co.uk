<%

Response.Clear
On Error Resume Next
Dim objError
Set objError = Server.GetLastError()

Dim strEmailBody
strEmailBody = "" &_
	"ASP 500 Error" & vbCrLf &_
	"Occurred at: " & Now & vbCrLf & _
	"Server: " & Request.ServerVariables("SERVER_NAME") & vbCrLf & vbCrLf

If Len(CStr(objError.ASPCode)) > 0 Then
	strEmailBody = strEmailBody &_
		"IIS Error Number: " & objError.ASPCode & vbCrLf
End If

If Len(CStr(objError.Number)) > 0 Then
	strEmailBody = strEmailBody &_
		"COM Error Number: " & objError.Number & " (0x" & Hex(objError.Number) & ")" & vbCrLf
End If

If Len(CStr(objError.Source)) > 0 Then
	strEmailBody = strEmailBody &_
		"Error Source: " & objError.Source & vbCrLf
End If

If Len(CStr(objError.File)) > 0 Then
	strEmailBody = strEmailBody &_
		"File Name: " & objError.File & vbCrLf
End If

If Len(CStr(objError.Line)) > 0 Then
	strEmailBody = strEmailBody &_
		"Line Number: " & objError.Line & vbCrLf
End If

If Len(CStr(objError.Description)) > 0 Then
	strEmailBody = strEmailBody &_
		"Brief Description: " & objError.Description & vbCrLf
End If

If Len(CStr(objError.ASPDescription)) > 0 Then
	strEmailBody = strEmailBody &_
		"Full Description: " & objError.ASPDescription & vbCrLf
End If

If Len(CStr(Request.ServerVariables("QUERY_STRING"))) > 0 Then
	strEmailBody = strEmailBody & vbCrLf &_
		"Query String: " & Request.ServerVariables("QUERY_STRING") & vbCrLf
End If

Dim oKey
strEmailBody = strEmailBody & vbCrLf & "Form collection (if any): " & vbCrLf
For Each oKey in Request.Form
	strEmailBody = strEmailBody &_
	oKey & " : " & Request.Form(oKey) & vbCrLf
Next

strEmailBody = strEmailBody & vbCrLf & "Cookies collection (if any): " & vbCrLf
For Each oKey in Request.Cookies
	strEmailBody = strEmailBody &_
	oKey & " : " & Request.Form(oKey) & vbCrLf
	For Each oSubKey in oKey
		strEmailBody = strEmailBody &_
		oKey & "." & oVal & " : " & Request.Form(oKey)(oVal) & vbCrLf
	Next
Next

' Test code for getting session variables
'strEmailBody = strEmailBody & vbCrLf & "Session collection (if any): " & vbCrLf
'For Each oKey in Session.Items
'	strEmailBody = strEmailBody &_
'	oKey & " : " & Request.Form(oKey) & vbCrLf
'Next

strEmailBody = strEmailBody & vbCrLf &_
	"User ID: " & Session("sponworld_usr_id") & vbCrLf & _
	"Proxy User ID (if logged in as proxy): " & Session("sponworld_proxyuserid") & vbCrLf & _
	"Order ID (if any): " & Session("SMOrder_OrderID") & vbCrLf

strEmailBody = strEmailBody & vbCrLf &_
	"IP: " & Request.ServerVariables("REMOTE_ADDR") & vbCrLf

If Len(CStr(Request.ServerVariables("HTTP_USER_AGENT"))) > 0 Then
	strEmailBody = strEmailBody &_
		"User Agent: " & Request.ServerVariables("HTTP_USER_AGENT") & vbCrLf
End If

'--------------------SEND EMAIL ALERT------------------
Call SendMail("andy.robb@nvisage.uk.com", "andy.robb@nvisage.uk.com", "500 Alert: " & Request.ServerVariables("SERVER_NAME"), strEmailBody)


'###########################################################################################################
' Send plain text email
'###########################################################################################################
Sub SendMail(ByVal strTo, ByVal strFrom, ByVal strSubject, ByVal strMessage)
	
	Dim objMail
	Set objMail = Server.CreateObject("CDO.Message")
	
	objMail.Configuration.Fields.Item _
	("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2

	objMail.Configuration.Fields.Item _
	("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
	
	objMail.Configuration.Fields.Item _
	("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25

	objMail.Configuration.Fields.Update
	objMail.To = strTo
	objMail.From = strFrom
	objMail.Subject = strSubject
	
	objMail.TextBody = strMessage

	objMail.Send

	set objMail = Nothing

End Sub
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">


<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>Memory foam mattresses, hypoallergenic pillows, toppers, visco memory foam mattress pad, covers and overlay</title>
<meta name="keywords" content="Memory foam mattresses, hypoallergenic pillows, visco elastic mattress mattresses, toppers, visco memory foam mattress pad, covers and overlay
" />
<meta name="description" content="Care and comfort sell memory foam mattresses, hypoallergenic pillows, toppers, visco memory foam mattress pad, covers and overlay. Also they specialise in hypoallergenic foam mattress, anti dust mites bed and anti allergy bedding." />
   <link rel="stylesheet" type="text/css" href="/css/common.css" media="all" />
	<link rel="stylesheet" type="text/css" href="/css/screen.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="/css/print.css" media="print" />

<!-- add these new stylesheet references -->




<script type="text/javascript" src="/javascript/main.js"></script>

  <script language="JavaScript" type="text/javascript">
    <!--
    function showTandC(){
	    var tandcWin;
	    tandcWin = window.open("/privacypolicy/window.asp","PrivPol","height=600,width=520,scrollbars,menubar,resizable");
	    return false;
    }
    function popReceipt(sURL){
	    var tandcWin;
	    tandcWin = window.open(sURL,"Receipt","height=600,width=520,scrollbars,toolbar,menubar,resizable");
	    return false;
    }
    //-->
    </script>
</head>


<div id="site_container"> 
  <div id="main_container"> <div id="top">

			
			<div class="left">
				<a href="/" title="Navigate to: Home page"><img src="/images/careandcomfort_logo.jpg" alt="Absolute Care and Comfort" width="300" height="80"/></a>
				
				<div class="search_bar">
					
					<h2><img src="/images/header_text.gif" alt="Promoting good health through good sleep with memory foam" width="455" height="22"/></h2>

					<form method="get" action="/search/">
						<input type="text" onclick="javascript:blankText(this);" class="text" name="searchbox" value="SEARCH"/>
						<input type="image" class="button" src="/images/buttons/btn_search.gif" />

						
					</form>

				</div>

				<img class="to_products" src="/images/buttons/btn_to_products.gif" alt="Products" width="75" height="15"/>
			</div>

			<div class="right">
			

				<div class="quick_basket">
					<img class="basket" src="/images/icons/basket.gif" alt="Basket" width="25" height="25"/>
					<a href="/basket/" title="Navigate to: Basket"><img class="to_basket" src="/images/buttons/btn_to_basket.gif" alt="Basket" width="120" height="12"/></a>
					<div class="items">0 items</div><div class="total">&pound;0.00 total</div>

					
					<div class="links"><a href="/shoppingbasket/" title="View your shopping basket">View</a>&nbsp;/&nbsp;<a href="/shoppingbasket/checkout.asp" title="go to checkout">Checkout</a></div>

				</div>
			</div>

			<div id="top_nav">
				<ul>
					<li><a href="/index.asp?id=69" title="Navigate to: Welcome to Care and Comfort">Home</a><div class="tl"></div><div class="tr"></div></li><li class="selected"><a href="/aboutus/index.asp?id=64" title="Navigate to: More about us">About us</a><div class="tl"></div><div class="tr"></div></li><li><a href="/contactus/index.asp?id=65" title="Navigate to: Contact the staff at Care and Comfort">Contact us</a><div class="tl"></div><div class="tr"></div></li><li><a href="/howtoshop/index.asp?id=66" title="Navigate to: How to shop">How to shop</a><div class="tl"></div><div class="tr"></div></li><li><a href="/help/index.asp?id=67" title="Navigate to: Help">Help</a><div class="tl"></div><div class="tr"></div></li><li><a href="/links/index.asp?id=68" title="Navigate to: Links">Links</a><div class="tl"></div><div class="tr"></div></li>

				</ul>
			</div>
		</div>




		<div id="left_main">
				
				<div class="left_nav">
					
					<ul class="product_nav">

					 <li><a title="Browse our Mattresses category" href="/categories/?0,0,0,0,12">Mattresses</a></li><li><a title="Browse our Toppers category" href="/categories/?0,0,0,0,13">Toppers</a></li><li><a title="Browse our Pillows category" href="/categories/?0,0,0,0,15">Pillows</a></li><li><a title="Browse our Beds category" href="/beds/?id=73">Beds</a></li>
			<!-- </dl>

						<li><a href="/links/" title="Navigate to: Mattresses">Mattresses</a></li>
						<li><a href="/links/" title="Navigate to: Toppers">Toppers</a></li>
						<li><a href="/links/" title="Navigate to: Covers">Covers</a></li>
						<li><a href="/links/" title="Navigate to: Pillows">Pillows</a></li>
						<li><a href="/beds/" title="Navigate to: Beds">Beds</a></li>
						<li><a href="/links/" title="Navigate to: How it works">How it works</a></li>-->

					</ul>
					
					<img class="to_basket" src="/images/buttons/btn_to_benefits.gif" alt="Benefits" width="120" height="20"/>

					<ul class="other_nav">
					
						
						<li><a href="/findyoursleepsolution/index.asp?id=78" title="Navigate to: Find your sleep solution">Find your sleep solution</a></li><li><a href="/antiallergy/index.asp?id=81" title="Navigate to: Anti-Allergy">Anti-allergy</a></li><li><a href="/whichmattressisbest/index.asp?id=79" title="Navigate to: Which mattress is best">Which mattress?</a></li><li><a href="/howcanithelp/index.asp?id=80" title="Navigate to: How can it help">How can it help?</a></li><li><a href="/howdoesitwork/index.asp?id=76" title="Navigate to: How does it work">How does it work?</a></li><li><a href="index.asp?id=77" title="Navigate to: Why Ultimate Sleeper &trade;">Why Ultimate Sleeper &trade;?<br /></a></li>

					</ul>


					<img class="contact_header" src="/images/contact.gif" alt="Contact" width="50" height="13"/>

					<div class="contact_details">
						<img src="/images/icons/email.gif" alt="Email" width="20" height="21"/><p>01702-533767
						<br/>or <a href="mailto:info@care-and-comfort.co.uk" title="email us">email us</a>.</p>
					</div>

				</div>
			</div>
	<div id="main_content">
	
	
			
<!-- Begin Content -->	
	<img width="472" height="192" alt="About Care and Comfort" src="/uploadedimages/cms/photo_large_aboutus.jpg" />
<div id="breadcrumb"> 					You are in: <span class="coloured">Site error</span> 				</div>

 

<!-- Begin Content -->	


	<h2>We're sorry, but a server error just occurred!</h2>

	<p>This has probably happened because of a problem caused by information you entered on a form or because the server is very busy.</p>

	<p>Our website team have been notified of this error by email and will try and examine the cause as soon as possible. They will be able to track what you were doing when the error happened, and in most cases they will be in contact with you by email to let you know the error has been fixed so you can continue.</p> 

	<p>You can always try again and if the error happens again, just try again later when we've fixed it.</p>
	
	<p><a href="/">Return to home page</a></p>

<div class="spacer" style="height:170px">         <!-- -->       </div>
<br />       <br />  </div> <div id="right_main">
				
				<span class="spacer"><!----></span>
				
				<div id="backpain" class="signpost">
					<h2>A family company</h2>

					<p>If you are unfamiliar with using an on-line Shopping Basket to order, please ring on 01702-533767.</p>

					
					
					<div class="clear"></div>
					
				</div>
			
				<div id="backpain_bottom"></div>

				
				<div id="breath_easy" class="signpost">
					<h2>Find your sleep solution</h2>

					<p>How can an Ultimate Sleeper &trade; memory foam mattress help?</p>
					
					<a href="/findyoursleepsolution/?id=78" title="Navigate to: Find your sleep solution"><img src="/images/buttons/btn_to_breath_easy.gif" alt="Navigate to: Find your sleep solution" width="15" height="15"/></a>

					<div class="clear"></div>
					
				</div>
			
				<div id="breath_easy_bottom"></div>
				
				<div id="care_kids" class="signpost">
					<h2>Breathe easy</h2>

					<p>Memory foam mattresses are allergy resistant and do not provide a suitable environment for dust mites to live.</p>
					<a href="/antiallergy/?id=81" title="Navigate to: Backpain"><img src="/images/buttons/btn_to_carekids.gif" alt="Navigate to: Backpain" width="15" height="15"/></a>

					<div class="clear"></div>
				</div>
				<div id="care_kids_bottom">
				</div>
				
				

				<div id="sleep_better" class="signpost">
					<h2>Which mattress is best</h2>

					<p>Pick the perfect solution for your needs</p>

					<a href="/whichmattressisbest/?id=79" title="Navigate to: Which mattress is best"><img src="/images/buttons/btn_to_sleepbetter.gif" alt="Which mattress is best" width="15" height="15"/></a>
				<div class="clear"></div>
				</div>
				<div id="sleep_better_bottom">
				</div>
				
				

				<div id="more_help" class="signpost">
					<h2>Here to help</h2>

					<p>For any questions or technical help please ring Martin Ashmore - Mobile 07791-605722 Mon - Sat 9am until 9pm and Sun 1pm until 5pm.</p>

					<a href="/needtoknowinformation/?id=82" title="Navigate to: More help"><img src="/images/buttons/btn_to_morehelp.gif" alt="Navigate to: More help" width="15" height="15"/></a>
					<div class="clear"></div>
				</div>
				<div id="more_help_bottom">
				</div>
			</div>
<div id="footer">
				<p>
				 <a href="/index.asp?id=69"  title="Navigate to: Welcome to Care and Comfort">Home</a>&nbsp;&nbsp;<a href="/aboutus/index.asp?id=64"  title="Navigate to: More about us">About us</a>&nbsp;&nbsp;<a href="/contactus/index.asp?id=65"  title="Navigate to: Contact the staff at Care and Comfort">Contact us</a>&nbsp;&nbsp;<a href="/howtoshop/index.asp?id=66"  title="Navigate to: How to shop">How to Shop</a>&nbsp;&nbsp;<a href="/termsandconditions/index.asp?id=70"  title="Navigate to: Terms and conditions">Terms and conditions</a>&nbsp;&nbsp;<a href="/privacypolicy/index.asp?id=90"  title="Navigate to: Privacy Policy">Privacy Policy</a>&nbsp;&nbsp;<br />

				<br/>
				
				&copy; Care and Comfort&nbsp;2007&nbsp;All rights reserved.&nbsp;&nbsp;<a href="http://www.nvisage.co.uk/" title="Website development by NVisage">Website development</a> by NVisage</p>


			</div>
			<div class="clear"></div>

 
	


</div>
</div>
</body>
</html>  
