<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


	<xsl:template match="/">
		
		<table>

		<xsl:for-each select="VueSites/VueSite">
			
			<xsl:if test="(position() mod 2 = 1)">
				<xsl:text disable-output-escaping="yes">&lt;tr&gt;</xsl:text>
			</xsl:if>

			<td>					
				<xsl:element name="A">
				  <xsl:attribute name="HREF">/entertainment/cinema/listing.asp?id=37&amp;cid=<xsl:value-of select="@id"/></xsl:attribute>
				   <xsl:attribute name="TITLE">20/03/2007View film listings for <xsl:value-of select="@name"/>
				  </xsl:attribute>
				  <xsl:value-of select="@name"/>
				</xsl:element>					
			
			</td>

			<xsl:if test="(position() mod 2 = 0)">
				<xsl:text disable-output-escaping="yes">&lt;/tr&gt;</xsl:text>
			</xsl:if>
	

		</xsl:for-each>
		
		</table>

	</xsl:template>

</xsl:stylesheet>