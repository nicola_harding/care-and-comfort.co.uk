<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  
  <xsl:key name="uniqueDates" match="FilmsAndTimes/FilmList/Film/ViewTimeList/ViewTime" use="@date"/>
  
	<xsl:template match="/">
		
		<div id="film_listing">

		<xsl:for-each select="FilmsAndTimes/FilmList/Film">
			
			<div class="film">
				
				<div class="title">
					<span class="text"><xsl:value-of select="@name"/></span>

					<span class="classification">(<xsl:value-of select="@classification"/>)</span>
				</div>


				<xsl:for-each select="ViewTimeList/ViewTime">
          <xsl:variable name="nodesOnSameDate" select="key('uniqueDates', @date)"/>
          
					  <xsl:value-of select="@date"/>&#160;<xsl:value-of select="@time"/><br/>
          <!--xsl:for-each select="$nodesOnSameDate">
            <xsl:sort select="@time"/>
          </xsl:for-each-->
          
				</xsl:for-each>
			
			</div>
		</xsl:for-each>
		
		</div>

	</xsl:template>

</xsl:stylesheet>