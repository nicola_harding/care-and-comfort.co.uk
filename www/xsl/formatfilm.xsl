<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


	<xsl:template match="root">
		<xsl:for-each select="Film"> 

<xsl:element name="films">		
<xsl:element name="film">

<xsl:attribute name="name"> <xsl:value-of select="@name"/></xsl:attribute>
<xsl:attribute name="classification"> <xsl:value-of select="@classification"/></xsl:attribute>	
<xsl:attribute name="filmID"> <xsl:value-of select="@filmID"/></xsl:attribute>
<xsl:attribute name="booklink"> <xsl:value-of select="@booklink"/></xsl:attribute>		
				  

		<xsl:for-each select="ViewTimeList/ViewTime">
			
                                      <xsl:sort select="@date"/> 

                                     
		<xsl:element name="feature">
<xsl:value-of select="@date"/>
			<xsl:element name="url">
<xsl:value-of select="@booklink"/>	
</xsl:element>	
			<xsl:element name="time">
<xsl:value-of select="@time"/>	
</xsl:element>
			<xsl:element name="date">
<xsl:value-of select="@date"/>	
</xsl:element>	
					</xsl:element>


		</xsl:for-each>
</xsl:element>
</xsl:element>
		</xsl:for-each>
		
	</xsl:template>

</xsl:stylesheet>