<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:key name="features-by-date" match="feature" use="date" />
<xsl:template match="films">
<xsl:for-each select="film">

<div class="film">

<div class="title">
					<span class="text"><xsl:value-of select="@name"/> </span>

					<span class="classification">( rated <xsl:value-of select="@classification"/> )</span>
				</div>

	<table class="showing">

	<xsl:for-each select="feature[count(. | key('features-by-date', date)[1]) = 1]">
	<xsl:sort select="date" />
	<tr>
		<td nowrap="nowrap" style="width:110px;" class="date">
			 <xsl:call-template name="fixDate">
			  <xsl:with-param name="InDate">
			    <xsl:value-of select="date" />
			  </xsl:with-param>
			</xsl:call-template>
	
		</td>

		<xsl:for-each select="key('features-by-date', date)">
		<xsl:sort select="time" />
		<td style="width:110px;" >
			<xsl:element name="a">
					  <xsl:attribute name="href"><xsl:value-of select="url"/></xsl:attribute>
					   <xsl:attribute name="TITLE">Make a booking for the <xsl:value-of select="time" /> showing </xsl:attribute>
					 <xsl:value-of select="time" />
					</xsl:element>
			
		</td>
		</xsl:for-each>
	</tr>
	</xsl:for-each>
	
	</table>
</div>
</xsl:for-each>
</xsl:template>

<xsl:template name="fixDate">
  <xsl:param name="InDate"/>
    <xsl:call-template name="date-to-text-short">
       <xsl:with-param name="InDate">
        <xsl:value-of select="concat('',substring($InDate,9,2),'/',substring($InDate,6,2),'/',substring($InDate,1,4))" />
       </xsl:with-param>
    </xsl:call-template>
 </xsl:template>

<xsl:template name="date-to-text-short">
  <xsl:param name="InDate"/>
  <xsl:value-of select="substring($InDate, 1, 2)"/>
  <xsl:choose>
   <xsl:when test="substring($InDate, 4, 2)='01'">
    <xsl:value-of select="$jan"/>
   </xsl:when>
   <xsl:when test="substring($InDate, 4, 2)='02'">
    <xsl:value-of select="$feb"/>
   </xsl:when>
   <xsl:when test="substring($InDate, 4, 2)='03'">
    <xsl:value-of select="$mar"/>
   </xsl:when>
   <xsl:when test="substring($InDate, 4, 2)='04'">
    <xsl:value-of select="$apr"/>
   </xsl:when>
   <xsl:when test="substring($InDate, 4, 2)='05'">
    <xsl:value-of select="$may"/>
   </xsl:when>
   <xsl:when test="substring($InDate, 4, 2)='06'">
    <xsl:value-of select="$jun"/>
   </xsl:when>
   <xsl:when test="substring($InDate, 4, 2)='07'">
    <xsl:value-of select="$jul"/>
   </xsl:when>
   <xsl:when test="substring($InDate, 4, 2)='08'">
    <xsl:value-of select="$aug"/>
   </xsl:when>
   <xsl:when test="substring($InDate, 4, 2)='09'">
    <xsl:value-of select="$sep"/>
   </xsl:when>
   <xsl:when test="substring($InDate, 4, 2)='10'">
    <xsl:value-of select="$oct"/>
   </xsl:when>
   <xsl:when test="substring($InDate, 4, 2)='11'">
    <xsl:value-of select="$nov"/>
   </xsl:when>
   <xsl:when test="substring($InDate, 4, 2)='12'">
    <xsl:value-of select="$dec"/>
   </xsl:when>
  </xsl:choose>
  <xsl:value-of select="substring($InDate, 7, 4)"/>
 </xsl:template>

  <xsl:variable name="jan" select="' January '"/>
 <xsl:variable name="feb" select="' February '"/>
 <xsl:variable name="mar" select="' March '"/>
 <xsl:variable name="apr" select="' April '"/>
 <xsl:variable name="may" select="' May '"/>
 <xsl:variable name="jun" select="' June '"/>
 <xsl:variable name="jul" select="' July '"/>
 <xsl:variable name="aug" select="' August '"/>
 <xsl:variable name="sep" select="' September '"/>
 <xsl:variable name="oct" select="' October '"/>
 <xsl:variable name="nov" select="' November '"/>
 <xsl:variable name="dec" select="' December '"/>

</xsl:stylesheet>