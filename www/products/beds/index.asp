<!--#include virtual="/includes/careandcomfortAPI.asp"-->
<!--#include virtual="/ecomm/setup.asp"-->
<%
'// cms content id setting for 1st content item that will hold the meta tags
Dim nCntId 

if len("" & request.querystring("id")) > 0 then
    nCntId = request.QueryString("id")
else
    nCntId = CMS_CONTENT_ID_HOME
end if
%>
<!--#include virtual="/cms/setup.asp"-->
<!--#include virtual="/includes/doctype.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <%=buildPageTitleAndMetas(request.querystring,sCurFldr,nCatID,nRngID,nGrpID,nColID,nPrdID)%>  
    <!--#include virtual="/includes/mainCSS.asp" -->
	  <!--#include virtual="/includes/javascript.asp" -->
</head>

<!--#include virtual="/includes/templateStart.asp" -->
<!--#include virtual="/includes/templateTopNav.asp" -->
<!--#include virtual="/includes/templateLeftNav.asp" -->


<!--#include virtual="/includes/templateBeginMainContent.asp" -->
<!-- Begin Content -->	

<%



sSQL = "SELECT  tblProduct.*, tblRange.rnID, tblRange.rnName FROM  " & _
		" tblProduct INNER JOIN tblProductToRange ON tblProduct.prID = tblProductToRange.keyFprID INNER JOIN " & _
		" tblRange ON tblProductToRange.keyFrnID = tblRange.rnID " & _
		" WHERE (tblProduct.keyFinID = "  & INST_ID & ") AND (tblProduct.prID = " & nPrdID & ") AND (tblProduct.prOnSale = 1) AND (tblProduct.prDisplay = 1) " 	
	

nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aProduct)

If nErr = 0 Then

Dim aTempProductOption, sSelectedPrdId, sSelectedPrdPrice
Dim sAddProdInputs, dAddProdPrice
 
sSelectedPrdId = VbNullString
sSelectedPrdPrice = 0
'find out the price of the selected item

If Request.form("options") <> VBNullString Then

	aTempProductOption = Split(Request.form("options"),"_")

	If UBound(aTempProductOption) = 3 Then
		
		sSelectedPrdId = aTempProductOption(2)
		sSelectedPrdPrice = aTempProductOption(3)

	End If

End If

For Each objIn In Request.Form
	
	If Left(objIn,8) = "ADD_PROD" Then
		
		dAddProdPrice = dAddProdPrice + Cdbl(Request.Form(objIn))
		sAddProdInputs = sAddProdInputs & "<input type=""hidden"" name=""" & objIn & """ value=""" & Request.Form(objIn) & """/>" & vbcrlf
	End If


Next

%>

	<!--<img src="/ecomm/graphics/<%'=aProduct(PR_IMAGEMAIN,0)%>"  height="194" alt="<%'=aProduct(PR_IMAGEMAIN,0)%> range"/>-->


	<div id="breadcrumb">
		You are in: <a href="/categories/?<%=buildParms("",nCatID,aProduct(49,0),0,0,0)%>"><%=aProduct(50,0)%></a> - <%=aProduct(PR_NAME,0)%>
	</div>
	
	<h1><%=aProduct(PR_NAME,0)%></h1>
	
	<p class="intro"><%=aProduct(PR_FURTHERINFO,0)%></p>
	
	<p><%=aProduct(PR_MISC1,0)%></p>

	<div class="product_details">
					
					
			

			<div class="main_images">

				<img src="/ecomm/graphics/<%=aProduct(PR_IMAGELARGE,0)%>" alt="<%=aProduct(PR_NAME,0)%> main image"/>
			</div>

			
			<div class="product_buy">
				
				<p class="details">
				
					<strong><%=aProduct(PR_NAME,0)%></strong>

					<%=aProduct(PR_DESCSHORT,0)%>
					
					<%
					GetSingleProductLineArray aProduct,0,aProductSingleDimension 
					
					nPriceErr = getAppropriatePrice(aProductSingleDimension, nOutputPrice, nOutputVAT, nOriginalPrice, nOriginalVAT) 
					
					nPriceWithVat = CDbl(nOutputPrice) + CDbl(nOutputVAT)

					aProductSingleDimension = VBNull
					
					Dim dDisplayPrice 
					
					IF sSelectedPrdPrice <> 0 Then
						dDisplayPrice = sSelectedPrdPrice + dAddProdPrice
					Else
						nPriceWithVat = nOutputVAT
					End If
					%>

					RRP excl VAT <%=nOutputPrice%> RRP inc VAT <%=getPriceForexHTML(dDisplayPrice,2)%>

				</p>
				<form method="post" action="/shoppingbasket/buy.asp?<%=buildParms("",nCatID,nRngID,0,0,aProduct(PR_ID,0))%>">
					<%=sAddProdInputs%>
					<input type="hidden" name="prID" value="<%
					
					If sSelectedPrdId <> VbNullString Then
						Response.Write sSelectedPrdId
					Else
						Response.Write aProduct(PR_ID,0)
					End If
					
					%>"/>

					<input type="hidden" name="qty" value="1"/>
					<input class="buy" type="image" name="buy" src="/images/buttons/btn_buy.gif"/>
				</form>
				<%
				sProductOptionsSql = "SELECT  tblProduct.* FROM tblProduct WHERE  (prIsChild = 1) AND (prParentID = " & nPrdID & ") AND (prOnSale = 1) AND (prDisplay = 1) AND (tblProduct.keyFinID = "  & INST_ID & ")"
				
				%>			
				<form method="post" action="/products/beds/?<%=buildParms("",nCatID,nRngID,0,0,aProduct(PR_ID,0))%>">
				<%
				nProductOptionsErr = getRowsBySQL(DSN_ECOM_DB, sProductOptionsSql, aProductOptions)

				If nProductOptionsErr = 0 Then

			

				%>
				<div class="description">Choose the size of your bed:</div>
					
					<select name="options">
						<option value="">--Please select--</option>
	
						<%
							for intJ = 0 to ubound(aProductOptions,2)
								
								GetSingleProductLineArray aProductOptions,intJ,aProductOptionSingleDimension 
					
								nPriceErr = getAppropriatePrice(aProductOptionSingleDimension, nOutputPrice, nOutputVAT, nOriginalPrice, nOriginalVAT) 
								
								nPriceWithVat = CDbl(nOutputPrice) + CDbl(nOutputVAT)

								aProductOptionSingleDimension = VBNull
							
								Response.Write "<option value=""PR_OPTION_" & aProductOptions(PR_ID,intJ) & "_" & nPriceWithVat & """"
								
								If Request.Form("options") = "PR_OPTION_" & aProductOptions(PR_ID,intJ) & "_" & nPriceWithVat  Then Response.Write " selected=""selected"""

								%>><%=aProductOptions(PR_DESCSHORT,intJ)%> - <%=getPriceForexHTML(nPriceWithVat,2)%></option>
							%>
							<%							
							Next
						%>
					</select>
				</div>
				<%
				End If
				
				 
				sHeadboardOptionsSql = "SELECT tblProduct_1.* FROM tblProductToProduct INNER JOIN tblProduct AS tblProduct_1 ON " & _
				"tblProductToProduct.keyFp2ID = tblProduct_1.prID WHERE (tblProduct_1.prOnSale = 1) AND (tblProduct_1.prDisplay = 1) " & _
				"AND (tblProduct_1.prPassParms = 1) AND (tblProductToProduct.keyFp1ID = "

				
				If sSelectedPrdId <> VbNullString Then
					sHeadboardOptionsSql = sHeadboardOptionsSql & sSelectedPrdId
				Else
					sHeadboardOptionsSql = sHeadboardOptionsSql & nPrdID
				End If
							

				sHeadboardOptionsSql = sHeadboardOptionsSql & ") AND (tblProduct_1.prPassParms = 1) AND (tblProduct_1.keyFinID = "  & INST_ID & ")"
								
				
				nHeadboardOptionsErr = getRowsBySQL(DSN_ECOM_DB, sHeadboardOptionsSql, aHeadboardOption)

				If nHeadboardOptionsErr = 0 Then
				%>
				
						
					<div class="related_products">
						
						<h2>Headboard?:</h2>

						<div class="product_list">
						<%
						for intJ = 0 to ubound(aHeadboardOption,2)
								
							GetSingleProductLineArray aHeadboardOption,intJ,aProductOptionSingleDimension 
				
							nPriceErr = getAppropriatePrice(aProductOptionSingleDimension, nOutputPrice, nOutputVAT, nOriginalPrice, nOriginalVAT) 
							
							nPriceWithVat = CDbl(nOutputPrice) + CDbl(nOutputVAT)

							aProductOptionSingleDimension = VBNull
							

							Response.Write "<input type=""checkbox"" name=""ADD_PROD_" & aHeadboardOption(PR_ID,intJ) & """ value=""" & FormatNumber(nPriceWithVat,2) & """"
							
							If Request.Form("ADD_PROD_" & aHeadboardOption(PR_ID,intJ)) <> vbNullString Then Response.Write " checked=""checked"""
							
							%>/>&nbsp;<u><%=aHeadboardOption(PR_NAME,intJ)%></u>&nbsp;<%=getPriceForexHTML(nPriceWithVat,2)%><br>
							<%
								
													
						Next
						%>
							</fieldset>
							
							

							
						</div>
				</div>
				<%
				End If

				sAdditionalProductsSql = "SELECT tblProduct.* FROM tblProductToCollection INNER JOIN " & _
					"tblProduct ON tblProductToCollection.keyFprID = tblProduct.prID WHERE     (tblProduct.prOnSale = 1) AND (tblProduct.prDisplay = 1) " & _
					"AND (tblProductToCollection.keyFclID = " & BED_COLLECTION_ID & ") AND (tblProduct.keyFinID = " & INST_ID & ") ORDER BY tblProductToCollection.p2cDisplay;"
					
												
				nAdditionalProductsErr = getRowsBySQL(DSN_ECOM_DB, sAdditionalProductsSql, aAdditionalProducts)

				If nAdditionalProductsErr = 0 Then
				%>
					<div class="related_products">
						
						<h2>Additional choices</h2>

						<div class="product_list">
							
							<fieldset>
							<%
							for intJ = 0 to ubound(aAdditionalProducts,2)
								
								GetSingleProductLineArray aAdditionalProducts,intJ,aProductOptionSingleDimension 
					
								nPriceErr = getAppropriatePrice(aProductOptionSingleDimension, nOutputPrice, nOutputVAT, nOriginalPrice, nOriginalVAT) 
								
								nPriceWithVat = CDbl(nOutputPrice) + CDbl(nOutputVAT)

								aProductOptionSingleDimension = VBNull
								
								Response.Write "<input type=""checkbox"" name=""ADD_PROD_" & aAdditionalProducts(PR_ID,intJ) & """ value=""" & FormatNumber(nPriceWithVat,2) & """" 
							
								If Request.Form("ADD_PROD_" & aAdditionalProducts(PR_ID,intJ)) <> vbNullString Then Response.Write " checked=""checked"""
							
								%>/>&nbsp;<u><%=aAdditionalProducts(PR_NAME,intJ)%></u>&nbsp;<%=getPriceForexHTML(nPriceWithVat,2)%><br>
	
							<%							
							Next
							%>
							</fieldset>
							
							<input class="calculate" name="calculate" src="/images/buttons/btn_calculate.gif" type="image">

							
						</div>
					</div>

					</form>

				<%
				End If
				%>
			
			

					


<%
'once more can assume that sibling products have already been found by 
'left hand nav.

If nChildProductsNavErr = 0 Then

%>

	<div class="additional_choices">
	<%									
	for intJ = 0 to ubound(aChildProductsNav,2)

		If Cint(nPrdID) <> CInt(aChildProductsNav(PR_ID,intJ)) Then
		%>
		<div class="a_choice">
			
			<img src="/ecomm/graphics/<%=aChildProductsNav(PR_IMAGEMAIN,intJ)%>" alt="<%=aChildProductsNav(PR_NAME,intJ)%> main image"/>
			

			<a title="Read more about <%=aChildProductsNav(PR_NAME,intJ)%>" href="/products/?<%=buildParms("",nCatID,0,0,0,aChildProductsNav(PR_ID,intJ))%>"><%=aChildProductsNav(PR_NAME,intJ)%></a>
		
		</div>
		<%
		End If
	Next
	%>
	</div>
	<%

End IF

%>
<div class="clear">
						</div>
	</div>

<%

End IF
%>


<!-- End Content -->
<!--#include virtual="/includes/templateEndMainContent.asp" -->


<!--#include virtual="/includes/templateRightColumn.asp" -->


<!--#include virtual="/includes/templateFooter.asp" -->
<!--#include virtual="/includes/templateEnd.asp" -->