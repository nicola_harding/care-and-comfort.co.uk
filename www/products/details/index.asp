<!--#include virtual="/includes/careandcomfortAPI.asp"-->
<!--#include virtual="/ecomm/setup.asp"-->
<%
'// cms content id setting for 1st content item that will hold the meta tags
Dim nCntId 

if len("" & request.querystring("id")) > 0 then
    nCntId = request.QueryString("id")
else
    nCntId = CMS_CONTENT_ID_HOME
end if
%>
<!--#include virtual="/cms/setup.asp"-->
<!--#include virtual="/includes/doctype.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <%=buildPageTitleAndMetas(request.querystring,sCurFldr,nCatID,nRngID,nGrpID,nColID,nPrdID)%>  
    <!--#include virtual="/includes/mainCSS.asp" -->
	  <!--#include virtual="/includes/javascript.asp" -->
</head>

<!--#include virtual="/includes/templateStart.asp" -->
<!--#include virtual="/includes/templateTopNav.asp" -->
<!--#include virtual="/includes/templateLeftNav.asp" -->


<!--#include virtual="/includes/templateBeginMainContent.asp" -->
<!-- Begin Content -->	

<%


sSQL = "SELECT  tblProduct.*, tblRange.rnID, tblRange.rnName FROM  " & _
		" tblProduct INNER JOIN tblProductToRange ON tblProduct.prID = tblProductToRange.keyFprID INNER JOIN " & _
		" tblRange ON tblProductToRange.keyFrnID = tblRange.rnID " & _
		" WHERE (tblProduct.keyFinID = "  & INST_ID & ") AND (tblProduct.prID = " & nPrdID & ") AND (tblProduct.prOnSale = 1) AND (tblProduct.prDisplay = 1) ORDER BY prCode,prName;" 	
		



nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aProduct)


If nErr = 0 Then

	sProductName = Trim(Replace(aProduct(PR_NAME,0),"&trade;",""))
%>

	
	<div id="breadcrumb">
		You are in: <a href="/categories/?<%=buildParms("",nCatID,0,0,0,0)%>">Mattresses</a> <!--- <a href="/categories/?<%'=buildParms("",nCatID,aProduct(49,0),0,0,0)%>"><%'=aProduct(50,0)%></a>--> - <span class="coloured"><%=aProduct(PR_NAME,0)%></span>
	</div>
	

	<h1><%=aProduct(PR_NAME,0)%></h1>
	
	<p class="intro"><%=aProduct(PR_DESCSHORT,0)%></p>
	
	
	<div class="product_features_benefits">
		<img src="/ecomm/graphics/<%=aProduct(PR_IMAGELARGE,0)%>" alt="Image of <%=aProduct(PR_NAME,0)%>"/>
		
		

		
			<h2>Features</h2>
			
			<p><%=aProduct(PR_DESCLONG,0)%></p>

			<h3>Benefits</h3>

			<p><%=aProduct(PR_FURTHERINFO,0)%></p>

		
	</div>
	
	<h2 class="grid_header">Choose from our <%=aProduct(PR_NAME,0)%> range</h2> 
	

<%
' can assume that an array of sub categories has already been populated
'if on this page already

	sChildProductSql = "SELECT  tblProduct.* FROM tblProduct WHERE  (prIsChild = 1) AND (prParentID = " & nPrdID & ")  AND (prDisplay = 1) AND (tblProduct.keyFinID = "  & INST_ID & ") ORDER BY prCode,prName;"
	
		
	nChildProductsErr = getRowsBySQL(DSN_ECOM_DB, sChildProductSql, aChildProducts)

If nChildProductsErr = 0 Then
	
	%>
	<table id="price_grid">
	<tr>
		<th>Description</th>
		<th>Width</th>
		<th>Item No.</th>
		<th>RRP</th>
		<!--<th>RRP<br/>ex VAT</th>-->

		<th></th>
	</tr>
	
	<%

	Dim aProductSingleDimension(48)      
	
	
	for intJ = 0 to ubound(aChildProducts,2)

	 ' ++++++++++++++++++++++++++++++++++++++++++++++++
        ' Put all the fields of the current array row into an 1 dimensional array array for the bespoke code to use
        for nFieldIdx = 0 to ubound(aChildProducts, 1)
           ' Need this to stop population of aProduct when all the product db fields are retrieved from the aCartItems datasat
           if nFieldIdx <= ubound(aProductSingleDimension) then
                aProductSingleDimension(nFieldIdx) = aChildProducts(nFieldIdx,intJ)
           end if
        next                        
        ' Retrieve price for display and store it in a hidden field for the product_final.asp step to use
        nPriceErr = getAppropriatePrice(aProductSingleDimension, nOutputPrice, nOutputVAT, nOriginalPrice, nOriginalVAT) 
		
		

	  If nPriceErr = 0 Then
		
		nPriceWithVat = CDbl(nOutputPrice) + CDbl(nOutputVAT)
	  
	   
        ' ++++++++++++++++++++++++++++++++++++++++++++++++         
		%>
			<tr>
				<td><%=aChildProducts(PR_NAME,intJ)%></td>
				<td><%=aChildProducts(PR_MISC1,intJ)%></td>
				<td><%=aChildProducts(PR_CODE,intJ)%></td>
				
				<td><%
				If CDbl(nOutputPrice) <  CDbl(nOriginalPrice) Then
						Response.Write "<span class=""strike"">" & getPriceForexHTML(nOriginalPrice,2) & "</span><br/>"		
				End IF				
	
				Response.Write getPriceForexHTML(nOutputPrice,2)%></td>
				<td>
					<form method="post" action="/shoppingbasket/buy.asp?<%=buildParms("",nCatID,nRngID,0,0,aChildProducts(PR_ID,intJ))%>">
						
						<input type="hidden" name="prID" value="<%=aChildProducts(PR_ID,intJ)%>"/>
						<input type="hidden" name="qty" value="1"/>

						<input type="image" name="buy" src="/images/buttons/btn_buy.gif"/>
					</form>
				</td>
			</tr>

		<%
		End If
	Next
	
	If NVISAGE_IP = REMOTE_HOST Then
	%>
	<tr>
				<td>Test product</td>
				<td>Desc</td>
				<td>4596</td>
				
				<td>2</td>
				<td>
					<form method="post" action="/shoppingbasket/buy.asp?4596 ,0,0,0,12">
						
						<input type="hidden" name="prID" value="4596"/>
						<input type="hidden" name="qty" value="1"/>

						<input type="image" name="buy" src="/images/buttons/btn_buy.gif"/>
					</form>
				</td>
			</tr>

	</tr>
	<%
	End If
	
	 aProductSingleDimension(48) = null 

	%>
	</table>
	<%

End If

End If

%>

<div class="bottom_grid">
	<!--#include virtual="/includes/contentfooter.asp" -->

</div>
<!-- End Content -->
<!--#include virtual="/includes/templateEndMainContent.asp" -->


<!--#include virtual="/includes/templateRelatedProducts.asp" -->


<!--#include virtual="/includes/templateFooter.asp" -->
<!--#include virtual="/includes/templateEnd.asp" -->