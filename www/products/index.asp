<!--#include virtual="/includes/careandcomfortAPI.asp"-->
<!--#include virtual="/ecomm/setup.asp"-->
<%
'// cms content id setting for 1st content item that will hold the meta tags
Dim nCntId 

if len("" & request.querystring("id")) > 0 then
    nCntId = request.QueryString("id")
else
    nCntId = CMS_CONTENT_ID_HOME
end if
%>
<!--#include virtual="/cms/setup.asp"-->
<!--#include virtual="/includes/doctype.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <%=buildPageTitleAndMetas(request.querystring,sCurFldr,nCatID,nRngID,nGrpID,nColID,nPrdID)%> 
    <!--#include virtual="/includes/mainCSS.asp" -->
	  <!--#include virtual="/includes/javascript.asp" -->
</head>

<!--#include virtual="/includes/templateStart.asp" -->
<!--#include virtual="/includes/templateTopNav.asp" -->
<!--#include virtual="/includes/templateLeftNav.asp" -->


<!--#include virtual="/includes/templateBeginMainContent.asp" -->
<!-- Begin Content -->	

<%

sSQL = "SELECT  tblProduct.*, tblRange.rnID, tblRange.rnName FROM  " & _
		" tblProduct INNER JOIN tblProductToRange ON tblProduct.prID = tblProductToRange.keyFprID INNER JOIN " & _
		" tblRange ON tblProductToRange.keyFrnID = tblRange.rnID " & _
		" WHERE (tblProduct.keyFinID = "  & INST_ID & ") AND (tblProduct.prID = " & nPrdID & ") AND (tblProduct.prOnSale = 1) AND (tblProduct.prDisplay = 1) " 	
	

nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aProduct)

If nErr = 0 Then


	
%>

	<!--<img src="/ecomm/graphics/<%'=aProduct(PR_IMAGEMAIN,0)%>"  height="194" alt="<%'=aProduct(PR_IMAGEMAIN,0)%> range"/>-->


	<div id="breadcrumb">
		You are in: <a href="/categories/?<%=buildParms("",nCatID,aProduct(49,0),0,0,0)%>"><%=aProduct(50,0)%></a> - <%=aProduct(PR_NAME,0)%>
	</div>
	
	<h1><%=aProduct(PR_NAME,0)%></h1>
	
	<p class="intro"><%=aProduct(PR_FURTHERINFO,0)%></p>
	
	<p><%=aProduct(PR_MISC1,0)%></p>

	<div class="product_details">
					
		<form method="post" action="/shoppingbasket/buy.asp?<%=buildParms("",nCatID,nRngID,0,0,aProduct(PR_ID,0))%>">
			
			<input type="hidden" name="prID" value="<%=aProduct(PR_ID,0)%>"/>
			<input type="hidden" name="qty" value="1"/>

			<div class="main_images">

				<img src="/ecomm/graphics/<%=aProduct(PR_IMAGELARGE,0)%>" alt="<%=aProduct(PR_NAME,0)%> main image"/>
			</div>

			
			<div class="product_buy">
				
				<p class="details">
				
					<strong><%=aProduct(PR_NAME,0)%></strong>

					<%=aProduct(PR_DESCSHORT,0)%>
					
					<%
					GetSingleProductLineArray aProduct,0,aProductSingleDimension 
					
					nPriceErr = getAppropriatePrice(aProductSingleDimension, nOutputPrice, nOutputVAT, nOriginalPrice, nOriginalVAT) 
					
					nPriceWithVat = CDbl(nOutputPrice) + CDbl(nOutputVAT)

					aProductSingleDimension = VBNull
					%>

					RRP excl VAT <%=getPriceForexHTML(aProduct(PR_PRICE,0),2)%> RRP inc VAT <%=getPriceForexHTML(aProduct(PR_VAT,0),2)%>

				</p>
				<input class="buy" type="image" name="buy" src="/images/buttons/btn_buy.gif"/>
				<%
				sProductOptionsSql = "SELECT  tblProduct.* FROM tblProduct WHERE  (prIsChild = 1) AND (prParentID = " & nPrdID & ") AND (prOnSale = 1) AND (prDisplay = 1) AND (tblProduct.keyFinID = "  & INST_ID & ")"
								
				nProductOptionsErr = getRowsBySQL(DSN_ECOM_DB, sProductOptionsSql, aProductOptions)

				If nProductOptionsErr = 0 Then
				%>
				<div class="description">Further options:</div>
					
					<select name="options">
						<option value="">--Please select--</option>
	
						<%
							for intJ = 0 to ubound(aProductOptions,2)
								
								GetSingleProductLineArray aProductOptions,intJ,aProductOptionSingleDimension 
					
								nPriceErr = getAppropriatePrice(aProductOptionSingleDimension, nOutputPrice, nOutputVAT, nOriginalPrice, nOriginalVAT) 
								
								nPriceWithVat = CDbl(nOutputPrice) + CDbl(nOutputVAT)

								aProductOptionSingleDimension = VBNull
							%>
								<option value="<%=aProductOptions(PR_ID,intJ)%>"><%=aProductOptions(PR_DESCSHORT,intJ)%> - <%=getPriceForexHTML(nPriceWithVat,2)%></option>
							<%							
							Next
						%>
					</select>
				<%
				End If
				%>
			</div>
		</form>

					


<%
'once more can assume that sibling products have already been found by 
'left hand nav.

If nChildProductsNavErr = 0 Then

%>

	<div class="additional_choices">
	<%									
	for intJ = 0 to ubound(aChildProductsNav,2)

		If Cint(nPrdID) <> CInt(aChildProductsNav(PR_ID,intJ)) Then
		%>
		<div class="a_choice">
			
			<img src="/ecomm/graphics/<%=aChildProductsNav(PR_IMAGEMAIN,intJ)%>" alt="<%=aChildProductsNav(PR_NAME,intJ)%> main image"/>
			

			<a title="Read more about <%=aChildProductsNav(PR_NAME,intJ)%>" href="/products/?<%=buildParms("",nCatID,0,0,0,aChildProductsNav(PR_ID,intJ))%>"><%=aChildProductsNav(PR_NAME,intJ)%></a>
		
		</div>
		<%
		End If
	Next
	%>
	</div>
	<%

End IF

%>
<div class="clear">
						</div>
	</div>

<%

End IF
%>


<!-- End Content -->
<!--#include virtual="/includes/templateEndMainContent.asp" -->


<!--#include virtual="/includes/templateRightColumn.asp" -->


<!--#include virtual="/includes/templateFooter.asp" -->
<!--#include virtual="/includes/templateEnd.asp" -->