<!--#include virtual="/includes/careandcomfortAPI.asp"-->
<!--#include virtual="/ecomm/setup.asp"-->
<%
'// cms content id setting for 1st content item that will hold the meta tags
Dim nCntId 

if len("" & request.querystring("id")) > 0 then
    nCntId = request.QueryString("id")
else
    nCntId = CMS_CONTENT_ID_HOME
end if
%>
<!--#include virtual="/cms/setup.asp"-->

<%
Dim sDisplayChildrenThumbnails, nProductParentId, sRelatedList
Dim sNote                       : sNote = ""
Dim sDisplayChildItemsOnSale    : sDisplayChildItemsOnSale = ""
Dim sDisplayStandAloneItem      : sDisplayStandAloneItem = ""
Dim nRelatedProductId           : nRelatedProductId = 0
Dim blnIsAnyItemFoundOnSale     : blnIsAnyItemFoundOnSale = false
Dim sBreadCrumbItems, sPostageMsg, nMeID
Dim bVAT                        : bVAT = true
Dim nPostage                    : nPostage = 0
Dim blnPostZoneCovered          : blnPostZoneCovered = false
Dim aMember                     : aMember = null

sBreadCrumbItems = "Products�/products/index.asp?0,0,0,0,3" 

' // Picks up querystring and sets values into nCatID --> nPrdID
call parseParms(request.querystring,"",nCatID,nRngID,nGrpID,nColID,nPrdID)

nMeID = CLng("0" & session("meID"))
if (nMeID > 0) then
    nErr = loadMemberBespoke(nMeID, aMember)
    if nErr > 0 then
        response.redirect "./error.asp"
    else
        '// now lets calculate postage etc //
        if aMember(ME_USEALTADDRESS, 0) then
            nPostZone           = mid(aMember(ME_ALTADDRESS5, 0), 4, 1)
            blnPostZoneCovered  = aMember(ME_UBOUND+2,0)
            bVAT                = (mid(aMember(ME_ALTADDRESS5, 0), 6, 1) = "1")
        else
            nPostZone           = mid(aMember(ME_ADDRESS5, 0), 4, 1)
            blnPostZoneCovered  = aMember(ME_UBOUND+1,0)
            bVAT                = (mid(aMember(ME_ADDRESS5, 0), 6, 1) = "1")
        end if
    end if
    
end if

' // Load product - method is from ecommscripts virtual directory
nErr = loadProduct(nPrdID,aProduct)
if nErr > 0 or isNull(aProduct) then
    response.redirect("?" & buildParms("",nCatID,nRngID,nGrpID,nColID,nPrdID))
else
    ' // just been asked to display //
    if nRngID > 0 then  
        dim aRange  
        nErr = LoadProductRange(nPrdID, aRange)
        if nErr = 0 and (not isNull(aRange)) then
            ' // Build the breadcrumb trail, separator is ; (this is configurable when you call the breadcrumb display function located /rootscripts/common.asp)
            ' // Nix has chosen not too make the range name hyperlinked as they will already be on the product page
            sBreadCrumbItems = sBreadCrumbItems & ";" & aRange(RN_NAME,0) & "�/products/index.asp?" & buildParms("",nCatID,aRange(RN_ID,0),0,0,0) 
        end if   		
    end if   
	 
    ' // Build the breadcrumb trail, separator is ; (this is configurable when you call the breadcrumb display function located /rootscripts/common.asp)
    ' // Nix has chosen not too make the product name hyperlinked as they will already be on the product page
    sBreadCrumbItems = sBreadCrumbItems & ";" & aProduct(PR_NAME,0) & "�" 

    nProductParentId = aProduct(PR_PARENTID,0)
    if (not aProduct(PR_ISCHILD,0)) and (not aProduct(PR_ISPARENT,0)) then
        ' // No children products, this is a standalone product
        nErr = LoadStandAloneProduct(nPrdID,sDisplayStandAloneItem, blnIsAnyItemFoundOnSale)
        if nErr > 0 then
            sDisplayStandAloneItem = "<p>Sorry, no details could be retrieved for this product.</p>"
        else
            if len("" & aProduct(PR_SIZE,0)) > 0 then
                sNote = sNote & " - " & aProduct(PR_SIZE,0)
            end if
            if len("" & aProduct(PR_COLOUR,0)) > 0 then
                sNote = sNote & " - " & aProduct(PR_COLOUR,0)
            end if
        end if   
    else
        if (aProduct(PR_ISPARENT,0)) then	
            call LoadChildrenProducts(nProductParentId,nPrdID,aMember,sDisplayChildrenThumbnails,sDisplayChildItemsOnSale, blnIsAnyItemFoundOnSale)
            ' Just freeing memory asap here
            aMember = null
            if nErr > 0 then
                sDisplayChildItemsOnSale = "<p>Sorry, no sale items could be retrieved for this product.</p>"
            else
                if sDisplayChildItemsOnSale = "" then
                    sDisplayChildItemsOnSale = "<p>Sorry, no sale items could be retrieved for this product.</p>"
                end if
            end if  
        end if  
     end if       
    
    ' // Retrieve related products         
    nErr = getRelatedProducts(nPrdID, aRprds)    
    if nErr = 0 then
         if (not isNull(aRprds)) and isarray(aRprds) then
        
            ' // DISPLAY_PRODUCTS_CUSTOM                    = "related of home page focus" products display
            ' // DISPLAY_PRODUCTS_CUSTOM_HORIZONTAL_AMOUNT  = amount to list horizontally
            sRelatedList = writeList_generic(DISPLAY_PRODUCTS_CUSTOM, _
                                             aRprds, _
                                             DISPLAY_PRODUCTS_CUSTOM_HORIZONTAL_AMOUNT, _
                                             "Related items") 
        end if
   else
        sRelatedList = "<br />"
   end if 
        
end if
%>
<!--#include virtual="/includes/doctype.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <%=getCMSMetaData(nCntId,"","","")%>  
    <!--#include virtual="/includes/mainCSS.asp" -->
	  <!--#include virtual="/includes/javascript.asp" -->
</head>

<!--#include virtual="/includes/templateStart.asp" -->
<!--#include virtual="/includes/templateTopNav.asp" -->
<!--#include virtual="/includes/templateLeftNav.asp" -->


<!--#include virtual="/includes/templateBeginMainContent.asp" -->
<!-- Begin Content -->	
    
   
     <p class="breadcrumb">
        <%
       ' response.Write displayBreadCrumb(sBreadCrumbItems, _
                                     '    BREADCRUMB_STRING_VARIABLE_SITE_AREA_SEPARATOR, _
                                     '    BREADCRUMB_STRING_VARIABLE_HYPERLINK_SEPARATOR, _
                                     '    BREADCRUMB_DISPLAY_SITE_AREA_SEPARATOR, _
                                     '    BREADCRUMB_DISPLAY_INTRODUCTION_TEXT, _
                                     '   BREADCRUMB_DISPLAY_HYPERLINK_TITLE_TEXT)
        %>
    </p>
    	
	<!-- Start showing the products full details --> 
    <h1><%=aProduct(PR_NAME,0)%></h1> 
    
    <!-- next div has class of product_img_row --> 
    <!-- row contains 1 large image and a series of thumbs --> 
    <!-- large img must be 225 x 225 - borders added by css --> 
    <div class="product_img_row">
        <%if len("" & aProduct(PR_IMAGELARGE,0)) > 0 then%>
            <a href="#" title="View product's main image" onclick="switch_prod_image('/ecomm/graphics/<%=aProduct(PR_IMAGELARGE,0)%>');">    
	        <img src="/ecomm/graphics/<%=aProduct(PR_IMAGELARGE,0)%>" alt="<%=aProduct(PR_NAME,0)%>" width="225" height="225" id="main_product_image" /></a>
	    <%
	    end if  
        %>
	    <div class="thumbs_panel">
            <%=sDisplayChildrenThumbnails%>
            <br / >
            <%if len("" & aProduct(PR_FILE1,0)) > 0 then%>
	            View <a href="/ecomm/files/<%=aProduct(PR_FILE1,0)%>" target="_blank" title="view more picture detail for: <%=aProduct(PR_NAME,0)%>">more images</a>
	        <%end if%>
        </div> 
    </div> 
    <%
    if len("" & aProduct(PR_DESCLONG,0)) > 0 then
        response.Write "<h2>" & para_text(aProduct(PR_DESCLONG,0)) & "</h2>" 
    end if
    if len("" & aProduct(PR_FURTHERINFO,0)) then
        response.Write "<p>" & para_text(aProduct(PR_FURTHERINFO,0)) & "</p>" 
    end if
    %>
        <form class="product_form" action="/shoppingbasket/buy.asp?<%=buildParms("",nCatID,nRngID,nGrpID,nColID,nPrdID)%>" name="product_purchase" method="post" > 
        <fieldset> 
        <%
        if (not aProduct(PR_ISCHILD,0)) and (not aProduct(PR_ISPARENT,0)) then
            // No children products, this is a standalone product
            response.Write sDisplayStandAloneItem
        else
            if (aProduct(PR_ISPARENT,0)) then	 
                response.Write sDisplayChildItemsOnSale
            end if  
        end if     
        %>
          <input type="hidden" name="sNote" id="sNote" value="<%=sNote%>" />
          <input type="hidden" name="prID" id="Hidden1" value="<%=nPrdID%>" />		  
		  <input type="hidden" name="qty" id="Hidden2" value="1" />            
        <%
        if aProduct(PR_ONSALE,0) = true then 
            if blnIsAnyItemFoundOnSale then     
            'if IsEnvironmentPreLive(sHostHeader) then
        %>
            <input type="image" class="submit_button" src="/ecomm/buttons/button_addbasket.gif" alt="add the item to my shopping basket" />
            <%
            'end if
            end if
            %>              
            </fieldset> 
        </form>
        <%
            if blnIsAnyItemFoundOnSale then
                ' Display delivery amount message
                if (nMeID > 0) then
                    nErr = calcCartPostage(null, nPostZone, nPostage, sPostageMsg)  
                else
                    nErr = calcCartPostage(null, null, null, sPostageMsg)  
                end if
                response.Write sPostageMsg
                
                ' // Get the lead time for this product if one exists
                nErr = loadLeadTime(aProduct(PR_KEYFLTID,0),aLeadTime)
	            if nErr = 0 and (not isNull(aLeadTime)) and isarray(aLeadTime) then
		            'response.write DFLT_LEADTIME_IS
		            response.write "<p>Delivery approx: <strong>" & aLeadTime(LT_TEXT,0) & "</strong></p>"
	            else
	                response.write DFLT_LEADTIME_IS
	            end if 
	        end if      
	    else
            response.write "<strong>Product unavailable.</strong></fieldset></form>"                                    
        end if
        %>

            
         <!-- spacer div breaks the float--> 
          <!--div class="spacer"--> 
            <!-- --> 
          <!--/div--> 
      
        <!-- related_products --> 
        <%
        response.write "<br />" & sRelatedList
        %> 
<!-- End Content -->
<!--#include virtual="/includes/templateEndMainContent.asp" -->


<!--#include virtual="/includes/templateRightColumn.asp" -->


<!--#include virtual="/includes/templateFooter.asp" -->
<!--#include virtual="/includes/templateEnd.asp" -->
