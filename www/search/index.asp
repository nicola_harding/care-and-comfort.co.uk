
<!--#include virtual="/includes/careandcomfortAPI.asp"-->
<!--#include virtual="/includes/search_functions.asp"-->
<!--#include virtual="/ecomm/setup.asp"-->
<!--#include virtual="/cms/setup.asp"-->

<%
dim nPrdPerPage : nPrdPerPage =12
nTotalPages    = 20
intRecordcount = 0 
nPageCur = CInt("0" & Request.QueryString("pg"))
sCurFldr = "Products"
call parseParms(request.querystring,sCurFldr,nCatID,nRngID,nGrpID,nColID,nPrdID)
if len(nPageCur) = 0 or  nPageCur  = 0 then
	nPageCur = nGrpID
end if

Dim id
Dim strSQLpr
sCurFldr = "Search"

if (request.QueryString("id")<> "") then
	id = request.QueryString("id")
else
	id=1
end if
Dim sQ, nPage, sTmp, nResults
sQ = "" & Trim(Request("searchbox"))
nPage = CInt("0" & Request.Querystring("pg"))
if len(trim(sQ)) = 0 then
 response.redirect("../")
end if 
%>

<!--#include virtual="/includes/doctype.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <%=getCMSMetaData(nCntId,"","","")%>  
    <!--#include virtual="/includes/mainCSS.asp" -->
	  <!--#include virtual="/includes/javascript.asp" -->
</head>

<!--#include virtual="/includes/templateStart.asp" -->
<!--#include virtual="/includes/templateTopNav.asp" -->
<!--#include virtual="/includes/templateLeftNav.asp" -->


<!--#include virtual="/includes/templateBeginMainContent.asp" -->
<!-- Begin Content -->	
			<div id="breadcrumb">
				You are in: <span class="coloured"><%=sCurFldr%></span>
			</div>
			
			
				
				       <h2>Catalogue search results for: <i><%=left(sQ,25)%></i></h2>

					 <!--  <hr />-->
					    <%
						'//ECOMMERCE PRODUCT CATALOG SEARCH
						if (len(sQ) > 0) then
							sTmp = getSearchResultsProds(sQ, 10, false, nPage, nResults)
							
							if len(sTmp) > 0 then

							
						
								response.write sTmp
							else
								'response.write "<p>Sorry, your search returned no results.</p>"
							end if
							'if nResults > 0 then
							'	response.write sTmp
						  'else
							'response.write "<p>Sorry, your search returned no results.</p>"
							'end if
						end if
						
						
						If nTotalPages > 0 Then
						%>
						
						
						<p class="pr_pagination">
								<div class="pr_pagination_L">
									<%if nPageCur > 1 then%>
									<a href="./?pg=1&searchbox=<%=sQ%>" >|&lt;</a>&nbsp;&nbsp;&nbsp;&nbsp;
									<%end if
									if nPageCur > 1  AND nPageCur - 2  > 0 then%>
									<a href="./?pg=<%=nPageCur - 2%>&searchbox=<%=sQ%>">&lt;&lt;</a>&nbsp;&nbsp;&nbsp;
									<%end if
									if nPageCur > 1 then%>
									<a href="./?pg=<%=nPageCur - 1%>&searchbox=<%=sQ%>">&lt;</a>
									<%end if%>&nbsp;
								</div>
								
								<div class="pr_pagination_M">
									Page <strong><%=cint(nPageCur)%></strong> of <%=cint(nTotalPages)%>&nbsp;&nbsp;&nbsp;
								</div>
								
								<div class="pr_pagination_R">
									<%if nPageCur <> nTotalPages  then%>
									<a href="./?pg=<%=nPageCur + 1%>&searchbox=<%=sQ%>">&gt;</a>&nbsp;&nbsp;&nbsp;
									<%end if
									if nPageCur <> nTotalPages AND nPageCur + 2 =< nTotalPages then%>
									<a href="./?pg=<%=nPageCur + 2%>&searchbox=<%=sQ%>">&gt;&gt;</a>&nbsp;&nbsp;&nbsp;&nbsp;
									<%end if
									if nPageCur <> nTotalPages then%>
									<a href="./?pg=<%=nTotalPages%>&searchbox=<%=sQ%>">&gt;|</a>
									<%end if%>&nbsp;
								</div>
						</p>
						<%
						End If
						%>
						<br />
						
						<h2 style="clear:both;padding-top:10px;">Website search results for: <i><%=left(sQ,25)%></i></h2>
						<%
						'//CMS 100 CONTENT SEARCH
						if (len(sQ) > 0) then
							sTmp = getSearchResultsCMS(sQ, 10, false, nPage, nResults)
							if nResults > 0 then
								response.write sTmp
						  else
							response.write "<p>Sorry, your website search returned no results.</p>"
							end if
						end if
						%>
					
		<!-- End Content -->
<!--#include virtual="/includes/templateEndMainContent.asp" -->


<!--#include virtual="/includes/templateRightColumn.asp" -->


<!--#include virtual="/includes/templateFooter.asp" -->
<!--#include virtual="/includes/templateEnd.asp" -->
