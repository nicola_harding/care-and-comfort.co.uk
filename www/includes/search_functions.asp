<%
function xgetSearchResultsProds(strQ, nMaxRes, bPaging, nPage, nResults)
	Dim sSQL, oRS, i, oCn, j, sTmp, pageCount, pageCur, sParms, sQ
	sQ = strQ
	sQ = replace(sQ, " ", "%")
	'strQ = replace(strQ, " ", "+")
	nResults = 0
	'sSQL = "SELECT  P.prID, P.prCode, P.prName, P.prDescShort, P.prImageThumb, P.prImageMain, P.prDisplay, P2R.keyFrnID, R2C.keyFcaID" _
	sSQL = "SELECT  P.*, P2R.keyFrnID, R2C.keyFcaID" _
		& " FROM (tblProduct AS P INNER JOIN tblProductToRange AS P2R ON P2R.keyFprID = P.prID) INNER JOIN tblRangeToCategory AS R2C ON P2R.keyFrnID = R2C.keyFrnID" _
		& " WHERE (P.prDisplay=True" _
		& " AND  P.prName LIKE '%" & SQLFixUp(sQ) & "%' OR P.prDescShort LIKE '%" & SQLFixUp(sQ) & "%' OR P.prDescLong LIKE '%" & SQLFixUp(sQ) & "%' OR P.prCode LIKE '%" & SQLFixUp(sQ) & "%') AND P.prDisplay=True" _
		& " ORDER BY P.prName ASC;"
		'response.write sSQL
	sTmp = ""
	Set oCn = Server.CreateObject("ADODB.Connection")
	oCn.open DSN_ECOM_DB
	pageCur = CInt("0" & nPage)
	Set oRS = Server.CreateObject("ADODB.Recordset")
	oRS.CursorLocation = adUseClient
	oRS.PageSize = nMaxRes
	oRS.Open sSQL, oCn, adOpenStatic, adLockReadOnly, adCmdText	
	pageCount = oRS.PageCount
	'// set the current page //
	If 1 > pageCur Then pageCur = 1
	If pageCur > pageCount Then pageCur = pageCount
	'// get the req'd page from the db //
	If pageCur > 0 Then
		oRS.AbsolutePage = pageCur
		If Not oRS.EOF and oRS.AbsolutePage = pageCur Then
			Do While oRS.AbsolutePage = pageCur AND Not oRS.EOF
				'0 PR_ID, 2PR_NAME, 3PR_DESCSHORT
				sTmp = sTmp & getProductSummary(strQ, oRS(PR_ID),"" & oRS(PR_NAME),"" & oRS(PR_DESCSHORT), "" & oRS(8), "" & oRS(7))
				oRS.MoveNext
				nResults = nResults + 1
			Loop
		End If
	End If
	oRS.Close
	Set oRS = Nothing
	oCn.Close
	Set oCn = Nothing
	Dim parm, parmToken
	parmToken = "?"
	for each parm in Request.Querystring
		if parm <> "pg" then
			sParms = sParms & parmToken & parm & "=" & Request.Querystring(parm)
			parmToken = "&"
		end if
	next
	sParms = sParms & parmToken
	if bPaging then
		sTmp = sTmp & "<table width=""100%""><tr><td colspan=""2"">&nbsp;</td></tr><tr>" _
			& "<td width=""50%"" align=""left"">"
		if pageCur > 1 Then
			sTmp = sTmp & "<a href=""" & sParms & "pg=" & pageCur - 1 & """>" _
				& "previous page</a>"
		else
			sTmp = sTmp & "&nbsp;"
		end if
		sTmp = sTmp & "</td><td width=""50%"" align=""right"">"
		if pageCur < pageCount then
			sTmp = sTmp & "<a href=""" & sParms & "pg=" & pageCur + 1 & """>" _
				& "next page</a>"
		else
			sTmp = sTmp & "&nbsp;"
		end if
	  sTmp = sTmp & "</td></tr></table>"
	else
		sTmp = sTmp & "<table width=""100%""><tr><td>&nbsp;</td></tr><tr>" _
			& "<td width=""50%"" align=""left"">"
		sTmp = sTmp & "<p class=""black_8pt""></p>"
	  sTmp = sTmp & "</td></tr></table>"
	end if
	xgetSearchResultsProds = sTmp
end function

function getSearchResultsProds(strQ, nMaxRes, bPaging, nPage, nResults)
	Dim sSQL, oRS, i, oCn, j, sTmp, pageCount, pageCur, sParms, sQ
	sQ = strQ
	sQ = replace(sQ, " ", "%")
	'strQ = replace(strQ, " ", "+")
	nResults = 0
	'sSQL = "SELECT  P.prID, P.prCode, P.prName, P.prDescShort, P.prImageThumb, P.prImageMain, P.prDisplay, P2R.keyFrnID, R2C.keyFcaID" _
	sSQL = "SELECT DISTINCT P.*, tblCategory.caID " _
		& " FROM   tblProduct AS P INNER JOIN tblProductToRange AS P2R ON P2R.keyFprID = P.prID INNER JOIN "_
                      &  "tblRangeToCategory AS R2C ON P2R.keyFrnID = R2C.keyFrnID INNER JOIN " _
                      & "tblRange AS RN ON P2R.keyFrnID = RN.rnID INNER JOIN "_
                      & "tblCategory AS CA ON R2C.keyFcaID = CA.caID INNER JOIN " _
                      & "tblRangeToCategory ON RN.rnID = tblRangeToCategory.keyFrnID INNER JOIN " _
                      & "tblCategory ON tblRangeToCategory.keyFcaID = tblCategory.caID " _
		& " WHERE (P.prDisplay=1" _
		& " AND  P.prName LIKE '%" & SQLFixUp(sQ) & "%' OR P.prDescShort LIKE '%" & SQLFixUp(sQ) & "%' OR P.prDescLong LIKE '%" & SQLFixUp(sQ) & "%' OR P.prCode LIKE '%" & SQLFixUp(sQ) & "%' OR RN.rnName LIKE '%" & SQLFixUp(sQ) & "%' OR CA.caName LIKE '%" & SQLFixUp(sQ) & "%') AND P.prDisplay=1" _
		& " ORDER BY P.prName ASC;"
	

'sSQL = "SELECT DISTINCT  PC.*, tblCategory.caID, P.prImageThumb FROM tblProduct AS P INNER JOIN " & _
'	"tblProductToRange AS P2R ON P2R.keyFprID = P.prID INNER JOIN " & _
'	"tblRangeToCategory AS R2C ON P2R.keyFrnID = R2C.keyFrnID INNER JOIN " & _
'	"tblRange AS RN ON P2R.keyFrnID = RN.rnID INNER JOIN " & _
'	"tblCategory AS CA ON R2C.keyFcaID = CA.caID INNER JOIN " & _
'	"tblRangeToCategory ON RN.rnID = tblRangeToCategory.keyFrnID INNER JOIN " & _
'	"tblCategory ON tblRangeToCategory.keyFcaID = tblCategory.caID INNER JOIN " & _
'	"tblProduct PC ON P.prID = PC.prParentID " & _
'	"WHERE    P.prIsParent = 1 AND (PC.prDisplay = 1) AND (P.prDisplay = 1) AND (P.prName LIKE '%" & SQLFixUp(sQ) & "%') AND (P.prDisplay = 1) OR " & _
'	"(P.prDisplay = 1) AND (P.prDescShort LIKE '%" & SQLFixUp(sQ) & "%') OR " & _
'	"(P.prDisplay = 1) AND (P.prDescLong LIKE '%" & SQLFixUp(sQ) & "%') OR " & _ 
'    "(P.prDisplay = 1) AND (P.prCode LIKE '%" & SQLFixUp(sQ) & "%') OR " & _
'    "(P.prDisplay = 1) AND (RN.rnName LIKE '%" & SQLFixUp(sQ) & "%') OR " & _
'    "(P.prDisplay = 1) AND (CA.caName LIKE '%" & SQLFixUp(sQ) & "%') " & _ 
'	"UNION SELECT DISTINCT  P.*, tblCategory.caID, P.prImageThumb FROM tblProduct AS P INNER JOIN " & _
'	"tblProductToRange AS P2R ON P2R.keyFprID = P.prID INNER JOIN " & _
'	 "tblRangeToCategory AS R2C ON P2R.keyFrnID = R2C.keyFrnID INNER JOIN " & _
'	 "tblRange AS RN ON P2R.keyFrnID = RN.rnID INNER JOIN " & _
'	 "tblCategory AS CA ON R2C.keyFcaID = CA.caID INNER JOIN " & _
'	 "tblRangeToCategory ON RN.rnID = tblRangeToCategory.keyFrnID INNER JOIN " & _
'	 "tblCategory ON tblRangeToCategory.keyFcaID = tblCategory.caID " & _
'	 "WHERE (P.prName = '%" & HTMLDecodeSQLFixup(sQ) & "%' OR P.prCode = '%" & HTMLDecodeSQLFixup(sQ) & "%') AND P.prDisplay=1 AND P.prIsParent = 0 ORDER BY prName ASC"
	
sSQL = "SELECT DISTINCT P.*, tblCategory.caID, P.prImageThumb " & _
		"FROM tblProduct AS P INNER JOIN " & _
		" tblProductToRange AS P2R ON P2R.keyFprID = P.prID INNER JOIN " & _
		" tblRangeToCategory AS R2C ON P2R.keyFrnID = R2C.keyFrnID INNER JOIN " & _
		" tblRange AS RN ON P2R.keyFrnID = RN.rnID INNER JOIN " & _
		"  tblCategory AS CA ON R2C.keyFcaID = CA.caID INNER JOIN " & _
		" tblRangeToCategory ON RN.rnID = tblRangeToCategory.keyFrnID INNER JOIN " & _
		" tblCategory ON tblRangeToCategory.keyFcaID = tblCategory.caID " & _
		" WHERE     (P.prName LIKE '%mattress%' OR  (P.prDescShort LIKE '%mattress%') OR " & _
		" (P.prDescLong LIKE '%" & HTMLDecodeSQLFixup(sQ) & "%') OR " & _
		" (P.prCode LIKE '%" & HTMLDecodeSQLFixup(sQ) & "%') OR " & _
         "             (RN.rnName LIKE '%" & HTMLDecodeSQLFixup(sQ) & "%') OR " & _
           "           (CA.caName LIKE '" & HTMLDecodeSQLFixup(sQ) & "%')) AND (P.prDisplay = 1) AND (P.prIsChild = 0) " & _
	"UNION SELECT DISTINCT  P.*, tblRangeToCategory.keyFcaID, PP.prImageThumb " & _
	" FROM tblProduct As P INNER JOIN " & _
	" tblProductToRange ON P.prParentID = tblProductToRange.keyFprID INNER JOIN " & _
	" tblRangeToCategory ON tblProductToRange.keyFrnID = tblRangeToCategory.keyFrnID " & _
	" INNER JOIN tblProduct PP ON P.prParentID = PP.prID " & _
	 "WHERE (P.prName = '" & HTMLDecodeSQLFixup(sQ) & "' OR P.prCode = '%" & HTMLDecodeSQLFixup(sQ) & "%') AND P.prDisplay=1 AND P.prIsParent = 0 ORDER BY prName ASC"
	

	

	
	'Response.Write sSQL
	

	nErr = write_boxes(sSQL,false,3, nPrdPerPage, nPageCur, nTotalPages)
	
	getSearchResultsProds = nErr 
end function


function getSearchResultsCMS(strQ, nMaxRes, bPaging, nPage, nResults)
	Dim sSQL, oRS, i, oCn, j, sTmp, pageCount, pageCur, sParms, sQ
	sQ = strQ
	sQ = replace(sQ, " ", "%")
	'strQ = replace(strQ, " ", "+")
	nResults = 0
	'sSQL = "SELECT C.content_id,C.content_title,C.content_html " _
		'& " FROM content AS C " _
		'& " WHERE (C.content_status='A' AND C.published=1" _
		'& " AND C.content_html LIKE '%" & SQLFixUp(sQ) & "%'" _
		'& " AND C.content_title NOT LIKE '%Ecomm%')" _
		'& " ORDER BY C.content_title ASC;" 
		'response.write sSQL
	
	sTmp = ""
	
	sSQL = "SELECT cntID, cntTitle, cntHTML, keyFinID FROM tblContent " & _
			"WHERE (cntStatus = 1) AND (keyFinID = " & INST_ID & ") AND (cntHTML LIKE '%" & SQLFixUp(sQ) & "%' OR cntHTML LIKE '" & SQLFixUp(sQ) & "%'  OR cntHTML LIKE '%" & SQLFixUp(sQ) & "' );"

	
	Set oCn = Server.CreateObject("ADODB.Connection")
	'oCn.open "DSN=walkandtravelCMS_Access;UID=;PWD=;"
	oCn.open DSN_CMS_DB
	pageCur = CInt("0" & nPage)
	Set oRS = Server.CreateObject("ADODB.Recordset")
	oRS.CursorLocation = adUseClient
	oRS.PageSize = nMaxRes
	oRS.Open sSQL, oCn, adOpenStatic, adLockReadOnly, adCmdText	
	pageCount = oRS.PageCount
	'// set the current page //
	If 1 > pageCur Then pageCur = 1
	If pageCur > pageCount Then pageCur = pageCount
	'// get the req'd page from the db //
	If pageCur > 0 Then
		oRS.AbsolutePage = pageCur
		If Not oRS.EOF and oRS.AbsolutePage = pageCur Then
		sTmp = ""
			Do While oRS.AbsolutePage = pageCur AND Not oRS.EOF
				sTmp = sTmp & getContentSummary(strQ, oRS(0),"" & oRS(1),"" & oRS(2),"" & oRS(1))
				oRS.MoveNext
				nResults = nResults + 1
			Loop
		End If
	End If
	oRS.Close
	Set oRS = Nothing
	oCn.Close
	Set oCn = Nothing
	Dim parm, parmToken
	parmToken = "?"
	for each parm in Request.Querystring
		if parm <> "pg" then
			sParms = sParms & parmToken & parm & "=" & Request.Querystring(parm)
			parmToken = "&"
		end if
	next
	sParms = sParms & parmToken
	if bPaging then
		sTmp = sTmp & "<table width=""100%""><tr><td colspan=""2"">&nbsp;</td></tr><tr>" _
			& "<td width=""50%"" align=""left"">"
		if pageCur > 1 Then
			sTmp = sTmp & "<a href=""" & sParms & "pg=" & pageCur - 1 & """>" _
				& "previous page</a>"
		else
			sTmp = sTmp & "&nbsp;"
		end if
		sTmp = sTmp & "</td><td width=""50%"" align=""right"">"
		if pageCur < pageCount then
			sTmp = sTmp & "<a href=""" & sParms & "pg=" & pageCur + 1 & """>" _
				& "next page</a>"
		else
			sTmp = sTmp & "&nbsp;"
		end if
	  sTmp = sTmp & "</td></tr></table>"
	else
		sTmp = sTmp & "<table width=""100%""><tr><td>&nbsp;</td></tr><tr>" _
			& "<td width=""50%"" align=""left"">"
		sTmp = sTmp & "<p class=""black_8pt""></p>"
	  sTmp = sTmp & "</td></tr></table>"
	end if
	getSearchResultsCMS = sTmp
end function

'------------------------------------------------------------------

function getProductSummary(sQ, nID, sTitle, sDesc, nCID, nRID)
   Dim sTmp, sHeader, sSummary
   sHeader = sTitle
   sSummary = sDesc
   if len(sSummary) > 150 then
   	sSummary = left(sSummary,150) & "..."
   end if
   sTmp = "<a class=""CMS_subheading"" href=""/products/detail.asp?" & buildParms("",0,0,nRID,0,nID) &  """>"  & sHeader & "</a>" & vbcrlf 
   sTmp = sTmp	& "<p class=""search_display"">" & sSummary & "</p>"  & vbcrlf 
   sTmp = sTmp	& "<p>&nbsp;</p>"  & vbcrlf		
   getProductSummary = sTmp
end function

function getContentSummary(sQ, nID, sTitle, sHTML, sTemplate)
	Dim sTmp, sHeader, sSummary
	sHeader = getHeading("" & sHTML)
	if sHeader = "" then
		sHeader = sTitle
	end if
   sSummary = getSummary("" & sHTML)
   sTmp = ""

   sTmp = sTmp & "<p><strong><a href=""search_display.asp?id=" & nID & "&amp;terms=" & sQ & """ class=""CMS_subheading"">"
 
   sTmp = sTmp & sHeader & "</a></strong></p>" & vbcrlf _
		& "<p class=""search_display"">" & sSummary & "</p>"  & vbcrlf _
		& ""  & vbcrlf		
	getContentSummary = sTmp
end function
function getHeading(sHtml)
	dim nStartPos, nEndPos, nCount
	nStartPos = instr(sHtml, "<p>") + 20
	nEndPos = instr(sHtml, "</p>")
	nCount = nEndPos - nStartPos
	getHeading = ""
	'getHeading = stripTags(mid(sHtml, nStartPos, nCount))
end function
function getSummary(sHTML)
	dim nStartPos, nEndPos, nCount

	'nStartPos = instr(sHtml, "</p>") + 4
	nEndPos = 5000
	nCount = nEndPos - 0
	getSummary = left(stripTags("" & sHtml), 150) & "..."
end function
Function stripTags(strIn)
    '// okay we need to replace every '<tag>' & '</tag>' with '' //
    '// might as well use regular expressions //
    '// Microsoft Visual Basic Regular Expressions 5.5 project reference //
    Dim MyReg
    Set MyReg = New RegExp
    MyReg.IgnoreCase = True
    MyReg.Global = True
    MyReg.Pattern = "<(.|\n)+?>"
    StripTags = MyReg.Replace("" & strIn, "")
End Function
Function SQLFixUp(sSQL)
sSQL = replace(sSQL,"'","")
sSQL = replace(sSQL,chr(10),"")
SQLFixUp = sSQL
End function
%>
