<%
'  UTILITY DATE FUNCTIONS INCLUDE
Function UKDate(date1)
Dim datemonth, dateday, dateyear
Dim datehour, dateminute, datesecond
datemonth = Month(date1)
dateday = Day(date1)
dateyear = Year(date1)
datehour = Hour(date1)
dateminute = Minute(date1)
dateSecond = Second(date1)
UKDate = dateday & "/" & datemonth & "/" & dateyear & " " & datehour & ":" & dateminute & ":" & dateSecond
End Function

Function USDate(date1)
Dim datemonth, dateday, dateyear
Dim datehour, dateminute, datesecond
datemonth = Month(date1)
dateday = Day(date1)
dateyear = Year(date1)
datehour = Hour(date1)
dateminute = Minute(date1)
dateSecond = Second(date1)
USDate = datemonth & "/" & dateday & "/" & dateyear & " " & datehour & ":" & dateminute & ":" & dateSecond
End Function

Function NoDate(hour,minute)
Dim Result
Result="01/01/2000"
if hour > 9 then
 Result=Result & " " & hour
else
 Result=Result & " 0" & hour
end if

if minute > 9 then
 Result=Result & ":" & minute
else
 Result=Result & ":0" & minute
end if
Nodate=Result & ":00"
End Function

Function DisplayDate(date1)
if NOT IsDate(date1) then date1=Date()
datToday = date1
intThisDay = Day(datToday)
strWeekDayName = WeekDayName(WeekDay(datToday), False, vbSunday)
intThisMonth = Month(datToday)
strThisMonth = MonthName(intThisMonth)
intThisYear = Year(datToday)
strThisYear = CStr(intThisYear)
suffix = "th"
Select Case intThisDay
  Case 1,21,31: Suffix = "st"
  Case 2,22: Suffix = "nd"
  Case 3,23: Suffix = "rd"
End Select
DisplayDate = strWeekDayName & "&nbsp;" & intThisDay & suffix _
               & "&nbsp;" & strThisMonth & "&nbsp;" & strThisYear
End Function

Function DaysToGo(fromdate,todate)
if NOT IsDate(fromdate) then fromdate=Date()
datFromdate = fromdate
if NOT IsDate(todate) then todate=Date()
datTodate = todate
DaysToGo = DateDiff("d",datFromDate,datToDate)
End Function
%>
