<!--#include virtual="/scripts/aspstart.asp"-->
<!--#include virtual="/scripts/adovbs.asp"-->


<!--#include virtual="/admin/rootscripts/setup.asp"-->

<%
'//Added in By Ryan 14/02/06

'// Partners
const PA_ID 	  	 = 0
const PA_USERNAME 	 = 1
const PA_PASSWORD 	 = 2
const PA_FIRSTNAME 	 = 3
const PA_LASTNAME 	 = 4
const PA_COMPANYNAME = 5
const PA_EMAIL 		 = 6
const PA_KEYFLVLID 	 = 7
const PA_LASTLOGIN 	 = 8
const PA_UBOUND		 = 8

'// partner levels
const PL_ID			 = 0
const PL_NAME		 = 1
const PL_DESC		 = 2
const PL_PERMS		 = 3
const PL_UBOUND		 = 3

'// document folders
const DF_ID			 = 0
const DF_PARENTID	 = 1
const DF_NAME		 = 2
const DF_DESC		 = 3
const DF_SHOW		 = 4
const DF_DISPLAYOD	 = 5
const DF_ISPARENT	 = 6
const DF_UBOUND		 = 6

'// Documents
const DC_ID			 = 0
const DC_NAME		 = 1
const DC_DESC		 = 2
const DC_FILE		 = 3
const DC_DATEADDED	 = 4
const DC_PUBLISHDATE = 5
const DC_ENDDATE	 = 6
const DC_SHOW		 = 7
const DC_DISPLAYOD	 = 8
const DC_KEYFFLDID	 = 9 ' FORIEGN KEY 4 FOLDER
const DC_UBOUND		 = 9

'// generic paging row fetcher //
function getRowsPageBySQL(sDSN, nMaxCount, nPageCur, nTotalPages, sSQL, aRows)

	dim nErr:nErr = 0
	dim oConn, oRS, bRows, i, j, sTmp, nPageCount
	bRows = false
	nTotalPages = 0
	if len("" & sSQL) > 0 then
		set oConn = server.createobject("ADODB.Connection")
		oConn.open sDSN
		Set oRS = Server.CreateObject("ADODB.Recordset")
		oRS.CursorLocation = adUseClient
		oRS.PageSize = nMaxCount
		oRS.Open sSQL, oConn, adOpenStatic, adLockReadOnly, adCmdText
		
		nPageCount = oRS.PageCount
		'// set the current page //
		If 1 > nPageCur Then nPageCur = 1
		If nPageCur > nPageCount Then nPageCur = nPageCount
		'// get the req'd page from the db //
		nTotalPages = nPageCount
		If nPageCur > 0 Then
			oRS.AbsolutePage = nPageCur
			i = 0
			If Not oRS.EOF and oRS.AbsolutePage = nPageCur Then
				nFieldCount = oRS.Fields.Count
				Do While oRS.AbsolutePage = nPageCur AND Not oRS.EOF
					if bRows = True Then
						Redim Preserve aRows(nFieldCount, i)
					Else
						Redim aRows(nFieldCount,0)
					End if
					for j = 0 to nFieldCount - 1
						aRows(j,i) = oRS(j)
					next
					bRows = true
					i = i + 1
					oRS.MoveNext
				Loop
			Else
				aRows = null
				nErr = 2	'// no matching rows //
			End If
		Else
			aRows = null
			nErr = 2	'// no matching rows //
		End If
		oRS.Close
		set oRS = nothing
		oConn.Close
		Set oConn = Nothing
	else
		'// bad input parms //
		aRows = null
		nErr = 1
	end if
	
	getRowsPageBySQL = nErr
end function



'// generic record saving function // -- 
function saveRecords(sDSN, sTable, sFilter, aFields, nIdField, nFieldCount)
	'response.write "DSN: " & sDSN & "sTable: " & sTable & " sFilter: " & sFilter & " nIdField: " & nIdField & " nFieldCount: " & nFieldCount & "<BR />"
	dim nErr:nErr = 0
	dim oConn, oRs, nID


	if (len("" & sTable) > 0) and (len("" & sFilter) > 0) then
		nID = aFields(nIdField,0)
		set oConn = server.createobject("ADODB.Connection")
		oConn.open sDSN
		set oRS = server.createObject("ADODB.Recordset")	    
    
    '// DRU NV - 20/09/2006 : major change to save record //
    '// .filter far too costly on large recordsets //
    '// amended to 2 distinct table opening branches //
		if nID = 0 then
		  oRS.open sTable, oConn, adOpenDynamic, adLockOptimistic, adCmdTable	
			oRS.addnew
		else
		  oRS.open "" & sTable & " WHERE " & sFilter & "=" & nID & "", oConn, adOpenDynamic, adLockOptimistic, adCmdTable		
			'oRS.filter = sFilter & "=" & nID
		end if
    
		'oRS.open sTable, oConn, adOpenDynamic, adLockOptimistic, adCmdTable
		'if nID = 0 then
		'	oRS.addnew
		'else
		'	oRS.filter = sFilter & "=" & nID
		'end if
    
		for i = 1 to nFieldCount
			'response.write i & ":" & aFields(i,0) & "<br />"
			oRS(i) = aFields(i,0)
		next
		oRS.update
		
		nID = oRS.fields(sFilter)
		oRS.close
		set oRS = nothing
		oConn.close
		set oConn = nothing
		aFields(nIdField,0) = nID

		Response.Write "nID " &  nID & "<br/>"
	else
		'// bad input parms //
		nErr = 1
	end if
	saveRecord = nErr
end function

'// generic sql executor //
sub sqlExec(sDSN, sSQL)
	dim oConn
	if len("" & sSQL) > 0 then
		set oConn = server.createobject("ADODB.Connection")
		oConn.open sDSN
		oConn.execute sSQL
		oConn.close
		set oConn = nothing
	end if
end sub

' [hardin] this method in theory should be reusable across other Ecommerce sites. 
' The Bespoke overload below is specifically tailored to hand duplicate reduced rate product purchasing
function getAppropriatePrice(aProduct, nOutputPrice, nOutputVAT, nOriginalPrice, nOriginalVAT)
dim nErr	: nErr	= 0
dim aExtra  :aExtra = null 

    ' unlikely to ever occurr
    if (len("" & aProduct(PR_PRICE)) = 0) or isnull(aProduct(PR_PRICE)) then
        nErr = 1000
    else  
		
        nOriginalPrice    = formatnumber(aProduct(PR_PRICE),2) 
        nOriginalVAT      = formatnumber(aProduct(PR_VAT),2) 
     
        if len("" & session("trader")) > 0 then
            'response.Write "<br>nix a"
           
            ' Frigging the usage of db field: PR_HTMLPAGE for trader pricing value
            ' ===========================================
            if len("" & aProduct(PR_HTMLPAGE)) = 0 or isnull(aProduct(PR_HTMLPAGE)) then
                nErr = 100 
            else
                if not isPosDec(aProduct(PR_HTMLPAGE)) then
                    nErr = 101   
                else
                    'response.Write "<br>nix b"
                    On error resume next 
                    nOutputPrice = formatnumber(aProduct(PR_HTMLPAGE),2) 
                            
                    if err.number = 0 then
                        'response.Write "<br>nix c"
                        if VAT_ON_ALL > 0 then 
                            'response.Write "<br>nix d"       
                            nOutputVAT = nOutputPrice - (nOutputPrice / 1.175)
                        else
                            nErr = 102
                        end if
                    else
                        nErr = 101                          
                    end if
                end if
           end if    
           ' =========================================== 
        else
            'response.Write "<br>nix e"
            ' -------------------------------------------
            if aProduct(PR_PROMO) = true then
                'response.Write "<br>nix f"
                
                if len("" & aProduct(PR_PROMOPRICE)) > 0 then
                    'response.Write "<br>nix g"
                    nOutputPrice    = formatnumber(aProduct(PR_PROMOPRICE),2) 
                    nOutputVAT      = formatnumber(aProduct(PR_PROMOVAT),2)
                else
                    nErr = 200
                end if
            else
                'response.Write "<br>nix h"
                if aProduct(PR_PRICE) > 0 then
                    'response.Write "<br>nix i"
                    nOutputPrice    = formatnumber(aProduct(PR_PRICE),2)  
                    nOutputVAT      = formatnumber(aProduct(PR_VAT),2)
                else
                    nErr = 300	 	
                end if
            end if
            ' -------------------------------------------
        end if      ' len("" & session("trader"))
        
        ' If some error has occured, set the pricing outputs to the standard pricing value
        if nErr > 0 then
            'response.Write "<br>nix - bad but at least values are set" 
            nOutputPrice    = formatnumber(aProduct(PR_PRICE),2)  
            nOutputVAT      = formatnumber(aProduct(PR_VAT),2) 
        end if      
    end if          ' len("" & aProduct(PR_PRICE)) = 0
    
    getAppropriatePrice = nErr
end function

function getAppropriatePriceHTML(aProduct)
Dim nErr	            : nErr	= 0
Dim nOutputPrice        : nOutputPrice = 0
Dim nOutputVAT          : nOutputVAT = 0
Dim nOriginalPrice      : nOriginalPrice = 0
Dim nOriginalVAT        : nOriginalVAT = 0
Dim sErrorMessage       : sErrorMessage = ""
Dim sDetail             : sDetail = ""
Dim sCurSymbol          : sCurSymbol = ""
Dim nCurConversion      : nCurConversion = 1    ' This will keep product prices normal for British Pound viewing

    nErr = getAppropriatePrice(aProduct, nOutputPrice, nOutputVAT, nOriginalPrice, nOriginalVAT)  
    if nErr > 0 then    
        call setupAndsendErrorEmail(aProduct(PR_ID), nErr)
                
        sDetail = sDetail & " <span class=""option_price"">" & getPriceForexHTML(nOutputPrice, 2) & "</span>" 
    else                
        if len("" & session("trader")) > 0 then
        ' =========================================== 
            
            sDetail = sDetail & " <span class=""option_price"">" & getPriceForexHTML(nOutputPrice, 2) & "</span>" 	
        ' ===========================================
        else    
           if aProduct(PR_PROMO) = true then
               
                 select case g_nProductTypeId  
                    case PRODUCT_TYPE_PHOTOBOOK
                        
                        if aProduct(PR_PRICE) > 0 then
                            sDetail = sDetail & " " & chr(64) & getPriceForexHTML(nOriginalPrice, 2)
                        end if
                        if len("" & aProduct(PR_PROMOPRICE)) > 0 then
                        
                            sDetail = sDetail & " <br />(Order a 2nd copy for just " & getPriceForexHTML(nOutputPrice, 2) & ")" 	
                        end if
                            
                    case else
                            if aProduct(PR_PRICE) > 0 then
                                sDetail = sDetail & " <br />Was " & getPriceForexHTML(nOriginalPrice, 2) & " <span class=""pink"">Now: " & getPriceForexHTML(nOutputPrice, 2) & "</span>" 	
                            else
                                if len("" & aProduct(PR_PROMOPRICE)) > 0 then
                                    sDetail = sDetail & " <strong>Now only: " & getPriceForexHTML(nOutputPrice, 2) & "</strong>" 	
                                end if
                            end if
                end select              
            else
                if aProduct(PR_PRICE) > 0 then
                    sDetail = sDetail & " " & chr(64) & " " & getPriceForexHTML(nOutputPrice, 2)  	 	
                end if
            end if 
        end if   
    end if           
    getAppropriatePriceHTML = sDetail
end function

' // [hardin] This will take a price and spit out a nicely formatted forex amount and symbol (including pounds when no forex applies)
function getPriceForexHTML(nAmount, nFormatDecimalPlaces)
dim nCurConversion  : nCurConversion = 1
dim nSplitterPosition1 : nSplitterPosition1 = 0
dim nSplitterPosition2 : nSplitterPosition2 = 0

    ' Extract CONVERSION RATE info: Session holds the html symbol, the iso code (for email/protx usage), conversation rate via a: | and a *   Example: "symbol|GBP*conversion_rate"
    if len("" & session("Forex")) > 0 then
        nSplitterPosition1  = instr(session("Forex"), ECOM_CUR_SPLITTER1)
        nSplitterPosition2  = instr(session("Forex"), ECOM_CUR_SPLITTER2)
        nCurConversion      = mid(session("Forex"), nSplitterPosition2+1, len(session("Forex")) - nSplitterPosition2)
    end if
  
    'response.Write "<br />nSplitterPosition1=" & nSplitterPosition1
    'response.Write "<br />nSplitterPosition2=" & nSplitterPosition2
    'response.Write "<br />sCurSymbol=" & sCurSymbol
    'response.Write "<br />sCurISOCode=" & sCurISOCode
    'response.Write "<br />nCurConversion=" & nCurConversion
    
    getPriceForexHTML = getForexSymbol() & getPriceForex(nAmount, nFormatDecimalPlaces)
end function

' // [hardin] Hacked: This will take a price and spit out a nicely formatted forex amount and iso code (including pounds when no forex applies)
function getPriceForexHTMLemail(nAmount, nFormatDecimalPlaces)
dim nCurConversion  : nCurConversion = 1
dim nSplitterPosition1 : nSplitterPosition1 = 0
dim nSplitterPosition2 : nSplitterPosition2 = 0

    ' Extract 3 bits of info: Session holds the html symbol, the iso code (for email/protx usage), conversation rate via a: | and a *   Example: "symbol|GBP*conversion_rate"
    if len("" & session("Forex")) > 0 then
        nCurConversion      = mid(session("Forex"), nSplitterPosition2+1, len(session("Forex")) - nSplitterPosition2)
    end if
    getPriceForexHTMLemail = getForexISO() & " " & getPriceForex(nAmount, nFormatDecimalPlaces) 
end function

' // [hardin] This will take a price and spit out a numeric forex amount (pounds amount used when no forex session applies)
function getPriceForex(nAmount, nFormatDecimalPlaces)
dim nCurConversion  : nCurConversion = 1
dim nSplitterPosition1 : nSplitterPosition1 = 0
dim nSplitterPosition2 : nSplitterPosition2 = 0

    ' Extract CONVERSION RATE info: Session holds the html symbol, the iso code (for email/protx usage), conversation rate via a: | and a *   Example: "symbol|GBP*conversion_rate"
    if len("" & session("Forex")) > 0 then
        nSplitterPosition1  = instr(session("Forex"), ECOM_CUR_SPLITTER1)
        nSplitterPosition2  = instr(session("Forex"), ECOM_CUR_SPLITTER2)
        nCurConversion      = mid(session("Forex"), nSplitterPosition2+1, len(session("Forex")) - nSplitterPosition2)
    end if
    
    getPriceForex = formatnumber(nCurConversion*nAmount, nFormatDecimalPlaces)
end function

' // [hardin] This will take a price and spit out a nicely formatted forex amount and symbol (including pounds when no forex applies)
function getForexSymbol()
dim sCurSymbol      : sCurSymbol = ECOM_CUR_SYMBOL
dim sCurISOCode     : sCurISOCode = ECOM_CUR_ISO_POUND
dim nSplitterPosition1 : nSplitterPosition1 = 0
dim nSplitterPosition2 : nSplitterPosition2 = 0

    ' Extract SYMBOL INFO of info: Session holds the html symbol, the iso code (for email/protx usage), conversation rate via a: | and a *   Example: "symbol|GBP*conversion_rate"
    if len("" & session("Forex")) > 0 then
        nSplitterPosition1  = instr(session("Forex"), ECOM_CUR_SPLITTER1)
        nSplitterPosition2  = instr(session("Forex"), ECOM_CUR_SPLITTER2)
        sCurSymbol          = left(session("Forex"), (nSplitterPosition1-1))
     else
        ' UK is the default currency
        sCurSymbol  = ECOM_CUR_SYMBOL
    end if
    getForexSymbol = sCurSymbol
end function

Function GetSingleProductLineArray(aProductArray,CurrentIndex,aOutArray)
	
	Redim  aOutArray(48)      
	

	 ' ++++++++++++++++++++++++++++++++++++++++++++++++
        ' Put all the fields of the current array row into an 1 dimensional array array for the bespoke code to use
        for nFieldIdx = 0 to ubound(aProductArray, 1)
           ' Need this to stop population of aProduct when all the product db fields are retrieved from the aCartItems datasat
           if nFieldIdx <= ubound(aOutArray) then
                aOutArray(nFieldIdx) = aProductArray(nFieldIdx,CurrentIndex)
           end if
        next                    

End Function

function getCartItemsAndTotal(nCtID, nItemsInCart, nCartTotal)
dim nErr:nErr = 0
dim nPrice, nVat
dim aProduct(48)
dim aCartItems
dim nFieldIdx
dim nOriginalPrice, nOriginalVAT, nDiscountVal
nItemsInCart    = 0 
nCartTotal      = 0
nDiscountVal    = 0

    if nCtID > 0 then
        nErr = getCartItemsProductsByCartID(nCtID, aCartItems)
    
        if nErr = 0 and (not isNull(aCartItems)) then
            for nIdx = 0 to ubound(aCartItems, 2)
               
                ' ================================================ 
                ' Put all the fields of the current array row into an array for the bespoke code to use
                for nFieldIdx = 0 to ubound(aCartItems, 1)
                   ' Need this to stop population of aProduct when all the product db fields are retrieved from the aCartItems datasat
                   if nFieldIdx <= ubound(aProduct) then
                        aProduct(nFieldIdx) = aCartItems(nFieldIdx,nIdx)
                   end if
                next
                                
                nErr = getAppropriatePrice(aProduct, nPrice, nVAT, nOriginalPrice, nOriginalVAT)                    

                if aCartItems(PR_PROMO, nIdx) then
                    nPrice = aCartItems(PR_PROMOPRICE, nIdx)
                    nVAT = aCartItems(PR_PROMOVAT, nIdx)
                else
                    nPrice = aCartItems(PR_PRICE, nIdx)
                    nVAT = aCartItems(PR_VAT, nIdx)
                end if    
                ' ================================================           
               
                nQty        = aCartItems(PR_UBOUND + 1, nIdx)
		if cint(0 & nQty) = 0 then
                	nQty = 1
		end if
		nItemsInCart  = nItemsInCart + nQty
		
		               

                nSubTotal   = nQty * nPrice
                nTotalVAT   = nQty * nVAT
		
                nTotal      = nTotal + nSubTotal                
            next

	    if len(nTotal) > 0 then
            	nCartTotal  = formatnumber(nTotal, 2)  
	    end if
            
            'response.Write "<br>nix before discount: " & nCartTotal
            ' ================================================ 
            if len("" & session("promocode")) > 0 then
                nErr = calcPromotionalDiscount(session("promocode"), nCartTotal, nDiscountVal)
                if nErr = 0 then
                    nDiscountVal  = formatnumber(nDiscountVal, 4) 
                    nCartTotal    = formatnumber((nCartTotal - nDiscountVal), 4)
                    nCartTotal    = left(nCartTotal, len(nCartTotal)-2)
                    'response.Write "<br>nix after discount: " & nCartTotal
                end if
            end if
            ' ================================================ 
            
            '' ++++++++++++++++++++++++++++++++++++++++++++++++ 
            '' If applicable: Apply forex 
            'nCartTotal
            '' ++++++++++++++++++++++++++++++++++++++++++++++++
        else
            nItemsInCart = 0
            nCartTotal = 0
        end if
	else
		nErr = 1	'// no id supplied //
		nItemsInCart = 0
        nCartTotal = 0
	end if
	getCartItemsAndTotal = nErr
end function

function loadChildrenProducts(nPPrdID,nPrdID,aMember, pstrThumbs,pstrItems, pblnIsAnyItemFoundOnSale)
Dim sDetail, sChecked
Dim sTmp	: sTmp	= ""
Dim sSQL	: sSQL	= ""
Dim nErr	: nErr	= 0
Dim intX	: intX	= 0
Dim sSize   : sSize = ""
Dim sColour : sColour = ""
Dim aProduct(48)
Dim nFieldIdx
Dim sPriceDisplayHTML
Dim blnIsRadioCheckedRequired   : blnIsRadioCheckedRequired = true
Dim blnAreAnyThumbsDisplayed    : blnAreAnyThumbsDisplayed = false
Dim blnIsShippingRestricted     : blnIsShippingRestricted = false
Dim blnIsDetectionRequired      : blnIsDetectionRequired = true
Dim blnIsDetectionRequired2     : blnIsDetectionRequired2 = true
Dim blnContinue                 : blnContinue = false

if nPPrdID > 0 then
    ' fine
else
    nPPrdID = nPrdID
end if
	' // We are a parent, lets make sure we have children and were not phantom 
	sSQL = "SELECT PR.* FROM tblProduct AS PR WHERE PR.prParentID = " & nPPrdID & " OR PR.prID=" & nPrdID & " OR PR.prID=" & nPPrdID & " AND PR.prDisplay=1 AND PR.prOnSale=1"
	nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aChildFields)
	if nErr = 0 then       
        pstrThumbs = "<h3>Options:</h3>"
        for intRows = 0 to ubound(aChildFields,2)
            sDetail = ""
            blnContinue = false           
            
            if aChildFields(PR_ISCHILD,intRows) AND aChildFields(PR_ONSALE,intRows) then
                blnContinue = true
            end if
            
            ' if shipping restrictions exist         
            ' note: this code, setup in this logic structure should allow a registered member viewing forex in euros to still order 
            ' a product when it has resrictions, should the person be buying for a uk delivery address
            if aChildFields(PR_PASSPARMS,intRows) then
            
                ' detect from customer"s logged in details whether coverage to their postal address exists
                if (not isnull(aMember)) and isarray(aMember) then
                    '// now lets calculate postage etc //
                    if aMember(ME_USEALTADDRESS, 0) then
                        nPostZone = mid(aMember(ME_ALTADDRESS5, 0), 4, 1)
                    else
                        nPostZone = mid(aMember(ME_ADDRESS5, 0), 4, 1)
                    end if
                    ' If customer"s delivery zone falls outsdie the uk
                    if nPostZone <> 9 then
                        if blnIsDetectionRequired then
                            blnIsShippingRestricted = true 
                            blnIsDetectionRequired  = false       
                        end if
                        blnContinue = false
                    end if
                else
                    ' fallback for restricting purchases on products with international delivery resrictions is to look at the current forex context
                    if len("" & session("Forex")) > 0 then
                        if blnIsDetectionRequired then
                            blnIsShippingRestricted = true 
                            blnIsDetectionRequired  = false       
                        end if
                        blnContinue = false
                    end if   
                end if        
            end if         
            
            if blnContinue then 
                
                ' Tell the product page if at least one itme was found on sale           
                if blnIsDetectionRequired2 then
                    pblnIsAnyItemFoundOnSale = true    
                    blnIsDetectionRequired2  = false    
                end if
            
                if (len("" & aChildFields(PR_IMAGELARGE,intRows)) > 0) and (len("" & aChildFields(PR_IMAGEMAIN,intRows)) > 0) then  
                    blnAreAnyThumbsDisplayed = true 
                    ' // can fit nine thumbs syandard font size 
                    ' // click each thumb to update the src of the image by id ref through js file (can be re-written)            
                    pstrThumbs = pstrThumbs & "<a href=""#"" title=""product view:" & aChildFields(PR_NAME,intRows) & """ onclick=""switch_prod_image('/ecomm/graphics/" & aChildFields(PR_IMAGELARGE,intRows) & "','img_" & aChildFields(PR_ID,intRows) & "');"">"
                    pstrThumbs = pstrThumbs & "<img id=""img_" & aChildFields(PR_ID,intRows) & """ src=""/ecomm/graphics/" & aChildFields(PR_IMAGEMAIN,intRows) & """ alt=""product view:" & aChildFields(PR_NAME,intRows) & """ width=""" & IMG_SZ_PR_TH_W & """ height=""" & IMG_SZ_PR_TH_W & """ /></a>"
                end if              
                if len("" & aChildFields(PR_SIZE,intRows)) > 0 then
                    sDetail = aChildFields(PR_SIZE,intRows) 
                    sSize =  aChildFields(PR_SIZE,intRows)   
                end if
                
                ' ================================================ 
                ' Put all the fields of the current array row into an array for the bespoke code to use
                for nFieldIdx = 0 to ubound(aChildFields, 1)
                   aProduct(nFieldIdx) = aChildFields(nFieldIdx,intRows)
                next
                                
                sDetail = sDetail & getAppropriatePriceHTML(aProduct)
                ' ================================================               
                
                if len("" & aChildFields(PR_COLOUR,intRows)) > 0 then
                    sDetail = sDetail & " " & aChildFields(PR_COLOUR,intRows)
                    sColour = aChildFields(PR_COLOUR,intRows)
                end if                     
              
                if cint("0" & aChildFields(PR_ID,intRows)) = cint("0" & nPrdID) then 
                    sChecked = "checked"
                else
                    sChecked = ""
                end if
                
                if blnIsRadioCheckedRequired = true then	
                    sChecked = "checked"
                    blnIsRadioCheckedRequired = false
                else
                    sChecked = ""
                end if
                                
                'response.Write "<br / >nix checking child prod id " & aChildFields(PR_ID,intRows)
                'response.Write "<br / >nix checking child: " & sColour
                'response.Write "<br / >nix checking child: " & sSize
    
                ' // In children products, 
                sTmp = sTmp & "<input type=""hidden"" name=""child_colour" & aChildFields(PR_ID,intRows) & """ id=""child_colour" & aChildFields(PR_ID,intRows) & """ value=""" & sColour & """/>"
                sTmp = sTmp & "<input type=""hidden"" name=""child_size" & aChildFields(PR_ID,intRows) & """ id=""child_size" & aChildFields(PR_ID,intRows) & """ value=""" & aChildFields(PR_SIZE,intRows) & """/>"
		        sTmp = sTmp & "<input type=""radio"" " & sChecked & " value=""" & aChildFields(PR_ID,intRows) & """ name=""options"" id=""options"" class=""radio_left"" >" & vbcrlf
		        ' //onChange=""javascript: document.frmOptionChange.submit();""
		        sTmp = sTmp & "<label for=""options"" class=""long_label"">" & sDetail & "</label>" & vbcrlf 
	        end if
	    next
	    
	    if blnIsShippingRestricted then
	        pstrItems = DELIVERY_AVAILABILITY_MSG
	    end if	    
	    pstrItems = pstrItems & sTmp
	    
	    if blnAreAnyThumbsDisplayed = false then
	        ' // Dont show an option heading
	        pstrThumbs = ""    
	    end if
	else
		sTmp = ""
		pstrThumbs = ""  
	end if	
	loadChildrenProducts = nErr
end function

'// function to calculate the postage //
'// Minumum handling charge or �2.95 UK & 7.95 Europe
function calcCartPostage(aItems, nZone, nPostage, sPostageMsg)
dim nErr:nErr=0
dim nIdx:nIdx=0
dim nPrice:nPrice=0 
dim sCurSymbol

    ' ==================
    '
    ' if postage ever became dependant on price being paid, then remember to call function: getAppropriatePrice
    ' 
    ' ==================
    
    ' // Only zones the shop delivers to is: 9: UK and 0: EURO europe
   
    ' A forex session existing overrides the members delivery address
    if len("" & session("Forex")) > 0 then 
        if nZone = 0 then
            'response.Write "a"
            nPostage    = DELIVERY_COST_EURO
            sPostageMsg = replace(DELIVERY_MSG_EURO, "{0}",  getPriceForexHTML(nPostage, 2)) 
        else
            ' The user is viewing prices i nEuros but has a delivery address local to th UK
            if nZone = 9 then
                'response.Write "f"
                nPostage    = DELIVERY_COST_UK
                sPostageMsg = replace(DELIVERY_MSG_UK, "{0}",  getPriceForexHTML(nPostage, 2))    
            else
                ' if zone info is not available : custoemr nto logged in yet - interrogate currency context being browsed
                'response.Write "session=" & session("Forex")
                sCurSymbol = left(session("Forex"), (instr(session("Forex"), "|")-1))
                'response.Write "g"
                'response.Write "sCurSymbol=" & sCurSymbol

                if sCurSymbol = ECOM_CUR_SYMBOL_EURO then
                    'response.Write "h"
                    nPostage    = DELIVERY_COST_EURO
                    sPostageMsg = replace(DELIVERY_MSG_EURO, "{0}",  getPriceForexHTML(nPostage, 2))     
                else
                    'response.Write "i"
                    ' If the session cannot detect a valid currency symbol, 
                    ' Default it to a high amount so that the shop does not lose out on profits
                    nPostage    = DELIVERY_COST_EURO
                    sPostageMsg = replace(DELIVERY_MSG_EURO, "{0}",  getPriceForexHTML(nPostage, 2)) 
                end if
                
            end if   
        end if
        
    else   
        if nZone = 0 then
            'response.Write "a"
            nPostage    = DELIVERY_COST_EURO
            sPostageMsg = replace(DELIVERY_MSG_EURO, "{0}",  getPriceForexHTML(nPostage, 2)) 
        else
            ' The user has a delivery address local to th UK
            if nZone = 9 then
                'response.Write "f"
                nPostage    = DELIVERY_COST_UK
                sPostageMsg = replace(DELIVERY_MSG_UK, "{0}",  getPriceForexHTML(nPostage, 2))    
            else
                ' if zone info is not available : CUSTOMER is not logged in yet - interrogate currency context being browsed
                ' Default it to uk amount - the regular shoppers, not yet registered, will see the price due to this logic branch
                nPostage    = DELIVERY_COST_UK
                sPostageMsg = replace(DELIVERY_MSG_UK, "{0}",  getPriceForexHTML(nPostage, 2))             
            end if   
        end if      
    end if    
         
    calcCartPostage = nErr
end function

Function write_boxes(sSQL,sType,nBcount, nPrdPerPage, nPageCur, nTotalPages)
'version 1.0 RM 25/11/05
' nBcount	= Boxes per line
' sType		= Type of box, 0 default
' sSQL		= Query for products
dim strTemp	: strTemp 	= ""
dim sURL 	: sURL 		= ""
dim intX 	: intX 		= 0
dim intJ 	: intJ 	  	= 0
dim intI 	: intI 	  	= 0
dim intY 	: intY 		= 0
'//Execute Query
if nPrdPerPage = false then
nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aBox)
else
nErr =  getRowsPageBySQL(DSN_ECOM_DB, nPrdPerPage, nPageCur, nTotalPages, sSql, aBox)

end if


'//Ensure the default
if len(nBcount) = 0 then
	nBcount = 3
end if

if nErr = 0 then
'//if query was successfully executed then proceed to build boxes

'// Product List Container
'strTemp = "<div id=""product_list"">" & vbcrlf & "<!-- Begin Product listing -->"&vbcrlf

 '//Vertical Loop
	If nTotalPages > 0 Then
						%>
						
						
						<p class="pr_pagination">
								<div class="pr_pagination_L">
									<%if nPageCur > 1 then%>
									<a href="./?pg=1&searchbox=<%=sQ%>" >|&lt;</a>&nbsp;&nbsp;&nbsp;&nbsp;
									<%end if
									if nPageCur > 1  AND nPageCur - 2  > 0 then%>
									<a href="./?pg=<%=nPageCur - 2%>&searchbox=<%=sQ%>">&lt;&lt;</a>&nbsp;&nbsp;&nbsp;
									<%end if
									if nPageCur > 1 then%>
									<a href="./?pg=<%=nPageCur - 1%>&searchbox=<%=sQ%>">&lt;</a>
									<%end if%>&nbsp;
								</div>
								
								<div class="pr_pagination_M">
									Page <strong><%=cint(nPageCur)%></strong> of <%=cint(nTotalPages)%>&nbsp;&nbsp;&nbsp;
								</div>
								
								<div class="pr_pagination_R">
									<%if nPageCur <> nTotalPages  then%>
									<a href="./?pg=<%=nPageCur + 1%>&searchbox=<%=sQ%>">&gt;</a>&nbsp;&nbsp;&nbsp;
									<%end if
									if nPageCur <> nTotalPages AND nPageCur + 2 =< nTotalPages then%>
									<a href="./?pg=<%=nPageCur + 2%>&searchbox=<%=sQ%>">&gt;&gt;</a>&nbsp;&nbsp;&nbsp;&nbsp;
									<%end if
									if nPageCur <> nTotalPages then%>
									<a href="./?pg=<%=nTotalPages%>&searchbox=<%=sQ%>">&gt;|</a>
									<%end if%>&nbsp;
								</div>
						</p>
						<%
						End If

 For intJ = 0 to ubound(aBox,2)
	
	
	' If aBox(PR_ISPARENT, intJ) Then %>

	
	<div class="search_results">
		
		

		<div class="thumbnail">

			<%

			'Main product url, this will be the one to the main detail page
			sURL = "href="""
			
			'If aBox(49,intX) = CAT_MATTRESSES_ID Then
			'	sURL = sURL & "mattresses/"
			'ElseIf aBox(49,intX) = CAT_BEDS_ID Then
			'	sURL = sURL & "beds/"
			'End If 
			
			
			

			If aBox(PR_ISCHILD, intJ) Then 
				sURL = sURL &  "/products/details/?" & buildParms("",aBox(49, intJ),nRngID,0,0,aBox(PR_PARENTID, intJ)) & """"
				
				if len(aBox(50,intX)) > 0 then
					Response.Write "<a " & sURL & "><img src=""/ecomm/graphics/" & aBox(50,intJ) & """ width=""133""  alt=""" & aBox(PR_NAME,intJ)  & """ /></a>" & vbcrlf
				else
					Response.Write "<a " & sURL & "><img src=""/graphics/clearpix.gif"" width=""133"" height=""81"" alt=""No Image Supplied"" /></a>" & vbcrlf
				end if
			
			Else
				sURL = sURL &  "/products/details/?" & buildParms("",aBox(49, intJ),nRngID,0,0,aBox(PR_ID, intJ)) & """"
				if len(aBox(PR_IMAGETHUMB,intX)) > 0 then
					Response.Write "<a " & sURL & "><img src=""/ecomm/graphics/" & aBox(PR_IMAGETHUMB,intJ) & """ width=""133""  alt=""" & aBox(PR_NAME,intJ)  & """ /></a>" & vbcrlf
				else
					Response.Write "<a " & sURL & "><img src=""/graphics/clearpix.gif"" width=""133"" height=""81"" alt=""No Image Supplied"" /></a>" & vbcrlf
				end if 

			End If
			%>

		</div>
		
		<div class="content">
		
			<h3><%=aBox(PR_NAME,intJ)%>
			<%
			if len(aBox(PR_MISC1,intJ)) > 0 then
				Response.Write  "&nbsp;-&nbsp;" & aBox(PR_MISC1,intJ) & "</p>"&vbcrlf
			End If
			%>
			</h3>
			<%

			if len(aBox(PR_DESCSHORT,intJ)) > 0 then
				if len(aBox(PR_DESCSHORT,intJ)) > 100 then
					Response.Write  "<p>" & left(aBox(PR_DESCSHORT,intJ),100)  & "...</p>"&vbcrlf
				else
					Response.Write  "<p>" & aBox(PR_DESCSHORT,intJ) & "</p>"&vbcrlf
				end if
			else
				
			end if

			'if len(aBox(PR_PROMOPRICE,intJ)) > 0 and aBox(PR_PROMO,intJ) = True then
			'	strTemp =  strTemp  & "<font style=""text-decoration:line-through;"">Was &pound;"& formatnumber(aBox(PR_PRICE,intX),2) &"</font><br /><strong>Now: &pound;" & formatnumber(aBox(PR_PROMOPRICE,intX),2) & "</strong><br />"&vbcrlf
			'else
			'	strTemp =  strTemp  & "<strong>&pound;"& formatnumber(aBox(PR_PRICE,intX),2) &"</strong><br />"&vbcrlf
			'end if
			
			If NOT aBox(PR_ISPARENT, intJ) Then
				GetSingleProductLineArray aBox,intJ,aProductSingleDimension 
				
				nPriceErr = getAppropriatePrice(aProductSingleDimension, nOutputPrice, nOutputVAT, nOriginalPrice, nOriginalVAT) 
				
				nPriceWithVat = CDbl(nOutputPrice) + CDbl(nOutputVAT)
				
				'Response.Write nOutputPrice & "<br/>"
				'Response.Write nOutputVAT & "<br/>"
				aProductSingleDimension = VBNull
				
				Response.Write "<p><strong>Price:&nbsp;</strong>" & getPriceForexHTML(nOutputPrice,2) & "</p>"
			

			END If
			Response.Write  "<div class=""buttons""><a " & sURL & " class=""orange_link""><img src=""/ecomm/graphics/more_info.gif"" alt=""More info...""/></a>&nbsp;"
			
			If NOT aBox(PR_ISPARENT, intJ) Then
				if aBox(PR_ONSALE,intJ) = True then
					Response.Write  "<a href=""/shoppingbasket/buy.asp?prID=" & aBox(PR_ID,intJ) &"&amp;qty=1""><img src=""/images/buttons/btn_buy.gif"" alt=""Buy Now &gt;&gt;"" /></a>" &vbcrlf
				end if
			End If
		
		Response.Write  "</div></div><div class=""clear""></div></div>"
		
		'End If
		
		intTemp = intTemp + 1
	
	Next

	'write_boxes = strTemp
else
	response.write "<p>Sorry, your product search returned no results.</p>"
end if

end function

function doLoginRemind(sEmail, aMember)
  dim nErr:nErr=0
  dim sSQL:sSQL=""
  if (len("" & sEmail) > 0) then
    sSQL = "SELECT ME.* FROM tblMember AS ME WHERE ME.meEmail='" & SQLFixUp(sEmail) & "' AND ME.keyFinID=" & INST_ID & ";"
    nErr = getRowsBySQl(DSN_ECOM_DB, sSQL, aMember)
    if nErr = 0 then
			sendPasswordEmail(aMember)
    end if
  else
    nErr = 1  '// bad input parms //
    aMember = null
  end if
  doLoginRemind = nErr
end function


Function GetDefaultEcommMetaTags(sTitle) 

	Dim sMetaTags:sMetaTags = VBNullString
	
	sMetaTags = sMetaTags & "<title>" & sTitle & "</title>"
	
	GetDefaultEcommMetaTags = sMetaTags
End Function


'// load record from db also retrieve wether delivery is covered for the member"s //
function loadMemberBespoke(nID, aFields)
	dim nErr:nErr = 0
	dim sSQL
	if nID > 0 then
		sSQL = "SELECT ME.*, CTY.ctyActiveForDel, CTY2.ctyActiveForDel FROM tblMember AS ME " _
		     & "LEFT OUTER JOIN tblCountry as CTY ON CTY.ctyCode = ME.meAddress5 " _ 
		     & "LEFT OUTER JOIN tblCountry as CTY2 ON CTY2.ctyCode = ME.meAltAddress5 " _
		     & "WHERE ME.meID=" & nID & " AND ME.keyFinID=" & INST_ID & ";"
		'response.Write "sSQL=" & sSQL 
		nErr = getRowsBySQL(DSN_NEWS_DB, sSQL, aFields)
	else
		nErr = 1	'// no id supplied //
		aFields = null
	end if
	loadMemberBespoke = nErr
end function

' // Get Cat id from Range ID
function getRangeCategory(nRangeID)
    sSQL = "SELECT R2C.keyFcaID FROM tblRangeToCategory AS R2C WHERE R2C.keyFrnID=" & nRangeID
    nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aCatRange)
    if nErr = 0 then
	    getRangeCategory = aCatRange(0,0)
    else
	    gettRangeCategory = 0
    end if
end function

function isFinalCartLineItem(nProductTypeId)

  
        'if (nProductTypeId) or  (nProductTypeId) then
        'else
            select case nProductTypeId
                case PRODUCT_TYPE_ART_OF_TRAVEL, _
                    PRODUCT_TYPE_NOTICEBOARD_55MAX, _
                    PRODUCT_TYPE_CANVAS_MONTAGE, _
                    PRODUCT_TYPE_PINBOARD, _
                    PRODUCT_TYPE_BLINDS, _
                    PRODUCT_TYPE_WALLPAPER, _
                    PRODUCT_TYPE_TILES_CERAMIC, _
                    PRODUCT_TYPE_GARDEN_ART
                    isFinalCartLineItem = false
                case else
                    isFinalCartLineItem = true 
            end select 
        'end if
   
end function


Function HTMLDecodeSQLFixup(TextIn)
if (NOT IsEmpty(TextIn)) AND (LEN(TextIn) > 0) then
  Dim temp
  temp = CStr(TextIn)
  temp = Replace(temp, "'", "''")
  temp = Replace(temp, "|", "' & chr(124) & '")
  temp = replace(temp, "SELECT", "", 1, -1, vbTextCompare)
  temp = replace(temp, "DELETE", "", 1, -1, vbTextCompare)
  temp = replace(temp, "UPDATE", "", 1, -1, vbTextCompare)
  temp = replace(temp, "INSERT", "", 1, -1, vbTextCompare)
  'temp = replace(temp, ";", " ")
  temp = Replace( temp, "&quot;", chr(34) )
	temp = Replace( temp, "&lt;"  , chr(60) )
	temp = Replace( temp, "&gt;"  , chr(62) )
	temp = Replace( temp, "&amp;" , chr(38) )
	temp = Replace( temp, "&nbsp;", chr(32) )
	For i = 1 to 255
		tmp = Replace( temp, "&#" & i & ";", chr( i ) )
	Next

	temp = Replace( temp, "%", chr(32) )
  HTMLDecodeSQLFixup = temp
Else
 SQLFixup = TextIn
End If
End Function




%>

