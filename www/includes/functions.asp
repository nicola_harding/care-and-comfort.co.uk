

<%



const isEmail_re     = "^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$"


' Check if string is a valid email address
function isEmail (s)
   isEmail = validateRE (s, isEmail_re)
end function

function isInteger (s)
   isInteger = validateRE (s, isInteger_re)
end function

function validateRE (s, regex)
   dim re
   set re = new regexp
   re.pattern = regex
   re.ignorecase = true
   validatere = re.test (s)
   set re = nothing
end function


function SafeSQL (value, maxlength)
 
	Dim sRetString

	sRetString = Trim(value)

	If sRetString <> VBNullString Then
		If maxlength <> VBNullString AND IsNumeric(maxlength) Then
			sRetString = Left(sRetString,maxlength)
		End If
		
		sRetString = Replace(sRetString,";","")
	End If
	
	SafeSQL = sRetString

End Function



%>