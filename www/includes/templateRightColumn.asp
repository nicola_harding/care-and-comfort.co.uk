<div id="right_main">
				<%
				If LEN(TRIM(sBreathEasy)) = 0 Then
					sBreathEasy = "mattresses"
				End IF
				%>
				<span class="spacer"><!----></span>
				
				<div id="backpain" class="signpost">
					<h2>A family company</h2>

					<p>If you are unfamiliar with using an on-line Shopping Basket to order, please ring on <%=nvCmsGetContentHTML(102)%>.</p>
					
					
					<div class="clear"></div>
					
				</div>
			
				<div id="backpain_bottom"></div>

				
				<div id="breath_easy" class="signpost">
					<h2>Find your sleep solution</h2>

					<p>How can an Ultimate Sleeper&trade; memory foam mattress help?</p>
					
					<a href="/findyoursleepsolution/?id=78" title="Navigate to: Find your sleep solution"><img src="/images/buttons/btn_to_breath_easy.gif" alt="Navigate to: Find your sleep solution" width="15" height="15"/></a>

					<div class="clear"></div>
					
				</div>
			
				<div id="breath_easy_bottom"></div>
				
				<div id="care_kids" class="signpost">
					<h2>Breathe easy</h2>

					<p>Memory foam <%=sBreathEasy%> are allergy resistant and do not provide a suitable environment for dust mites to live.</p>
					<a href="/antiallergy/?id=81" title="Navigate to: Backpain"><img src="/images/buttons/btn_to_carekids.gif" alt="Navigate to: Backpain" width="15" height="15"/></a>
					<div class="clear"></div>
				</div>
				<div id="care_kids_bottom">
				</div>
				
				

				<div id="sleep_better" class="signpost">
					<h2>Which mattress is best</h2>

					<p>Pick the perfect solution for your needs</p>
					<a href="/whichmattressisbest/?id=79" title="Navigate to: Which mattress is best"><img src="/images/buttons/btn_to_sleepbetter.gif" alt="Which mattress is best" width="15" height="15"/></a>
				<div class="clear"></div>
				</div>
				<div id="sleep_better_bottom">
				</div>
				
				

				<div id="more_help" class="signpost">
					<h2>Here to help</h2>

					<p><%=nvCmsGetContentHTML(103)%></p>
					<a href="/needtoknowinformation/?id=82" title="Navigate to: More help"><img src="/images/buttons/btn_to_morehelp.gif" alt="Navigate to: More help" width="15" height="15"/></a>
					<div class="clear"></div>
				</div>
				<div id="more_help_bottom">
				</div>
			</div>
