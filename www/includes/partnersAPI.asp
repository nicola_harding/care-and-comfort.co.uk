<%
'//Added in By Ryan 14/02/06

'// Partners
const PA_ID 	  	 = 0
const PA_USERNAME 	 = 1
const PA_PASSWORD 	 = 2
const PA_FIRSTNAME 	 = 3
const PA_LASTNAME 	 = 4
const PA_COMPANYNAME = 5
const PA_EMAIL 		 = 6
const PA_KEYFLVLID 	 = 7
const PA_LASTLOGIN 	 = 8
const PA_UBOUND		 = 8

'// partner levels
const PL_ID			 = 0
const PL_NAME		 = 1
const PL_DESC		 = 2
const PL_PERMS		 = 3
const PL_UBOUND		 = 3

'// document folders
const DF_ID			 = 0
const DF_PARENTID	 = 1
const DF_NAME		 = 2
const DF_DESC		 = 3
const DF_SHOW		 = 4
const DF_DISPLAYOD	 = 5
const DF_ISPARENT	 = 6
const DF_UBOUND		 = 6

'// Documents
const DC_ID			 = 0
const DC_NAME		 = 1
const DC_DESC		 = 2
const DC_FILE		 = 3
const DC_DATEADDED	 = 4
const DC_PUBLISHDATE = 5
const DC_ENDDATE	 = 6
const DC_SHOW		 = 7
const DC_DISPLAYOD	 = 8
const DC_KEYFFLDID	 = 9 ' FORIEGN KEY 4 FOLDER
const DC_UBOUND		 = 9

'// generic paging row fetcher //
function getRowsPageBySQL(sDSN, nMaxCount, nPageCur, nTotalPages, sSQL, aRows)

	dim nErr:nErr = 0
	dim oConn, oRS, bRows, i, j, sTmp, nPageCount
	bRows = false
	nTotalPages = 0
	if len("" & sSQL) > 0 then
		set oConn = server.createobject("ADODB.Connection")
		oConn.open sDSN
		Set oRS = Server.CreateObject("ADODB.Recordset")
		oRS.CursorLocation = adUseClient
		oRS.PageSize = nMaxCount
		oRS.Open sSQL, oConn, adOpenStatic, adLockReadOnly, adCmdText
		
		nPageCount = oRS.PageCount
		'// set the current page //
		If 1 > nPageCur Then nPageCur = 1
		If nPageCur > nPageCount Then nPageCur = nPageCount
		'// get the req'd page from the db //
		nTotalPages = nPageCount
		If nPageCur > 0 Then
			oRS.AbsolutePage = nPageCur
			i = 0
			If Not oRS.EOF and oRS.AbsolutePage = nPageCur Then
				nFieldCount = oRS.Fields.Count
				Do While oRS.AbsolutePage = nPageCur AND Not oRS.EOF
					if bRows = True Then
						Redim Preserve aRows(nFieldCount, i)
					Else
						Redim aRows(nFieldCount,0)
					End if
					for j = 0 to nFieldCount - 1
						aRows(j,i) = oRS(j)
					next
					bRows = true
					i = i + 1
					oRS.MoveNext
				Loop
			Else
				aRows = null
				nErr = 2	'// no matching rows //
			End If
		Else
			aRows = null
			nErr = 2	'// no matching rows //
		End If
		oRS.Close
		set oRS = nothing
		oConn.Close
		Set oConn = Nothing
	else
		'// bad input parms //
		aRows = null
		nErr = 1
	end if
	
	getRowsPageBySQL = nErr
end function

'// generic row fetcher //
function getRowsBySQL(sDSN, sSQL, aFields)
	dim nErr:nErr = 0
	dim oConn, oRS
	if len("" & sSQL) > 0 then
		set oConn = server.createobject("ADODB.Connection")
		oConn.open sDSN
    'response.write sSQL
		set oRS = oConn.execute(sSQL)
		if not oRS.eof then
			aFields = oRS.getRows
		else
			'// no record found //
			nErr = 2
			aFields = null
		end if
		oRS.close
		set oRS = nothing
		oConn.close
		set oConn = nothing
	else
		'// bad parms //
		nErr = 1
		aFields = null
	end if
	getRowsBySQL = nErr
end function

'// generic record saving function // -- 
function saveRecord(sDSN, sTable, sFilter, aFields, nIdField, nFieldCount)
'response.write "DSN: " & sDSN & "sTable: " & sTable & " sFilter: " & sFilter & " nIdField: " & nIdField & " nFieldCount: " & nFieldCount & "<BR />"
	dim nErr:nErr = 0
	dim oConn, oRs, nID
	if (len("" & sTable) > 0) and (len("" & sFilter) > 0) then
		nID = aFields(nIdField,0)
		set oConn = server.createobject("ADODB.Connection")
		oConn.open sDSN
		set oRS = server.createObject("ADODB.Recordset")	    
    
    '// DRU NV - 20/09/2006 : major change to save record //
    '// .filter far too costly on large recordsets //
    '// amended to 2 distinct table opening branches //
		if nID = 0 then
		  oRS.open sTable, oConn, adOpenDynamic, adLockOptimistic, adCmdTable	
			oRS.addnew
		else
		  oRS.open "" & sTable & " WHERE " & sFilter & "=" & nID & "", oConn, adOpenDynamic, adLockOptimistic, adCmdTable		
			'oRS.filter = sFilter & "=" & nID
		end if
    
		'oRS.open sTable, oConn, adOpenDynamic, adLockOptimistic, adCmdTable
		'if nID = 0 then
		'	oRS.addnew
		'else
		'	oRS.filter = sFilter & "=" & nID
		'end if
    
		for i = 1 to nFieldCount
			'response.write i & ":" & aFields(i,0) & "<br />"
			oRS(i) = aFields(i,0)
		next

		oRS.update
		
		'nID = oRS.fields(sFilter)
		oRS.close
		Dim sSQLL : sSQLL = "SELECT TOP 1 " & sFilter & " FROM " & sTable & " ORDER BY " & sFilter & " DESC;"  
		nErr = getRowsBySQL(DSN_PARTNERS, sSQLL, aFx1)
		nID = aFx1(0,0)
		set oRS = nothing
		oConn.close
		set oConn = nothing
		
		aFields(nIdField,0) = nID
	else
		'// bad input parms //
		nErr = 1
	end if
end function

'// generic sql executor //
sub sqlExec(sDSN, sSQL)
	dim oConn
	if len("" & sSQL) > 0 then
		set oConn = server.createobject("ADODB.Connection")
		oConn.open sDSN
		
		oConn.execute sSQL
		oConn.close
		set oConn = nothing
	end if
end sub

'// save record to db //
function savePartner(aFields)
	dim nErr:nErr = 0
	if isarray(aFields) AND not isNull(aFields) then
		nErr = saveRecord(DSN_PARTNERS, "tblPartners", "paID", aFields, PA_ID, PA_UBOUND)
	else
		nErr = 1	'// bad input supplied //
	end if
	saveDistributor = nErr
end function

'// load record from db //
function loadPartner(nID, aFields)
	dim nErr:nErr = 0
	dim sSQL
	if nID > 0 then
		sSQL = "SELECT PA.* FROM tblPartners AS PA WHERE PA.paID=" & nID & ";"
		nErr = getRowsBySQL(DSN_PARTNERS, sSQL, aFields)
	else
		nErr = 1	'// no id supplied //
		aFields = null
	end if
	loadDistributor = nErr
end function

'// delete record //
function deletePartner(nID)
	dim nErr:nErr = 0
	dim sSQL, sPath, sTmp
	if nID > 0 then
		sSQL = "DELETE FROM tblPartners WHERE paID=" & nID & ";"
		sqlExec DSN_PARTNERS, sSQL
	else
		nErr = 1	'// no id supplied //
		aFields = null
	end if
	deleteDistributor = nErr
end function

'// generic drop down creator //
function genericDropDownFromDB(sDSN, sSQL, sName, nID)
	dim sTmp:sTmp = ""
	dim oConn, oRS
	if (len("" & sSQL) > 0) and (len("" & sName) > 0) then
		sTmp = "<select name=""" & sName & """ class=""dropDown"">" & vbcrlf
		sTmp = sTmp & "<option value=""0"">Please select ------&gt;</option>" & vbcrlf
		set oConn = server.createobject("ADODB.Connection")
		oConn.open sDSN
		set oRS = oConn.execute(sSQL)
		if not oRS.eof then
			while not oRS.eof
				sTmp = sTmp & "<option value=""" & oRS(0) & """"
				if nID = oRS(0) then sTmp = sTmp & " selected"
				sTmp = sTmp & ">" & oRS(1) & "</option>" & vbcrlf
				oRS.movenext
			wend
		end if
		oRS.close
		set oRS = nothing
		oConn.close
		set oConn = nothing
		sTmp = sTmp & "</select>" & vbcrlf
	else
		sTmp = ""
	end if
	genericDropDownFromDB = sTmp
end function

'// generic drop down creator //
function publicDropDownFromDB(sDSN, sSQL, sName, nID)
	dim sTmp:sTmp = ""
	dim oConn, oRS
	if (len("" & sSQL) > 0) and (len("" & sName) > 0) then
		sTmp = "<select name=""" & sName & """ class=""dropDown"">" & vbcrlf
		sTmp = sTmp & "<option value=""0"">Choose A Country</option>" & vbcrlf
		set oConn = server.createobject("ADODB.Connection")
		oConn.open sDSN
		set oRS = oConn.execute(sSQL)
		if not oRS.eof then
			while not oRS.eof
				sTmp = sTmp & "<option value=""" & oRS(0) & """"
				if nID = oRS(0) then sTmp = sTmp & " selected"
				sTmp = sTmp & ">" & oRS(1) & "</option>" & vbcrlf
				oRS.movenext
			wend
		end if
		oRS.close
		set oRS = nothing
		oConn.close
		set oConn = nothing
		sTmp = sTmp & "</select>" & vbcrlf
	else
		sTmp = ""
	end if
	publicDropDownFromDB = sTmp
end function

'// generic select creator //
function genericSelectFromDB(sDSN, sSQL, sName, nID, nSz)
	dim sTmp:sTmp = ""
	dim oConn, oRS
	if (len("" & sSQL) > 0) and (len("" & sName) > 0) then
		sTmp = "<select name=""" & sName & """ multiple size=""" & nSz & """>" & vbcrlf
		sTmp = sTmp & "<option value=""0"">Please select</option>" & vbcrlf
		set oConn = server.createobject("ADODB.Connection")
		oConn.open sDSN
		set oRS = oConn.execute(sSQL)
		if not oRS.eof then
			while not oRS.eof
				sTmp = sTmp & "<option value=""" & oRS(0) & """"
				if nID = oRS(0) then sTmp = sTmp & " selected"
				sTmp = sTmp & ">" & oRS(1) & "</option>" & vbcrlf
				oRS.movenext
			wend
		end if
		oRS.close
		set oRS = nothing
		oConn.close
		set oConn = nothing
		sTmp = sTmp & "</select>" & vbcrlf
	else
		sTmp = ""
	end if
	genericSelectFromDB = sTmp
end function
%>

