<%
' // sPage is configured in templateTopNav.asp

dim sYourDetails : sYourDetails = "Your Details"
dim sOptions : sOptions = "Options"
dim sConfirmation : sConfirmation = "Confirm"
dim sPayment : sPayment = "Payment"
dim sReceipt : sReceipt = "Receipt"
dim sUpload : sUpload = "<span class=""highlight"">UPLOAD</span>"

if instr(sPage, "register.asp") or instr(sPage, "registerdetails.asp") or instr(sPage, "registerview.asp") or instr(sPage, "login.asp") then
    sYourDetails = "<strong>" & sYourDetails & "</strong>"
end if
if instr(sPage, "options.asp") then
    sOptions = "<strong>" & sOptions & "</strong>"
end if
if instr(sPage, "confirm.asp") then
    sConfirmation = "<strong>" & sConfirmation & "</strong>"
end if
'if instr(sPage, "register.asp") then
'    sPayment = "<strong>" & sPayment & "</strong>"
'end if
if instr(sPage, "receipt.asp") or instr(sPage, "printreceipt.asp") then
    sReceipt = "<strong>" & sReceipt & "</strong>"
end if
if instr(sPage, "upload.asp") then
    sUpload = sUpload 
end if
%>
     <div id="breadcrumb">
       You are in: <span class="coloured"><%=sYourDetails %> &gt; <%=sOptions %> &gt; <%=sConfirmation %> &gt; <%=sPayment %> &gt; <%=sReceipt %> </span>
    </div>
   