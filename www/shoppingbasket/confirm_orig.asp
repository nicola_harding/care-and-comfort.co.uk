<!--#include virtual="/includes/careandcomfortAPI.asp"-->
<!--#include virtual="/ecomm/setup.asp"-->
<!--#include virtual="/ecomm/countryselector.asp"-->
<!--#include virtual="/shoppingbasket/protex_functions.asp"-->

<!--#include virtual="/cms/setup.asp"-->
<!-- #include virtual="/includes/dates.asp" -->
<%
dim nErr:nErr=0
dim nMeID, nCtID, nOdID, sX, nDiscountVal, sPromoError

session("isSuccessYes") = ""
session("hasBeen")      = ""
session("promocode")    = "" 

if len("" & request("promo")) > 0 then
    session("promocode") = request("promo")   
end if 

nMeID = CLng("0" & session("meID"))
nCtID = CLng("0" & session("ctID"))
nOdID = CLng("0" & session("odID"))

if (nMeID = 0) or (nCtID = 0) then response.redirect "./"

'// let's update the cart with the member ID //
dim aMember     :aMember=null
dim aCart       :aCart=null
dim aCartItems  :aCartItems=null
dim sSQL        :sSQL=""
dim nPostZone   :nPostZone=0
dim bVAT        :bVAT=true
dim nPostage    :nPostage=0

nErr = loadMember(nMeID, aMember)
if nErr > 0 then
    response.redirect "./error.asp"
end if

nErr = loadCart(nCtID, aCart)
if nErr = 0 then
    aCart(CT_KEYFMEID, 0) = nMeID
    saveCart(aCart)
    '// now let's calculate postage etc //
    if aMember(ME_USEALTADDRESS, 0) then
        nPostZone = mid(aMember(ME_ALTADDRESS5, 0), 4, 1)
        bVAT = (mid(aMember(ME_ALTADDRESS5, 0), 6, 1))
    else
        nPostZone = mid(aMember(ME_ADDRESS5, 0), 4, 1)
        bVAT = (mid(aMember(ME_ADDRESS5, 0), 6, 1))
    end if

    if bVAT = 1 then
        bVAT = true
    else
        bVAT = false
    end if
    
    sSQL = "SELECT CI.*, PR.keyFpcID, PR.keyFwcID, PR.prPrice, PR.prPromoPrice, PR.prPromo, PR.keyFptID FROM tblCartItem AS CI LEFT JOIN tblProduct AS PR ON CI.keyFprID=PR.prID WHERE CI.keyFctID=" & nCtID & " AND CI.keyFinID=" & INST_ID & ";"
    nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aCartItems)
    if nErr = 0 then
        nErr = calcCartPostage(aCartItems, nPostZone, nPostage)
        if nErr > 0 then
          response.redirect "./error.asp"
        end if
    else
        '// cart is empty //
        response.redirect "./"
    end if
else
    response.redirect "./error.asp"
end if

'if sAct = "vchr" then
'	processVoucherForm sX, nMemberID
'end if
%>

<!--#include virtual="/includes/doctype.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title><%=SHOP_NAME%></title>
    <!--#include virtual="/includes/mainCSS.asp" -->
    <script language="JavaScript" type="text/javascript">
        <!--
        function showTandC(){
	        var tandcWin;
	        tandcWin = window.open("/termsandconditions/index.asp","TandC","height=600,width=680,scrollbars,menubar,resizable");
	        return false;
        }
        //-->
    </script>
    <%=javaSetDropDown("registration")%>
</head>
<!--#include virtual="/includes/body.asp" -->
<!--#include virtual="/includes/templateStart.asp" -->
<!--#include virtual="/includes/templateTopNav.asp" -->
<!--#include virtual="/includes/templateBeginContent.asp" -->
<!-- Begin Content --> 
    <div class="contact_block"> 
      <%=nvCmsGetContentHTML(CMS_CONTENT_ID_CONTACT_SUMMARY)%>     
    </div> 
    
    <!--#include virtual="/includes/templateBasketbreadCrumb.asp" -->  
     
    <!--div id="center_text_block"-->  
        <h2>Confirm Order</h2>
        <br/>
        <%=showCartRO(nCtID, aMember, BSKT_HDR_CLR, BSKT_ROW_CLR, BSKT_BGD_CLR, bVAT, nPostZone, nPostage)%>
    <!--/div-->
<!-- End Content -->
<!--#include virtual="/includes/templateEndContent.asp" -->

<!-- LHS navigation -->
<!--#include virtual="/includes/templateLHSnav.asp" -->
<!-- RHS navigation -->
<!--#in clude virtual="/includes/templateRHSnav.asp" -->

<!--#include virtual="/includes/templateFooter.asp" -->
<!--#include virtual="/includes/templateEnd.asp" -->

<%
function showCartRO(nCtID, aMember, sHedClr, sRowClr, sBkgClr, bVat, nPostZone, nPostage)
  dim nErr          : nErr=0
  dim sTmp          : sTmp=""
  dim aCartItems    : aCartItems=null
  dim nIdx          : nIdx=0
  dim sSQL          : sSQL=""
  dim nTotal, nSubTotal, nVat, nLineItemVAT, nTotalVAT, nTotalNoVAT, sName, sLeadTime, nPrice, nQty, nDiscountedTotal
  dim nProtexTotal
  dim sMbrAdd, sDelAdd
  nIdx = 0
  sTmp = ""
  if len("" & sLinkCont) = 0 then
    sLinkCont = "/shop/"
  end if  
  if nCtID > 0 then
    sSQL = "SELECT PR.*, CI.ciQuantity, LT.ltText, CI.ciID, PT.ptName, NT.ntText" 
    sSQL = sSQL & " FROM (((((tblCartItem AS CI LEFT JOIN tblProduct AS PR ON CI.keyFprID=PR.prID)" 
    sSQL = sSQL & " LEFT JOIN tblProduct AS PAR ON PR.prParentID=PAR.prID)"		
	sSQL = sSQL & " LEFT JOIN tblLeadTime AS LT ON PAR.keyFltID=LT.ltID)" 
    sSQL = sSQL & " LEFT JOIN tblProductType AS PT ON PR.keyFptID = PT.ptID)" 
    sSQL = sSQL & " LEFT JOIN tblNote as NT ON CI.keyFntID = NT.ntID)"
    sSQL = sSQL & " WHERE CI.keyFctID=" & nCtID & " AND CI.keyFinID=" & INST_ID & " AND PR.keyFinID=" & INST_ID & " AND ((LT.keyFinID IS NULL) OR LT.keyFinID=" & INST_ID & ")"
    sSQL = sSQL & " AND PR.prDisplay=1 AND PR.prOnSale=1"
    sSQl = sSQL & " ORDER BY CI.ciID ASC;"
    'response.Write "<br>sSQL=" & sSQL 
    nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aCartItems)
    if nErr = 0 then
%>
    <table align="center" width="95%" border="0" bgcolor="<%=sBkgClr%>" cellspacing="0" cellpadding="0" >
    <tr bgcolor="<%=sHedClr%>" class="cartHeaders">
    <td>&nbsp;</td>
    <td>Description</td>
    <td>&nbsp;</td>
    <td align="center">Quantity</td>
    <td>&nbsp;</td>
    <td align="right">Price&nbsp;&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">Line&nbsp;Total</td>
    <td>&nbsp;</td></tr>
<%
for nIdx=0 to UBound(aCartItems, 2)
    nCiID = aCartItems(PR_UBOUND + 3, nIdx)
    sName = "<strong>" & aCartItems(PR_NAME, nIdx) & "</strong>"  
    ' // Include colour and size description
    if len(aCartItems(PR_UBOUND + 5, nIdx)) > 0 then
        sName = sName & "<br />" & aCartItems(PR_UBOUND + 5, nIdx) 
    end if     
    sLeadTime = aCartItems(50, nIdx) 
    if len("" & sLeadTime) = 0 then
        sLeadTime = DFLT_LEADTIME_IS_ECOMM
    end if 
    if aCartItems(PR_PROMO, nIdx) then
        nPrice = aCartItems(PR_PROMOPRICE, nIdx)
        nVAT = aCartItems(PR_PROMOVAT, nIdx)
    else
        nPrice = aCartItems(PR_PRICE, nIdx)
        nVAT = aCartItems(PR_VAT, nIdx)
    end if
    nQty            = aCartItems(PR_UBOUND + 1, nIdx)
    nSubTotal       = nQty * nPrice
    nLineItemVAT    = nQty * nVAT  
    nTotalVAT       = nTotalVAT + nLineItemVAT
    nTotal          = nTotal + nSubTotal
    nTotalNoVAT     = nTotal - nTotalVAT
  
    
    nPostage    = formatnumber(nPostage, 4) 
    nTotalVAT   = formatnumber(nTotalVAT, 4)  
%>
<tr bgcolor="<%=sRowClr%>" class="dark_font">
    <td>&nbsp;</td>
    <td valign="top"><%=sName%><br><small>Delivery - <%=sLeadTime%></small></td>
    <td>&nbsp;</td>
    <td valign="top" align="center"><%=nQty%></td>
    <td>&nbsp;</td>
    <td valign="top" align="right">&pound;<%=nPrice%></td>
    <td>&nbsp;</td>
    <td valign="top" align="right">&pound;<%=nSubTotal%></td>
    <td>&nbsp;</td>
</tr>
<%
next
%>
<tr bgcolor="<%=sHedClr%>" class="cartHeaders">
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">Sub&nbsp;Total</td>
    <td>&nbsp;</td>
    <td align="right">&pound;<%=nSubTotal%></td>
    <td>&nbsp;</td>
</tr>
<%
if bVAT or (VAT_OPTS = 0) then
  nTotal = nTotal + nPostage
%>
<tr bgcolor="<%=sRowClr%>">
    <td>&nbsp;</td>
    <td colspan="3">VAT Included</td>
    <td>&nbsp;</td>
    <td align="right">(&pound;<%=left(nTotalVAT, len(nTotalVAT)-2)%>)</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="1">&nbsp;</td>
</tr>
<%
else
    nTotal = nTotal - nTotalVAT + nPostage
%>
<tr bgcolor="<%=sRowClr%>">
    <td>&nbsp;</td>
    <td colspan="3">VAT</td>
    <td>&nbsp;</td>
    <td align="right">-&pound;<%=left(nTotalVAT, len(nTotalVAT)-2)%></td>
    <td>&nbsp;</td>
    <td align="right">-&pound;<%=left(nTotalVAT, len(nTotalVAT)-2)%></td>
    <td colspan="1">&nbsp;</td>
</tr>
<%
end if
%>
<tr>
    <td>&nbsp;</td>
    <td colspan="3">Postage &amp; Packaging</td>
    <td>&nbsp;</td>
    <td align="right">&pound;<%=left(nPostage, len(nPostage)-2)%></td>
    <td>&nbsp;</td>
    <td align="right">&pound;<%=left(nPostage, len(nPostage)-2)%></td>
    <td colspan="1">&nbsp;</td>
</tr>
<%
if len("" & session("promocode")) > 0 then
    nErr = calcPromotionalDiscount(session("promocode"), nTotal, nDiscountVal)

    if nErr = 10 then
        sPromoError = "<p class=""red_text"">Incorrect promotional code entered.</p><br />"        
    else
        if nErr = 20 then
            sPromoError = "<p class=""red_text"">The promotion has ended.</p><br />"         
        end if
    end if
end if

if nDiscountVal > 0 then
    nDiscountVal        = formatnumber(nDiscountVal, 4) 
    nDiscountedTotal    = formatnumber((nTotal - nDiscountVal), 4)
    nProtexTotal = nDiscountedTotal
%>
    <tr bgcolor="<%=sRowClr%>" >
    <td>&nbsp;</td>
    <td>Discount:</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right"><strong>-&pound;<%=left(nDiscountVal, len(nDiscountVal)-1)%></strong></td>
    <td>&nbsp;</td></tr>

    <tr bgcolor="<%=sHedClr%>" class="cartHeaders">
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">Total</td>
    <td>&nbsp;</td>
    <td align="right"><strong>&pound;<%=left(nDiscountedTotal, len(nDiscountedTotal)-2)%></strong></td>
    <td>&nbsp;</td></tr>
<%
else    
%>
    <tr bgcolor="<%=sHedClr%>" class="cartHeaders">
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td align="right"><strong>Total</strong></td>
        <td>&nbsp;</td>
        <td align="right"><strong>&pound;<%=nTotal%></strong></td>
        <td colspan="1">&nbsp;</td>
    </tr>
<%
end if
' // Always end the total amount (Exclude any discounts, as the main protx function will calculate it again later)
' // This allows the protx receipt to reflect the original amount and the discounted amount, coz ppl like to see they saved money ;)
nProtexTotal = nTotal

response.write "</table>"

sMbrAdd = ""
sDelAdd = ""
sMbrAdd = sMbrAdd & "&nbsp;" & trim("" & aMember(ME_TITLE, 0) & " " & aMember(ME_FIRSTNAME, 0) & " " & aMember(ME_SURNAME, 0)) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aMember(ME_ADDRESS1, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aMember(ME_ADDRESS2, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aMember(ME_ADDRESS3, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aMember(ME_ADDRESS4, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aMember(ME_POSTCODE, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & getCountry(aMember(ME_ADDRESS5, 0)) & "<br />" & vbcrlf
if aMember(ME_USEALTADDRESS, 0) then
  sDelAdd = sDelAdd & "&nbsp;" & trim("" & aMember(ME_ALTTITLE, 0) & " " & aMember(ME_ALTFIRSTNAME, 0) & " " & aMember(ME_ALTSURNAME, 0)) & "<br />" & vbcrlf
  sDelAdd = sDelAdd & "&nbsp;" & aMember(ME_ALTADDRESS1, 0) & "<br />" & vbcrlf
  sDelAdd = sDelAdd & "&nbsp;" & aMember(ME_ALTADDRESS2, 0) & "<br />" & vbcrlf
  sDelAdd = sDelAdd & "&nbsp;" & aMember(ME_ALTADDRESS3, 0) & "<br />" & vbcrlf
  sDelAdd = sDelAdd & "&nbsp;" & aMember(ME_ALTADDRESS4, 0) & "<br />" & vbcrlf
  sDelAdd = sDelAdd & "&nbsp;" & aMember(ME_ALTPOSTCODE, 0) & "<br />" & vbcrlf
  sDelAdd = sDelAdd & "&nbsp;" & getCountry(aMember(ME_ALTADDRESS5, 0)) & "<br />" & vbcrlf
else
  sDelAdd = sMbrAdd
end if
sMbrAdd = replace(sMbrAdd, "&nbsp;<br />", "")
sDelAdd = replace(sDelAdd, "&nbsp;<br />", "")
%>

<table align="center" width="100%" border="0" bgcolor="<%=sBkgClr%>" cellspacing="0" cellpadding="2" class="text">
    <tr><td width="45%">&nbsp;</td><td width="10%">&nbsp;</td><td width="45%">&nbsp;</td></tr>
    <tr>
        <td bgcolor="<%=sHedClr%>" class="cartHeaders">&nbsp;Billing Address&nbsp;</td>
        <td>&nbsp;</td>
        <td bgcolor="<%=sHedClr%>" class="cartHeaders">&nbsp;Delivery Address&nbsp;</td>
    </tr>
    <tr bgcolor="<%=sRowClr%>">
        <td valign="top">
        <p><%=sMbrAdd%></p>
        </td>
        <td>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td valign="top">
         <p><%=sDelAdd%></p></td></tr>
    <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
</table>

<%
dim sPostOpt, nNoteID, sGiftMsg, nNote2ID, sDelMsg, bPartShip
dim blnIsPostInfoRetrieved : blnIsPostInfoRetrieved = false

nErr = getPostOptions(nCtID, sPostOpt, nNoteID, sGiftMsg, nNote2ID, sDelMsg, bPartShip)
if nErr = 0 then
    blnIsPostInfoRetrieved = true
    if len(sDelMsg) > 0 then
        sTmp = sTmp & "<table width=""100%"" border=""0"" bgcolor=""" & sBkgClr & """ cellpadding=""2"" cellspacing=""0"" class=""text"">"
        sTmp = sTmp & "<tr><td bgcolor=""" & sHedClr & """ class=""cartHeaders"">&nbsp;Order Options&nbsp;</td></tr>"
        sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;Your order will contain the following delivery instruction:&nbsp;</td></tr>"
        sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td><i>" & replace(sDelMsg, vbcrlf, "&nbsp;<br />") & "</i></td></tr>"
    end if
    sTmp = sTmp & "</table>"
end if
response.write sTmp
sTmp = ""
%>

<p class="form_title">Are these details correct?</p>
<p>
    <a href="./index.asp"><img src="/ecomm/buttons/button_noeditorder.gif" border="0" alt="No, Edit Order" /></a>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="./register.asp"><img src="/ecomm/buttons/button_noeditaddress.gif" border="0" alt="No, Edit Address" /></a>
</p>

<%
if len("" & sPromoError) > 0 then
    response.Write sPromoError
end if
%>

<table summary="Promotional Discount Options" border="0" bgcolor="<%=BSKT_BGD_CLR%>" cellpadding="0" width="100%" cellspacing="0">
    <tr bgcolor="<%=BSKT_PROMO_BGD_CLR%>" class="cartHeaders">
        <td colspan="2"><b>&nbsp;Use a promotional discount code?&nbsp;</b></td>
    </tr>
    <tr bgcolor="<%=BSKT_ROW_CLR%>">
        <td colspan=2 valign="top">	
            <FORM action="/shoppingbasket/confirm.asp" method="post" id="frmPromotion" name="frmPromotion" class="main_form"> 
            <fieldset class="fields">
                <label for="promo" class="standard">Please enter your code here:</label>
                <input type="text" class="standard" name="promo" value="<%=session("promocode")%>">
                <br /><br /><br />
                <input type="image" src="/ecomm/buttons/button_submit.gif" class="image_right" value="Update my total to reflect my promotional code discount" alt="Update my total to reflect my promotional code discount" />                        
            </fieldset>
            </FORM>
        </td>
    </tr>
</table>
   
    
<table summary="Payment Options" border="0" bgcolor="<%=BSKT_BGD_CLR%>" cellpadding="0" width="100%" cellspacing="0">
<%
if "" & nPostZone="9" then
%>
    <tr bgcolor="<%=BSKT_HDR_CLR%>" class="cartHeaders">
        <td colspan="2"><b>&nbsp;Pay for your order?&nbsp;</b></td>
    </tr>

<%
'*** SAVE LOCAL COPY OF ORDER, BACKS UP TRANSACTIONS, this pulled from Pay.asp

'// this is where we are going to do all of the hard work //
'// take one cart and make one order - couldnt be easier //
dim aOrder      : aOrder = null
dim aOrderItem  : aOrderItem = null

nErr = loadMember(nMeID, aMember)
if nErr > 0 then
  response.redirect "./error.asp"
end if

nErr = loadCart(nCtID, aCart)
if nErr = 0 then
    aCart(CT_KEYFMEID, 0) = nMeID
    saveCart(aCart)
    '// now lets calculate postage etc //
    if aMember(ME_USEALTADDRESS, 0) then
        nPostZone = mid(aMember(ME_ALTADDRESS5, 0), 4, 1)
        bVAT = (mid(aMember(ME_ALTADDRESS5, 0), 6, 1) = "1")
    else
        nPostZone = mid(aMember(ME_ADDRESS5, 0), 4, 1)
        bVAT = (mid(aMember(ME_ADDRESS5, 0), 6, 1) = "1")
    end if
    
    sSQL = "SELECT CI.*, PR.keyFpcID, PR.keyFwcID, PR.prPrice, PR.prPromoPrice, PR.prPromo, PR.keyFptID FROM tblCartItem AS CI LEFT JOIN tblProduct AS PR ON CI.keyFprID=PR.prID WHERE CI.keyFctID=" & nCtID & " AND CI.keyFinID=" & INST_ID & ";"
    'sSQL = "SELECT CI.*, PR.keyFpcID, PR.keyFwcID FROM tblCartItem AS CI LEFT JOIN tblProduct AS PR ON CI.keyFprID=PR.prID WHERE CI.keyFctID=" & nCtID & " AND CI.keyFinID=" & INST_ID & ";"
    nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aCartItems)
    if nErr = 0 then
        ' // nix says: This is doing no logic at the mo as their postage is included in products
        'nErr = calcCartPostage(aCartItems, nPostZone, nPostage)
        'if nErr > 0 then
        '    response.redirect "./error.asp"
        'end if
    else
        '// cart is empty //
        response.redirect "./"
    end if
    aCartItems = null
else
    response.redirect "./error.asp"
end if

' // Nix: Not calling this as it is called already within this function
'nErr = getPostOptions(nCtID, sPostOpt, nNoteID, sGiftMsg, nNote2ID, sDelMsg, bPartShip)

if blnIsPostInfoRetrieved then
  '// lets do the notes if we need to //
  if len("" & sGiftMsg) > 0 then
    if nNoteID > 0 then
      '// update the note //
      nErr = updateNote(nNoteID, sGiftMsg)
    else
      '// create the note //
      nNoteID = addNote(sGiftMsg)
    end if
  elseif nNoteID > 0 then
    '// delete the note //
    nErr = deleteNote(nNoteID)
    nNoteID = 0
  end if
  if len("" & sDelMsg) > 0 then
    if nNote2ID > 0 then
      '// update the note //
      nErr = updateNote(nNote2ID, sDelMsg)
    else
      '// create the note //
      nNote2ID = addNote(sDelMsg)
    end if
  elseif nNote2ID > 0 then
    '// delete the note //
    nErr = deleteNote(nNote2ID)
    nNote2ID = 0
  end if
  
    nErr = setPostOptions(nCtID, sPostOpt, nNoteID, sGiftMsg, nNote2ID, sDelMsg, bPartShip)
  
     nTotalNoVAT2 = nTotalNoVAT
     nVAT2        = nVAT
     nTotal2      = nTotal
  
    if nOdID > 0 then
        nErr = loadOrder(nOdID, aOrder)
        if nErr = 0 then
            '// empty out the OrderItems //
            sSQL = "DELETE FROM tblOrderItem WHERE keyFodID=" & nOdID & " AND keyFinID=" & INST_ID & ";"
            call sqlExec(DSN_ECOM_DB, sSQL)
        else
            redim aOrder(OD_UBOUND, 0)
            aOrder(OD_ID, 0) = 0
            aOrder(OD_KEYFINID, 0) = INST_ID
        end if
    else
        redim aOrder(OD_UBOUND, 0)
        aOrder(OD_ID, 0) = 0
        aOrder(OD_KEYFINID, 0) = INST_ID
    end if
    aOrder(OD_KEYFMEID, 0) = nMeID
    aOrder(OD_DATE, 0) = CDate(now())
    aOrder(OD_SESSIONID, 0) = session.sessionID
    aOrder(OD_IPADDRESS, 0) = request.servervariables("REMOTE_HOST")
    aOrder(OD_EMAIL, 0) = aMember(ME_EMAIL, 0)
    aOrder(OD_PHONE, 0) = aMember(ME_TEL, 0)
    aOrder(OD_TITLE, 0) = aMember(ME_TITLE, 0)
    aOrder(OD_FIRSTNAME, 0) = aMember(ME_FIRSTNAME, 0)
    aOrder(OD_SURNAME, 0)  = aMember(ME_SURNAME, 0)
    aOrder(OD_ADDRESS1, 0) = aMember(ME_ADDRESS1, 0)
    aOrder(OD_ADDRESS2, 0) = aMember(ME_ADDRESS2, 0)
    aOrder(OD_ADDRESS3, 0) = aMember(ME_ADDRESS3, 0)
    aOrder(OD_ADDRESS4, 0) = aMember(ME_ADDRESS4, 0)
    aOrder(OD_ADDRESS5, 0) = aMember(ME_ADDRESS5, 0)
    aOrder(OD_POSTCODE, 0) = aMember(ME_POSTCODE, 0)
    if aMember(ME_USEALTADDRESS, 0) then
        aOrder(OD_DELTITLE, 0)    = aMember(ME_ALTTITLE, 0)
        aOrder(OD_DELFIRSTNAME, 0)= aMember(ME_ALTFIRSTNAME, 0)
        aOrder(OD_DELSURNAME, 0)  = aMember(ME_ALTSURNAME, 0)
        aOrder(OD_DELADDRESS1, 0) = aMember(ME_ALTADDRESS1, 0)
        aOrder(OD_DELADDRESS2, 0) = aMember(ME_ALTADDRESS2, 0)
        aOrder(OD_DELADDRESS3, 0) = aMember(ME_ALTADDRESS3, 0)
        aOrder(OD_DELADDRESS4, 0) = aMember(ME_ALTADDRESS4, 0)
        aOrder(OD_DELADDRESS5, 0) = aMember(ME_ALTADDRESS5, 0)
        aOrder(OD_DELPOSTCODE, 0) = aMember(ME_ALTPOSTCODE, 0)
    else
        aOrder(OD_DELTITLE, 0)    = aMember(ME_TITLE, 0)
        aOrder(OD_DELFIRSTNAME, 0)= aMember(ME_FIRSTNAME, 0)
        aOrder(OD_DELSURNAME, 0)  = aMember(ME_SURNAME, 0)
        aOrder(OD_DELADDRESS1, 0) = aMember(ME_ADDRESS1, 0)
        aOrder(OD_DELADDRESS2, 0) = aMember(ME_ADDRESS2, 0)
        aOrder(OD_DELADDRESS3, 0) = aMember(ME_ADDRESS3, 0)
        aOrder(OD_DELADDRESS4, 0) = aMember(ME_ADDRESS4, 0)
        aOrder(OD_DELADDRESS5, 0) = aMember(ME_ADDRESS5, 0)
        aOrder(OD_DELPOSTCODE, 0) = aMember(ME_POSTCODE, 0)
    end if
    aOrder(OD_DELZONE, 0)     = nPostZone
    aOrder(OD_POSTTYPE, 0)    = sPostOpt
    aOrder(OD_POSTAGE, 0)     = nPostage
    '// others will be filled in after the next step //
    aOrder(OD_TRANSSUC, 0)    = false
    aOrder(OD_COMPLETEDALL, 0)= false 
    aOrder(OD_PARTSHIP, 0)    = bPartShip
    aOrder(OD_DISPATCHED, 0)  = false
    aOrder(OD_DISPATCHDATE, 0)= null
    aOrder(OD_KEYFN1ID, 0)    = nNoteId
    aOrder(OD_KEYFN2ID, 0)    = nNote2Id
    nErr = saveOrder(aOrder)
    if nErr = 0 then
        nErr = createOrderItems(aOrder(OD_ID, 0), nCtID, bVat, nPostage, nTotalNoVAT, nVAT, nTotal)  '// accepts postage and vatable - returns VAT and Totals //
    else
        response.redirect "./error.asp?err=Session%20expired"
    end if
  
    if nErr = 0 then
        aOrder(OD_TOTALNOVAT, 0)        = nTotalNoVAT2
        aOrder(OD_VAT, 0)               = nVAT2
        aOrder(OD_TOTALCHARGECALC, 0)   = nTotal2
        
        if nDiscountVal > 0 then
            aOrder(OD_PROMOCODE, 0) = session("promocode")
            aOrder(OD_PROMOTEXT, 0) = calcPromotionalText(session("promocode"), nTotal2)
            aOrder(OD_PROMODISC, 0) = nDiscountVal
        end if
        nErr = saveOrder(aOrder)
    else
        response.redirect "./error.asp?err=Session%20expired"
    end if
else
	response.redirect "./error.asp?err=Session%20expired"
end if

session("odID") = aOrder(OD_ID, 0)			
sCrypt          = compile_protex_stuff(aMember,nProtexTotal,nPostage)
%>
<tr bgcolor="<%=BSKT_ROW_CLR%>">
    <td colspan=2 valign="top">	
    <br />
    <FORM action="<%=vspSite%>" method="post" id="form1" name="form1"> 
        <input type="hidden" name="VPSProtocol" value="2.22">
        <input type="hidden" name="TxType" value="PAYMENT"><%'// 3 Types= PAYMENT( money taken straight away) Deferred(requires user to manually process in admin) and PREAUTH%>
        <input type="hidden" name="Vendor" value="<%=VendorName%>">
        <input type="hidden" name="Crypt" value="<%=sCrypt %>">
        <input type="image" src="/ecomm/buttons/button_yescontinue.gif" value="Yes, Continue and Pay By Card" alt="Yes, Continue and Pay By Card" />                        
    </FORM>
    </td>
</tr>
<tr bgcolor="<%=BSKT_ROW_CLR%>">
    <td colspan=2 align="center">	
        <br />Card payments are <strong><a href="http://www.protx.com" target="_blank">processed securely</a></strong> through <a href="http://www.protx.com" target="_blank">PROTX</a>.<br />
    </td>
</tr>
<tr>
    <td colspan="2" align="center">
        <img src="/ecomm/buttons/protx100_50.gif" width="100" height="50" alt="Payments Powered By Protx SECURE E-Payments." />
        <div>
	        <img src="/ecomm/buttons/american_express.gif" alt="American Express" />&nbsp;<img src="/ecomm/buttons/visa.gif" alt="Visa" />&nbsp;<img src="/ecomm/buttons/visaElectron.gif" alt="Visa Electron" />&nbsp;<img src="/ecomm/buttons/delta.gif" alt="Delta" />&nbsp;<img src="/ecomm/buttons/mastercard.gif" alt="Master Card" />&nbsp;<img src="/ecomm/buttons/maestro.gif" alt="Maestro" />&nbsp;<img src="/ecomm/buttons/switch.gif" alt="Switch" />&nbsp;<img src="/ecomm/buttons/solo.gif" alt="Solo" />
        </div>
    </td>
</tr>
<%
else
    '// outside of uk //
end if

response.write "</table>"

        else
            sTmp = "&nbsp;<br />Your shopping basket is currently empty."
        end if
    else
        sTmp = "&nbsp;<br />Your shopping basket is currently empty."
    end if
    showCartRO = sTmp
end function


'// accepts postage and vatable - returns VAT and Totals //
function createOrderItems(nOdID, nCtID, bVat, nPostage, nTotalNoVAT, nVAT, nTotal)
  dim nErr:nErr=0
  dim sSQL:sSQL=""
  dim nIdx:nIdx=0
  dim nJdx:nJdx=0
  dim aItems:aItems=null
  dim aOrderItem:aOrderItem=null
  dim sOrderDescipription  
  
  ' // array 0 --> 0,
  ' // array 1 --> " & nOdID & ",
  ' // array 2 --> PR.prCODE,
  ' // array 3 --> PR.prShortCode,
  ' // array 4 --> PT.ptName,
  ' // array 5 --> PR.prName,
  ' // array 6 --> PR.prPrice,
  ' // array 7 --> PR.prVAT,
  ' // array 8 --> 0,
  ' // array 9 --> 0,
  ' // array 10 --> 0,
  ' // array  --> 0,
  ' // array 12 --> CI.ciQuantity,
  ' // array  --> 1,
  ' // array 14 --> 0,
  ' // array  --> PR.prWrappable,
  ' // array 16 --> "",
  ' // array  --> "",
  ' // array 18 --> CI.keyFntID,
  ' // array  --> 0,
  ' // array 20 --> PR.keyFltID,
  ' // array  --> LT.ltText,
  ' // array 22 --> 0,
  ' // array  --> null,
  ' // array 24 --> "",
  ' // array  --> INST_ID,
  ' // array 26 --> PR.prIsChild,
  ' // array  --> PR.prPromo,
  ' // array 28 --> PR.prPromoPrice,
  ' // array  --> PR.prPromoVat,
  ' // array 30 --> PR.prStockQty,
  ' // array  --> PR.prStockTrigger,
  ' // array 32 --> PR.prColour,
  ' // array  --> PR.prSize,
  ' // array 34 --> PR.prParentID,
  ' // array  --> PRp.prName,
  ' // array 36 --> NT.ntText
  
    if (nOdID > 0) and (nCtID > 0) then
        sSQL = "SELECT 0," & nOdID & ",PR.prCODE,PR.prShortCode,PT.ptName,PR.prName,PR.prPrice,PR.prVAT,0,0,0,0,CI.ciQuantity,1,0," _
            & " PR.prWrappable,'','',CI.keyFntID,0,PR.keyFltID,LT.ltText,0,null,''," & INST_ID _
            & ",PR.prIsChild,PR.prPromo,PR.prPromoPrice,PR.prPromoVat,PR.prStockQty,PR.prStockTrigger,PR.prColour,PR.prSize," _
            & " PR.prParentID,PRp.prName,NT.ntText" _
            & " FROM (((((tblCartItem AS CI LEFT JOIN tblProduct AS PR ON CI.keyFprID=PR.prID)" _
            & " LEFT JOIN tblProductType AS PT ON PR.keyFptID=PT.ptID)" _ 
            & " LEFT JOIN tblProduct AS PAR ON PR.prParentID=PAR.prID)" _		
            & " LEFT JOIN tblLeadTime AS LT ON PAR.keyFltID=LT.ltID)" _ 
            & " LEFT JOIN tblProduct AS PRp ON  PR.prParentID = PRp.prID)" _
            & " LEFT JOIN tblNote as NT ON CI.keyFntID = NT.ntID" _
            & " WHERE PR.prOnSale=1 AND CI.keyFctID=" & nCtID & " AND CI.keyFinID=" & INST_ID & " AND PR.keyFinID=" & INST_ID & " AND ((PT.keyFinID IS NULL) OR PT.keyFinID=" & INST_ID & ") AND ((LT.keyFinID IS NULL) OR LT.keyFinID=" & INST_ID & ")" _
            & " ORDER BY CI.ciID ASC;"

    nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aItems)
    if nErr = 0 then
      for nIdx = 0 to UBound(aItems, 2)
        redim aOrderItem(OI_UBOUND, 0)
        for nJdx = 0 to OI_UBOUND
          aOrderItem(nJdx, 0) = aItems(nJdx, nIdx)
        next
		
        sOrderDescipription = aItems(36, nIdx)

        if aItems(OI_UBOUND + 1, nIdx) then
          if len("" & sOrderDescipription) > 0 then '// Colour and/or size
            aOrderItem(OI_DESCRIPTION, 0) = aOrderItem(OI_DESCRIPTION, 0) & ", " & sOrderDescipription
          end if
        end if		
        
        '// price //
        if aItems(OI_UBOUND + 2, nIdx) then
          aOrderItem(OI_UNITCOST, 0) = aItems(OI_UBOUND + 3, nIdx)
          aOrderItem(OI_UNITVAT, 0) = aItems(OI_UBOUND + 4, nIdx)
        end if
        '// availability //
        if len("" & aOrderItem(OI_LEADTIMETEXT, 0)) = 0 then
            if len("" & aItems(OI_LEADTIMETEXT, nIdx)) = 0 then
                aOrderItem(OI_LEADTIMETEXT, 0) = DFLT_LEADTIME_IS_SAVE
            else
                aOrderItem(OI_LEADTIMETEXT, 0) = aItems(OI_LEADTIMETEXT, nIdx)
            end if
        end if
        
        nVAT = (aOrderItem(OI_UNITVAT, 0) * aOrderItem(OI_QUANTITY, 0))
		nTotal = (aOrderItem(OI_UNITCOST, 0) * aOrderItem(OI_QUANTITY, 0))
		
        nErr = saveOrderItem(aOrderItem)
        aOrderItem = null
      next
		
      nTotalNoVAT = nTotal - nVat
      if bVat then
        nTotal = nTotal
      else
        nTotal = nTotalNoVAT
      end if
      nTotal = nTotal + nPostage
    end if
  else
    nErr = 1  '// bad input parms //
  end if
  createOrderItems = nErr
end function
%>