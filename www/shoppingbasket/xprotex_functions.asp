<%
' ***********************************************************************
' ** Script Name:    Functions.asp                                     **
' ** Version:      1.3  - 20-sep-05                                    **
' ** Author:      Mat Peck                                             **
' ** Function:    Contains simple procedures to encode, encrypt,       **
' **          decode, decrypt and split the information POSTed         **
' **           to and from VSP Form.                                   **
' **                                                                   **
' ** Revision History:                                                 **
' **  Version  Author     Date and notes                               **
' **    1.0    Mat Peck   18/01/2002 - First release                   **
' **    1.1    Mat Peck   07/03/2002 - Base64 routines patched         **
' **    1.2    Peter G    25-jan-05  - Update protocol 2.21 -> 2.22    **
' **    1.3    Ryan  M    20-sep-05  - Base64 patched+bespoke functions**
' ***********************************************************************

' ** Set variables to indentify the vendor **

VendorName            = PROTX_CLIENT_NAME 

'EncryptionPassword    = "n3qxb8QxCO7WUGAB"' simulator pwd
EncryptionPassword    = "z6eZK3bTeGR26TPi"

eoln = chr(13) & chr(10)

' ** Set up the Base64 arrays
const BASE_64_MAP_INIT ="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
dim nl
dim Base64EncMap(63)
dim Base64DecMap(127)

' **************************************************************************** 
' ** Your server's IP address or dns name and web app directory.  Full qualified **
' ** Examples: MyServer= "https://www.newco.com/asp-form-kit/", MyServer="192.168.0.1/asp-form-kit", MyServer="http://localhost/asp-form-kit/" **

MyServer              = PROTX_SITE_URL & "/shoppingbasket/"   

' ****************************************************************************
'  The protx site to send information to **

' // Live 


if IsEnvironmentLive(sHostHeader) then
    '** Live site - ONLY uncomment when going live **


	  if request.ServerVariables("REMOTE_ADDR") =  "84.12.51.209" then
	  vspSite = "https://ukvpstest.protx.com/vps2form/submit.asp"
	  else
	  '** Live site - ONLY uncomment when going live **
	  
	'vspSite = " https://ukvpstest.protx.com/VSPSimulator/VSPFormGateway.asp"

	  end if
	vspSite="https://ukvps.protx.com/vps2form/submit.asp"
else
    ' // pre-live env
    if IsEnvironmentPreLive(sHostHeader) then
        '** Test site for wallace while go live has not happened **
        'vspSite = " https://ukvpstest.protx.com/VSPSimulator/VSPFormGateway.asp"
        vspSite = "https://ukvpstest.protx.com/vps2Form/submit.asp"
    else
        ' // dev env    
        if IsEnvironmentDev(sHostHeader) then
            ' dev env
            '** Simulator site **
            ' vspSite = " https://ukvpstest.protx.com/VSPSimulator/VSPFormGateway.asp"
			vspSite = "https://ukvpstest.protx.com/vps2form/submit.asp"
                       
        else
            ' // technically not possible so write an annoying message for someone to pic up on this logic error
            response.Write "<br />Critical: Environment has not been detected correctly. Site setting will not be initialised correctly"
        end if
    end if  
end if
'Response.Write vspSite
' *****************************************************************************   
' // CALL MAIN initcodecs() METHOD HERE:     
 call initcodecs()
 
' ** The initCodecs() routine initialises the Base64 arrays
PUBLIC SUB initCodecs()
      nl = "<P>" & chr(13) & chr(10)
      dim max, idx
      max = len(BASE_64_MAP_INIT)
      for idx = 0 to max - 1
           Base64EncMap(idx) = mid(BASE_64_MAP_INIT, idx + 1, 1)
      next
      for idx = 0 to max - 1
           Base64DecMap(ASC(Base64EncMap(idx))) = idx
      next
END SUB

' ** Base 64 Encoding function **
PUBLIC FUNCTION base64Encode(plain)
  call initCodecs
      if len(plain) = 0 then
           base64Encode = ""
           exit function
      end if
      dim ret, ndx, by3, first, second, third
      by3 = (len(plain) \ 3) * 3
      ndx = 1
      do while ndx <= by3
           first  = asc(mid(plain, ndx+0, 1))
           second = asc(mid(plain, ndx+1, 1))
           third  = asc(mid(plain, ndx+2, 1))
           ret = ret & Base64EncMap(  (first \ 4) AND 63 )
           ret = ret & Base64EncMap( ((first * 16) AND 48) + ((second \ 16) AND 15 ) )
           ret = ret & Base64EncMap( ((second * 4) AND 60) + ((third \ 64) AND 3 ) )
           ret = ret & Base64EncMap( third AND 63)
           ndx = ndx + 3
      loop
      ' check for stragglers
      if by3 < len(plain) then
           first  = asc(mid(plain, ndx+0, 1))
           ret = ret & Base64EncMap(  (first \ 4) AND 63 )
           if (len(plain) MOD 3 ) = 2 then
                second = asc(mid(plain, ndx+1, 1))
                ret = ret & Base64EncMap( ((first * 16) AND 48) +((second \16) AND 15 ) )
                ret = ret & Base64EncMap( ((second * 4) AND 60) )
           else
                ret = ret & Base64EncMap( (first * 16) AND 48)
                ret = ret & "="
           end if
           ret = ret & "="
      end if
      base64Encode = ret
 END FUNCTION
 
' ** Base 64 decoding function **
 PUBLIC FUNCTION base64Decode(scrambled)
      if len(scrambled) = 0 then
           base64Decode = ""
           exit function
      end if
      ' ignore padding
      dim realLen
      realLen = len(scrambled)
      do while mid(scrambled, realLen, 1) = "="
           realLen = realLen - 1
      loop
      do while instr(scrambled," ")<>0
          scrambled=left(scrambled,instr(scrambled," ")-1) & "+" & mid(scrambled,instr(scrambled," ")+1)
      loop
      dim ret, ndx, by4, first, second, third, fourth
      ret = ""
      by4 = (realLen \ 4) * 4
      ndx = 1
      do while ndx <= by4
           first  = Base64DecMap(asc(mid(scrambled, ndx+0, 1)))
           second = Base64DecMap(asc(mid(scrambled, ndx+1, 1)))
           third  = Base64DecMap(asc(mid(scrambled, ndx+2, 1)))
           fourth = Base64DecMap(asc(mid(scrambled, ndx+3, 1)))
           ret = ret & chr( ((first * 4) AND 255) +   ((second \ 16) AND 3))
           ret = ret & chr( ((second * 16) AND 255) + ((third \ 4) AND 15) )
           ret = ret & chr( ((third * 64) AND 255) +  (fourth AND 63) )
           ndx = ndx + 4
      loop
      ' check for stragglers, will be 2 or 3 characters
      if ndx < realLen then
           first  = Base64DecMap(asc(mid(scrambled, ndx+0, 1)))
           second = Base64DecMap(asc(mid(scrambled, ndx+1, 1)))
           ret = ret & chr( ((first * 4) AND 255) +   ((second \ 16) AND 3))
           if realLen MOD 4 = 3 then
                third = Base64DecMap(asc(mid(scrambled,ndx+2,1)))
                ret = ret & chr( ((second * 16) AND 255) + ((third \ 4) AND 15) )
           end if
      end if
      base64Decode = ret
 END FUNCTION
 
' ** The SimpleXor encryption algorithm. **
' ** NOTE:    This is a placeholder really.  Future releases of VSP Form will use AES or TwoFish.  Proper encryption **
' **       This simple function and the Base64 will deter script kiddies and prevent the "View Source" type tampering **
' **      It won't stop a half decent hacker though, but the most they could do is change the amount field to something **
' **      else, so provided the vendor checks the reports and compares amounts, there is no harm done.  Its still **
' **      more secure than the other PSPs who dont both encrypting their forms at all **

Public Function SimpleXor(InString,Key)
    Dim myIN, myKEY, myC, myPub
    Dim Keylist()    
    myIN = InString
    myKEY = Key  
    redim KeyList(Len(myKEY))  
    i = 1
    do while i<=Len(myKEY)
        KeyList(i) = Asc(Mid(myKEY, i, 1))
        i = i + 1
    loop         
    j = 1
    i = 1
    do while i<=Len(myIn)
        myC = myC & Chr(Asc(Mid(myIN, i, 1)) Xor KeyList(j))
        i = i + 1
        If j = Len(myKEY) Then j = 0
        j = j + 1
    loop
    SimpleXor = myC
End Function

' ** The getToken function. **
' ** NOTE:    A function of convenience that extracts the value from the "name=value&name2=value2..." VSP reply string **
' ** Works even if one of the values is a URL containing the & or = signs.  **
public function getToken(thisString,thisToken)
  ' Cant just rely on & characters because these may be provided in the URLs.
  Dim Tokens
  Dim subString
  Tokens = Array( "Status", "StatusDetail", "VendorTxCode", "VPSTxId", "TxAuthNo", "AVSCV2", "Amount", "AddressResult", "PostCodeResult", "CV2Result", "GiftAid", "3DSecureStatus", "CAVV" )
  if instr(thisString,thisToken+"=")=0 then 
    ' If the token isnt present, empty the output.  We can error later
    getToken=""    
  else  
    ' Right get the rest of the string
    subString=mid(thisString,instr(thisString,thisToken)+len(thisToken)+1)    
    ' Now strip off all remaining tokens if they are present. 
    i = LBound( Tokens )
    do while i <= UBound( Tokens )  
      'Find the next token and lop it off
      if Tokens(i)<>thisToken then
        if instr(subString,"&"+Tokens(i))<>0 then 
            substring=left(substring,instr(subString,"&"+Tokens(i))-1)
        end if      
      end if 
      i = i + 1  
    loop     
    getToken=subString  
  end if
end function


function compile_protex_stuff (aMember,nTotalNoPostage,nSubTotal,nPostageCost)
    dim nFinalInvTotal  : nFinalInvTotal = 0
    dim nDiscountVal    : nDiscountVal = 0
    dim nErr            : nErr = 0
    dim nPrice, nVat
    dim aProduct(48)
    dim nFieldIdx
    dim nOriginalPrice, nOriginalVAT
    dim sNoteText
    ' // Generate a unique transaction ID - with the customers shop order reference number
    randomize
    VendorTxcode = session("odID") & "-" & cstr(round(rnd*100000000))

    ' // Get Cart Info
    sSQL = "SELECT PR.*, CI.ciQuantity, LT.ltText, CI.ciID, PT.ptName, NT.ntText FROM ((((tblCartItem AS CI LEFT JOIN tblProduct AS PR ON CI.keyFprID=PR.prID) LEFT JOIN tblLeadTime AS LT ON PR.keyFltID=LT.ltID) LEFT JOIN tblProductType AS PT ON PR.keyFptID = PT.ptID) LEFT JOIN tblNote as NT ON CI.keyFntID = NT.ntID)"
    sSQL = sSQL & " WHERE CI.keyFctID=" & nCtID & " AND CI.keyFinID=" & INST_ID & " AND PR.keyFinID=" & INST_ID & " AND ((LT.keyFinID IS NULL) OR LT.keyFinID=" & INST_ID & ")"
    sSQL = sSQL & " AND PR.prDisplay=1 AND PR.prOnSale=1"
    sSQl = sSQL & " ORDER BY CI.ciID ASC;"
    nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aCartItems)

    ' // Build cart item description
    for nIdx = 0 to UBound(aCartItems, 2)
        ThisDescription = ThisDescription & aCartItems(PR_NAME,nIdx) 
        if len("" & aCartItems(PR_UBOUND+5,nIdx)) > 0 then
            sNoteText = cstr(getNoteExtraction(aCartItems(PR_UBOUND+5,nIdx), "text"))
            if len("" & sNoteText) > 0 then
		        ThisDescription = ThisDescription & " - " & sNoteText
            end if    	
        end if
        ThisDescription = ThisDescription & ", " 
    next
    ThisDescription = left(ThisDescription, len(ThisDescription)-2)

    ' // Calculate discount (if any)
    if len("" & session("promocode")) > 0 and (session("promocode") <> "") then
        nErr = calcPromotionalDiscount(session("promocode"), nTotalNoPostage, nDiscountVal)
        nFinalInvTotal = formatnumber((nSubTotal - nDiscountVal), 2)
    else
        nFinalInvTotal = nSubTotal
    end if  

    ' // Build the crypt string plaintext
    sTemp = "VendorTxCode=" & VendorTxcode & "&" ' unique transaction ID

	
    sTemp = sTemp + "Amount=" & nFinalInvTotal & "&" ' Total amount
    sTemp = sTemp + "Currency=GBP&" ' Currency
    sTemp = sTemp + "Description=" & ThisDescription & "&" ' items in cart?
    sTemp = sTemp + "SuccessURL=" & MyServer & "receipt.asp?PAY=CC&success=true&e=" & encodeID(session("odID")) ' & "&postOpt=" & session("POSTOPT")
    sTemp = sTemp + "&FailureURL=" & MyServer & "receipt.asp?PAY=CC&success=false&e=" & encodeID(session("odID")) ' & "&postOpt=" & session("POSTOPT")
    if len(aMember(ME_EMAIL, 0)) > 0 then
      sTemp = sTemp + "&CustomerEMail=" & aMember(ME_EMAIL, 0) & "&"
    end if
    if len(PROTX_VENDOR_EMAIL) > 0 then
      sTemp = sTemp + "VendorEMail=" & PROTX_VENDOR_EMAIL & "&"
    end if
    if len(aMember(ME_FIRSTNAME, 0)) > 0 then
      sTemp = sTemp + "CustomerName=" & aMember(ME_TITLE, 0) & " " & aMember(ME_FIRSTNAME, 0) & " " & aMember(ME_SURNAME, 0) & "&"
    end if    
    
    if aMember(ME_USEALTADDRESS, 0) then
        if len(aMember(ME_ALTADDRESS1, 0)) > 0 then
          sTemp = sTemp + "DeliveryAddress=" & aMember(ME_ALTADDRESS1, 0) & " " & aMember(ME_ALTADDRESS2, 0) & " " & aMember(ME_ALTADDRESS3, 0) & " " & aMember(ME_ALTADDRESS4, 0) & " " & getCountry(aMember(ME_ALTADDRESS5, 0)) & "&"
          sTemp = sTemp + "DeliveryPostCode=" & aMember(ME_ALTPOSTCODE, 0) & "&"
        else
            sTemp = sTemp + "DeliveryAddress=" & aMember(ME_ADDRESS1, 0) & " " & aMember(ME_ADDRESS2, 0) & " " & aMember(ME_ADDRESS3, 0) & " " & aMember(ME_ADDRESS4, 0) & " " & getCountry(aMember(ME_ADDRESS5, 0))  & "&"
            sTemp = sTemp + "DeliveryPostCode=" & aMember(ME_POSTCODE, 0) & "&"   
        end if
    else
        sTemp = sTemp + "DeliveryAddress=" & aMember(ME_ADDRESS1, 0) & " " & aMember(ME_ADDRESS2, 0) & " " & aMember(ME_ADDRESS3, 0) & " " & aMember(ME_ADDRESS4, 0) & " " & getCountry(aMember(ME_ADDRESS5, 0))  & "&"
        sTemp = sTemp + "DeliveryPostCode=" & aMember(ME_POSTCODE, 0) & "&"       
    end if    
    
    if len(aMember(ME_ADDRESS1, 0)) > 0 then
      sTemp = sTemp + "BillingAddress=" & aMember(ME_ADDRESS1, 0) & " " & aMember(ME_ADDRESS2, 0) & " " & aMember(ME_ADDRESS3, 0) & " " & aMember(ME_ADDRESS4, 0) & " " & getCountry(aMember(ME_ADDRESS5, 0))  & "&"
      sTemp = sTemp + "BillingPostCode=" & aMember(ME_POSTCODE, 0) & "&"
    else
      sTemp = sTemp + "BillingAddress=" & aMember(ME_ALTADDRESS1, 0) & " " & aMember(ME_ALTADDRESS2, 0) & " " & aMember(ME_ALTADDRESS3, 0) & " " & aMember(ME_ALTADDRESS4, 0) & " " & getCountry(aMember(ME_ALTADDRESS5, 0)) & "&"
      sTemp = sTemp + "BillingPostCode=" & aMember(ME_ALTPOSTCODE, 0) & "&"
    end if
    ' ** new 2.22 fields
    if len(aMember(ME_TEL, 0)) > 0 then
      sTemp = sTemp + "ContactNumber=" & aMember(ME_TEL, 0) & "&"
    end if
    if len(aMember(ME_FAX, 0)) > 0 then
      sTemp = sTemp + "ContactFax=" & aMember(ME_FAX, 0)  & "&"
    end if
    if ThisAllowGiftAid <> "" then
      sTemp = sTemp + "AllowGiftAid=" & ThisAllowGiftAid & "&"
    end if
    if ThisApplyAVSCV2 <> "" then
      sTemp = sTemp + "ApplyAVSCV2=" & ThisApplyAVSCV2 & "&"
    end if
    if ThisApply3DSecure <> "" then
      sTemp = sTemp + "Apply3DSecure=" & ThisApply3DSecure & "&"
    end if
    
    ' // This will be displayed in the protx email sent to the shop and customer
    ' // Item count - higher 4 vs the usual 3 - if we display a discount value:
    if nDiscountVal > 0 then
        sTemp = sTemp & "Basket=" & UBound(aCartItems, 2) + 4 & ":" 
    else
        sTemp = sTemp & "Basket=" & UBound(aCartItems, 2) + 3 & ":"        
    end if         
   ' Response.Write sTemp
	'Response.End
    for nIdx=0 to UBound(aCartItems, 2)   
        sNoteText = ""
        
        ' ================================================ 
        ' Put all the fields of the current array row into an array for the bespoke code to use
        for nFieldIdx = 0 to ubound(aCartItems, 1)
           ' Need this to stop population of aProduct when all the product db fields are retrieved from the aCartItems datasat
           if nFieldIdx <= ubound(aProduct) then
                aProduct(nFieldIdx) = aCartItems(nFieldIdx,nIdx)
           end if
        next                        
        nErr = getAppropriatePrice(aProduct, nPrice, nVAT, nOriginalPrice, nOriginalVAT)                    
        ' ================================================ 
        
        if len("" & aCartItems(PR_UBOUND+5,nIdx)) > 0 then
            sNoteText = " - " & cstr(getNoteExtraction(aCartItems(PR_UBOUND+5,nIdx), "text"))
        end if
               
        ' // Item product name - short description, quantity, item cost price before VAT, VAT, price (incl VAT), total line item price        
        sTemp = sTemp & aCartItems(PR_NAME,nIdx) & sNoteText & ":" & aCartItems(PR_UBOUND + 1,nIdx) & ":" & formatnumber(nPrice - nVAT, 4) & ":" & formatnumber(nVAT, 4) & ":" & nPrice & ":" & (nPrice * aCartItems(PR_UBOUND + 1,nIdx)) & ":"
    next
    ' // Need to add delivery charge in aswell
    sTemp = sTemp & "Delivery:---:---:---:---:" & formatnumber(nPostageCost, 2)
    
    if nDiscountVal > 0 then
        sTemp = sTemp & ":Discount:---:---:---:---:" & formatnumber(nDiscountVal, 3)
    end if
    sTemp = sTemp & ":Final total:::::" & nFinalInvTotal    
    sTemp = sTemp + "&EMailMessage=Thank you for shopping with " & SHOP_NAME & "."
   
    ' ** Encrypt the plaintext string for inclusion in the hidden field **
    crypt = base64Encode(SimpleXor(sTemp,EncryptionPassword))
    ' compile_protex_stuff = sTemp
    compile_protex_stuff = crypt
end function

%>
