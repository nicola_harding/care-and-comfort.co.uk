<!--#include virtual="/includes/careandcomfortAPI.asp"-->
<!--#include virtual="/ecomm/setup.asp"-->
<!--#include virtual="/ecomm/countryselector.asp"-->
<!--#include virtual="/cms/setup.asp"-->
<!--#include virtual="/includes/dates.asp" -->

<%
Dim g_sFldr:g_sFldr="shop"
Dim id, LHSid
LHSid=14
  
dim nErr:nErr=0
dim nMeID, nCtID, nOdID
dim aMember:aMember=null
dim aOrder:aOrder=null

nMeID = CLng("0" & session("meID"))
nCtID = CLng("0" & session("ctID"))
nOdID = CLng("0" & session("odID"))
if (nOdID = 0) OR (nMeID = 0) then
	response.redirect "./error.asp?err=Session%20expired"
end if

dim nTotalNoVat,nPostage,nVat,nTotalChargeCalc, bVat:bVat=true
dim sCCName, sCCType, sCCNo, sCCExpMM, sCCExpYY, sCCIssMM, sCCIssYY, sCCIssNo, sCCChkNo
dim dtTmp, dtNow, dtNowMM, dtNowYY, sMsg
dim sVchrCode, sVchrDesc, nVchrDisc, nDiscount

nErr = loadMember(nMeID, aMember)
if nErr = 0 then
  if VAT_OPTS > 0 then
    if aMember(ME_USEALTADDRESS, 0) then
      bVAT = (mid(aMember(ME_ALTADDRESS5, 0), 6, 1) = "1")
    else
      bVAT = (mid(aMember(ME_ADDRESS5, 0), 6, 1) = "1")
    end if
  end if
  sCCName = trim("" & aMember(ME_TITLE, 0) & " " & aMember(ME_FIRSTNAME, 0) & " " & aMember(ME_SURNAME, 0))
end if

nErr = loadOrder(nOdID, aOrder)
if nErr = 0 then
	nTotalNoVat = aOrder(OD_TOTALNOVAT, 0)
	nVat = aOrder(OD_VAT, 0)
	nPostage = aOrder(OD_POSTAGE, 0)
  if (VCHR_OPTS = 1) and (len("" & aOrder(OD_PROMOCODE, 0)) > 0) then
    sVchrCode = aOrder(OD_PROMOCODE, 0)
    sVchrDesc = aOrder(OD_PROMOTEXT, 0)
    nDiscount = aOrder(OD_PROMODISC, 0)
  else
    sVchrCode = ""
    sVchrDesc = ""
    nDiscount = 0
  end if
  if bVAT then
	  nTotalChargeCalc = nTotalNoVat + nVat + nPostage - nDiscount
  else
	  nTotalChargeCalc = nTotalNoVat + nPostage - nDiscount
  end if
else
	'// what now? no order details to show I guess //
	response.redirect "./error.asp?err=Session%20expired"
end if

if len("" & request.form("cancel.x")) > 0 then
  '// clear the cart //
  'session("OdID") = 0
  'session("CtID") = 0
  response.redirect("./receipt.asp?PAY=CC&e=" & encodeID(nOdID))
elseif request.form("submitted") = "yes" then
	'// process the credit card stuff and send them on to receipt.asp //
	sCCName= Trim("" & request.form("ccName"))
	sCCType	 = Trim("" & request.form("ccType"))
	sCCNo	 = Trim("" & request.form("ccNo"))
	sCCExpMM = Trim("" & request.form("ccExpMM"))
	sCCExpYY = Trim("" & request.form("ccExpYY"))
	sCCIssMM = Trim("" & request.form("ccIssMM"))
	sCCIssYY = Trim("" & request.form("ccIssYY"))
	sCCIssNo = Trim("" & request.form("ccIssNo"))
	sCCChkNo = Trim("" & request.form("ccChkNo"))
	'// okay we have all of the bits of the form we need just need to do some validation //
	'// do we have a card type? //
	if len("" & sCCName) < 3 then
		nErr = nErr + 1
		sMsg = sMsg & "Please enter the cardholder's name.<br />"
	end if
	if len("" & sCCType) = 0 then
		nErr = nErr + 1
		sMsg = sMsg & "Please select a card type.<br />"
	end if
	'// then the Card Number - min 15 digits (some Amex ONLY have 15) and only numerals //
	sCCNo = replace(sCCNo, "-", "")
	sCCNo = replace(sCCNo, " ", "")
	if len("" & sCCNo) < 15 or not isPosInt(sCCNo) then
		nErr = nErr + 1
		sMsg = sMsg & "Please enter a valid card number.<br />"
	end if
	dtNow = Now()
	dtNowMM = datepart("m", dtNow)
	dtNowYY = right(datepart("yyyy", dtNow), 2)
	'// check the expiry date //
	if len("" & sCCExpMM) = 2 and len("" & sCCExpYY) = 2 then
		'// manually check the month and year as VBScript can't be trusted //
		'// if year is greater than this year we are done //
		if CInt(sCCExpYY) = CInt(dtNowYY) then
			'// further checks required //
			if CInt(sCCExpMM) < CInt(dtNowMM) then
				'// expired //
				nErr = nErr + 1
				sMsg = sMsg & "Please enter a valid expiry date.<br />"
			end if
		elseif CInt(sCCExpYY) < CInt(dtNowYY) then
			'// expired //
			nErr = nErr + 1
			sMsg = sMsg & "Please enter a valid expiry date.<br />"
		end if
	else
		nErr = nErr + 1
		sMsg = sMsg & "Please enter a valid expiry date.<br />"
	end if
	'// next check the issue date - only if entered //
	if len("" & sCCIssMM) = 2 and len("" & sCCIssYY) = 2 then
		'// manually check the month and year as VBScript can't be trusted //
		'// if year is less than this year we are done //
		if CInt(sCCIssYY) = CInt(dtNowYY) then
			'// further checks required //
			if CInt(sCCIssMM) > CInt(dtNowMM) then
				'// not current //
				nErr = nErr + 1
				sMsg = sMsg & "Please enter a valid issue date.<br />"
			end if
		elseif (CInt(sCCIssYY) < 90) and (CInt(sCCIssYY) > CInt(dtNowYY)) then
			'// not current //
			nErr = nErr + 1
			sMsg = sMsg & "Please enter a valid issue date.<br />"
		end if
	elseif len("" & sCCIssMM) < 2 xor len("" & sCCIssYY) < 2 then
		'// onlyhalf a date //
		nErr = nErr + 1
		sMsg = sMsg & "Please enter a valid issue date.<br />"
	end if
	'// now just check that any additional fields are in the right format //
	if len(sCCIssNo) > 0 and not isPosInt(sCCIssNo) then
		nErr = nErr + 1
		sMsg = sMsg & "Please enter a valid issue number.<br />"
	end if
	if (not len(sCCChkNo) > 0) or (not isPosInt(sCCChkNo)) then
		nErr = nErr + 1
		sMsg = sMsg & "Please enter a valid security number.<br />"
	end if
	if nErr = 0 then
		'// make payment - do all of the dB updating and emailing that is required//
		nErr = makeCCPayment(aOrder, aMember, sCCName, sCCType, sCCNo, sCCExpMM, sCCExpYY, sCCIssMM, sCCIssYY, sCCIssNo, sCCChkNo)
		'// clear the cart //
		session("OdID") = 0
		session("CtID") = 0
		response.redirect("./receipt.asp?PAY=CC&e=" & encodeID(nOdID))
	else
		'// fall through to displaying the message //
    sMsg = "Sorry, we were unable to accept your credit card details for the following reason(s):<br />&nbsp;<br />" & sMsg
	end if
else
	'// just making the first pass nothing to do or see here //
end if


%>

<!--#include virtual="/includes/doctype.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Hen&amp;Hammock</title>
<!--#include virtual="/includes/mainCSS.asp" -->
<script language="JavaScript" type="text/javascript">
<!--
function showTandC(){
	var tandcWin;
	tandcWin = window.open("/termsandconditions/","TandC","height=600,width=680,scrollbars,menubar,resizable");
	return false;
}
//-->
</script>
<%=javaSetDropDown("registration")%>
</head>
<!--#include virtual="/includes/body.asp" -->
<!--#include virtual="/includes/templateStart.asp" -->
<!--#include virtual="/includes/templateTopNav.asp" -->

<div id="message_bar">
	<img src="/images/pictures/candles_message.jpg" alt="Shopping Basket" width="474" height="179" class="img_left" border="0" />
		<!-- this div has class of thin_intro --> 
		<div class="thin_intro">
			<h1 class="green">Shopping Basket</h1> 
        	<!-- additional classes of grey and intro_text applied to the p --> 
        	<p class="intro_text grey">Here is your shopping baskey</p>
		</div> 
</div> 

<!--#include virtual="/includes/templateBeginContent.asp" -->

<!-- Begin Content -->
          
					<h1 class="no_display">Shopping Basket</h1>
          
				<div id="cart_steps">Your Details &gt; Options &gt; Confirmation &gt; <strong>Payment</strong> &gt; Receipt</div>
					<div id="center_text_block">  
          
				<h2>Payment - Credit Card</h2>
		<form name="payment" action="./paymentCC.asp" method="POST">
		<input type="hidden" name="submitted" value="yes" />		
    <table width="95%" align="center" border="0" cellpadding="0" cellspacing="2" bgcolor="<%=BSKT_BGD_CLR%>" class="text">
      <tr>
        <td width="43%" align="right" bgcolor="<%=BSKT_HDR_CLR%>" class="shopping_title">Total 
          Items: </td>
        <td colspan="2" bgcolor="<%=BSKT_ROW_CLR%>">&nbsp;&nbsp;&pound;<%=FormatNumber(nTotalNoVat+nVat, 2)%></td>
      </tr>
      <%if bVat OR (VAT_OPTS = 0) then%>
      <tr>
        <td align="right" bgcolor="<%=BSKT_HDR_CLR%>" class="shopping_title">VAT 
          included: </td>
        <td colspan="2" bgcolor="<%=BSKT_ROW_CLR%>">&nbsp;&nbsp;&pound;<%=FormatNumber(nVat, 2)%></td>
      </tr>
      <%else%>
      <tr>
        <td align="right" bgcolor="<%=BSKT_HDR_CLR%>" class="shopping_title">Less 
          VAT: </td>
        <td colspan="2" bgcolor="<%=BSKT_ROW_CLR%>">&nbsp;&nbsp;&pound;<%=FormatNumber(nVat, 2)%></td>
      </tr>
      <%end if%>
      <%if (nDiscount > 0) AND (VCHR_OPTS = 1) then%>
      <tr>
        <td align="right" bgcolor="<%=BSKT_HDR_CLR%>" class="shopping_title">Promotional Discount: </td>
        <td colspan="2" bgcolor="<%=BSKT_ROW_CLR%>">&nbsp;-&pound;<%=FormatNumber(nDiscount, 2)%></td>
      </tr>
      <%end if%>
      <tr>
        <td align="right" bgcolor="<%=BSKT_HDR_CLR%>" class="shopping_title">Post 
          &amp; Packing: </td>
        <td colspan="2" bgcolor="<%=BSKT_ROW_CLR%>">&nbsp;&nbsp;&pound;<%=FormatNumber(nPostage, 2)%></td>
      </tr>
      <tr>
        <td align="right" bgcolor="<%=BSKT_HDR_CLR%>" class="shopping_title">Total 
          To Pay: </td>
        <td colspan="2" bgcolor="<%=BSKT_ROW_CLR%>">&nbsp;&nbsp;<b>&pound;<%=FormatNumber(nTotalChargeCalc, 2)%></b></td>
      </tr>
      <tr>
        <td align="right">&nbsp;</td>
        <td colspan="2">&nbsp;</td>
      </tr>
      <%if len("" & sMsg) > 0 then
			response.write "<tr><td align=""right"">&nbsp;</td><td colspan=""2""><p style=""font-size:120%;color:#990000;font-weight:bold;"">" & sMsg & "</p></td></tr>"
		end if%>
      <tr bgcolor="<%=BSKT_ROW_CLR%>">
        <td align="right"><label for="ccName">Cardholder's Name: </label></td>
        <td colspan="2">&nbsp;
          <input class="shopping_inputbox" type="text" name="ccName" id="ccName" size="21" value="<%=sCCName%>" /></td>
      </tr>
      <tr bgcolor="<%=BSKT_ROW_CLR%>">
        <td align="right"><label for="ccNo">Card Number: </label></td>
        <td colspan="2">&nbsp;
          <input class="shopping_inputbox" type="text" name="ccNo" id="ccNo" size="21" value="<%=sCCNo%>" /></td>
      </tr>
      <tr bgcolor="<%=BSKT_ROW_CLR%>">
        <td align="right">Card Type: </td>
        <td colspan="2">&nbsp;
          <select class="shopping_inputbox" name="ccType">
            <option value="">Please select</option>
            <option value="VISA"<%if sCCType="VISA" then response.write " selected"%>>VISA</option>
            <option value="VISAELECTRON"<%if sCCType="VISAELECTRON" then response.write " selected"%>>VISA ELECTRON</option>
            <option value="VISADELTA"<%if sCCType="VISADELTA" then response.write " selected"%>>VISA DELTA</option>
            <option value="MASTERCARD"<%if sCCType="MASTERCARD" then response.write " selected"%>>MASTERCARD</option>
            <option value="AMEX"<%if sCCType="AMEX" then response.write " selected"%>>AMERICAN 
            EXPRESS</option>
            <option value="MAESTRO"<%if sCCType="MAESTRO" then response.write " selected"%>>MAESTRO</option>
            <option value="SWITCH"<%if sCCType="SWITCH" then response.write " selected"%>>SWITCH</option>
            <option value="SOLO"<%if sCCType="SOLO" then response.write " selected"%>>SOLO</option>
          </select></td>
      </tr>
      <tr bgcolor="<%=BSKT_ROW_CLR%>">
        <td align="right">Expiry Date: </td>
        <td colspan="2">&nbsp;
          <select class="shopping_inputbox" name="ccExpMM">
            <option value="">MM</option>
            <option value="01"<%if sCCExpMM="01" then response.write " selected"%>>01 
            Jan</option>
            <option value="02"<%if sCCExpMM="02" then response.write " selected"%>>02 
            Feb</option>
            <option value="03"<%if sCCExpMM="03" then response.write " selected"%>>03 
            Mar</option>
            <option value="04"<%if sCCExpMM="04" then response.write " selected"%>>04 
            Apr</option>
            <option value="05"<%if sCCExpMM="05" then response.write " selected"%>>05 
            May</option>
            <option value="06"<%if sCCExpMM="06" then response.write " selected"%>>06 
            Jun</option>
            <option value="07"<%if sCCExpMM="07" then response.write " selected"%>>07 
            Jul</option>
            <option value="08"<%if sCCExpMM="08" then response.write " selected"%>>08 
            Aug</option>
            <option value="09"<%if sCCExpMM="09" then response.write " selected"%>>09 
            Sep</option>
            <option value="10"<%if sCCExpMM="10" then response.write " selected"%>>10 
            Oct</option>
            <option value="11"<%if sCCExpMM="11" then response.write " selected"%>>11 
            Nov</option>
            <option value="12"<%if sCCExpMM="12" then response.write " selected"%>>12 
            Dec</option>
          </select>
          / 
          <select class="shopping_inputbox" name="ccExpYY">
            <option value="">YYYY</option>
            <option value="04"<%if sCCExpYY="04" then response.write " selected"%>>2004</option>
            <option value="05"<%if sCCExpYY="05" then response.write " selected"%>>2005</option>
            <option value="06"<%if sCCExpYY="06" then response.write " selected"%>>2006</option>
            <option value="07"<%if sCCExpYY="07" then response.write " selected"%>>2007</option>
            <option value="08"<%if sCCExpYY="08" then response.write " selected"%>>2008</option>
            <option value="09"<%if sCCExpYY="09" then response.write " selected"%>>2009</option>
            <option value="10"<%if sCCExpYY="10" then response.write " selected"%>>2010</option>
            <option value="11"<%if sCCExpYY="11" then response.write " selected"%>>2011</option>
            <option value="12"<%if sCCExpYY="12" then response.write " selected"%>>2012</option>
          </select></td>
      </tr>
      <tr bgcolor="<%=BSKT_ROW_CLR%>">
        <td align="right">Issue Date: </td>
        <td colspan="2">&nbsp;
          <select class="shopping_inputbox" name="ccIssMM">
            <option value="">MM</option>
            <option value="01"<%if sCCIssMM="01" then response.write " selected"%>>01 
            Jan</option>
            <option value="02"<%if sCCIssMM="02" then response.write " selected"%>>02 
            Feb</option>
            <option value="03"<%if sCCIssMM="03" then response.write " selected"%>>03 
            Mar</option>
            <option value="04"<%if sCCIssMM="04" then response.write " selected"%>>04 
            Apr</option>
            <option value="05"<%if sCCIssMM="05" then response.write " selected"%>>05 
            May</option>
            <option value="06"<%if sCCIssMM="06" then response.write " selected"%>>06 
            Jun</option>
            <option value="07"<%if sCCIssMM="07" then response.write " selected"%>>07 
            Jul</option>
            <option value="08"<%if sCCIssMM="08" then response.write " selected"%>>08 
            Aug</option>
            <option value="09"<%if sCCIssMM="09" then response.write " selected"%>>09 
            Sep</option>
            <option value="10"<%if sCCIssMM="10" then response.write " selected"%>>10 
            Oct</option>
            <option value="11"<%if sCCIssMM="11" then response.write " selected"%>>11 
            Nov</option>
            <option value="12"<%if sCCIssMM="12" then response.write " selected"%>>12 
            Dec</option>
          </select>
          / 
          <select class="shopping_inputbox" name="ccIssYY">
            <option value="">YYYY</option>
            <option value="99"<%if sCCIssYY="99" then response.write " selected"%>>1999</option>
            <option value="00"<%if sCCIssYY="00" then response.write " selected"%>>2000</option>
            <option value="01"<%if sCCIssYY="01" then response.write " selected"%>>2001</option>
            <option value="02"<%if sCCIssYY="02" then response.write " selected"%>>2002</option>
            <option value="03"<%if sCCIssYY="03" then response.write " selected"%>>2003</option>
            <option value="04"<%if sCCIssYY="04" then response.write " selected"%>>2004</option>
            <option value="05"<%if sCCIssYY="05" then response.write " selected"%>>2005</option>
            <option value="06"<%if sCCIssYY="06" then response.write " selected"%>>2006</option>
            <option value="07"<%if sCCIssYY="07" then response.write " selected"%>>2007</option>
          </select></td>
      </tr>
      <tr bgcolor="<%=BSKT_ROW_CLR%>">
        <td align="right"><label for="ccIssNo">Issue Number: </label></td>
        <td colspan="2">&nbsp;
          <input class="shopping_inputbox" type="text" name="ccIssNo" id="ccIssNo" size="4" value="<%=sCCIssNo%>" />
          &nbsp;<span style="font-size:75%;color:#663300; font-weight:normal;">Switch cards only</span></td>
      </tr>
      <tr bgcolor="<%=BSKT_ROW_CLR%>">
        <td align="right" valign="top"><label for="ccChkNo">Security Number: </label></td>
        <td width="9%" valign="top">&nbsp;
          <input class="shopping_inputbox" type="text" name="ccChkNo" size="4" value="<%=sCCChkNo%>" />
          </td>
        <td width="48%">
		<span style="font-size:75%;color:#663300;font-weight:normal;">Last 3 digits found 
          on the back of the card on signature strip NB on Amex found on the front of the card- the last 4 digits above main 
          number.</span>
		</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td colspan="2">&nbsp;</td>
      </tr>
    </table>
    <table width="95%" align="center" border="0" cellpadding="0" cellspacing="2">
		<tr><td>&nbsp;<br /><input  type="image" src="/ecomm/buttons/button_cancelpayment.gif" border="0" name="cancel" value="cancel payment" alt="cancel payment" /></td>
			<td>&nbsp;<br /><input  type="image" src="/ecomm/buttons/button_submitpayment.gif" border="0" name="submit" value="submit payment" alt="submit payment details" /></td></tr>
     	<tr>
		<td colspan="2" align="center">
		<p>&nbsp;&nbsp;</p>

		<!--<div style="text-align:center;width:500px;color:#663300;font-size:80%;">Shop in confidence - Earthworks has been trading for 11 years. We use the highest level of security and continually review and update our systems. <br />Earthworks Art and Design Ltd <br /> Company Reg Number 03254646 VAT Number 654 347133</div>-->
		<p>
		<!-- webbot bot="HTMLMarkup" startspan -->

		<!-- GeoTrust QuickSSL [tm] Smart Icon tag. Do not edit. -->
		<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript" SRC="https://smarticon.geotrust.com/si.js"></SCRIPT>
		<!-- end GeoTrust Smart Icon tag -->
		
		<!-- webbot bot="HTMLMarkup" endspan -->
		</p>
		
		</td>
		</tr>
	  </table>
		</form>
<div>
<img src="/ecomm/buttons/american_express.gif" alt="American Express" />&nbsp;<img src="/ecomm/buttons/visa.gif" alt="Visa" />&nbsp;<img src="/ecomm/buttons/visaElectron.gif" alt="Visa Electron" />&nbsp;<img src="/ecomm/buttons/delta.gif" alt="Delta" />&nbsp;<img src="/ecomm/buttons/mastercard.gif" alt="Master Card" />&nbsp;<img src="/ecomm/buttons/maestro.gif" alt="Maestro" />&nbsp;<img src="/ecomm/buttons/switch.gif" alt="Switch" />&nbsp;<img src="/ecomm/buttons/solo.gif" alt="Solo" />
</div></div>
<!-- End Content -->
<!--#include virtual="/includes/templateEndContent.asp" -->

<!-- LHS navigation -->
<!--#include virtual="/includes/templateLHSnav.asp" -->
<!-- RHS navigation -->
<!--#in clude virtual="/includes/templateRHSnav.asp" -->

<!--#include virtual="/includes/templateFooter.asp" -->
<!--#include virtual="/includes/templateEnd.asp" -->
        
<%
function makeCCPayment(aOrder, aMember, sCCName, sCCType, sCCNo, sCCExpMM, sCCExpYY, sCCIssMM, sCCIssYY, sCCIssNo, sCCChkNo)
	dim nErr:nErr = 0
	dim aItems:aItems = null
  dim sSQL:sSQL=""
	'// update dB with what has happened //
  aOrder(OD_COMPLETEDALL, 0) = true
  nErr = saveOrder(aOrder)
  if nErr = 0 then
    sSQL = "SELECT OI.*, NT.ntText,OD.keyFmeID,OD.odFirstName,OD.odSurname FROM ((tblOrderItem AS OI LEFT JOIN tblOrder AS OD ON OI.keyFodID=OD.odID) LEFT JOIN tblNote AS NT ON OI.keyFn1ID=NT.ntID)" _
        & " WHERE OI.keyFodID=" & aOrder(OD_ID, 0) & " AND OI.keyFinID=" & INST_ID & " ORDER BY OI.oiID ASC;"
    nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aItems)
  else
    '// what?? //
    exit function
  end if
  if nErr = 0 then
  	'// notifyShopOrderPlaced //
  	nErr = notifyShopCCOrderPlaced(aOrder, aMember, aItems, sCCName, sCCType, sCCNo, sCCExpMM, sCCExpYY, sCCIssMM, sCCIssYY, sCCIssNo, sCCChkNo)
  	'// notifyCustomerOrderConfirmed //
  	nErr = notifyCustomerOrderConfirmed(aOrder, aMember, aItems)
    '// squirt the order details to the EWtransit SQL db //
    nErr = updateExportDbOrderLines(aOrder(OD_ID, 0))
    '// update stock quantities //
    nErr = updateStockQuantitiesSold(aItems)
  else
  end if
	makeCCPayment = nErr
end function

function updateStockQuantitiesSold(aItems)
  dim nErr:nErr=0
  dim nIdx:nIdx=0
  dim sSQL:sSQL=""
  dim nWlID:nWlID=0
  dim sNote:sNote=""
  dim aTmp:aTmp=null
  for nIdx=0 to UBound(aItems, 2)
    sSQL = "UPDATE tblProduct SET prStockQty=(prStockQty-" & aItems(OI_QUANTITY, nIdx) & ") WHERE prCode='" & aItems(OI_PRODUCTCODE, nIdx) & "' AND keyFinID=" & INST_ID & ";"
    'response.write sSQL
    call sqlExec(DSN_ECOM_DB, sSQL)
    '// is it from someone's wishlist? //
    sNote = "" & aItems(OI_UBOUND + 1, nIdx)
    if instr("" & sNote, "WISHLIST:") then
      aTmp = split("" & sNote, ":")
      nWlID = clng("0" & aTmp(1))
      if nWlID > 0 then
        nErr = updateWishListItem(nWlID, aItems(OI_PRODUCTCODE, nIdx), aItems(OI_QUANTITY, nIdx), aItems(OI_UBOUND + 2, nIdx), aItems(OI_UBOUND + 3, nIdx), aItems(OI_UBOUND + 4, nIdx))
      end if
      aTmp = null
    end if
  next
  updateStockQuantitiesSold = nErr
end function

function updateWishListItem(nWlID, sCode, nQty, nMeID, sFirstName, sSurname)
  dim nErr:nErr=0
  dim sSQl:sSQL=""
  dim aWishItem:aWishItem=null
  dim nNtID:nNtID=0
  if (nWlID > 0) and (nQty > 0) and (len(""  & sCode) > 0) then
    sSQL = "SELECT WI.* FROM tblWishListItem AS WI LEFT JOIN tblProduct AS PR ON WI.keyFprCODE=PR.prCODE WHERE WI.keyFprCode='" & sCode & "' AND WI.keyFwlID=" & nWlID & " AND WI.wiPurchased=False AND WI.keyFinID=" & INST_ID & ";"
    nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aWishItem)
    if nErr = 0 then
      '// need to remove nQty from the wishlistitem //
      aWishItem(WI_QTY, 0) = aWishItem(WI_QTY, nIdx) - nQty
      nErr = saveWishListItem(aWishItem)
      '// need to move create a new purchased wishlistitem line //
      '// and add in the memberId of purchaser and name //
      nNtID = addNote("Bought by: " & sFirstName & " " & sSurname)
      aWishItem(WI_ID, 0) = 0 '// create new item //
      aWishItem(WI_QTY, 0) = nQty
      aWishItem(WI_PURCHASED, 0) = true
      aWishItem(WI_PURCHASEDMEID, 0) = nMeID
      aWishItem(WI_KEYFNTID, 0) = nNtID
      nErr = saveWishListItem(aWishItem)
    end if
  end if
  updateWishListItem = nErr
end function

function updateExportDbOrderLines(nOrder)
  dim nErr:nErr=0
  dim sSQL:sSQL=""
  dim oConn
  dim aLines:aLines=null
  dim oRS
  dim nIdx:nIdx=0
  dim nJdx:nJdx=0
  sSQL = "SELECT O.odID, O.keyFmeID, O.odDate, O.odEmail, O.odPhone, O.odTitle, O.odFirstName, O.odSurName" _
      & ", O.odAddress1, O.odAddress2, O.odAddress3, O.odAddress4, O.odAddress5, O.odPostCode" _
      & ", O.odDelTitle, O.odDelFirstName, O.odDelSurName, O.odDelAddress1, O.odDelAddress2, O.odDelAddress3, O.odDelAddress4, O.odDelAddress5, O.odDelPostCode" _
      & ", O.odPostType, O.odTotalNoVAT, O.odPostage, O.odVAT, O.odTotalChargeCalc, O.odTransID, O.odTransTime, O.odAuthCode, O.odTotalAmountTxt, O.odCompletedAll" _
      & ", O.odSourceCode, O.odSourceName, O.odPromoCode, O.odPromoText, O.odPromoDisc, O.odPartShip, Note1.ntText AS odGiftMessage, Note2.ntText AS odDelInstructions" _
      & ", Item.oiProductCode, Item.oiProductCodeShort, Item.oiType, Item.oiDescription, Item.oiUnitCost, Item.oiUnitVAT, Item.oiQuantity, Item.oiLeadTimeText, Note3.ntText AS oiNote1, Note4.ntText AS oiNote2" _
      & " FROM ((tblOrder AS O LEFT JOIN tblNote AS Note1 ON O.keyFn1ID = Note1.ntID) LEFT JOIN tblNote AS Note2 ON O.keyFn2ID = Note2.ntID)" _
      & " LEFT JOIN ((tblOrderItem AS Item LEFT JOIN tblNote AS Note3 ON Item.keyFn1ID = Note3.ntID) LEFT JOIN tblNote AS Note4 ON Item.keyFn2ID = Note4.ntID) ON O.odID = Item.keyFodID" _
      & " WHERE (O.odID=" & nOrder & " AND O.keyFinID=" & INST_ID & ");"
  nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aLines)
  if nErr = 0 then  
		set oConn = server.createobject("ADODB.Connection")
		oConn.open "Provider=SQLOLEDB; Data Source=localhost; Network Library=DBMSSOCN; Initial Catalog=EWtransit;User ID=ewTransit;Password=715n4rtw3;"
		set oRS = server.createObject("ADODB.Recordset")	
		oRS.open "OrderLines", oConn, adOpenDynamic, adLockOptimistic, adCmdTable
		for nIdx = 0 to UBound(aLines, 2)
		  oRS.addnew
      for nJdx = 0 to UBound(aLines, 1)
			  oRS(nJdx) = aLines(nJdx,nIdx)
      next
		  oRS.update
		next	
    '// PROMO CODE is at 35, Text at 36, Discount at 37 //
    '// aparaently uneeded by duradata etc //
    'if (VCHR_OPTS = 1) and (len("" & aLines(35,0)) > 0) then
		'  oRS.addnew
    '  for nJdx = 0 to 40
		'	  oRS(nJdx) = aLines(nJdx,0)
    '  next
    '  oRS(41) = "EWDISC"
    '  oRS(42) = aLines(35,0)
    '  oRS(43) = 0
    '  oRS(44) = aLines(36,0)
    '  oRS(45) = aLines(37,0)
    '  oRS(46) = 0
    '  oRS(47) = 1
    '  oRS(48) = ""
    '  oRS(49) = ""
    '  oRS(50) = ""
		'  oRS.update
    'end if	
		oRS.close
		set oRS = nothing
		oConn.close
		set oConn = nothing
  end if
  updateExportDbOrderLines = nErr 
end function

function notifyShopCCOrderPlaced(aOrder, aMember, aContents, sCCName, sCCType, sCCNo, sCCExpMM, sCCExpYY, sCCIssMM, sCCIssYY, sCCIssNo, sCCChkNo)
	dim nErr:nErr = 0
	dim sTo, sToName, sFrom, sFromName, sSubject
  'dim sCardInfo, sTmp01, sTmp02
	dim sChargeAdd,sDeliveryAdd,sCustName,sOrderDate,sOrderTime,sOrderMessage,sOrderInstruct,aTmp,i, sProdDesc
	dim sMsg:sMsg = ""
  dim sCard01:sCard01=""
  dim sTmp01:sTmp01=""
  dim sTmp02:sTmp02=""
	if (not isNull(aOrder)) and (isArray(aOrder)) then
		if not isNull(aContents) then
			aTmp = split(aOrder(OD_DATE, 0), " ")
			sOrderDate	 = aTmp(0)
			sOrderTime	 = aTmp(1)
			if UBound(aTmp) = 2 then
				'// also have a PM to add //
				sOrderTime = sOrderTime & " " & aTmp(2)
			end if
			sCustName	 = replace(trim(aOrder(OD_TITLE,0) & " " & aOrder(OD_FIRSTNAME,0) & " " & aOrder(OD_SURNAME,0)), "  ", " ")
			sChargeAdd	 = makeAddress(aOrder(OD_TITLE,0),aOrder(OD_FIRSTNAME,0),aOrder(OD_SURNAME,0) _
									  ,aOrder(OD_ADDRESS1,0),aOrder(OD_ADDRESS2,0),aOrder(OD_ADDRESS3,0) _
									  ,aOrder(OD_ADDRESS4,0),getCountry(aOrder(OD_ADDRESS5,0)),aOrder(OD_POSTCODE,0))
			sDeliveryAdd = makeAddress(aOrder(OD_DELTITLE,0),aOrder(OD_DELFIRSTNAME,0),aOrder(OD_DELSURNAME,0) _
									  ,aOrder(OD_DELADDRESS1,0),aOrder(OD_DELADDRESS2,0),aOrder(OD_DELADDRESS3,0) _
									  ,aOrder(OD_DELADDRESS4,0),getCountry(aOrder(OD_DELADDRESS5,0)),aOrder(OD_DELPOSTCODE,0))
			if aOrder(OD_KEYFN1ID,0) > 0 then
				sOrderMessage = getNote(aOrder(OD_KEYFN1ID,0))
			end if
			if aOrder(OD_KEYFN2ID,0) > 0 then
				sOrderInstruct = getNote(aOrder(OD_KEYFN2ID,0))
			end if
			sTmp01 = sTmp01 & "You have received the following order." & vbcrlf
			sTmp01 = sTmp01 & vbcrlf
			sTmp01 = sTmp01 & "ORDER DATE: "
      sTmp01 = sTmp01 & sOrderDate & " " & sOrderTime & vbcrlf
			sTmp01 = sTmp01 & vbcrlf
			sTmp01 = sTmp01 & "ONLINE ORDER REF: "
      sTmp01 = sTmp01 & "N" & right("00000" & aOrder(OD_ID, 0), 5) & vbcrlf
			sTmp01 = sTmp01 & vbcrlf
			sTmp01 = sTmp01 & "CUSTOMER NUMBER: "
      sTmp01 = sTmp01 & aOrder(OD_KEYFMEID, 0) & vbcrlf
			sTmp01 = sTmp01 & vbcrlf
			sTmp01 = sTmp01 & "FROM:  " & vbcrlf
      sTmp01 = sTmp01 & sChargeAdd & vbcrlf
      sTmp01 = sTmp01 & vbcrlf
      
			sTmp01 = sTmp01 & "EMAIL: "
      sTmp01 = sTmp01 & aOrder(OD_EMAIL, 0) & vbcrlf
      sTmp01 = sTmp01 & vbcrlf
      
			sTmp01 = sTmp01 & "TEL NO: "
      sTmp01 = sTmp01 & aOrder(OD_PHONE, 0) & vbcrlf
      sTmp01 = sTmp01 & vbcrlf
      
      sTmp01 = sTmp01 & "ORDER DETAILS:  " & vbcrlf
      '// loop items //
			for i = 0 to UBound(aContents,2)
				sProdDesc = aContents(OI_DESCRIPTION, i)
				if aContents(OI_KEYFN1ID, i) > 0 then
					sProdDesc = sProdDesc & " - " & getNote(aContents(OI_KEYFN1ID, i))
				end if
        'sMsg = sMsg & "Order: " & aOrder(OD_ID, 0) & vbcrlf
        sTmp01 = sTmp01 & "ITEM CODE: " & aContents(OI_PRODUCTCODE, i) & vbcrlf
        sTmp01 = sTmp01 & "ITEM DESCRIPTION: " & sProdDesc & vbcrlf
        sTmp01 = sTmp01 & "PRICE: £" & formatnumber(aContents(OI_UNITCOST,i),2) & vbcrlf
        sTmp01 = sTmp01 & "QUANTITY: " & aContents(OI_QUANTITY, i) & vbcrlf
        sTmp01 = sTmp01 & "LINE VALUE: £" & formatnumber((aContents(OI_UNITCOST,i) * aContents(OI_QUANTITY, i)),2) & vbcrlf
        sTmp01 = sTmp01 & "DELIVERY: " & aContents(OI_LEADTIMETEXT, i) & vbcrlf
				sTmp01 = sTmp01 & vbcrlf
			next
      if (VCHR_OPTS = 1) and (len("" & aOrder(OD_PROMOCODE, 0)) > 0) then
			  sTmp01 = sTmp01 & "ORDER VALUE: £ " & FormatNumber(aOrder(OD_TOTALCHARGECALC,0) + aOrder(OD_PROMODISC, 0) - (aOrder(OD_POSTAGE,0) + aOrder(OD_VAT,0)),2) & vbcrlf
        sTmp01 = sTmp01 & vbcrlf
        sTmp01 = sTmp01 & "DISCOUNT: £ " & formatnumber(aOrder(OD_PROMODISC, 0), 2) & vbcrlf & aOrder(OD_PROMOCODE, 0) & ": " & aOrder(OD_PROMOTEXT, 0) & vbcrlf
				sTmp01 = sTmp01 & vbcrlf
      else
			  sTmp01 = sTmp01 & "ORDER VALUE: £ " & FormatNumber(aOrder(OD_TOTALCHARGECALC,0) - (aOrder(OD_POSTAGE,0) + aOrder(OD_VAT,0)),2) & vbcrlf
      end if
			sTmp01 = sTmp01 & "POST & PACKAGING: £ " & FormatNumber(aOrder(OD_POSTAGE,0),2) & vbcrlf
			sTmp01 = sTmp01 & "VAT: £ " & FormatNumber(aOrder(OD_VAT,0),2) & vbcrlf
			sTmp01 = sTmp01 & "TOTAL INVOICE VALUE: £ " & FormatNumber(aOrder(OD_TOTALCHARGECALC,0),2) & vbcrlf
			sTmp01 = sTmp01 & vbcrlf
      
      'sTmp01 = sMsg
      
			sCard01 = sCard01 & "PLEASE PROCESS CARD PAYMENT" & vbcrlf
			sCard01 = sCard01 & "CARD NO.    : " & sCCNo & vbcrlf
			sCard01 = sCard01 & "CARD NAME   : " & sCCName & vbcrlf
			sCard01 = sCard01 & "CARD TYPE   : " & sCCType & vbcrlf
			sCard01 = sCard01 & "ISSUE DATE  : " & sCCIssMM & "/" & sCCIssYY & vbcrlf
			sCard01 = sCard01 & "EXPIRY DATE : " & sCCExpMM & "/" & sCCExpYY & vbcrlf
			sCard01 = sCard01 & "ISSUE NO.   : " & sCCIssNo & vbcrlf
			sCard01 = sCard01 & "SECURITY NO.: " & sCCChkNo & vbcrlf
			sCard01 = sCard01 & vbcrlf
      
      sTmp02 = sTmp02 & "DELIVER TO:  " & vbcrlf
      sTmp02 = sTmp02 & sDeliveryAdd & vbcrlf
      sTmp02 = sTmp02 & vbcrlf
      
			sTmp02 = sTmp02 & "ADDITIONAL INFORMATION:" & vbcrlf
			sTmp02 = sTmp02 & vbcrlf
			sTmp02 = sTmp02 & "MESSAGE TO RECIPIENT FROM CUSTOMER:" & vbcrlf
			sTmp02 = sTmp02 & sOrderMessage & vbcrlf
			sTmp02 = sTmp02 & vbcrlf
			sTmp02 = sTmp02 & "DELIVERY INSTRUCTIONS FROM CUSTOMER:" & vbcrlf
			sTmp02 = sTmp02 & sOrderInstruct & vbcrlf
			sTmp02 = sTmp02 & vbcrlf
			if aMember(ME_CONTACTBYUS, 0) then
				sTmp02 = sTmp02 & "MAY WE CONTACT THIS CUSTOMER:  Yes" & vbcrlf
			else
				sTmp02 = sTmp02 & "MAY WE CONTACT THIS CUSTOMER:  No" & vbcrlf
			end if
			sTmp02 = sTmp02 & vbcrlf
      if POST_OPTS > 0 then
  			if aOrder(OD_POSTTYPE,0) = "24H" then
  				sTmp02 = sTmp02 & "SPECIAL DELIVERY:  Yes" & vbcrlf
  			else
  				sTmp02 = sTmp02 & "SPECIAL DELIVERY:  No" & vbcrlf
  			end if
      end if
			if aOrder(OD_PARTSHIP,0) then
				sTmp02 = sTmp02 & "PARTIAL SHIPMENT ALLOWED:  Yes" & vbcrlf
			else
				sTmp02 = sTmp02 & "PARTIAL SHIPMENT ALLOWED:  No" & vbcrlf
			end if
			sTmp02 = sTmp02 & vbcrlf
      
			sTo = EML_SHOPTOEMAIL
			sToName = EML_SHOPTONAME
			sFrom = EML_SHOPFROMEMAIL
			sFromName = EML_SHOPFROMNAME
			sSubject = SHOP_NAME & " Order (Order ref: " & aOrder(OD_ID,0) & ")"
      sMsg = sTmp01 & sCard01 & sTmp02
			nErr = SendMailEnc(sTo, sToName, sFrom, sFromName, sSubject, sMsg, "", "", true, false)
      sMsg = sTmp01 & sTmp02
			nErr = SendMailEnc(sTo, sToName, sFrom, sFromName, sSubject, sMsg, "", "", false, false)
		else
			nErr = 2
		end if
	else
		nErr = 1
	end if
	notifyShopCCOrderPlaced = nErr
end function

function notifyCustomerOrderConfirmed(aOrder, aMember, aContents)
	dim nErr:nErr = 0
	dim sTo, sToName, sFrom, sFromName, sSubject
	dim sChargeAdd,sDeliveryAdd,sCustName,sOrderDate,sOrderTime,sOrderMessage,sOrderInstruct,aTmp,i, sProdDesc
	dim sMsg:sMsg = ""
	if (not isNull(aOrder)) and (isArray(aOrder)) then
		if not isNull(aContents) then
			aTmp = split(aOrder(OD_DATE, 0), " ")
			sOrderDate	 = aTmp(0)
			sOrderTime	 = aTmp(1)
			if UBound(aTmp) = 2 then
				'// also have a PM to add //
				sOrderTime = sOrderTime & " " & aTmp(2)
			end if
			if len("" & aOrder(OD_FIRSTNAME,0)) > 0 then
				sCustName	 = aOrder(OD_FIRSTNAME,0)
			else
				sCustName	 = replace(trim(aOrder(OD_TITLE,0) & " " & aOrder(OD_FIRSTNAME,0) & " " & aOrder(OD_SURNAME,0)), "  ", " ")
			end if
			sChargeAdd	 = makeAddress(aOrder(OD_TITLE,0),aOrder(OD_FIRSTNAME,0),aOrder(OD_SURNAME,0) _
									  ,aOrder(OD_ADDRESS1,0),aOrder(OD_ADDRESS2,0),aOrder(OD_ADDRESS3,0) _
									  ,aOrder(OD_ADDRESS4,0),getCountry(aOrder(OD_ADDRESS5,0)),aOrder(OD_POSTCODE,0))
			sDeliveryAdd = makeAddress(aOrder(OD_DELTITLE,0),aOrder(OD_DELFIRSTNAME,0),aOrder(OD_DELSURNAME,0) _
									  ,aOrder(OD_DELADDRESS1,0),aOrder(OD_DELADDRESS2,0),aOrder(OD_DELADDRESS3,0) _
									  ,aOrder(OD_DELADDRESS4,0),getCountry(aOrder(OD_DELADDRESS5,0)),aOrder(OD_DELPOSTCODE,0))
			if aOrder(OD_KEYFN1ID,0) > 0 then
				sOrderMessage = getNote(aOrder(OD_KEYFN1ID,0))
			end if
			if aOrder(OD_KEYFN2ID,0) > 0 then
				sOrderInstruct = getNote(aOrder(OD_KEYFN2ID,0))
			end if
			sMsg = sMsg & "Dear " & sCustName & "," & vbcrlf
			sMsg = sMsg & vbcrlf
			sMsg = sMsg & "Thank you for your order which will be processed as soon as possible. Your" _
                  & " order details are listed below - if there are any changes you need to make," _
                  & " please contact us without delay either by replying to this email or by" _
                  & " phoning us on  " & SHOP_TEL_NO & "." & vbcrlf
			sMsg = sMsg & vbcrlf
			sMsg = sMsg & "Your order details are as follows:" & vbcrlf
      
			sMsg = sMsg & vbcrlf
			sMsg = sMsg & "ORDER DATE: "
      sMsg = sMsg & sOrderDate & " " & sOrderTime & vbcrlf
			sMsg = sMsg & vbcrlf
			sMsg = sMsg & "ONLINE ORDER REF: "
      sMsg = sMsg & "N" & right("00000" & aOrder(OD_ID, 0), 5) & vbcrlf
			sMsg = sMsg & vbcrlf
			sMsg = sMsg & "CUSTOMER ID NUMBER: "
      sMsg = sMsg & aOrder(OD_KEYFMEID, 0) & vbcrlf
			sMsg = sMsg & vbcrlf
			sMsg = sMsg & "FROM:  " & vbcrlf
      sMsg = sMsg & sChargeAdd & vbcrlf
      sMsg = sMsg & vbcrlf
      
			sMsg = sMsg & "EMAIL: "
      sMsg = sMsg & aOrder(OD_EMAIL, 0) & vbcrlf
      sMsg = sMsg & vbcrlf
      
			sMsg = sMsg & "TEL NO: "
      sMsg = sMsg & aOrder(OD_PHONE, 0) & vbcrlf
      sMsg = sMsg & vbcrlf
      
      sMsg = sMsg & "ORDER DETAILS:  " & vbcrlf
      '// loop items //
			for i = 0 to UBound(aContents,2)
				sProdDesc = aContents(OI_DESCRIPTION, i)
				if aContents(OI_KEYFN1ID, i) > 0 then
					sProdDesc = sProdDesc & " - " & getNote(aContents(OI_KEYFN1ID, i))
				end if
        'sMsg = sMsg & "Order: " & aOrder(OD_ID, 0) & vbcrlf
        sMsg = sMsg & "ITEM CODE: " & aContents(OI_PRODUCTCODE, i) & vbcrlf
        sMsg = sMsg & "ITEM DESCRIPTION: " & sProdDesc & vbcrlf
        sMsg = sMsg & "PRICE: £" & formatnumber(aContents(OI_UNITCOST,i),2) & vbcrlf
        sMsg = sMsg & "QUANTITY: " & aContents(OI_QUANTITY, i) & vbcrlf
        sMsg = sMsg & "LINE VALUE: £" & formatnumber((aContents(OI_UNITCOST,i) * aContents(OI_QUANTITY, i)),2) & vbcrlf
        sMsg = sMsg & "DELIVERY: " & aContents(OI_LEADTIMETEXT, i) & vbcrlf
				sMsg = sMsg & vbcrlf
			next
			'sMsg = sMsg  & "ORDER VALUE: £ " & FormatNumber(aOrder(OD_TOTALCHARGECALC,0) - (aOrder(OD_POSTAGE,0) + aOrder(OD_VAT,0)),2) & vbcrlf
      if (VCHR_OPTS = 1) and (len("" & aOrder(OD_PROMOCODE, 0)) > 0) then
			  sMsg = sMsg  & "ORDER VALUE: £ " & FormatNumber(aOrder(OD_TOTALCHARGECALC,0) + aOrder(OD_PROMODISC,0) - (aOrder(OD_POSTAGE,0) + aOrder(OD_VAT,0)),2) & vbcrlf
        sMsg = sMsg  & vbcrlf
        sMsg = sMsg  & "DISCOUNT: £ " & formatnumber(aOrder(OD_PROMODISC, 0), 2) & vbcrlf & aOrder(OD_PROMOCODE, 0) & ": " & aOrder(OD_PROMOTEXT, 0) & vbcrlf
				sMsg = sMsg  & vbcrlf
      else
			  sMsg = sMsg  & "ORDER VALUE: £ " & FormatNumber(aOrder(OD_TOTALCHARGECALC,0) - (aOrder(OD_POSTAGE,0) + aOrder(OD_VAT,0)),2) & vbcrlf
      end if
			sMsg = sMsg & "POST & PACKAGING: £ " & FormatNumber(aOrder(OD_POSTAGE,0),2) & vbcrlf
			sMsg = sMsg & "VAT: £ " & FormatNumber(aOrder(OD_VAT,0),2) & vbcrlf
			sMsg = sMsg & "TOTAL INVOICE VALUE: £ " & FormatNumber(aOrder(OD_TOTALCHARGECALC,0),2) & vbcrlf
			sMsg = sMsg & vbcrlf
      
			sMsg = sMsg & "DELIVER TO:  " & vbcrlf
      sMsg = sMsg & sDeliveryAdd & vbcrlf
      sMsg = sMsg & vbcrlf
      
			if len("" & sOrderMessage) then
				sMsg = sMsg & "Your items will be sent with the following gift message:" & vbcrlf
				sMsg = sMsg & sOrderMessage &  vbcrlf
				sMsg = sMsg & vbcrlf
			end if
			if len("" & sOrderInstruct) then
				sMsg = sMsg & "Your items will be sent with the following delivery instructions:" & vbcrlf
				sMsg = sMsg & sOrderInstruct &  vbcrlf
				sMsg = sMsg & vbcrlf
			end if
      sMsg = sMsg & "Thank you for shopping at " & SHOP_NAME & "." & vbcrlf & vbcrlf
      sMsg = sMsg & SHOP_NAME & vbcrlf
      sMsg = sMsg & "[W]: " & SITE_URL & vbcrlf
      sMsg = sMsg & "[E]: " & SHOP_TOEMAIL & vbcrlf
      sMsg = sMsg & "[T]: " & SHOP_TEL_NO & vbcrlf
			'sMsg = sMsg & "We are doing our best to deliver your order as fast as possible." _
      '            & " Usually we despatch within 24 hours of receiving your order but you should allow up to 7 days (UK)" _
      '            & " and 10 days elsewhere.  If we foresee a longer delay we will contact you." & vbcrlf
			'sMsg = sMsg & SHOP_TANDC_URL & vbcrlf
			'sMsg = sMsg & vbcrlf
      'sMsg = sMsg & "For further information see our FAQs page at " & SHOP_TANDC_URL _
      '            & " or email us at " & EML_SHOPTOEMAIL & " or call us on " & SHOP_TEL_NO & "."
			'sMsg = sMsg & "If you have any other queries relating to this order, please contact us." & vbcrlf
			'sMsg = sMsg & "Telephone: " & SHOP_TEL_NO & vbcrlf
			'sMsg = sMsg & "Email: " & EML_SHOPTOEMAIL & vbcrlf
			'sMsg = sMsg & "Post: " & SHOP_POST_ADDRESS & vbcrlf
			sMsg = sMsg & vbcrlf
			sTo = aOrder(OD_EMAIL,0)
			sToName = trim("" & aOrder(OD_TITLE,0) & " " & aOrder(OD_SURNAME,0) & " " & aOrder(OD_SURNAME,0))
			sFrom = EML_SHOPFROMEMAIL
			sFromName = EML_SHOPFROMNAME
			sSubject = SHOP_NAME & " Order Confirmation (Order ref: " & aOrder(OD_ID,0) & ")"
			nErr = SendMailEnc(sTo, sToName, sFrom, sFromName, sSubject, sMsg, "", "", false, false)
		else
			nErr = 2
		end if
	else
		nErr = 1
	end if
	notifyCustomerOrderConfirmed = nErr
end function


function makeAddress(sTitle,sFirstName,sSurName,sAdd1,sAdd2,sAdd3,sAdd4,sAdd5,sPostCode)
	dim sTmp:sTmp = ""
	sTmp = replace(trim(sTitle & " " & sFirstName & " " & sSurName), "  ", " ") & vbcrlf
	if len("" & sAdd1) then	sTmp = sTmp & sAdd1 & vbcrlf
	if len("" & sAdd2) then	sTmp = sTmp & sAdd2 & vbcrlf
	if len("" & sAdd3) then	sTmp = sTmp & sAdd3 & vbcrlf
	if len("" & sAdd4) then	sTmp = sTmp & sAdd4 & vbcrlf
	if len("" & sAdd5) then	sTmp = sTmp & sAdd5 & vbcrlf
	if len("" & sPostCode) then	sTmp = sTmp & sPostCode & vbcrlf
	makeAddress = sTmp
end function
%>