<!--#include virtual="/ecomm/setup.asp"-->
<!--#include virtual="/ecomm/countryselector.asp"-->
<%
  Dim g_sFldr:g_sFldr="shop"
	Dim id, LHSid
  LHSid=14
  
dim nErr:nErr=0
dim nMeID, nCtID, nOdID, sDesc
dim sPayType:sPayType=trim("" & request.querystring("PAY"))
sPayType = "CC"
if len("" & sPayType) = 0 then
  response.redirect "./confirm.asp"
end if

nMeID = CLng("0" & session("meID"))
nCtID = CLng("0" & session("ctID"))
nOdID = CLng("0" & session("odID"))

if (nCtID = 0) OR (nMeID = 0) then
	response.redirect "./error.asp?err=Session%20expired"
end if

'// this is where we are going to do all of the hard work //
'// take one cart and make one order - couldn't be easier //
dim aMember:aMember=null
dim aCart:aCart=null
dim aCartItems:aCartItems=null
dim sSQL:sSQL=""
dim nPostZone:nPostZone=0
dim bVAT:bVAT=true
dim nPostage:nPostage=0
dim aOrder:aOrder=null
dim aOrderItem:aOrderItem=null
nErr = loadMember(nMeID, aMember)
if nErr > 0 then
  response.redirect "./error.asp"
end if
nErr = loadCart(nCtID, aCart)
if nErr = 0 then
  aCart(CT_KEYFMEID, 0) = nMeID
  saveCart(aCart)
  '// now let's calculate postage etc //
  if aMember(ME_USEALTADDRESS, 0) then
    nPostZone = mid(aMember(ME_ALTADDRESS5, 0), 4, 1)
    bVAT = (mid(aMember(ME_ALTADDRESS5, 0), 6, 1) = "1")
  else
    nPostZone = mid(aMember(ME_ADDRESS5, 0), 4, 1)
    bVAT = (mid(aMember(ME_ADDRESS5, 0), 6, 1) = "1")
  end if
  sSQL = "SELECT CI.*, PR.keyFpcID, PR.keyFwcID, PR.prPrice, PR.prPromoPrice, PR.prPromo FROM tblCartItem AS CI LEFT JOIN tblProduct AS PR ON CI.keyFprID=PR.prID WHERE CI.keyFctID=" & nCtID & " AND CI.keyFinID=" & INST_ID & ";"
  'sSQL = "SELECT CI.*, PR.keyFpcID, PR.keyFwcID FROM tblCartItem AS CI LEFT JOIN tblProduct AS PR ON CI.keyFprID=PR.prID WHERE CI.keyFctID=" & nCtID & " AND CI.keyFinID=" & INST_ID & ";"
  nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aCartItems)
  if nErr = 0 then
    nErr = calcCartPostage(aCartItems, nPostZone, nPostage)
    if nErr > 0 then
      response.redirect "./error.asp"
    end if
  else
    '// cart is empty //
    response.redirect "./"
  end if
  aCartItems = null
else
  response.redirect "./error.asp"
end if

dim sPostOpt,sGiftMsg,sDelMsg,bPartShip, nNote1ID, nNote2ID, sVCHR
nErr = getPostOptions(nCtID, sPostOpt, nNote1ID, sGiftMsg, nNote2ID, sDelMsg, bPartShip)

if nErr = 0 then
  '// let's do the notes if we need to //
  if len("" & sGiftMsg) > 0 then
    if nNote1ID > 0 then
      '// update the note //
      nErr = updateNote(nNote1ID, sGiftMsg)
    else
      '// create the note //
      nNote1ID = addNote(sGiftMsg)
    end if
  elseif nNote1ID > 0 then
    '// delete the note //
    nErr = deleteNote(nNote1ID)
    nNote1ID = 0
  end if
  if len("" & sDelMsg) > 0 then
    if nNote2ID > 0 then
      '// update the note //
      nErr = updateNote(nNote2ID, sDelMsg)
    else
      '// create the note //
      nNote2ID = addNote(sDelMsg)
    end if
  elseif nNote2ID > 0 then
    '// delete the note //
    nErr = deleteNote(nNote2ID)
    nNote2ID = 0
  end if
  nErr = setPostOptions(nCtID, sPostOpt, nNote1ID, sGiftMsg, nNote2ID, sDelMsg, bPartShip)
  if nOdID > 0 then
    nErr = loadOrder(nOdID, aOrder)
    if nErr = 0 then
      '// empty out the OrderItems //
      sSQL = "DELETE FROM tblOrderItem WHERE keyFodID=" & nOdID & " AND keyFinID=" & INST_ID & ";"
      call sqlExec(DSN_ECOM_DB, sSQL)
    else
      redim aOrder(OD_UBOUND, 0)
      aOrder(OD_ID, 0) = 0
      aOrder(OD_KEYFINID, 0) = INST_ID
    end if
  else
    redim aOrder(OD_UBOUND, 0)
    aOrder(OD_ID, 0) = 0
    aOrder(OD_KEYFINID, 0) = INST_ID
  end if
  aOrder(OD_KEYFMEID, 0) = nMeID
  aOrder(OD_DATE, 0) = CDate(now())
  aOrder(OD_SESSIONID, 0) = session.sessionID
  aOrder(OD_IPADDRESS, 0) = request.servervariables("REMOTE_HOST")
  aOrder(OD_EMAIL, 0) = aMember(ME_EMAIL, 0)
  aOrder(OD_PHONE, 0) = aMember(ME_TEL, 0)
  aOrder(OD_TITLE, 0) = aMember(ME_TITLE, 0)
  aOrder(OD_FIRSTNAME, 0) = aMember(ME_FIRSTNAME, 0)
  aOrder(OD_SURNAME, 0) = aMember(ME_SURNAME, 0)
  aOrder(OD_ADDRESS1, 0) = aMember(ME_ADDRESS1, 0)
  aOrder(OD_ADDRESS2, 0) = aMember(ME_ADDRESS2, 0)
  aOrder(OD_ADDRESS3, 0) = aMember(ME_ADDRESS3, 0)
  aOrder(OD_ADDRESS4, 0) = aMember(ME_ADDRESS4, 0)
  aOrder(OD_ADDRESS5, 0) = aMember(ME_ADDRESS5, 0)
  aOrder(OD_POSTCODE, 0) = aMember(ME_POSTCODE, 0)
  if aMember(ME_USEALTADDRESS, 0) then
    aOrder(OD_DELTITLE, 0) = aMember(ME_ALTTITLE, 0)
    aOrder(OD_DELFIRSTNAME, 0) = aMember(ME_ALTFIRSTNAME, 0)
    aOrder(OD_DELSURNAME, 0) = aMember(ME_ALTSURNAME, 0)
    aOrder(OD_DELADDRESS1, 0) = aMember(ME_ALTADDRESS1, 0)
    aOrder(OD_DELADDRESS2, 0) = aMember(ME_ALTADDRESS2, 0)
    aOrder(OD_DELADDRESS3, 0) = aMember(ME_ALTADDRESS3, 0)
    aOrder(OD_DELADDRESS4, 0) = aMember(ME_ALTADDRESS4, 0)
    aOrder(OD_DELADDRESS5, 0) = aMember(ME_ALTADDRESS5, 0)
    aOrder(OD_DELPOSTCODE, 0) = aMember(ME_ALTPOSTCODE, 0)
  else
    aOrder(OD_DELTITLE, 0) = aMember(ME_TITLE, 0)
    aOrder(OD_DELFIRSTNAME, 0) = aMember(ME_FIRSTNAME, 0)
    aOrder(OD_DELSURNAME, 0) = aMember(ME_SURNAME, 0)
    aOrder(OD_DELADDRESS1, 0) = aMember(ME_ADDRESS1, 0)
    aOrder(OD_DELADDRESS2, 0) = aMember(ME_ADDRESS2, 0)
    aOrder(OD_DELADDRESS3, 0) = aMember(ME_ADDRESS3, 0)
    aOrder(OD_DELADDRESS4, 0) = aMember(ME_ADDRESS4, 0)
    aOrder(OD_DELADDRESS5, 0) = aMember(ME_ADDRESS5, 0)
    aOrder(OD_DELPOSTCODE, 0) = aMember(ME_POSTCODE, 0)
  end if
  aOrder(OD_DELZONE, 0) = nPostZone
  aOrder(OD_POSTTYPE, 0) = sPostOpt
  aOrder(OD_POSTAGE, 0) = nPostage
  '// others will be filled in after the next step //
  aOrder(OD_TRANSSUC, 0) = false
  aOrder(OD_COMPLETEDALL, 0) = false
  aOrder(OD_PARTSHIP, 0) = bPartShip
  aOrder(OD_DISPATCHED, 0) = false
  aOrder(OD_DISPATCHDATE, 0) = null
  aOrder(OD_KEYFN1ID, 0) = nNote1Id
  aOrder(OD_KEYFN2ID, 0) = nNote2Id
  nErr = saveOrder(aOrder)
  if nErr = 0 then
    nErr = createOrderItems(aOrder(OD_ID, 0), nCtID, bVat, nPostage, nTotalNoVAT, nVAT, nTotal, sDesc)  '// accepts postage and vatable - returns VAT and Totals //
  else
	  response.redirect "./error.asp?err=Session%20expired"
  end if
  if nErr = 0 then
    aOrder(OD_TOTALNOVAT, 0) = nTotalNoVAT
    aOrder(OD_VAT, 0) = nVAT
    aOrder(OD_TOTALCHARGECALC, 0) = nTotal
    nErr = saveOrder(aOrder)
  else
	  response.redirect "./error.asp?err=Session%20expired"
  end if
else
	response.redirect "./error.asp?err=Session%20expired"
end if

session("ctID") = 0
session("odID") = 0
session("numItemsInCart") = 0


'// but for now //
'// this is where we branch from the payment methods //
'session("odID") = aOrder(OD_ID, 0)
'select case sPayType
'  case "CC"
'    response.redirect "./paymentCC.asp"
'  case "INV"
'    response.redirect "./paymentINV.asp"
'  case "PRO"
'    response.redirect "./paymentPRO.asp"
'  case else
'    response.redirect "./confirm.asp"
'end select

'// Build world pay form and attempt to auto submit it //
%>
<!--#include virtual="/includes/doctype.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title><%=SHOP_NAME%></title>
    <!--#include virtual="/includes/mainCSS.asp" -->
</head>
<!--#include virtual="/includes/body.asp" -->
<!--#include virtual="/includes/templateStart.asp" -->
<!--#include virtual="/includes/templateTopNav.asp" -->
<!--#include virtual="/includes/templateBeginContent.asp" -->
<!-- Begin Content -->   
    <p>
        <%=SHOP_NAME%> uses WorldPay for accepting credit, debit and charge cards payments for purchases on the www.earthworks.co.uk website. 
        WorldPay is part of the second biggest bank in Europe and is now a market-leading payment service provider with over 20,000 customers 
        trading over the Internet and more than 1,200 strategic banking and e-commerce partners worldwide.
    </p>
    <p>
    The payment processing pages you see here are held on the Worldpay secure server to ensure the safe secure handling of your 
    credit card information. The transactions are carried out over a secure link. All Worldpay transactions are encoded for your security.
    </p>

    <form name="wp" action="https://select.worldpay.com/wcc/purchase" method=POST>
    <input type=hidden name="instId" value="81794">
    <input type=hidden name="cartId" value="<%=encodeID(aOrder(OD_ID, 0))%>">
    <input type=hidden name="amount" value="<%=aOrder(OD_TOTALCHARGECALC, 0)%>">
    <input type=hidden name="currency" value="GBP">
    <input type=hidden name="desc" value="<%=sDesc%>">
    <input type=hidden name="testMode" value="0">
    <!--<input type=hidden name="MC_callback" value="<%=request.servervariables("HTTP_HOST")%>/shoppingbasket/receipt.asp">-->
    <input type=hidden name="MC_callback" value="www.earthworks.co.uk/shoppingbasket/receipt.asp">
    <input type=hidden name="name" value="<%=trim("" & aOrder(OD_TITLE, 0) & " " & aOrder(OD_FIRSTNAME, 0) & " " & aOrder(OD_SURNAME, 0))%>">
    <input type=hidden name="address" value="<%=aOrder(OD_ADDRESS1, 0)%>,&#10;<%=aOrder(OD_ADDRESS2, 0)%>,&#10;<%=aOrder(OD_ADDRESS3, 0)%>,&#10;<%=aOrder(OD_ADDRESS4, 0)%>">
    <input type=hidden name="postcode" value="<%=aOrder(OD_POSTCODE, 0)%>">
    <input type=hidden name="country" value="<%=left("" & aOrder(OD_ADDRESS5, 0), 2)%>">
    <input type=hidden name="tel" value="<%=aOrder(OD_PHONE, 0)%>">
    <input type=hidden name="email" value="<%=aOrder(OD_EMAIL, 0)%>">
    <p>If you are not immediately redirected to the WorldPay payment gateway please click the Submit button.</p>
    <input type=submit value="Submit">
    </form> 
    <script language="JavaScript" type="text/javascript">
    <!--
    document.forms['wp'].submit();
    //-->
    </script>
<!-- End Content -->
<!--#include virtual="/includes/templateEndContent.asp" -->

<!-- LHS navigation -->
<!--#include virtual="/includes/templateLHSnav.asp" -->
<!-- RHS navigation -->
<!--#in clude virtual="/includes/templateRHSnav.asp" -->

<!--#include virtual="/includes/templateFooter.asp" -->
<!--#include virtual="/includes/templateEnd.asp" -->
<%
'// accepts postage and vatable - returns VAT and Totals //
function createOrderItems(nOdID, nCtID, bVat, nPostage, nTotalNoVAT, nVAT, nTotal, sDesc)
  dim nErr:nErr=0
  dim sSQL:sSQL=""
  dim nIdx:nIdx=0
  dim nJdx:nJdx=0
  dim aItems:aItems=null
  dim aOrderItem:aOrderItem=null
  if (nOdID > 0) and (nCtID > 0) then
    sSQL = "SELECT 0," & nOdID & ",PR.prCODE,PR.prShortCode,PT.ptName,PR.prName,PR.prPrice,PR.prVAT,0,0,0,0,CI.ciQuantity,1,0,PR.prWrappable,'','',CI.keyFntID,0,PR.keyFltID,LT.ltText,0,null,''," & INST_ID _
        & ",PR.prIsChild,PR.prPromo,PR.prPromoPrice,PR.prPromoVat,PR.prStockQty,PR.prStockTrigger,PR.prColour,PR.prSize" _
        & " FROM ((tblCartItem AS CI LEFT JOIN tblProduct AS PR ON CI.keyFprID=PR.prID) LEFT JOIN tblProductType AS PT ON PR.keyFptID=PT.ptID) LEFT JOIN tblLeadTime AS LT ON PR.keyFltID=LT.ltID" _
        & " WHERE PR.prOnSale=1 AND CI.keyFctID=" & nCtID & " AND CI.keyFinID=" & INST_ID & " AND PR.keyFinID=" & INST_ID & " AND ((PT.keyFinID  IS NULL) OR PT.keyFinID=" & INST_ID & ") AND ((LT.keyFinID IS NULL) OR LT.keyFinID=" & INST_ID & ")" _
        & " ORDER BY CI.ciID ASC;"
    'response.write sSQL
    nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aItems)
    if nErr = 0 then
      for nIdx = 0 to UBound(aItems, 2)
        redim aOrderItem(OI_UBOUND, 0)
        for nJdx = 0 to OI_UBOUND
          aOrderItem(nJdx, 0) = aItems(nJdx, nIdx)
        next
        '// do the differences //
        '// chid prod alts //
        if aItems(OI_UBOUND + 1, nIdx) then
          if len("" & aItems(OI_UBOUND + 7, nIdx)) > 0 then
            aOrderItem(OI_DESCRIPTION, 0) = aOrderItem(OI_DESCRIPTION, 0) & "," & aItems(OI_UBOUND + 7, nIdx)
          end if
          if len("" & aItems(OI_UBOUND + 8, nIdx)) > 0 then
            aOrderItem(OI_DESCRIPTION, 0) = aOrderItem(OI_DESCRIPTION, 0) & "," & aItems(OI_UBOUND + 8, nIdx)
          end if
        end if
        '// price //
        if aItems(OI_UBOUND + 2, nIdx) then
          aOrderItem(OI_UNITCOST, 0) = aItems(OI_UBOUND + 3, nIdx)
          aOrderItem(OI_UNITVAT, 0) = aItems(OI_UBOUND + 4, nIdx)
        end if
        '// availability //
        if aItems(OI_UBOUND + 5, nIdx) <= aItems(OI_UBOUND + 6, nIdx) then
          if len("" & aOrderItem(OI_LEADTIMETEXT, 0)) = 0 then
            aOrderItem(OI_LEADTIMETEXT, 0) = DFLT_LEADTIME_OS
          end if
        else
          aOrderItem(OI_LEADTIMETEXT, 0) = DFLT_LEADTIME_IS
        end if
        nVAT = nVAT + (aOrderItem(OI_UNITVAT, 0) * aOrderItem(OI_QUANTITY, 0))
        nTotal = nTotal + (aOrderItem(OI_UNITCOST, 0) * aOrderItem(OI_QUANTITY, 0))
        nErr = saveOrderItem(aOrderItem)
        sDesc = sDesc & aOrderItem(OI_QUANTITY, 0) & "x " _
                      & aOrderItem(OI_DESCRIPTION, 0) & " @ &pound;" _
                      & formatnumber(aOrderItem(OI_UNITCOST, 0), 2) & "<br />"
        aOrderItem = null
      next
      nTotalNoVAT = nTotal - nVat
      if bVat then
        nTotal = nTotal
        sDesc = sDesc & "Total VAT: &pound;" & Formatnumber(nVAT, 2) & "<br />"
      else
        nTotal = nTotalNoVAT
        sDesc = sDesc & "Less VAT: &pound;" & Formatnumber(nVAT, 2) & "<br />"
      end if
      nTotal = nTotal + nPostage
      sDesc = sDesc & "Postage: &pound;" & formatnumber(nPostage, 2) & "<br />"
    end if
  else
    nErr = 1  '// bad input parms //
  end if
  createOrderItems = nErr
end function
%>