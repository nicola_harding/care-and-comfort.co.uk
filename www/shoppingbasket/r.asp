<!--#include virtual="/ecomm/setup.asp"-->
<!--#include virtual="/ecomm/countryselector.asp"-->
<%
dim nOrderID, nMemberID, nCartID, sID, nErr
nErr = 0

sID = "" & request.querystring("q")

if len(sID) > 0 then
	nOrderID = unEncodeID(sID)
end if

'// first off update the order details //
if nOrderID > 0 then
	nErr = getCustomerIDFromOrderID(nOrderID,nMemberID)
	session("odID") = nOrderID
	session("meID") = nMemberID
	if nMemberID > 0 then
    nErr = nErr + getCartIDFromMemberID(nMemberID, nCartID)
    session("ctID") = nCartID
		response.redirect "./confirm.asp"
	else
		response.redirect "./login.asp"
	end if
end if
response.redirect "./error.asp?err=Sesssion%20expired"

function getCustomerIDFromOrderID(nOrderID,nMemberID)
  dim nErr:nErr=0
  dim aOrder:aOrder=null
  nMemberID = 0
  nErr = loadOrder(nOrderID, aOrder)
  if nErr = 0 then
    nMemberID = aOrder(OD_KEYFMEID, 0)
  end if
  aOrder = null
  getCustomerIDFromMemberID=nErr
end function

function getCartIDFromMemberID(nMemberID,nCartID)
  dim nErr:nErr=0
  dim sSQL:sSQL=""
  nCartID = 0
  sSQL = "SELECT TOP 1 ctID FROM tblCart WHERE keyFinID=" & INST_ID & " AND keyFmeID=" & nMemberID & " ORDER BY ctID DESC;"
  nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aOrder)
  if nErr = 0 then
    nCartID = aOrder(0, 0)
  end if
  aOrder = null
  getCartIDFromMemberID=nErr
end function
%>