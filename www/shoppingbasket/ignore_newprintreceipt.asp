<!--#include virtual="/ecomm/setup.asp"-->
<!--#include virtual="/ecomm/countryselector.asp"-->
<%
Dim g_sFldr:g_sFldr="shop"
Dim id
  
dim nErr:nErr=0
dim nOdID:nOdID=0
dim nMeID:nMeID=0
dim sHeader:sHeader=""
dim aOrder:aOrder=null
dim aOrderItems:aOrderItems=null
dim bSuccess:bSuccess=false

nOdID = unencodeID("" & request.querystring("e"))
'// build the receipt
if nOdID > 0 then
	nErr = getMemberFromOrderID(nOdID,aMember)
  nMeID = aMember(ME_ID, 0)
  session("MeID") = nMeID
  nErr = loadOrder(nOdID, aOrder)
  if nErr = 0 then
    bSuccess = aOrder(OD_COMPLETEDALL, 0)
  else
  end if
	'// display the order contents //
	'sOrderHTML = displayOrder(nOrderID,true,BASKET_HDR_CLR,BASKET_ROW_CLR,nItems)
else
	'// the shopping basket is currently empty //
	'sOrderHTML = "no order details to show."
end if
%>
<!--#include virtual="/includes/doctype.asp"-->
<html>
<head>
<title>Jeanstore</title>
<meta http-equiv="Content-Language" content="en-gb">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="author" content="NVisage (http://www.nvisage.co.uk)">
<meta name="description" content="Levi Wrangler Lee denim jeans jackets shoes belts shirts from the Jean Store">
<meta name="keywords" content="jean store levi wrangler lee denim jeans jackets shoes belts shirts">
<!--#include virtual="/includes/copyright.asp"-->

<!--generic stylesheets-->
<!--#include virtual="/includes/main_css.asp"-->
<!--section specific stylesheet-->
<link type="text/css" rel="stylesheet" href="/css/section_basket_w3c.css" media="screen, tv, projection">
<script language="JavaScript" type="text/javascript">
<!--
function showTandC(){
	var tandcWin;
	tandcWin = window.open("/termsandconditions/tandc.asp","TandC","height=600,width=520,scrollbars,menubar,resizable");
	return false;
}
function popReceipt(sURL){
	var tandcWin;
	tandcWin = window.open(sURL,"Receipt","height=600,width=520,scrollbars,toolbar,menubar,resizable");
	return false;
}
//-->
</script>
<%=javaSetDropDown("registration")%>
</head>
<!--#include virtual="/includes/bodytag.asp"-->
<!--#include virtual="/includes/accessible_links.asp"-->

<!--begin template_start-->

<!--begin main container-->
<div id="main_container">
	
				<a name="main_content"></a>

						<!-- Begin Page Text-->
						<h2><%if bSuccess then
                    response.write "Your Receipt"
                  else
                    response.write "Payment Cancelled"
                  end if%></h2>
            <h3><%=SHOP_NAME%></h3>
					
          
          
        <%=showOrder(nOdId, aMember, BSKT_HDR_CLR, BSKT_ROW_CLR, BSKT_BGD_CLR)%>
        <div class="text">
        	<p><%=SHOP_NAME%> order number: <%=aOrder(OD_ID,0)%></p>
        	<p><%=SHOP_NAME%> member number: <%=aOrder(OD_KEYFMEID,0)%></p>
        	<p>Order Date: <%=aOrder(OD_DATE,0)%></p>
        </div>       
        </div>        
<!--end template_end-->
<!--#include virtual="/includes/endbodytag.asp"-->
<%
function getMemberFromOrderID(nOdID,aMember)
  dim nErr:nErr=0
  dim sSQL:sSQL = ""
  if nOdID > 0 then
    sSQL = "SELECT ME.* FROM tblMember AS ME LEFT JOIN tblOrder AS OD ON OD.keyFmeID=ME.meID" _
        & " WHERE OD.odID=" & nOdID & " AND OD.keyFinID=" & INST_ID & " AND ME.keyFinID=" & INST_ID & ";"
    nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aMember)
  else
    nErr = 1  '// bad input parms //
  end if
  getMemberFromOrderID = nErr
end function

function showOrder(nOdID, aMember, sHedClr, sRowClr, sBkgClr)
  dim nErr:nErr=0
  dim sTmp:sTmp=""
  dim aItems:aItems=null
  dim nIdx:nIdx=0
  dim sSQL:sSQL=""
  dim nTotal, nSubTotal, nVat, nTotalVAT
  dim sName, sLeadTime, nPrice, nQty
  nIdx = 0
  sTmp = ""
  dim sMbrAdd, sDelAdd
  if nOdID > 0 then
    sSQL = "SELECT OD.*,OI.* FROM tblOrder AS OD LEFT JOIN tblOrderItem AS OI ON OD.odID=OI.keyFodID" _
        & " WHERE OD.odID=" & nOdID & " AND OD.keyFinID= "& INST_ID & " AND OI.keyFinID=" & INST_ID & " ORDER BY OI.oiID ASC;"
    nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aItems)
    if nErr = 0 then
%>
    
    <table border="0" cellspacing="0" cellpadding="0" class="text">
    <tr class="shopping_title">
    <td class="bold_highlight">&nbsp;</td>
    <td class="bold_highlight">Description</td>
    <td class="bold_highlight">&nbsp;</td>
    <td align="center" class="bold_highlight">Quantity</td>
    <td class="bold_highlight">&nbsp;</td>
    <td align="right" class="bold_highlight">Price</td>
    <td class="bold_highlight">&nbsp;</td>
    <td align="right" class="bold_highlight">Sub Total</td>
    <td class="bold_highlight">&nbsp;</td></tr>
<%for nIdx=0 to UBound(aItems, 2)
%>
<tr bgcolor="<%=sRowClr%>">
<td>&nbsp;</td>
<td valign="top"><%=aItems(OD_UBOUND + 1 + OI_DESCRIPTION, nIdx)%><!--<br><small>Delivery - <%=aItems(OD_UBOUND + 1 + OI_LEADTIMETEXT, nIdx)%></small>--></td>
<td>&nbsp;</td>
<td valign="top" align="center"><%=aItems(OD_UBOUND + 1 + OI_QUANTITY, nIdx)%></td>
<td>&nbsp;</td>
<td valign="top" align="right">&pound;<%=formatnumber(aItems(OD_UBOUND + 1 + OI_UNITCOST, nIdx), 2)%></td>
<td>&nbsp;</td>
<td valign="top" align="right">&pound;<%=formatnumber(aItems(OD_UBOUND + 1 + OI_UNITCOST, nIdx) * aItems(OD_UBOUND + 1 + OI_QUANTITY, nIdx), 2)%></td>
<td>&nbsp;</td></tr>
<%next%>
<tr bgcolor="<%=sHedClr%>" class="shopping_title">
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td align="right">Sub Total</td>
<td>&nbsp;</td>
<td align="right">&pound;<%=formatnumber(aItems(OD_TOTALNOVAT, 0) + aItems(OD_VAT, 0), 2)%></td>
<td>&nbsp;</td></tr>


<%if bVAT or (VAT_OPTS = 0) then
%>
<tr bgcolor="<%=sRowClr%>">
<td>&nbsp;</td>
<td colspan="3">VAT Included</td>
<td>&nbsp;</td>
<td align="right">(&pound;<%=formatnumber(aItems(OD_VAT, 0), 2)%>)</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td colspan="1">&nbsp;</td>
</tr>
<%else
%>
<tr bgcolor="<%=sRowClr%>">
<td>&nbsp;</td>
<td colspan="3">VAT</td>
<td>&nbsp;</td>
<td align="right">-&pound;<%=formatnumber(aItems(OD_VAT, 0), 2)%></td>
<td>&nbsp;</td>
<td align="right">-&pound;<%=formatnumber(aItems(OD_VAT, 0), 2)%></td>
<td colspan="1">&nbsp;</td>
</tr>
<%end if%>

<tr>
<td>&nbsp;</td>
<td colspan="3">Postage &amp; Packaging</td>
<td>&nbsp;</td>
<td align="right">&pound;<%=formatnumber(aItems(OD_POSTAGE, 0), 2)%></td>
<td>&nbsp;</td>
<td align="right">&pound;<%=formatnumber(aItems(OD_POSTAGE, 0), 2)%></td>
<td colspan="1">&nbsp;</td>
</tr>


<tr class="shopping_title">
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td align="right">Total</td>
<td>&nbsp;</td>
<td align="right">&pound;<%=formatnumber(aItems(OD_TOTALCHARGECALC, 0), 2)%></td>
<td colspan="1">&nbsp;</td>
</tr>


</table>

<%
sMbrAdd=""
sDelAdd=""

sMbrAdd = sMbrAdd & "&nbsp;" & trim("" & aOrder(OD_TITLE, 0) & " " & aOrder(OD_FIRSTNAME, 0) & " " & aOrder(OD_SURNAME, 0)) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aOrder(OD_ADDRESS1, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aOrder(OD_ADDRESS2, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aOrder(OD_ADDRESS3, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aOrder(OD_ADDRESS4, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aOrder(OD_POSTCODE, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & getCountry(aOrder(OD_ADDRESS5, 0)) & "<br />" & vbcrlf

sDelAdd = sDelAdd & "&nbsp;" & trim("" & aOrder(OD_DELTITLE, 0) & " " & aOrder(OD_DELFIRSTNAME, 0) & " " & aOrder(OD_DELSURNAME, 0)) & "<br />" & vbcrlf
sDelAdd = sDelAdd & "&nbsp;" & aOrder(OD_DELADDRESS1, 0) & "<br />" & vbcrlf
sDelAdd = sDelAdd & "&nbsp;" & aOrder(OD_DELADDRESS2, 0) & "<br />" & vbcrlf
sDelAdd = sDelAdd & "&nbsp;" & aOrder(OD_DELADDRESS3, 0) & "<br />" & vbcrlf
sDelAdd = sDelAdd & "&nbsp;" & aOrder(OD_DELADDRESS4, 0) & "<br />" & vbcrlf
sDelAdd = sDelAdd & "&nbsp;" & aOrder(OD_DELPOSTCODE, 0) & "<br />" & vbcrlf
sDelAdd = sDelAdd & "&nbsp;" & getCountry(aOrder(OD_DELADDRESS5, 0)) & "<br />" & vbcrlf

sMbrAdd = replace(sMbrAdd, "&nbsp;<br />", "")
sDelAdd = replace(sDelAdd, "&nbsp;<br />", "")
%>


<table border="0" cellspacing="0" cellpadding="0" class="text">
<tr><td width="45%">&nbsp;<br>&nbsp;</td><td width="10%">&nbsp;</td><td width="45%">&nbsp;</td></tr>
<tr><td bgcolor="<%=sHedClr%>" class="shopping_title">&nbsp;Billing Address&nbsp;</td>
<td class="shopping_title">&nbsp;</td><td bgcolor="<%=sHedClr%>" class="shopping_title">&nbsp;Delivery Address&nbsp;</td></tr>
<tr bgcolor="<%=sRowClr%>"><td valign="top">
<p><%=sMbrAdd%></p>
</td><td>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign="top">
<p><%=sDelAdd%></p></td></tr>
<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
</table>

<%
dim sPostOpt, nNoteID, sGiftMsg, nNote2ID, sDelMsg, bPartShip
nErr = getPostOptions(nCtID, sPostOpt, nNoteID, sGiftMsg, nNote2ID, sDelMsg, bPartShip)
if nErr = 0 then
sTmp = sTmp & " <table border=""0"" cellspacing=""0"" cellpadding=""0"" class=""text"">" _
		& "<tr><td>&nbsp;</td></tr>"
sTmp = sTmp & "<tr><td bgcolor=""" & sHedClr & """ class=""shopping_title"">&nbsp;Order Options&nbsp;</td></tr>"
if bPartShip then
sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;Order items may be shipped seperately,&nbsp;</td></tr>"
else
sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;Order items will be shipped as a single parcel,&nbsp;</td></tr>"
end if
if sPostOpt = "24H" then
sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;via special delivery.&nbsp;</td></tr>"
else
sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;via standard delivery.&nbsp;</td></tr>"
end if
if len(sDelMsg) > 0 then
sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;Your order will contain the following delivery instruction:&nbsp;</td></tr>"
sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td><i>" & replace(sDelMsg, vbcrlf, "&nbsp;<br />") & "</i></td></tr>"
end if
if len(sGiftMsg) > 0 then
sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;Your order will contain the following gift message:&nbsp;</td></tr>"
sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td><i>" & replace(sGiftMsg, vbcrlf, "&nbsp;<br />") & "</i></td></tr>"
end if
sTmp = sTmp & "<td>&nbsp;</td></tr></table>"
end if
response.write sTmp
sTmp = ""
%>
     
<%
    else
      sTmp = "&nbsp;<br />No details to show."
    end if
  else
    sTmp = "&nbsp;<br />No details to show."
  end if
  showOrder = sTmp
end function
%>