<!--#include virtual="/includes/careandcomfortAPI.asp"-->
<!--#include virtual="/ecomm/setup.asp"-->
<!--#include virtual="/ecomm/countryselector.asp"-->
<!--#include virtual="/shoppingbasket/protex_functions.asp"-->
<!--#include virtual="/cms/setup.asp"-->
<!--#include virtual="/includes/dates.asp" -->


<%
dim nErr:nErr=0
dim nMeID, nCtID, nOdID, sX, nDiscountVal, sPromoError
dim blnPostZoneCovered : blnPostZoneCovered = false

session("isSuccessYes") = ""
session("hasBeen")      = ""
session("promocode")    = "" 

if len("" & request("promo")) > 0 then
    session("promocode") = request("promo")   
end if 

nMeID = CLng("0" & session("meID"))
nCtID = CLng("0" & session("ctID"))
nOdID = CLng("0" & session("odID"))

if (nMeID = 0) or (nCtID = 0) then response.redirect "./"

'// let's update the cart with the member ID //
dim aMember     :aMember=null
dim aCart       :aCart=null
dim aCartItems  :aCartItems=null
dim sSQL        :sSQL=""
dim nPostZone   :nPostZone=0
dim bVAT        :bVAT=true
dim nPostage    :nPostage=0

nErr = loadMemberBespoke(nMeID, aMember)
if nErr > 0 then
    response.redirect "./error.asp?confirm=lmb"
end if

nErr = loadCart(nCtID, aCart)
if nErr = 0 then
    aCart(CT_KEYFMEID, 0) = nMeID
    saveCart(aCart)  
    '// now lets calculate postage etc //
    if aMember(ME_USEALTADDRESS, 0) then
        nPostZone           = mid(aMember(ME_ALTADDRESS5, 0), 4, 1)
        blnPostZoneCovered  = aMember(ME_UBOUND+2,0)
        bVAT                = (mid(aMember(ME_ALTADDRESS5, 0), 6, 1) = "1")
    else
        nPostZone           = mid(aMember(ME_ADDRESS5, 0), 4, 1)
        blnPostZoneCovered  = aMember(ME_UBOUND+1,0)
        bVAT                = (mid(aMember(ME_ADDRESS5, 0), 6, 1) = "1")
    end if
    if blnPostZoneCovered = "" or len(blnPostZoneCovered) = 0 then
        blnPostZoneCovered = false
    end if

    ' This is for international customers vat logic
    if cbool(bVAT) then
        bVAT = true
    else
        bVAT = false
    end if      
    
    'latests just archived sSQL = "SELECT CI.*, PR.prName, PC.pcID, PC.pcCode, PC.pcName, PC.pcRateUK, PC.pcRateEU, PC.pcRateROW FROM tblCartItem AS CI INNER JOIN tblProduct PR ON CI.keyFprID=PR.prID LEFT JOIN tblPostageCategory PC ON PR.keyFpcID=PC.pcID WHERE CI.keyFctID=" & nCtID & " AND CI.keyFinID=" & INST_ID & ";"
       
    'original: sSQL = "SELECT CI.*, PR.keyFpcID, PR.keyFwcID, PR.prPrice, PR.prPromoPrice, PR.prPromo, PR.keyFptID FROM tblCartItem AS CI LEFT JOIN tblProduct AS PR ON CI.keyFprID=PR.prID WHERE CI.keyFctID=" & nCtID & " AND CI.keyFinID=" & INST_ID & ";"
    
    sSQL = "SELECT CI.*, PR.prName, PC.pcID, PC.pcCode, PC.pcName, PC.pcRateUK, PC.pcRateEU, PC.pcRateROW, NT.ntText FROM tblCartItem AS CI " _
            & " INNER JOIN tblProduct PR ON CI.keyFprID=PR.prID LEFT JOIN tblPostageCategory PC ON PR.keyFpcID=PC.pcID " _
            & " LEFT JOIN tblNote NT ON NT.ntID=CI.keyFntID " _
            & " WHERE CI.keyFctID=" & nCtID & " AND CI.keyFinID=" & INST_ID & ";"
    
    nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aCartItems)
    'response.Write "<br />sSQL: " & sSQL
    
    if nErr = 0 then
        nErr = calcCartPostage(aCartItems, nPostZone, nPostage, sPostageMsg)
        aCartItems = null
        if nErr > 0 then
            response.redirect "./error.asp?confirm=postage1" & nErr
        end if        
    else
        '// cart is empty //
        response.redirect "./"
    end if
else
    response.redirect "./error.asp?confirm=cart"
end if

'if sAct = "vchr" then
'	processVoucherForm sX, nMemberID
'end if
%>

<!--#include virtual="/includes/doctype.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
     <%=GetDefaultEcommMetaTags("Confirm order - " & SHOP_NAME)%>  
    <!--#include virtual="/includes/mainCSS.asp" -->
	  <!--#include virtual="/includes/javascript.asp" -->
</head>

<!--#include virtual="/includes/templateStart.asp" -->
<!--#include virtual="/includes/templateTopNav.asp" -->
<!--#include virtual="/includes/templateLeftNav.asp" -->


<!--#include virtual="/includes/templateBeginMainContent.asp" -->
	
<!-- Begin Content -->
    
  <!--#include virtual="/includes/templateBasketbreadCrumb.asp" -->  
<h1>Confirm Order</h1>
<br/>
<%=showCartRO(nCtID, aMember, BSKT_HDR_CLR, BSKT_ROW_CLR, BSKT_BGD_CLR, bVAT, nPostZone, nPostage, blnPostZoneCovered)%>

<!-- End Content -->
<!--#include virtual="/includes/templateEndMainContent.asp" -->

<!--#include virtual="/includes/templateRightColumn.asp" -->

<!--#include virtual="/includes/templateFooter.asp" -->

<!--#include virtual="/includes/templateEnd.asp" -->

<%
function showCartRO(nCtID, aMember, sHedClr, sRowClr, sBkgClr, bVat, nPostZone, nPostage, blnPostZoneCovered)
  dim nErr          : nErr=0
  dim sTmp          : sTmp=""
  dim aCartItems    : aCartItems=null
  dim nIdx          : nIdx=0
  dim sSQL          : sSQL=""
  dim nTotal, nSubTotal, nVat, nLineItemVAT, nTotalVAT, nTotalNoVAT, sName, sLeadTime, nPrice, nQty, nDiscountedTotal
  dim nTotalNoPostage
  dim nProtexTotal
  dim sMbrAdd, sDelAdd
  dim aProduct(48)
  dim nFieldIdx
  dim nOriginalPrice, nOriginalVAT
  nIdx = 0
  sTmp = ""
  if len("" & sLinkCont) = 0 then
    sLinkCont = "/shop/"
  end if  
  if nCtID > 0 then
     nErr = getCartItemsProductsByCartID(nCtID, aCartItems)   
        if nErr = 0 then
%>
    <table border="0" class="cart_table" bgcolor="<%=sBkgClr%>" cellspacing="0" cellpadding="0" >
    <tr >
    <th>&nbsp;</th>
    <th>Description</th>
    <th>&nbsp;</th>
    <th align="center">Quantity</th>
    <th>&nbsp;</th>
    <th align="right">Price&nbsp;&nbsp;</th>
    <th>&nbsp;</th>
    <th align="right">Line&nbsp;Total</th>
    <th>&nbsp;</th></tr>
<%
dim blnIsFinalCartLineItem : blnIsFinalCartLineItem = true
dim sNote

for nIdx=0 to UBound(aCartItems, 2)
    blnIsFinalCartLineItem  = isFinalCartLineItem(aCartItems(PR_KEYFPTID, nIdx))
    nCiID                   = aCartItems(PR_UBOUND + 3, nIdx)
   
    sName = "<strong>" & aCartItems(PR_NAME, nIdx) & "</strong>" 
    ' Sizing details
    if len("" & aCartItems(PR_UBOUND + 7, nIdx)) > 0 then
        sName = sName & "<br />" & aCartItems(PR_UBOUND + 7, nIdx)
    end if
    if len("" & aCartItems(PR_SIZE, nIdx)) > 0 then
        sName = sName & "<br />" & aCartItems(PR_SIZE, nIdx)
    end if
    if len("" & aCartItems(PR_COLOUR, nIdx)) > 0 then
        sName = sName & "<br />" & aCartItems(PR_COLOUR, nIdx)
    end if
     ' Manufacturer / photographer details
    if clng("0" & aCartItems(PR_KEYFMFID, nIdx)) > 1 then
        sName = sName & "<br />Image by: " & aCartItems(PR_UBOUND + 8, nIdx)
    end if  

    
    
    ' ------------------------------------
    '
    ' Some of the cart product notes in this system containg double pipe separated (||) db product id and note
    ' The product id in the note is a workaround to relating products to eachother so that they make a single linitem
    ' for now, the basket and confirm and receipt displays do not use this this stored productid in the code display logic
    ' an ordering of the db returned data by cart item id is proving sufficient at this stage. 
    ' For safety: in the future, if needed, this stored note info could be used via getNoteExtraction("id") etc etc
    '  
    ' ------------------------------------
    nNtID = aCartItems(PR_UBOUND + 5, nIdx)	
    if nNtID > 0 then
        sNote     = getNote(nNtID) 
        sNoteText = cstr(getNoteExtraction(sNote, "text"))
        if len("" & sNoteText) > 0 then
		    sName = sName & "<br /> " & sNoteText
	    end if    
	end if    
          
    sLeadTime = aCartItems(50, nIdx) 
    if len("" & sLeadTime) = 0 then
        sLeadTime = DFLT_LEADTIME_IS_ECOMM
    end if 
    
    ' ================================================ 
    ' Put all the fields of the current array row into an array for the bespoke code to use
    for nFieldIdx = 0 to ubound(aCartItems, 1)
       ' Need this to stop population of aProduct when all the product db fields are retrieved from the aCartItems datasat
       if nFieldIdx <= ubound(aProduct) then
            aProduct(nFieldIdx) = aCartItems(nFieldIdx,nIdx)
       end if
    next   
    nErr = getAppropriatePrice(aProduct, nPrice, nVAT, nOriginalPrice, nOriginalVAT)                    
    ' ================================================ 
   
    nQty                = aCartItems(PR_UBOUND + 1, nIdx)
    if isnull(nQty) then
      nQty = 1
    end if
    nSubTotal           = nQty * nPrice
    nSubTotal           = formatnumber(ccur(nSubTotal), 2)
    nLineItemVAT        = nQty * nVAT  
    nTotalVAT           = nTotalVAT + nLineItemVAT
    nTotalNoPostage     = ccur(nTotalNoPostage) + ccur(nSubTotal)
    nTotalNoVAT         = nTotalNoPostage - nTotalVAT
      
    nPostage    = formatnumber(nPostage, 4) 
    nTotalVAT   = formatnumber(nTotalVAT, 4)  
    
    '+++++++++++++++++++++++
    '
    ' getPriceForexHTML: will take a price and spit out the html for the appropriate forex value and symbol
    '
    '+++++++++++++++++++++++
     
%>
<tr bgcolor="<%=sRowClr%>" <%if blnIsFinalCartLineItem then response.write "class=""line_item"""%>>
    <td>&nbsp;</td>
    <td valign="top">
        <%
        response.Write sName
        if len("" & sLeadTime) > 0 then 
            response.Write "<br /><small>Delivery - " & sLeadTime & "</small>"
        end if
        %>
    </td>
    <td>&nbsp;</td>
    <td valign="top" align="center"><%=nQty%></td>
    <td>&nbsp;</td>
    <td valign="top" align="right"><%=getPriceForexHTML(nPrice, 2)%></td>
    <td>&nbsp;</td>
    <td valign="top" align="right"><%=getPriceForexHTML(nSubTotal, 2)%></td>
    <td>&nbsp;</td>
</tr>
<%
next
%>
 <tr class="header_bold">
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">Sub Total</td>
    <td>&nbsp;</td>
    <td align="right"><%=getPriceForexHTML(nTotalNoPostage, 2)%></td>
    <td>&nbsp;</td>
</tr>
<%
' // If this is true, then the customer is being charged vat
if bVAT then
  nTotal = ccur(nTotalNoPostage) + ccur(nPostage)
%>
<tr bgcolor="<%=sRowClr%>">
    <td>&nbsp;</td>
    <td colspan="3">VAT Included</td>
    <td>&nbsp;</td>
    <td align="right">(<%=getPriceForexHTML(nTotalVAT, 2)%>)</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="1">&nbsp;</td>
</tr>
<%
else
    nTotal = ccur(nTotalNoPostage) - ccur(nTotalVAT) + ccur(nPostage)
%>
<tr bgcolor="<%=sRowClr%>">
    <td>&nbsp;</td>
    <td colspan="3">VAT</td>
    <td>&nbsp;</td>
    <td align="right">-<%=getPriceForexHTML(nTotalVAT, 2)%></td>
    <td>&nbsp;</td>
    <td align="right">-<%=getPriceForexHTML(nTotalVAT, 2)%></td>
    <td colspan="1">&nbsp;</td>
</tr>
<%
end if
if false Then
%>
<tr>
    <td>&nbsp;</td>
    <td colspan="3">Postage &amp; Packaging</td>
    <td>&nbsp;</td>
    <td align="right"><%=getPriceForexHTML(nPostage, 2)%></td>
    <td>&nbsp;</td>
    <td align="right"><%=getPriceForexHTML(nPostage, 2)%></td>
    <td colspan="1">&nbsp;</td>
</tr>
<%
End If
if len("" & session("promocode")) > 0 then
    nErr = calcPromotionalDiscount(session("promocode"), nTotalNoPostage, nDiscountVal)

    if nErr = 10 then
        sPromoError = "<p class=""red_text"">Incorrect promotional code entered.</p><br />"        
    else
        if nErr = 20 then
            sPromoError = "<p class=""red_text"">The promotion has ended.</p><br />"         
        end if
    end if
end if

if nDiscountVal > 0 then
    nDiscountVal        = formatnumber(nDiscountVal, 4) 
    nDiscountedTotal    = formatnumber((nTotal - nDiscountVal), 4)
%>
    <tr class="header_bold">
    <td>&nbsp;</td>
    <td>Discount:</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right"><strong>-<%=getPriceForexHTML(nDiscountVal, 3)%></strong></td>
    <td>&nbsp;</td></tr>

    <tr class="header_bold">
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">Total</td>
    <td>&nbsp;</td>
    <td align="right"><strong><%=getPriceForexHTML(nDiscountedTotal, 2)%></strong></td>
    <td>&nbsp;</td></tr>
<%
else    
%>
     <tr class="header_bold">
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td align="right"><strong>Total</strong></td>
        <td>&nbsp;</td>
        <td align="right"><strong><%=getPriceForexHTML(nTotal, 2)%></strong></td>
        <td colspan="1">&nbsp;</td>
    </tr>
<%
end if
' // Always end the total amount (Exclude any discounts, as the main protx function will calculate it again later)
' // This allows the protx receipt to reflect the original amount and the discounted amount, coz ppl like to see they saved money ;)
nProtexTotal = formatnumber(nTotal, 2)

response.write "</table>"

sMbrAdd = ""
sDelAdd = ""
sMbrAdd = sMbrAdd & "&nbsp;" & trim("" & aMember(ME_TITLE, 0) & " " & aMember(ME_FIRSTNAME, 0) & " " & aMember(ME_SURNAME, 0)) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aMember(ME_ADDRESS1, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aMember(ME_ADDRESS2, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aMember(ME_ADDRESS3, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aMember(ME_ADDRESS4, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aMember(ME_POSTCODE, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & getCountry(aMember(ME_ADDRESS5, 0)) & "<br />" & vbcrlf
if aMember(ME_USEALTADDRESS, 0) then
  sDelAdd = sDelAdd & "&nbsp;" & trim("" & aMember(ME_ALTTITLE, 0) & " " & aMember(ME_ALTFIRSTNAME, 0) & " " & aMember(ME_ALTSURNAME, 0)) & "<br />" & vbcrlf
  sDelAdd = sDelAdd & "&nbsp;" & aMember(ME_ALTADDRESS1, 0) & "<br />" & vbcrlf
  sDelAdd = sDelAdd & "&nbsp;" & aMember(ME_ALTADDRESS2, 0) & "<br />" & vbcrlf
  sDelAdd = sDelAdd & "&nbsp;" & aMember(ME_ALTADDRESS3, 0) & "<br />" & vbcrlf
  sDelAdd = sDelAdd & "&nbsp;" & aMember(ME_ALTADDRESS4, 0) & "<br />" & vbcrlf
  sDelAdd = sDelAdd & "&nbsp;" & aMember(ME_ALTPOSTCODE, 0) & "<br />" & vbcrlf
  sDelAdd = sDelAdd & "&nbsp;" & getCountry(aMember(ME_ALTADDRESS5, 0)) & "<br />" & vbcrlf
else
  sDelAdd = sMbrAdd
end if
sMbrAdd = replace(sMbrAdd, "&nbsp;<br />", "")
sDelAdd = replace(sDelAdd, "&nbsp;<br />", "")
%>

<table style="margin-top:25px;" class="cart_table"  width="100%" border="0" bgcolor="<%=sBkgClr%>" cellspacing="0" cellpadding="2">
   
    <tr class="header_bold">
        <th >&nbsp;Billing Address&nbsp;</th>
        <th>&nbsp;</th>
        <th>&nbsp;Delivery Address&nbsp;</th>
    </tr>
    <tr bgcolor="<%=sRowClr%>">
        <td valign="top">
       <%=sMbrAdd%>
        </td>
        <td>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td valign="top">
         <%=sDelAdd%></td></tr>


<%
dim sPostOpt, nNoteID, sGiftMsg, nNote2ID, sDelMsg, bPartShip
dim blnIsPostInfoRetrieved : blnIsPostInfoRetrieved = false

nErr = getPostOptions(nCtID, sPostOpt, nNoteID, sGiftMsg, nNote2ID, sDelMsg, bPartShip)
if nErr = 0 then
    blnIsPostInfoRetrieved = true
    if len(sDelMsg) > 0 then
        sTmp = sTmp & " <tr><td colspan=""3""><table width=""100%"" border=""0"" bgcolor=""" & sBkgClr & """ cellpadding=""2"" cellspacing=""0"" class=""text"">"
        sTmp = sTmp & "<tr><td bgcolor=""" & sHedClr & """ class=""cartHeaders"">&nbsp;Order Options&nbsp;</td></tr>"
        sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;Your order will contain the following delivery instruction:&nbsp;</td></tr>"
        sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td><i>" & replace(sDelMsg, vbcrlf, "&nbsp;<br />") & "</i></td></tr>"
        sTmp = sTmp & "</table></td></tr>"
    end if
   
end if
response.write sTmp
sTmp = ""
%>

    <tr><td colspan="3">&nbsp;</td></tr>
    <tr>
        <td colspan="3">
            <p class="form_header">Are these details correct?</p>
        </td>
    </tr>
</table>


<p>
    <a href="./index.asp"><img src="/ecomm/buttons/button_noeditorder.gif" border="0" alt="No, Edit Order" /></a>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="./register.asp"><img src="/ecomm/buttons/button_noeditprofile.gif" border="0" alt="No, Edit Address" /></a>
</p>
<br />
<%
if len("" & sPromoError) > 0 then
    response.Write sPromoError
end if
%>

<!--table summary="Promotional Discount Options" border="0" bgcolor="<%=BSKT_BGD_CLR%>" cellpadding="0" width="100%" cellspacing="0">
    <tr class="header_bold_promo">    
        <td colspan="2"><b>&nbsp;Use a promotional discount code?&nbsp;</b></td>
    </tr>
    <tr bgcolor="<%=BSKT_ROW_CLR%>">
        <td colspan=2 valign="top">	
            <%'=/shoppingbasket/confirm.asp %>
            <FORM action="#" method="post" id="frmPromotion" name="frmPromotion" class="main_form"> 
            <fieldset class="fields">
                <label for="promo" class="standard">Please enter your code here:</label>
                <input type="text" class="standard" name="promo" value="<%=session("promocode")%>">
                <br /><br /><br />
                <input type="image" src="/ecomm/buttons/button_submit.gif" class="image_right" onclick="javascript: alert('Developer note: Awaiting 55 Max voucher requirements (if any)')" class="image_right" value="Update my total to reflect my promotional code discount" alt="Update my total to reflect my promotional code discount" />                        
            </fieldset>
            </FORM>
        </td>
    </tr>
</table-->
   
    
<table summary="Payment Options" border="0" bgcolor="<%=BSKT_BGD_CLR%>" cellpadding="0" width="100%" cellspacing="0">
<%
if not blnPostZoneCovered then
'// outside of uk and EURO europe and US dollars regions //
%>
    <tr>
        <td colspan="2">
            <p class="red_text">Unfortunately the country you have chosen for your registered profile does not allow you to order online.
            <br /><br />Please <a href="./register.asp" title="Edit My Profile">change your profile's delivery address</a> 
            or <a href="/contactus/" title="contact us">contact us</a> for offline purchase and delivery price information.
            <br /><br />We apologise for any inconvenience this may have caused.</p>
        </td>
    </tr>
<%
else
'*** SAVE LOCAL COPY OF ORDER, BACKS UP TRANSACTIONS, this pulled from Pay.asp

'// this is where we are going to do all of the hard work //
'// take one cart and make one order - couldnt be easier //
dim aOrder      : aOrder = null
dim aOrderItem  : aOrderItem = null

nErr = loadCart(nCtID, aCart)
if nErr = 0 then
    aCart(CT_KEYFMEID, 0) = nMeID
    saveCart(aCart)    
else
    response.redirect "./error.asp?confirm=cart2"
end if

if blnIsPostInfoRetrieved then
    '// lets do the notes if we need to //
    if len("" & sGiftMsg) > 0 then
        if nNoteID > 0 then
            '// update the note //
            nErr = updateNote(nNoteID, sGiftMsg)
        else
            '// create the note //
            nNoteID = addNote(sGiftMsg)
        end if
    end if
    if len("" & sDelMsg) > 0 then
        if nNote2ID > 0 then
            '// update the note //
            nErr = updateNote(nNote2ID, sDelMsg)
        else
            '// create the note //
            nNote2ID = addNote(sDelMsg)
        end if
    end if  
    nErr = setPostOptions(nCtID, sPostOpt, nNoteID, sGiftMsg, nNote2ID, sDelMsg, bPartShip)
  
    nTotalNoVAT2 = nTotalNoVAT
    nVAT2        = nTotalVAT
    nTotal2      = nTotal
  
    if nOdID > 0 then
        nErr = loadOrder(nOdID, aOrder)
        if nErr = 0 then
            '// empty out the OrderItems //
            sSQL = "DELETE FROM tblOrderItem WHERE keyFodID=" & nOdID & " AND keyFinID=" & INST_ID & ";"
            call sqlExec(DSN_ECOM_DB, sSQL)
        else
            redim aOrder(OD_UBOUND, 0)
            aOrder(OD_ID, 0) = 0
            aOrder(OD_KEYFINID, 0) = INST_ID
        end if
    else
        redim aOrder(OD_UBOUND, 0)
        aOrder(OD_ID, 0) = 0
        aOrder(OD_KEYFINID, 0) = INST_ID
    end if
    aOrder(OD_KEYFMEID, 0)  = nMeID
    aOrder(OD_DATE, 0)      = CDate(now())
    aOrder(OD_SESSIONID, 0) = session.sessionID
    aOrder(OD_IPADDRESS, 0) = request.servervariables("REMOTE_HOST")
    aOrder(OD_EMAIL, 0) = aMember(ME_EMAIL, 0)
    aOrder(OD_PHONE, 0) = aMember(ME_TEL, 0)
    aOrder(OD_TITLE, 0) = aMember(ME_TITLE, 0)
    aOrder(OD_FIRSTNAME, 0)= aMember(ME_FIRSTNAME, 0)
    aOrder(OD_SURNAME, 0)  = aMember(ME_SURNAME, 0)
    aOrder(OD_ADDRESS1, 0) = aMember(ME_ADDRESS1, 0)
    aOrder(OD_ADDRESS2, 0) = aMember(ME_ADDRESS2, 0)
    aOrder(OD_ADDRESS3, 0) = aMember(ME_ADDRESS3, 0)
    aOrder(OD_ADDRESS4, 0) = aMember(ME_ADDRESS4, 0)
    aOrder(OD_ADDRESS5, 0) = aMember(ME_ADDRESS5, 0)
    aOrder(OD_POSTCODE, 0) = aMember(ME_POSTCODE, 0)
    if aMember(ME_USEALTADDRESS, 0) then
        aOrder(OD_DELTITLE, 0)    = aMember(ME_ALTTITLE, 0)
        aOrder(OD_DELFIRSTNAME, 0)= aMember(ME_ALTFIRSTNAME, 0)
        aOrder(OD_DELSURNAME, 0)  = aMember(ME_ALTSURNAME, 0)
        aOrder(OD_DELADDRESS1, 0) = aMember(ME_ALTADDRESS1, 0)
        aOrder(OD_DELADDRESS2, 0) = aMember(ME_ALTADDRESS2, 0)
        aOrder(OD_DELADDRESS3, 0) = aMember(ME_ALTADDRESS3, 0)
        aOrder(OD_DELADDRESS4, 0) = aMember(ME_ALTADDRESS4, 0)
        aOrder(OD_DELADDRESS5, 0) = aMember(ME_ALTADDRESS5, 0)
        aOrder(OD_DELPOSTCODE, 0) = aMember(ME_ALTPOSTCODE, 0)
    else
        aOrder(OD_DELTITLE, 0)    = aMember(ME_TITLE, 0)
        aOrder(OD_DELFIRSTNAME, 0)= aMember(ME_FIRSTNAME, 0)
        aOrder(OD_DELSURNAME, 0)  = aMember(ME_SURNAME, 0)
        aOrder(OD_DELADDRESS1, 0) = aMember(ME_ADDRESS1, 0)
        aOrder(OD_DELADDRESS2, 0) = aMember(ME_ADDRESS2, 0)
        aOrder(OD_DELADDRESS3, 0) = aMember(ME_ADDRESS3, 0)
        aOrder(OD_DELADDRESS4, 0) = aMember(ME_ADDRESS4, 0)
        aOrder(OD_DELADDRESS5, 0) = aMember(ME_ADDRESS5, 0)
        aOrder(OD_DELPOSTCODE, 0) = aMember(ME_POSTCODE, 0)
    end if
    aOrder(OD_DELZONE, 0)     = nPostZone
    aOrder(OD_POSTTYPE, 0)    = sPostOpt
    aOrder(OD_POSTAGE, 0)     = nPostage
    '// others will be filled in after the next step from receipt.asp page //
    aOrder(OD_TRANSSUC, 0)    = false
    aOrder(OD_COMPLETEDALL, 0)= false 
    aOrder(OD_PARTSHIP, 0)    = bPartShip
    aOrder(OD_DISPATCHED, 0)  = false
    aOrder(OD_DISPATCHDATE, 0)= null
    aOrder(OD_KEYFN1ID, 0)    = nNoteId
    aOrder(OD_KEYFN2ID, 0)    = nNote2Id
    
    nErr = saveOrder(aOrder)
    if nErr = 0 then
        nErr = createOrderItems(aOrder(OD_ID, 0), nCtID, bVat, nPostage, nTotalNoVAT, nVAT, nTotal)  '// accepts postage and vatable - returns VAT and Totals //
    else
        response.redirect "./error.asp?confirm=saveOrder"
    end if
  
    if nErr = 0 then
        aOrder(OD_TOTALNOVAT, 0)        = nTotalNoVAT2
        aOrder(OD_VAT, 0)               = nVAT2
        aOrder(OD_TOTALCHARGECALC, 0)   = nTotal2
        
        if nDiscountVal > 0 then
            aOrder(OD_PROMOCODE, 0) = session("promocode")
            aOrder(OD_PROMOTEXT, 0) = calcPromotionalText(session("promocode"), nTotal2)
            aOrder(OD_PROMODISC, 0) = nDiscountVal
        end if
        nErr = saveOrder(aOrder)
    else
        response.redirect "./error.asp?confirm=createOI"
    end if
else
	response.redirect "./error.asp?confirm=postage2"
end if

session("odID") = aOrder(OD_ID, 0)		




sCrypt          = compile_protex_stuff(aMember,nTotalNoPostage,nProtexTotal,nPostage)
%>
<tr class="header_bold">
    <td colspan="2"><b>&nbsp;Pay for your order?&nbsp;</b></td>
</tr>
<tr bgcolor="<%=BSKT_ROW_CLR%>">
    <td colspan=2 valign="top">	
    <br />
    <%
    'if IsEnvironmentPreLive(sHostHeader) then
    %>
    <FORM action="<%=vspSite%>" method="post" id="form1" name="form1"> 
        <input type="hidden" name="VPSProtocol" value="2.22"/>
        <input type="hidden" name="TxType" value="PAYMENT"/><%'// 3 Types= PAYMENT( money taken straight away) Deferred(requires user to manually process in admin) and PREAUTH%>
        <input type="hidden" name="Vendor" value="<%=VendorName%>"/>
        <input type="hidden" name="Crypt" value="<%=sCrypt %>"/>
        <input type="image" src="/ecomm/buttons/button_yescontinue.gif" value="Yes, Continue and Pay By Card" alt="Yes, Continue and Pay By Card" />                        
    </FORM>
    <%
    'End if
    %>
    </td>
</tr>
<tr bgcolor="<%=BSKT_ROW_CLR%>">
    <td colspan=2 align="center">	
        Card payments are <strong><a href="http://www.protx.com" target="_blank">processed securely</a></strong> through <strong><a href="http://www.protx.com" target="_blank">PROTX</a></strong>.<br />
    </td>
</tr>
<tr>
    <td colspan="2" align="center">
        <img src="/ecomm/buttons/american_express.gif" alt="American Express" />
        &nbsp;<img src="/ecomm/buttons/visa.gif" alt="Visa" />
        &nbsp;<img src="/ecomm/buttons/visaElectron.gif" alt="Visa Electron" />
        &nbsp;<img src="/ecomm/buttons/delta.gif" alt="Delta" />
        &nbsp;<img src="/ecomm/buttons/mastercard.gif" alt="Master Card" />
        &nbsp;<img src="/ecomm/buttons/maestro.gif" alt="Maestro" />
        <img src="/ecomm/buttons/switch.gif" alt="Switch" />
        <img src="/ecomm/buttons/solo.gif" alt="Solo" />
        <img src="/ecomm/buttons/protex.gif" alt="Payments Powered By Protx SECURE E-Payments." />
    </td>
</tr>
<%
end if

response.write "</table>"

        else
            sTmp = "&nbsp;<br />Your shopping basket is currently empty."
        end if
    else
        sTmp = "&nbsp;<br />Your shopping basket is currently empty."
    end if
    showCartRO = sTmp
end function


'// accepts postage and vatable - returns VAT and Totals //
function createOrderItems(nOdID, nCtID, bVat, nPostage, nTotalNoVAT, nVAT, nTotal)
  dim nErr:nErr=0
  dim sSQL:sSQL=""
  dim nIdx:nIdx=0
  dim nJdx:nJdx=0
  dim aItems:aItems=null
  dim aOrderItem:aOrderItem=null
  dim sItemNote, sExtractedNote
  dim aProduct(48)
  dim nOriginalPrice, nOriginalVAT
  dim nUnitPrice, nUnitVAT
  dim sSizeNote, sManufacturer
  
  ' // array 0 --> 0,
  ' // array 1 --> " & nOdID & ",
  ' // array 2 --> PR.prCODE,
  ' // array 3 --> PR.prShortCode,
  ' // array 4 --> PT.ptName,
  ' // array 5 --> PR.prName,
  ' // array 6 --> PR.prPrice,          OI_UNITCOST	
  ' // array 7 --> PR.prVAT,            OI_UNITVAT
  ' // array 8 --> 0,
  ' // array 9 --> 0,
  ' // array 10 --> 0,
  ' // array  --> 0,
  ' // array 12 --> CI.ciQuantity,
  ' // array  --> 1,
  ' // array 14 --> 0,
  ' // array  --> PR.prWrappable,
  ' // array 16 --> "",
  ' // array  --> "",
  ' // array 18 --> CI.keyFntID,
  ' // array  --> 0,
  ' // array 20 --> PR.keyFltID,
  ' // array  --> LT.ltText,
  ' // array 22 --> 0,
  ' // array  --> null,
  ' // array 24 --> "",
  ' // array  --> INST_ID,    -- this is the ubound of OI_UBOUND
  ' // array 26 --> PR.prIsChild,
  ' // array  --> PR.prPromo,           OI_UBOUND+2
  ' // array 28 --> PR.prPromoPrice,    OI_UBOUND+3
  ' // array  --> PR.prPromoVat,        OI_UBOUND+4
  ' // array 30 --> PR.prStockQty,
  ' // array  --> PR.prStockTrigger,
  ' // array 32 --> PR.prColour,
  ' // array  --> PR.prSize,
  ' // array 34 --> PR.prParentID,
  ' // array  --> PRp.prName,
  ' // array 36 --> NT.ntText,
  ' //array   --> PR.prId                            OI_UBOUND+12
  ' //array  38 --> PR.prHTMLpage                    OI_UBOUND+13
  ' //array   --> SZ.szCode + ' (' + SZ.szName + ')' OI_UBOUND+14             
  ' //array  40 --> MF.mfName                        OI_UBOUND+15
  
    if (nOdID > 0) and (nCtID > 0) then
        sSQL = "SELECT 0," & nOdID & ",PR.prCODE,PR.prShortCode,PT.ptName,PR.prName,PR.prPrice,PR.prVAT,0,0,0,0,CI.ciQuantity,1,0," _
            & " PR.prWrappable,'','',CI.keyFntID,0,PR.keyFltID,LT.ltText,0,null,''," & INST_ID _
            & ",PR.prIsChild,PR.prPromo,PR.prPromoPrice,PR.prPromoVat,PR.prStockQty,PR.prStockTrigger,PR.prColour,PR.prSize," _
            & " PR.prParentID,PRp.prName,NT.ntText, PR.prId, PR.prHTMLpage, SZ.szCode + ' (' + SZ.szName + ')', MF.mfName " _
            & " FROM (((((tblCartItem AS CI LEFT JOIN tblProduct AS PR ON CI.keyFprID=PR.prID)" _
            & " LEFT JOIN tblProductType AS PT ON PR.keyFptID=PT.ptID)" _ 
            & " LEFT JOIN tblProduct AS PAR ON PR.prParentID=PAR.prID)" _		
            & " LEFT JOIN tblLeadTime AS LT ON PAR.keyFltID=LT.ltID)" _ 
            & " LEFT JOIN tblProduct AS PRp ON  PR.prParentID = PRp.prID)" _
            & " LEFT JOIN tblNote as NT ON CI.keyFntID = NT.ntID" _
            & " LEFT OUTER JOIN tblSizeCategory as SZ ON PR.keyFszID = SZ.szID" _
            & " LEFT OUTER JOIN tblManufacturer as MF ON PR.keyFmfID = MF.mfID AND (PR.keyFmfID > 1)" _
            & " WHERE PR.prOnSale=1 AND CI.keyFctID=" & nCtID & " AND CI.keyFinID=" & INST_ID & " AND PR.keyFinID=" & INST_ID & " AND ((PT.keyFinID IS NULL) OR PT.keyFinID=" & INST_ID & ") AND ((LT.keyFinID IS NULL) OR LT.keyFinID=" & INST_ID & ")" _
            & " ORDER BY CI.ciID ASC;"
    'response.Write "sql order items: " &  sSQL
    nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aItems)
    if nErr = 0 and isarray(aItems) then
    
      for nIdx = 0 to UBound(aItems, 2)
        redim aOrderItem(OI_UBOUND, 0)
        for nJdx = 0 to OI_UBOUND
          aOrderItem(nJdx, 0) = aItems(nJdx, nIdx)
        next
		
        '// if item has a note - include it
        sItemNote       = aItems(36, nIdx)        
        sSizeNote       = aItems(OI_UBOUND+14, nIdx)
        sManufacturer   = aItems(OI_UBOUND+15, nIdx)
        
        if len("" & aItems(32, nIdx) ) > 0 then 
            aOrderItem(OI_DESCRIPTION, 0) = aOrderItem(OI_DESCRIPTION, 0) & ", " & aItems(32, nIdx) 
        end if 
        if len("" & aItems(33, nIdx) ) > 0 then 
            aOrderItem(OI_DESCRIPTION, 0) = aOrderItem(OI_DESCRIPTION, 0) & ", " & aItems(33, nIdx) 
        end if       	
        if len("" & sManufacturer) > 1 then 
            aOrderItem(OI_DESCRIPTION, 0) = aOrderItem(OI_DESCRIPTION, 0) & ", " & sManufacturer
        end if
        if len("" & sItemNote) > 0 then 
            sExtractedNote = cstr(getNoteExtraction(sItemNote, "text"))
            aOrderItem(OI_DESCRIPTION, 0) = aOrderItem(OI_DESCRIPTION, 0) & ", " & sExtractedNote
        end if
        ' // Price      
        ' ================================================ 
        ' Put key fields of the current array row into an array for the bespoke getAppropriatePrice code to use
        aProduct(PR_ID)         = aItems(OI_UBOUND + 12, nIdx)
        aProduct(PR_PRICE)      = aItems(OI_UNITCOST, nIdx) 
        aProduct(PR_VAT)        = aItems(OI_UNITVAT, nIdx) 
        aProduct(PR_PROMO)      = aItems(OI_UBOUND + 2, nIdx) 
        aProduct(PR_PROMOPRICE) = aItems(OI_UBOUND + 3, nIdx) 
        aProduct(PR_PROMOVAT)   = aItems(OI_UBOUND + 4, nIdx) 
        aProduct(PR_HTMLPAGE)   = aItems(OI_UBOUND + 13, nIdx) 
        nErr = getAppropriatePrice(aProduct, nUnitPrice, nUnitVAT, nOriginalPrice, nOriginalVAT)  
    
        'if IsEnvironmentPreLive(sHostHeader) then
            'response.Write "<br />nOdID: " & nOdID
            'response.Write "<br />nOutputPrice: " & nUnitPrice
            'response.Write "<br />nOutputVAT: " & nUnitVAT
        'end if
        
        aOrderItem(OI_UNITCOST, 0) = nUnitPrice
        aOrderItem(OI_UNITVAT, 0) = nUnitVAT    
        ' ================================================ 
          
        nVAT   = (aOrderItem(OI_UNITVAT, 0) * aOrderItem(OI_QUANTITY, 0))
		nTotal = (aOrderItem(OI_UNITCOST, 0) * aOrderItem(OI_QUANTITY, 0))
		
		'// Availability 
        if len("" & aOrderItem(OI_LEADTIMETEXT, 0)) = 0 then
            if len("" & aItems(OI_LEADTIMETEXT, nIdx)) = 0 then
                aOrderItem(OI_LEADTIMETEXT, 0) = DFLT_LEADTIME_IS_SAVE
            else
                aOrderItem(OI_LEADTIMETEXT, 0) = aItems(OI_LEADTIMETEXT, nIdx)
            end if
        end if
		
        nErr = saveOrderItem(aOrderItem)
        aOrderItem = null
      next
		
      nTotalNoVAT = nTotal - nVat
      if bVat then
        nTotal = nTotal
      else
        nTotal = nTotalNoVAT
      end if
      nTotal = nTotal + nPostage
    end if
  else
    nErr = 1  '// bad input parms //
  end if
  createOrderItems = nErr
end function
%>