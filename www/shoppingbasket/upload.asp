<!--#include virtual="/includes/careandcomfortAPI.asp"-->
<!--#include virtual="/ecomm/setup.asp"-->
<!--#include virtual="/ecomm/countryselector.asp"-->
<!--#include virtual="/shoppingbasket/protex_functions.asp"-->

<!--#include virtual="/cms/setup.asp"-->
<!--#include virtual="/includes/dates.asp" -->
<%
dim nErr                            : nErr=0
dim blnIsUploadInProgress           : blnIsUploadInProgress = false
'dim blnIsImgInfoTopToBeDisplayed   : blnIsImgInfoTopToBeDisplayed = true
dim blnIsImgInfoBottomToBeDisplayed : blnIsImgInfoBottomToBeDisplayed = true
dim blnIsHeadingStillToBeDisplayed  : blnIsHeadingStillToBeDisplayed = true
dim blnIsHeading2StillToBeDisplayed : blnIsHeading2StillToBeDisplayed = true
dim blnIsCMSInfoStillToBeDisplayed  : blnIsCMSInfoStillToBeDisplayed = true
dim blnIsCartItemRequiringUpload    : blnIsCartItemRequiringUpload = true
dim sCompleteItems                  : sCompleteItems = ""
dim nInformHowManyCompleted         : nInformHowManyCompleted = 0
dim nTotalCartItems                 : nTotalCartItems = 0
dim aCartItems, arrCompleteItems 
dim nMeID, nCtID, nOdID, sUploadPrefs, sTellClientWhichEnvOrderCameFrom
dim nQty, nQtyCartItemUploaded, nQtyCartItemRemaining   
             
nMeID       = CLng("0" & session("meID"))
nCtID       = request("cartid")
if len("" & nCtID) = 0 then
    nCtID   = request("c")
end if

nOdID = unencodeID("" & request("ordernumber")) 
if len("" & trim(nOdID)) = 0 or (nOdID = 0) then
    nOdID   = unencodeID("" & request.querystring("n")) 
end if

if len("" & request.querystring("progress")) > 0 then
    ' // User has started the upload process
    blnIsUploadInProgress = cbool(request.QueryString("progress"))   
    
    if blnIsUploadInProgress then
        nCartItemIdJustAttempted = request.QueryString("ci")   
        if len("" & nCartItemIdJustAttempted) > 0 then
            ' sending completed cart item ids in the upload fail / success redirect querystrings
            sCompleteItems = sCompleteItems & nCartItemIdJustAttempted 
        end if
        
        if nCtID > 0 then
            nErr = getCartItemsProductsByCartID(nCtID, aCartItems)
            if nErr > 0 then
                ' // Upload CMS falure message and failure instructions
                nCntId = 33 
            else
                if (isNull(aCartItems)) or (not isarray(aCartItems)) then
                    ' // Upload CMS falure message and failure instructions
                    nCntId = 33 
                end if
            end if
        else
            ' // Upload CMS falure message and failure instructions
            nCntId = 33             
        end if    
    else
        ' // Upload CMS falure message and failure instructions
        nCntId = 33             
    end if    
else
    ' // First time entry into the upload functionality
    blnIsUploadInProgress  = false
    sCompleteItems      = ""
    
    if nCtID > 0 then
        nErr = getCartItemsProductsByCartID(nCtID, aCartItems)
        if nErr > 0 then
            ' // Upload CMS falure message and failure instructions
            nCntId = 33  
        else
            if (isNull(aCartItems)) or (not isarray(aCartItems)) then
                ' // Upload CMS falure message and failure instructions
                nCntId = 33  
            end if
        end if
    else
        ' // Upload CMS falure message and failure instructions
        nCntId = 33  
    end if 
end if
%>
<!--#include virtual="/includes/doctype.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <%=GetDefaultEcommMetaTags("Upload images " & SHOP_NAME)%>  
    <!--#include virtual="/includes/mainCSS.asp" -->
	 <!--#include virtual="/includes/javascript.asp" -->

	 
    <%=javaSetDropDown("registration")%>
</head>

<!--#include virtual="/includes/templateStart.asp" -->
<!--#include virtual="/includes/templateTopNav.asp" -->
<!--#include virtual="/includes/templateLeftNav.asp" -->


<!--#include virtual="/includes/templateBeginMainContent.asp" -->
<!-- Begin Content --> 

<div class="contact_block"> 
  <%=nvCmsGetContentHTML(CMS_CONTENT_ID_CONTACT_SUMMARY)%>     
</div> 

<!--#include virtual="/includes/templateBasketbreadCrumb.asp" -->  

<%
' // If the upload workflow has been interrupted, either due to lack of a cart id or upload failure, 
' // then display the upload failure and failure instructions msg
if len("" & nCntId) > 0 then
    response.Write  nvCmsGetContentHTML(nCntId)
    blnIsCMSInfoStillToBeDisplayed = false
end if 

if (not isNull(aCartItems)) and (isarray(aCartItems)) then   

    ' Live 
    if IsEnvironmentLive(sHostHeader) then
       sTellClientWhichEnvOrderCameFrom = "Live order: reference: "
    else
        ' Pre-live env
        if IsEnvironmentPreLive(sHostHeader) then
            sTellClientWhichEnvOrderCameFrom = "Pre-live testing order: reference: "
        else
            ' dev env    
            if IsEnvironmentDev(sHostHeader) then
                sTellClientWhichEnvOrderCameFrom = "Development test order: reference: "
            else
                sTellClientWhichEnvOrderCameFrom = "Environment unknown - be careful: order reference: "
            end if
        end if  
    end if
    
    
    ' // Determine the total amount of items purchased
    for nJdx = 0 to ubound(aCartItems, 2)        
        nTotalCartItems = nTotalCartItems + (1)*cint(aCartItems(PR_UBOUND+1, nJdx))       
    next
    
    ' // preparing for split()
    ' // faffing with arrays lengths and sending completed cart items id in the fail / success redirect querystrings
    if len("" & sCompleteItems) > 0 then
        sCompleteItems = sCompleteItems & ","
        sSeparator = ","
    else
        sSeparator = ""
    end if

    ' // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    ' // Display an upload box for every item in the cart
    ' // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    for nIdx = 0 to ubound(aCartItems, 2)
        blnIsHeadingStillToBeDisplayed  = true
        blnIsHeading2StillToBeDisplayed = true        
        intQtyOrdered                   = aCartItems(PR_UBOUND + 1, nIdx)                
        sName                           = "<strong>" & aCartItems(PR_NAME, nIdx) & "</strong>"  
        nQtyCartItemUploaded            = 0
        
        ' // Include colour and size description
        if len(aCartItems(PR_UBOUND + 5, nIdx)) > 0 then
            sName = sName & " (" & aCartItems(PR_UBOUND + 5, nIdx) & ")" 
        end if                        

        ' // Determine which upload message to display 
        if len("" & request.querystring("progress")) > 0 then
            if blnIsUploadInProgress then
                if nInformHowManyCompleted > 0 then
                    if nInformHowManyCompleted = nTotalCartItems then
                        ' // CMS upload completed message
                        nCntId = 32
                    else
                        ' // CMS Upload instructions message
                        nCntId = 34
                    end if
                else
                    ' // CMS Upload instructions message
                    nCntId = 34
                end if  
            else
                ' // Upload CMS falure message and failure instructions
                nCntId = 33 
            end if   
        else    
            ' // CMS Upload instructions message
            nCntId = 34
        end if    

        ' // Display upload message only once    
        if blnIsCMSInfoStillToBeDisplayed = true then        
            response.Write nvCmsGetContentHTML(nCntId)
            blnIsCMSInfoStillToBeDisplayed = false
        end if 
            
    ' // ---------------------------------------------------------
    ' // now loop through the current line (basket) items quantity
    ' // ---------------------------------------------------------
    for nLoopPosition = 1 to intQtyOrdered        
        
        ' // ---------------------------------------------------------
        ' // Look at how many items have already been uploaded and help relay this to the user
        if len("" & sCompleteItems) > 0 then
                arrCompleteItems = split(sCompleteItems, ",")             

                if (not isNull(arrCompleteItems)) and isarray(arrCompleteItems) then                  
                    for intCartItemRow = 0 to ubound(arrCompleteItems)-1                     
                        ' // If the line (basket) item image has already been uploaded - dont produce an upload box ....BUT *
                        if cint(aCartItems(CI_ID, nIdx)) = cint(arrCompleteItems(intCartItemRow)) then                      
                            ' We only need to find this info out once per line item
                            if nLoopPosition = 1 then
                                nQtyCartItemUploaded     = nQtyCartItemUploaded + 1                     
                                nInformHowManyCompleted  = nInformHowManyCompleted + 1
                            end if                       
                        end if
                    next
                    
                    ' BUT * - Important: We need to check that the appearance of the cart item id 
                    ' in the "uploads completed" string (sCompleteItems) matches the quantity for the line (basket) item
                    if nQtyCartItemUploaded = intQtyOrdered then
                        blnIsCartItemRequiringUpload = false
                    else
                        ' This will prevent the display of an upload box for the images already uploaded (for this one line item)
                        if nQtyCartItemUploaded > 0 then
                            nLoopPosition = nLoopPosition + nQtyCartItemUploaded    
                        end if
                        blnIsCartItemRequiringUpload = true
                    end if                
                end if                
        else
                blnIsCartItemRequiringUpload = true
        end if  
        ' // ---------------------------------------------------------
    
        ' // If the cart item in the order has not been uploaded - then produce an upload box 
        if blnIsCartItemRequiringUpload = true then          
            nQtyCartItemRemaining = (intQtyOrdered-nQtyCartItemUploaded)
            
            if blnIsHeadingStillToBeDisplayed  then 
                response.Write "<hr /><p class=""message"">Shopping basket item " & (nIdx+1) & ":</p><br />"          
                if intQtyOrdered = 1 then
                    response.Write "<p class=""upload_text_small"">(" & nQtyCartItemRemaining & " upload remaining) <br />"
                    response.Write "You ordered 1 item by the description of: </p>" & sName 
                else
                    
                    if nQtyCartItemRemaining = 1 then
                        response.Write "<p class=""upload_text_small"">(" & nQtyCartItemRemaining & " upload remaining)<br />"
                        response.Write "You ordered " & intQtyOrdered & " items by the description of: </p>" & sName 
                    else
                        response.Write "<p class=""upload_text_small"">(" & nQtyCartItemRemaining & " uploads remaining) <br />"
                        response.Write "You ordered " & intQtyOrdered & " items by the description of: </p>" & sName
                    end if
                    
                end if 
                blnIsHeadingStillToBeDisplayed = false     
            end if          
            
            sUploadPrefs = sTellClientWhichEnvOrderCameFrom & nOdID 
            sUploadPrefs = sUploadPrefs & "<br/>Line item (" & nLoopPosition & " of " & intQtyOrdered & "): " & sName 
    %>
    <form name="frmUpload<%=aCartItems(CI_ID, nIdx)%>_<%=nLoopPosition%>" action="https://<%=PROTX_CLIENT_NAME%>.sharefile.com/remoteupload.aspx" enctype="multipart/form-data" class="upload_form" method="post">
        <fieldset class="fields">
            <!-- Begin: html form for upload progress -->
            <script type="text/javascript" src="https://<%=PROTX_CLIENT_NAME%>.sharefile.com/javascript/remoteupload.js"></script>
            
            <br /><strong>Please indicate your print preference:</strong><br />
            <label class="standard">&nbsp;</label><input type="radio" name="print_colour" id="print_colour_no_change"  value="<br />Print preference: no change" checked />
            <label for="print_colour_no_change" >no change</label> 
            &nbsp;&nbsp;
            <input type="radio" name="print_colour" id="print_colour_bw" value="<br />Print preference: black and white" class="check" />
            <label for="print_colour_bw">black and white</label>
            &nbsp;&nbsp;
            <input type="radio" name="print_colour" id="print_colour_sepia" value="<br />Print preference: sepia" class="check" />
            <label for="print_colour_sepia">sepia</label>
            <br />
            <label for="instructions" class="standard" ><strong>Instructions</strong></label>
            <textarea name="instructions" id="instructions" class="standard" rows="5" cols="85"></textarea> 
            <br />          
            <input type="hidden" name="details" value="<%=sUploadPrefs%>" />
            <input type="hidden" name="title" value="<%=sTellClientWhichEnvOrderCameFrom%><%=nOdID%>" />            
            <br />
            <label class="standard" for="file1"></label>
            <input type="file" id="file1" name="file1" class="standard"/><br /><br />
            
            <input type="hidden" name="redirectonsuccess" value="<%=SITE_URL%>/shoppingbasket/upload.asp?progress=1&c=<%=nCtID%>&n=<%=encodeID(nOdID)%>&ci=<%=request.QueryString("ci") & sSeparator & aCartItems(CI_ID, nIdx)%>" />
            <input type="hidden" name="redirectonerror" value="<%=SITE_URL%>/shoppingbasket/upload.asp?progress=0&c=<%=nCtID%>&n=<%=encodeID(nOdID)%>&ci=<%=request.QueryString("ci") & sSeparator & aCartItems(CI_ID, nIdx)%>" />
            <input type="hidden" name="debug" value="false" />
            <input type="hidden" value="" id="__MEDIACHASE_FORM_UNIQUEID" name="__MEDIACHASE_FORM_UNIQUEID" />
            <input type="hidden" name="baseURL" id="baseURL" value="https://<%=PROTX_CLIENT_NAME%>.sharefile.com" />           
            
            <input type="hidden" name="pid" value="<%=UPLOAD_IMAGE_UNIQUE_FORM_ID%>" />
            <br /><input type="image" src="/ecomm/buttons/buttons_upload.gif"  value="Upload" class="image_right" onclick="javascript: return funcValidateAndSetForm('frmUpload<%=aCartItems(CI_ID, nIdx)%>_<%=nLoopPosition%>');" />
             <!-- End: html form for upload progress -->
        </fieldset>
    </form>    
    <%
            else
                 if blnIsHeading2StillToBeDisplayed  then 
                    response.Write "<hr><p class=""message"">Shopping basket item " & (nIdx+1) & ":</p><br />"          
                    response.Write "<p class=""upload_text_small"">(no uploads remaining)</p><br />"
                    blnIsHeading2StillToBeDisplayed = false
                end if 
            end if          ' blnIsCartItemRequiringUpload = true    
        next        ' // loop through the quantities of the basket item 
        
        if nIdx = ubound(aCartItems, 2) then
            response.Write "<hr />"
        end if       
    next                ' // loop through the basket items 


    if blnIsImgInfoBottomToBeDisplayed then
        if nInformHowManyCompleted > 0 then
            if nInformHowManyCompleted = 1 then
                 response.write "<h2>" & nInformHowManyCompleted & " image out of " & nTotalCartItems & " received.</h2>"
            else
                 response.write "<h2>" & nInformHowManyCompleted & " images out of " & nTotalCartItems & " received.</h2>"
            end if 
            if nInformHowManyCompleted = nTotalCartItems then
                response.write "<p class=""upload_text"">Upload process completed!</p><br /><br /><br />"    
                response.Write "<p class=""text"">&nbsp;<b><a href=""./printreceipt.asp?e=" & encodeID(nOdID) & """ target=""_blank"" onClick=""return popReceipt('./printreceipt.asp?e=" & encodeID(nOdID) & "')"">Print your Receipt</a></b></p>"
                response.write "<a href=""/products/index.asp?0,0,0,0,3""><img src=""/ecomm/buttons/button_continueshopping.gif"" alt=""Continue shopping"" class=""no_border"" /></a>"    
            else
                response.write "<span class=""upload_text"">Please continue to upload your images...</span><br /><br /><br /><br /><br />" 
                response.Write "<p class=""text"">&nbsp;<b><a href=""./printreceipt.asp?e=" & encodeID(nOdID) & """ target=""_blank"" onClick=""return popReceipt('./printreceipt.asp?e=" & encodeID(nOdID) & "')"">Print your Receipt</a></b></p>"
            end if
        else
            response.Write "<p class=""text""><b><a href=""./printreceipt.asp?e=" & encodeID(nOdID) & """ target=""_blank"" onClick=""return popReceipt('./printreceipt.asp?e=" & encodeID(nOdID) & "')"">Print your Receipt</a></b></p>"
        end if       
        blnIsImgInfoBottomToBeDisplayed = false
    else
        response.Write "<p class=""text""><b><a href=""./printreceipt.asp?e=" & encodeID(nOdID) & """ target=""_blank"" onClick=""return popReceipt('./printreceipt.asp?e=" & encodeID(nOdID) & "')"">Print your Receipt</a></b></p>"
    end if 

end if                  ' //(not isNull(aCartItems)) and (isarray(aCartItems))
%>
<!-- End Content -->
<!--#include virtual="/includes/templateEndMainContent.asp" -->


<!--#include virtual="/includes/templateRightColumn.asp" -->


<!--#include virtual="/includes/templateFooter.asp" -->
<!--#include virtual="/includes/templateEnd.asp" -->