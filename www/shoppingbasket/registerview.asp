<!--#include virtual="/includes/careandcomfortAPI.asp"-->
<!--#include virtual="/ecomm/setup.asp"-->
<!--#include virtual="/ecomm/countryselector.asp"-->
<!--#include virtual="/cms/setup.asp"-->
<!--#include virtual="/includes/dates.asp" -->
<%
dim nErr    : nErr=0
dim nMeID, nCtID, id

nMeID = CLng("0" & session("meID"))
nCtID = CLng("0" & session("ctID"))

if nMeID = 0 then response.redirect "./login.asp"

nErr = loadMember(nMeID, aMember)
if nErr = 0 then
    '// check that we have enough details for ecomm - if not go back to register.asp //
    if (len("" & aMember(ME_EMAIL, 0)) = 0) _
        OR (len("" & aMember(ME_ADDRESS1, 0)) = 0) _
        OR (len("" & aMember(ME_ADDRESS3, 0)) = 0) _
        OR (len("" & aMember(ME_ADDRESS4, 0)) = 0) _
        OR (len("" & aMember(ME_ADDRESS5, 0)) = 0) _
        OR (len("" & aMember(ME_TEL, 0)) = 0) then
        response.redirect "./register.asp"
    end if
else
    response.redirect "./login.asp"
end if
%>
<!--#include virtual="/includes/doctype.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <%=getCMSMetaData(nCntId,"","","")%>  
    <!--#include virtual="/includes/mainCSS.asp" -->
	  <!--#include virtual="/includes/javascript.asp" -->
</head>

<!--#include virtual="/includes/templateStart.asp" -->
<!--#include virtual="/includes/templateTopNav.asp" -->
<!--#include virtual="/includes/templateLeftNav.asp" -->


<!--#include virtual="/includes/templateBeginMainContent.asp" -->
	
<!-- Begin Content -->
    <div class="contact_block"> 
      <%=nvCmsGetContentHTML(CMS_CONTENT_ID_CONTACT_SUMMARY)%>     
    </div> 
    
   	<!--#include virtual="/includes/templateBasketbreadCrumb.asp" -->  
<a name="Details"></a><h2>Your Details</h2>
<br />
<form name="Registration" id="Registration" method="POST" action="./register.asp">
<input type="hidden" name="submitted" value="yes" />
<input type="hidden" name="meID" value="<%=nMeID%>" />
<table width="90%" border="0" cellpadding="0" cellspacing="0" class="text" align="center">
    <tr>
    <td width="50%" valign="top">Email Address:</td>
    <td width="50%" valign="top"><%=aMember(ME_EMAIL, 0)%></td></tr>
    <tr>
    <td valign="top">Title (Mr/Ms/etc.):</td>
    <td valign="top"><%=aMember(ME_TITLE, 0)%></td></tr>
    <tr>
    <td valign="top">First Name:</td>
    <td valign="top"><%=aMember(ME_FIRSTNAME, 0)%></td></tr>
    <tr>
    <td valign="top">Surname:</td>
    <td valign="top"><%=aMember(ME_SURNAME, 0)%></td></tr>
    <tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td></tr>
    <td valign="top">Telephone Number:</td>
    <td valign="top"><%=aMember(ME_TEL, 0)%></td></tr>
    <tr>
    <td valign="top">Mobile Number:</td>
    <td valign="top"><%=aMember(ME_MOB, 0)%></td></tr>
    <tr>
    <td valign="top">Fax Number:</td>
    <td valign="top"><%=aMember(ME_FAX, 0)%></td></tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td></tr>
    <tr>
    <td valign="top">Address Line 1:</td>
    <td valign="top"><%=aMember(ME_ADDRESS1, 0)%></td></tr>
    <tr>
    <td valign="top">Address Line 2:</td>
    <td valign="top"><%=aMember(ME_ADDRESS2, 0)%></td></tr>
    <tr>
    <td valign="top">Town/City:</td>
    <td valign="top"><%=aMember(ME_ADDRESS3, 0)%></td></tr>
    <tr>
    <td valign="top">County:</td> <!-- /State -->
    <td valign="top"><%=aMember(ME_ADDRESS4, 0)%></td></tr>
    <tr>
    <td valign="top">Postal Code:</td><!-- /Zip-->
    <td valign="top"><%=aMember(ME_POSTCODE, 0)%></td></tr>
    <tr>
        <td valign="top">Country:</td>
        <td valign="top">
            <%=getCountry(aMember(ME_ADDRESS5, 0))%>
        </td>
    </tr>
    
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td></tr>
    <tr>
    <td align="right"><%=getTickCrossIcon(not aMember(ME_USEALTADDRESS, 0))%>&nbsp;&nbsp;</td>
    <td align="left">Use this address for delivery?</td></tr>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td></tr>
    <tr>
    <td colspan="2"><a name="Altdetails"></a><div class="form_title">Delivery Address if different</div></td></tr>
    <tr>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td></tr>
    <tr>
    <td valign="top">Title (Mr/Ms/etc.):</td>
    <td valign="top"><%=aMember(ME_ALTTITLE, 0)%></td></tr>
    <tr>
    <td valign="top">First Name:</td>
    <td valign="top"><%=aMember(ME_ALTFIRSTNAME, 0)%></td></tr>
    <tr>
    <td valign="top">Surname:</td>
    <td valign="top"><%=aMember(ME_ALTSURNAME, 0)%></td></tr>
    <tr>
    <td valign="top">Address Line 1:</td>
    <td valign="top"><%=aMember(ME_ALTADDRESS1, 0)%></td></tr>
    <tr>
    <td valign="top">Address Line 2:</td>
    <td valign="top"><%=aMember(ME_ALTADDRESS2, 0)%></td></tr>
    <tr>
    <td valign="top">Town/City:</td>
    <td valign="top"><%=aMember(ME_ALTADDRESS3, 0)%></td></tr>
    <tr>
    <td valign="top">County/State:</td>
    <td valign="top"><%=aMember(ME_ALTADDRESS4, 0)%></td></tr>
    <tr>
    <td valign="top">Postal Code:</td>
    <td valign="top"><%=aMember(ME_ALTPOSTCODE, 0)%></td></tr>
    <tr>
        <td valign="top">Country:</td>
        <td valign="top">
            <%=getCountry(aMember(ME_ALTADDRESS5, 0))%>
        </td>
    </tr>
    
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td></tr>
    <tr>
    <td align="right"><%=getTickCrossIcon(aMember(ME_USEALTADDRESS, 0))%>&nbsp;&nbsp;</td>
    <td align="left">Use this address for delivery?</td></tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td></tr>
    <tr>
    <td colspan="2"><a name="DataProtect"></a><div class="form_title">Privacy &amp; Data Protection</div></td></tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td></tr>
    <tr>
    <td colspan="2">
    Occasionally we would like to let customers know about special or limited offers.  We limit these to about 5 per year.<br />
    </td></tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td></tr>
    <tr>
    <td align="right" valign="top"><%=getTickCrossIcon(not aMember(ME_CONTACTBYUS, 0))%>&nbsp;&nbsp;</td>
    <td align="left">Please tick this if you do not want to be included:<br />
    All customer details are retained in strictest confidence, we do not share your details with any third party.<br />
    <a href="/privacypolicy/window.asp" target="_blank" onClick="return showTandC();">Read our privacy policy here</a>.</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td></tr>
    <!--tr>
    <td align="right"><%=getTickCrossIcon(aMember(ME_CONTACTBYOTHER, 0))%>&nbsp;&nbsp;</td>
    <td align="left">May our partner organisations contact you?</td></tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td></tr-->
    </table></form>

    <p class="form_title">&nbsp;&nbsp;&nbsp;&nbsp;Are these details correct?<br />&nbsp;<br />
    <a href="./register.asp"><img src="/ecomm/buttons/button_noeditaddress.gif" border="0" alt="No, Edit Address" class="shopLeft" /></a>
    <a href="./confirm.asp"><img src="/ecomm/buttons/button_yescontinue.gif" border="0" alt="Yes, Continue" class="shopRight" /></a>
    </p>
    <p>&nbsp;</p>
<!-- End Content -->
<!--#include virtual="/includes/templateEndMainContent.asp" -->

<!--#include virtual="/includes/templateRightColumn.asp" -->

<!--#include virtual="/includes/templateFooter.asp" -->

<!--#include virtual="/includes/templateEnd.asp" -->