<!--#include virtual="/ecomm/setup.asp"-->
<!--#include virtual="/includes/navigationFunctions.asp" -->
<!--#include virtual="/includes/dates.asp" -->
<!--#include virtual="/ecomm/countryselector.asp"-->

<%

  Dim g_sFldr:g_sFldr="shop"
	Dim id, LHSid
  LHSid=14
  
dim nErr:nErr=0
dim nOdID:nOdID=0
dim nMeID:nMeID=0
dim sHeader:sHeader=""
dim aOrder:aOrder=null
dim aOrderItems:aOrderItems=null
dim bSuccess:bSuccess=false
dim sPayType:sPayType="CC"

nOdID = unencodeID("" & request.form("cartID"))
'response.write nOdID
if nOdID > 0 then
	nErr = getMemberFromOrderID(nOdID,aMember)
  nMeID = aMember(ME_ID, 0)
  session("MeID") = nMeID
  
  '// success or cancel? //
  
  nErr = loadOrder(nOdID, aOrder)
  if nErr = 0 then
    if request.form("transStatus") = "Y" then
      bSuccess = true
      nErr = makeCCPayment(aOrder, aMember, True, request.form("transID"), request.form("transTime"), request.form("rawAuthCode"), request.form("authAmount"))
    else
      '// just show cancelled try again? // 
    end if
  end if
  
  
  'nErr = loadOrder(nOdID, aOrder)
  if nErr = 0 then
    bSuccess = aOrder(OD_COMPLETEDALL, 0)
  else
  end if
  if bSuccess then
    session("ctID") = 0
    session("odID") = 0
    nErr = setPostOptions(0, "", 0, "", 0, "", false)
  else
    '// not sure at the moment //
  end if
	'// display the order contents //
	'sOrderHTML = displayOrder(nOrderID,true,BASKET_HDR_CLR,BASKET_ROW_CLR,nItems)
else
	'// the shopping basket is currently empty //
	'sOrderHTML = "no order details to show."
end if

%>

<!--#include virtual="/includes/doctype.asp"-->
<html lang="en">
<head>
<title>Earthworks</title>
<meta http-equiv="Content-Language" content="en-gb">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="author" content="NVisage (http://www.nvisage.co.uk)">
<meta name="description" content="">
<meta name="keywords" content="">
<!--#include virtual="/includes/copyright.asp"-->
<!--#include virtual="/includes/WPshopCSS.asp"-->
</head>
<!--#include virtual="/includes/bodytag.asp"-->

<!--begin template_start-->

<!--main header-->
<!--#Xinclude virtual="/includes/main_header.asp"-->
<!--structural bits and bobs-->
<!--#include virtual="/includes/main_codeblock1.asp"-->
<!--side menu-->
<!--#Xinclude virtual="/includes/main_lhs_menu.asp"-->
			
			<!--begin RHS-->
			<div id="RHS">
          
          
<table width="663"  bgcolor="#F5F0E5" border="0" cellspacing="0" cellpadding="0" align="center">
<tr bgcolor="#F5F0E5"><td>
<center>
<img src="/i/82973/wp_new_header.gif" width="663" height="53" border="0"></center></td></tr></table>
          
          
					<h1 class="no_display">Shopping Basket</h1>
          
				<div id="cart_steps">Your Details &gt; Options &gt; Confirmation &gt; Payment &gt; <strong>Receipt</strong></div>
					<div id="center_text_block">  
          
          
          <%if bSuccess then %>
				<h2>&nbsp;<br />Your Receipt<br />&nbsp;</h2>
          <%else %>
				<h2>&nbsp;<br />Payment Cancelled<br />&nbsp;</h2>
          <%end if%>
					
          
          
          
        <%=showOrder(nOdID, aMember, BSKT_HDR_CLR, BSKT_ROW_CLR, BSKT_BGD_CLR)%>
         
         <%
         if bSuccess then
         %>
		<p class="content_title">Thank you, your order has been completed successfully. You will receive a confirmation email shortly.</p>
		<p class="content_title">Your Receipt - please print this out for your records.</p>
		<%
		response.write replace(sOrderHTML, "/graphics/clearpix.gif", "/i/82973/clearpix.gif")
		%>
		<p>Your Earthworks order reference: <%="N" & right("00000" & nOdID, 5)%></p>
		<p>Your World Pay payment reference: <%=request.form("transID")%></p>
		<p>Thank you for shopping at Earthworks.</p>
		<p>Please <a href="http://www.earthworks.co.uk/">click here</a> to continue browsing the Earthworks web site.<br />&nbsp;</p>
         
         <%
         else
         %>
		<p class="content_title">Order Cancelled</p>
		<%
		response.write replace(sOrderHTML, "/graphics/clearpix.gif", "/i/82973/clearpix.gif")
		%>
		<p>&nbsp;<br>We are sorry but your card has been  deemed invalid  for reasons unknown to us. If 
                you wish to try again, please <a href="http://www.earthworks.co.uk/shoppingbasket/r.asp?q=<%=encodeID(nOdID)%>">click here</a>.</p>
		<p>Thank you for shopping at Earthworks.</p>
		<p>Please <a href="http://www.earthworks.co.uk/">click here</a> to continue browsing the Earthworks web site.<br />&nbsp;</p>

         <%
         end if
         %> 
         
    <br />&nbsp;<br />
          </div>
                    
<WPDISPLAY ITEM=BANNER>
					
				&nbsp;<br />
			</div>
			<!--end RHS-->
<!--structural bits and bobs-->
<!--#include virtual="/includes/main_codeblock2.asp"-->
<!--footer-->
<!--#Xinclude virtual="/includes/footer_keypage6.asp"-->			
<!--end template_end-->
<!--#include virtual="/includes/endbodytag.asp"-->
<%
response.end

function getMemberFromOrderID(nOdID,aMember)
  dim nErr:nErr=0
  dim sSQL:sSQL = ""
  if nOdID > 0 then
    sSQL = "SELECT ME.* FROM tblMember AS ME LEFT JOIN tblOrder AS OD ON OD.keyFmeID=ME.meID" _
        & " WHERE OD.odID=" & nOdID & " AND OD.keyFinID=" & INST_ID & " AND ME.keyFinID=" & INST_ID & ";"
    nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aMember)
  else
    nErr = 1  '// bad input parms //
  end if
  getMemberFromOrderID = nErr
end function



function makeCCPayment(aOrder, aMember, bTransSuc, sTransID, sTransTime, sAuthCode, sTotal)
	dim nErr:nErr = 0
	dim aItems:aItems = null
  dim sSQL:sSQL=""
	'// update dB with what has happened //
  aOrder(OD_COMPLETEDALL, 0)   = true
  aOrder(OD_TRANSSUC, 0)       = bTransSuc
  aOrder(OD_TRANSID, 0)        = sTransID
  aOrder(OD_TRANSTIME, 0)      = sTransTime
  aOrder(OD_AUTHCODE, 0)       = sAuthCode
  aOrder(OD_TOTALAMOUNTTXT, 0) = sTotal
  if nErr = 0 then
    sSQL = "SELECT OI.* FROM tblOrderItem AS OI" _
        & " WHERE OI.keyFodID=" & aOrder(OD_ID, 0) & " AND OI.keyFinID=" & INST_ID & " ORDER BY OI.oiID ASC;"
    nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aItems)
  else
    '// what?? //
    exit function
  end if
  'response.write nErr & " " & sSQL
  if nErr = 0 then
    nErr = updateStockQuantity(aItems)
  	'// notifyShopOrderPlaced //
  	nErr = notifyShopCCOrderPlaced(aOrder, aMember, aItems)
  	'// notifyCustomerOrderConfirmed //
  	nErr = notifyCustomerOrderConfirmed(aOrder, aMember, aItems)
    '// write world pay vars to dB //
    nErr = saveOrder(aOrder)
  else
  end if
	makeCCPayment = nErr
end function

function updateStockQuantity(aItems)
  dim nErr:nErr=0
  dim nIdx:nIdx=0
  dim sSQL:sSQL=""
  for nIdx=0 to UBound(aItems, 2)
    sSQL = "UPDATE tblProduct SET prStockQty=(prStockQty-" & aItems(OI_QUANTITY, nIdx) & ") WHERE prCode='" & aItems(OI_PRODUCTCODE, nIdx) & "' AND keyFinID=" & INST_ID & ";"
    'response.write sSQL
    call sqlExec(DSN_ECOM_DB, sSQL)
  next
  updateStockQuantity = nErr
end function

function showOrder(nOdID, aMember, sHedClr, sRowClr, sBkgClr)
  dim nErr:nErr=0
  dim sTmp:sTmp=""
  dim aItems:aItems=null
  dim nIdx:nIdx=0
  dim sSQL:sSQL=""
  dim nTotal, nSubTotal, nVat, nTotalVAT
  dim sName, sLeadTime, nPrice, nQty
  nIdx = 0
  sTmp = ""
  dim sMbrAdd, sDelAdd
  if nOdID > 0 then
    sSQL = "SELECT OD.*,OI.* FROM tblOrder AS OD LEFT JOIN tblOrderItem AS OI ON OD.odID=OI.keyFodID" _
        & " WHERE OD.odID=" & nOdID & " AND OD.keyFinID= "& INST_ID & " AND OI.keyFinID=" & INST_ID & " ORDER BY OI.oiID ASC;"
    nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aItems)
    if nErr = 0 then
%>


    
    <table align="center" width="100%" border="0" bgcolor="<%=sBkgClr%>" cellspacing="0" cellpadding="0" class="text">
    <tr bgcolor="<%=sHedClr%>" class="shopping_title">
    <td>&nbsp;</td>
    <td>Description</td>
    <td>&nbsp;</td>
    <td align="center">Quantity</td>
    <td>&nbsp;</td>
    <td align="right">Price</td>
    <td>&nbsp;</td>
    <td align="right">Sub&nbsp;Total</td>
    <td>&nbsp;</td></tr>
<%for nIdx=0 to UBound(aItems, 2)
%>
<tr bgcolor="<%=sRowClr%>">
<td>&nbsp;</td>
<td valign="top"><%=aItems(OD_UBOUND + 1 + OI_DESCRIPTION, nIdx)%><br><small>Delivery - <%=aItems(OD_UBOUND + 1 + OI_LEADTIMETEXT, nIdx)%></small></td>
<td>&nbsp;</td>
<td valign="top" align="center"><%=aItems(OD_UBOUND + 1 + OI_QUANTITY, nIdx)%></td>
<td>&nbsp;</td>
<td valign="top" align="right">&pound;<%=formatnumber(aItems(OD_UBOUND + 1 + OI_UNITCOST, nIdx), 2)%></td>
<td>&nbsp;</td>
<td valign="top" align="right">&pound;<%=formatnumber(aItems(OD_UBOUND + 1 + OI_UNITCOST, nIdx) * aItems(OD_UBOUND + 1 + OI_QUANTITY, nIdx), 2)%></td>
<td>&nbsp;</td></tr>
<%next%>
<tr bgcolor="<%=sHedClr%>" class="shopping_title">
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td align="right">Sub&nbsp;Total</td>
<td>&nbsp;</td>
<td align="right">&pound;<%=formatnumber(aItems(OD_TOTALNOVAT, 0) + aItems(OD_VAT, 0), 2)%></td>
<td>&nbsp;</td></tr>


<%if bVAT or (VAT_OPTS = 0) then
%>
<tr bgcolor="<%=sRowClr%>">
<td>&nbsp;</td>
<td colspan="3">VAT Included</td>
<td>&nbsp;</td>
<td align="right">(&pound;<%=formatnumber(aItems(OD_VAT, 0), 2)%>)</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td colspan="1">&nbsp;</td>
</tr>
<%else
%>
<tr bgcolor="<%=sRowClr%>">
<td>&nbsp;</td>
<td colspan="3">VAT</td>
<td>&nbsp;</td>
<td align="right">-&pound;<%=formatnumber(aItems(OD_VAT, 0), 2)%></td>
<td>&nbsp;</td>
<td align="right">-&pound;<%=formatnumber(aItems(OD_VAT, 0), 2)%></td>
<td colspan="1">&nbsp;</td>
</tr>
<%end if%>

<tr>
<td>&nbsp;</td>
<td colspan="3">Postage &amp; Packaging</td>
<td>&nbsp;</td>
<td align="right">&pound;<%=formatnumber(aItems(OD_POSTAGE, 0), 2)%></td>
<td>&nbsp;</td>
<td align="right">&pound;<%=formatnumber(aItems(OD_POSTAGE, 0), 2)%></td>
<td colspan="1">&nbsp;</td>
</tr>
<tr bgcolor="<%=sHedClr%>" class="shopping_title">
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td align="right">Total</td>
<td>&nbsp;</td>
<td align="right">&pound;<%=formatnumber(aItems(OD_TOTALCHARGECALC, 0), 2)%></td>
<td colspan="1">&nbsp;</td>
</tr>

</table>

<%
sMbrAdd=""
sDelAdd=""

sMbrAdd = sMbrAdd & "&nbsp;" & trim("" & aOrder(OD_TITLE, 0) & " " & aOrder(OD_FIRSTNAME, 0) & " " & aOrder(OD_SURNAME, 0)) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aOrder(OD_ADDRESS1, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aOrder(OD_ADDRESS2, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aOrder(OD_ADDRESS3, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aOrder(OD_ADDRESS4, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aOrder(OD_POSTCODE, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & getCountry(aOrder(OD_ADDRESS5, 0)) & "<br />" & vbcrlf

sDelAdd = sDelAdd & "&nbsp;" & trim("" & aOrder(OD_DELTITLE, 0) & " " & aOrder(OD_DELFIRSTNAME, 0) & " " & aOrder(OD_DELSURNAME, 0)) & "<br />" & vbcrlf
sDelAdd = sDelAdd & "&nbsp;" & aOrder(OD_DELADDRESS1, 0) & "<br />" & vbcrlf
sDelAdd = sDelAdd & "&nbsp;" & aOrder(OD_DELADDRESS2, 0) & "<br />" & vbcrlf
sDelAdd = sDelAdd & "&nbsp;" & aOrder(OD_DELADDRESS3, 0) & "<br />" & vbcrlf
sDelAdd = sDelAdd & "&nbsp;" & aOrder(OD_DELADDRESS4, 0) & "<br />" & vbcrlf
sDelAdd = sDelAdd & "&nbsp;" & aOrder(OD_DELPOSTCODE, 0) & "<br />" & vbcrlf
sDelAdd = sDelAdd & "&nbsp;" & getCountry(aOrder(OD_DELADDRESS5, 0)) & "<br />" & vbcrlf

sMbrAdd = replace(sMbrAdd, "&nbsp;<br />", "")
sDelAdd = replace(sDelAdd, "&nbsp;<br />", "")
%>


<table align="center" width="100%" border="0" bgcolor="<%=sBkgClr%>" cellspacing="0" cellpadding="0" class="text">
<tr><td width="45%">&nbsp;</td><td width="10%">&nbsp;</td><td width="45%">&nbsp;</td></tr>
<tr><td bgcolor="<%=sHedClr%>" class="shopping_title">&nbsp;Billing Address&nbsp;</td>
<td class="shopping_title">&nbsp;</td><td bgcolor="<%=sHedClr%>" class="shopping_title">&nbsp;Delivery Address&nbsp;</td></tr>
<tr bgcolor="<%=sRowClr%>"><td valign="top">
<p><%=sMbrAdd%></p>
</td><td>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign="top">
<p><%=sDelAdd%></p></td></tr>
<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
</table>

<%
dim sPostOpt, nNoteID, sGiftMsg, nNote2ID, sDelMsg, bPartShip
nErr = getPostOptions(nCtID, sPostOpt, nNoteID, sGiftMsg, nNote2ID, sDelMsg, bPartShip)
if nErr = 0 then
sTmp = sTmp & "<table width=""100%"" border=""0"" bgcolor=""" & sBkgClr & """ cellpadding=""0"" cellspacing=""0"" class=""text"">"
sTmp = sTmp & "<tr><td bgcolor=""" & sHedClr & """ class=""shopping_title"">&nbsp;Order Options&nbsp;</td></tr>"
if bPartShip then
sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;Order items may be shipped seperately,&nbsp;</td></tr>"
else
sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;Order items will be shipped as a single parcel,&nbsp;</td></tr>"
end if
if sPostOpt = "24H" then
sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;via special delivery.&nbsp;</td></tr>"
else
sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;via standard delivery.&nbsp;</td></tr>"
end if
if len(sDelMsg) > 0 then
sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;Your order will contain the following delivery instruction:&nbsp;</td></tr>"
sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td><i>" & replace(sDelMsg, vbcrlf, "&nbsp;<br />") & "</i></td></tr>"
end if
if len(sGiftMsg) > 0 then
sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;Your order will contain the following gift message:&nbsp;</td></tr>"
sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td><i>" & replace(sGiftMsg, vbcrlf, "&nbsp;<br />") & "</i></td></tr>"
end if
sTmp = sTmp & "<td>&nbsp;</td></tr></table>"
end if
response.write sTmp
sTmp = ""
%>

        
<%
    else
      sTmp = "&nbsp;<br />No details to show."
    end if
  else
    sTmp = "&nbsp;<br />No details to show."
  end if
  showOrder = sTmp
end function


function notifyShopCCOrderPlaced(aOrder, aMember, aContents)
	dim nErr:nErr = 0
	dim sTo, sToName, sFrom, sFromName, sSubject
	dim sChargeAdd,sDeliveryAdd,sCustName,sOrderDate,sOrderTime,sOrderMessage,sOrderInstruct,aTmp,i, sProdDesc
	dim sMsg:sMsg = ""
	if (not isNull(aOrder)) and (isArray(aOrder)) then
		if not isNull(aContents) then
			aTmp = split(aOrder(OD_DATE, 0), " ")
			sOrderDate	 = aTmp(0)
			sOrderTime	 = aTmp(1)
			if UBound(aTmp) = 2 then
				'// also have a PM to add //
				sOrderTime = sOrderTime & " " & aTmp(2)
			end if
			sCustName	 = replace(trim(aOrder(OD_TITLE,0) & " " & aOrder(OD_FIRSTNAME,0) & " " & aOrder(OD_SURNAME,0)), "  ", " ")
			sChargeAdd	 = makeAddress(aOrder(OD_TITLE,0),aOrder(OD_FIRSTNAME,0),aOrder(OD_SURNAME,0) _
									  ,aOrder(OD_ADDRESS1,0),aOrder(OD_ADDRESS2,0),aOrder(OD_ADDRESS3,0) _
									  ,aOrder(OD_ADDRESS4,0),getCountry(aOrder(OD_ADDRESS5,0)),aOrder(OD_POSTCODE,0))
			sDeliveryAdd = makeAddress(aOrder(OD_DELTITLE,0),aOrder(OD_DELFIRSTNAME,0),aOrder(OD_DELSURNAME,0) _
									  ,aOrder(OD_DELADDRESS1,0),aOrder(OD_DELADDRESS2,0),aOrder(OD_DELADDRESS3,0) _
									  ,aOrder(OD_DELADDRESS4,0),getCountry(aOrder(OD_DELADDRESS5,0)),aOrder(OD_DELPOSTCODE,0))
			if aOrder(OD_KEYFN1ID,0) > 0 then
				sOrderMessage = getNote(aOrder(OD_KEYFN1ID,0))
			end if
			if aOrder(OD_KEYFN2ID,0) > 0 then
				sOrderInstruct = getNote(aOrder(OD_KEYFN2ID,0))
			end if
			sMsg = sMsg & "You have received the following order." & vbcrlf
			sMsg = sMsg & vbcrlf
			sMsg = sMsg & "ORDER DATE: "
      sMsg = sMsg & sOrderDate & " " & sOrderTime & vbcrlf
			sMsg = sMsg & vbcrlf
			sMsg = sMsg & "ONLINE ORDER REF: "
      sMsg = sMsg & "N" & right("00000" & aOrder(OD_ID, 0), 5) & vbcrlf
			sMsg = sMsg & vbcrlf
			sMsg = sMsg & "CUSTOMER ID NUMBER: "
      sMsg = sMsg & aOrder(OD_KEYFMEID, 0) & vbcrlf
			sMsg = sMsg & vbcrlf
			sMsg = sMsg & "FROM:  " & vbcrlf
      sMsg = sMsg & sChargeAdd & vbcrlf
      sMsg = sMsg & vbcrlf
      
			sMsg = sMsg & "EMAIL: "
      sMsg = sMsg & aOrder(OD_EMAIL, 0) & vbcrlf
      sMsg = sMsg & vbcrlf
      
			sMsg = sMsg & "TEL NO: "
      sMsg = sMsg & aOrder(OD_PHONE, 0) & vbcrlf
      sMsg = sMsg & vbcrlf
      
      sMsg = sMsg & "ORDER DETAILS:  " & vbcrlf
      '// loop items //
			for i = 0 to UBound(aContents,2)
				sProdDesc = aContents(OI_DESCRIPTION, i)
				if aContents(OI_KEYFN1ID, i) > 0 then
					sProdDesc = sProdDesc & " - " & getNote(aContents(OI_KEYFN1ID, i))
				end if
        'sMsg = sMsg & "Order: " & aOrder(OD_ID, 0) & vbcrlf
        sMsg = sMsg & "ITEM CODE: " & aContents(OI_PRODUCTCODE, i) & vbcrlf
        sMsg = sMsg & "ITEM DESCRIPTION: " & sProdDesc & vbcrlf
        sMsg = sMsg & "PRICE: �" & formatnumber(aContents(OI_UNITCOST,i),2) & vbcrlf
        sMsg = sMsg & "QUANTITY: " & aContents(OI_QUANTITY, i) & vbcrlf
        sMsg = sMsg & "LINE VALUE: �" & formatnumber((aContents(OI_UNITCOST,i) * aContents(OI_QUANTITY, i)),2) & vbcrlf
        sMsg = sMsg & "DELIVERY: " & aContents(OI_LEADTIMETEXT, i) & vbcrlf
				sMsg = sMsg & vbcrlf
			next
			sMsg = sMsg & "ORDER VALUE � " & FormatNumber(aOrder(OD_TOTALCHARGECALC,0) - (aOrder(OD_POSTAGE,0) + aOrder(OD_VAT,0)),2) & vbcrlf
			sMsg = sMsg & "POST & PACKAGING: � " & FormatNumber(aOrder(OD_POSTAGE,0),2) & vbcrlf
			sMsg = sMsg & "VAT: � " & FormatNumber(aOrder(OD_VAT,0),2) & vbcrlf
			sMsg = sMsg & "TOTAL INVOICE VALUE: � " & FormatNumber(aOrder(OD_TOTALCHARGECALC,0),2) & vbcrlf
			sMsg = sMsg & vbcrlf
      
			'sMsg = sMsg & "PLEASE PROCESS CARD PAYMENT" & vbcrlf
			'sMsg = sMsg & "CARD NO.    : " & sCCNo & vbcrlf
			'sMsg = sMsg & "CARD NAME   : " & sCCName & vbcrlf
			'sMsg = sMsg & "CARD TYPE   : " & sCCType & vbcrlf
			'sMsg = sMsg & "ISSUE DATE  : " & sCCIssMM & "/" & sCCIssYY & vbcrlf
			'sMsg = sMsg & "EXPIRY DATE : " & sCCExpMM & "/" & sCCExpYY & vbcrlf
			'sMsg = sMsg & "ISSUE NO.   : " & sCCIssNo & vbcrlf
			'sMsg = sMsg & "SECURITY NO.: " & sCCChkNo & vbcrlf
			'sMsg = sMsg & vbcrlf
      
			sMsg = sMsg & "DELIVER TO:  " & vbcrlf
      sMsg = sMsg & sDeliveryAdd & vbcrlf
      sMsg = sMsg & vbcrlf
      
			sMsg = sMsg & "ADDITIONAL INFORMATION:" & vbcrlf
			sMsg = sMsg & vbcrlf
			sMsg = sMsg & "MESSAGE TO RECIPIENT FROM CUSTOMER:" & vbcrlf
			sMsg = sMsg & sOrderMessage & vbcrlf
			sMsg = sMsg & vbcrlf
			sMsg = sMsg & "DELIVERY INSTRUCTIONS FROM CUSTOMER:" & vbcrlf
			sMsg = sMsg & sOrderInstruct & vbcrlf
			sMsg = sMsg & vbcrlf
			if aMember(ME_CONTACTBYUS, 0) then
				sMsg = sMsg & "MAY WE CONTACT THIS CUSTOMER:  Yes" & vbcrlf
			else
				sMsg = sMsg & "MAY WE CONTACT THIS CUSTOMER:  No" & vbcrlf
			end if
			sMsg = sMsg & vbcrlf
      if POST_OPTS > 0 then
  			if aOrder(OD_POSTTYPE,0) = "24H" then
  				sMsg = sMsg & "SPECIAL DELIVERY:  Yes" & vbcrlf
  			else
  				sMsg = sMsg & "SPECIAL DELIVERY:  No" & vbcrlf
  			end if
      end if
			if aOrder(OD_PARTSHIP,0) then
				sMsg = sMsg & "PARTIAL SHIPMENT ALLOWED:  Yes" & vbcrlf
			else
				sMsg = sMsg & "PARTIAL SHIPMENT ALLOWED:  No" & vbcrlf
			end if
			sMsg = sMsg & vbcrlf
      
			sTo = EML_SHOPTOEMAIL
			sToName = EML_SHOPTONAME
			sFrom = EML_SHOPFROMEMAIL
			sFromName = EML_SHOPFROMNAME
			sSubject = SHOP_NAME & " Order (Order ref: " & aOrder(OD_ID,0) & ")"
			nErr = SendMailEnc(sTo, sToName, sFrom, sFromName, sSubject, sMsg, "", "", false, false)
			'nErr = SendMailEnc(sTo, sToName, sFrom, sFromName, sSubject, sMsg, "", "", false, false)
		else
			nErr = 2
		end if
	else
		nErr = 1
	end if
	notifyShopCCOrderPlaced = nErr
end function


function notifyCustomerOrderConfirmed(aOrder, aMember, aContents)
	dim nErr:nErr = 0
	dim sTo, sToName, sFrom, sFromName, sSubject
	dim sChargeAdd,sDeliveryAdd,sCustName,sOrderDate,sOrderTime,sOrderMessage,sOrderInstruct,aTmp,i, sProdDesc
	dim sMsg:sMsg = ""
	if (not isNull(aOrder)) and (isArray(aOrder)) then
		if not isNull(aContents) then
			aTmp = split(aOrder(OD_DATE, 0), " ")
			sOrderDate	 = aTmp(0)
			sOrderTime	 = aTmp(1)
			if UBound(aTmp) = 2 then
				'// also have a PM to add //
				sOrderTime = sOrderTime & " " & aTmp(2)
			end if
			if len("" & aOrder(OD_FIRSTNAME,0)) > 0 then
				sCustName	 = aOrder(OD_FIRSTNAME,0)
			else
				sCustName	 = replace(trim(aOrder(OD_TITLE,0) & " " & aOrder(OD_FIRSTNAME,0) & " " & aOrder(OD_SURNAME,0)), "  ", " ")
			end if
			sChargeAdd	 = makeAddress(aOrder(OD_TITLE,0),aOrder(OD_FIRSTNAME,0),aOrder(OD_SURNAME,0) _
									  ,aOrder(OD_ADDRESS1,0),aOrder(OD_ADDRESS2,0),aOrder(OD_ADDRESS3,0) _
									  ,aOrder(OD_ADDRESS4,0),getCountry(aOrder(OD_ADDRESS5,0)),aOrder(OD_POSTCODE,0))
			sDeliveryAdd = makeAddress(aOrder(OD_DELTITLE,0),aOrder(OD_DELFIRSTNAME,0),aOrder(OD_DELSURNAME,0) _
									  ,aOrder(OD_DELADDRESS1,0),aOrder(OD_DELADDRESS2,0),aOrder(OD_DELADDRESS3,0) _
									  ,aOrder(OD_DELADDRESS4,0),getCountry(aOrder(OD_DELADDRESS5,0)),aOrder(OD_DELPOSTCODE,0))
			if aOrder(OD_KEYFNID,0) > 0 then
				sOrderMessage = getNote(aOrder(OD_KEYFNID,0))
			end if
			if aOrder(OD_KEYFN2ID,0) > 0 then
				sOrderInstruct = getNote(aOrder(OD_KEYFN2ID,0))
			end if
			sMsg = sMsg & "Dear " & sCustName & "," & vbcrlf
			sMsg = sMsg & vbcrlf
			sMsg = sMsg & "Thank you for your order which will be processed as soon as possible. Your" _
                  & " order details are listed below - if there are any changes you need to make," _
                  & " please contact us without delay either by replying to this email or by" _
                  & " phoning us on  " & SHOP_TEL_NO & "." & vbcrlf
			sMsg = sMsg & vbcrlf
			sMsg = sMsg & "Your order details are as follows:" & vbcrlf
      
			sMsg = sMsg & vbcrlf
			sMsg = sMsg & "ORDER DATE: "
      sMsg = sMsg & sOrderDate & " " & sOrderTime & vbcrlf
			sMsg = sMsg & vbcrlf
			sMsg = sMsg & "ONLINE ORDER REF: "
      sMsg = sMsg & "N" & right("00000" & aOrder(OD_ID, 0), 5) & vbcrlf
			sMsg = sMsg & vbcrlf
			sMsg = sMsg & "CUSTOMER ID NUMBER: "
      sMsg = sMsg & aOrder(OD_KEYFMEID, 0) & vbcrlf
			sMsg = sMsg & vbcrlf
			sMsg = sMsg & "FROM:  " & vbcrlf
      sMsg = sMsg & sChargeAdd & vbcrlf
      sMsg = sMsg & vbcrlf
      
			sMsg = sMsg & "EMAIL: "
      sMsg = sMsg & aOrder(OD_EMAIL, 0) & vbcrlf
      sMsg = sMsg & vbcrlf
      
			sMsg = sMsg & "TEL NO: "
      sMsg = sMsg & aOrder(OD_PHONE, 0) & vbcrlf
      sMsg = sMsg & vbcrlf
      
      sMsg = sMsg & "ORDER DETAILS:  " & vbcrlf
      '// loop items //
			for i = 0 to UBound(aContents,2)
				sProdDesc = aContents(OI_DESCRIPTION, i)
				if aContents(OI_KEYFN1ID, i) > 0 then
					sProdDesc = sProdDesc & " - " & getNote(aContents(OI_KEYFN1ID, i))
				end if
        'sMsg = sMsg & "Order: " & aOrder(OD_ID, 0) & vbcrlf
        sMsg = sMsg & "ITEM CODE: " & aContents(OI_PRODUCTCODE, i) & vbcrlf
        sMsg = sMsg & "ITEM DESCRIPTION: " & sProdDesc & vbcrlf
        sMsg = sMsg & "PRICE: �" & formatnumber(aContents(OI_UNITCOST,i),2) & vbcrlf
        sMsg = sMsg & "QUANTITY: " & aContents(OI_QUANTITY, i) & vbcrlf
        sMsg = sMsg & "LINE VALUE: �" & formatnumber((aContents(OI_UNITCOST,i) * aContents(OI_QUANTITY, i)),2) & vbcrlf
        sMsg = sMsg & "DELIVERY: " & aContents(OI_LEADTIMETEXT, i) & vbcrlf
				sMsg = sMsg & vbcrlf
			next
			sMsg = sMsg & "ORDER VALUE � " & FormatNumber(aOrder(OD_TOTALCHARGECALC,0) - (aOrder(OD_POSTAGE,0) + aOrder(OD_VAT,0)),2) & vbcrlf
			sMsg = sMsg & "POST & PACKAGING: � " & FormatNumber(aOrder(OD_POSTAGE,0),2) & vbcrlf
			sMsg = sMsg & "VAT: � " & FormatNumber(aOrder(OD_VAT,0),2) & vbcrlf
			sMsg = sMsg & "TOTAL INVOICE VALUE: � " & FormatNumber(aOrder(OD_TOTALCHARGECALC,0),2) & vbcrlf
			sMsg = sMsg & vbcrlf
      
			sMsg = sMsg & "DELIVER TO:  " & vbcrlf
      sMsg = sMsg & sDeliveryAdd & vbcrlf
      sMsg = sMsg & vbcrlf
      
			if len("" & sOrderMessage) then
				sMsg = sMsg & "Your items will be sent with the following gift message:" & vbcrlf
				sMsg = sMsg & sOrderMessage &  vbcrlf
				sMsg = sMsg & vbcrlf
			end if
			if len("" & sOrderInstruct) then
				sMsg = sMsg & "Your items will be sent with the following delivery instructions:" & vbcrlf
				sMsg = sMsg & sOrderInstruct &  vbcrlf
				sMsg = sMsg & vbcrlf
			end if
      sMsg = sMsg & "Thank you for shopping at " & SHOP_NAME & "." & vbcrlf & vbcrlf
      sMsg = sMsg & SHOP_NAME & vbcrlf
      sMsg = sMsg & "[W]: " & SITE_URL & vbcrlf
      sMsg = sMsg & "[E]: " & SHOP_TOEMAIL & vbcrlf
      sMsg = sMsg & "[T]: " & SHOP_TEL_NO & vbcrlf
			'sMsg = sMsg & "We are doing our best to deliver your order as fast as possible." _
      '            & " Usually we despatch within 24 hours of receiving your order but you should allow up to 7 days (UK)" _
      '            & " and 10 days elsewhere.  If we foresee a longer delay we will contact you." & vbcrlf
			'sMsg = sMsg & SHOP_TANDC_URL & vbcrlf
			'sMsg = sMsg & vbcrlf
      'sMsg = sMsg & "For further information see our FAQs page at " & SHOP_TANDC_URL _
      '            & " or email us at " & EML_SHOPTOEMAIL & " or call us on " & SHOP_TEL_NO & "."
			'sMsg = sMsg & "If you have any other queries relating to this order, please contact us." & vbcrlf
			'sMsg = sMsg & "Telephone: " & SHOP_TEL_NO & vbcrlf
			'sMsg = sMsg & "Email: " & EML_SHOPTOEMAIL & vbcrlf
			'sMsg = sMsg & "Post: " & SHOP_POST_ADDRESS & vbcrlf
			sMsg = sMsg & vbcrlf
			sTo = aOrder(OD_EMAIL,0)
			sToName = trim("" & aOrder(OD_TITLE,0) & " " & aOrder(OD_SURNAME,0) & " " & aOrder(OD_SURNAME,0))
			sFrom = EML_SHOPFROMEMAIL
			sFromName = EML_SHOPFROMNAME
			sSubject = SHOP_NAME & " Order Confirmation (Order ref: " & aOrder(OD_ID,0) & ")"
			nErr = SendMailEnc(sTo, sToName, sFrom, sFromName, sSubject, sMsg, "", "", false, false)
		else
			nErr = 2
		end if
	else
		nErr = 1
	end if
	notifyCustomerOrderConfirmed = nErr
end function


function makeAddress(sTitle,sFirstName,sSurName,sAdd1,sAdd2,sAdd3,sAdd4,sAdd5,sPostCode)
	dim sTmp:sTmp = ""
	sTmp = replace(trim(sTitle & " " & sFirstName & " " & sSurName), "  ", " ") & vbcrlf
	if len("" & sAdd1) then	sTmp = sTmp & sAdd1 & vbcrlf
	if len("" & sAdd2) then	sTmp = sTmp & sAdd2 & vbcrlf
	if len("" & sAdd3) then	sTmp = sTmp & sAdd3 & vbcrlf
	if len("" & sAdd4) then	sTmp = sTmp & sAdd4 & vbcrlf
	if len("" & sAdd5) then	sTmp = sTmp & sAdd5 & vbcrlf
	if len("" & sPostCode) then	sTmp = sTmp & sPostCode & vbcrlf
	makeAddress = sTmp
end function
%>