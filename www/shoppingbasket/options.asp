﻿<!--#include virtual="/includes/careandcomfortAPI.asp"-->
<!--#include virtual="/ecomm/setup.asp"-->
<!--#include virtual="/ecomm/countryselector.asp"-->
<!--#include virtual="/cms/setup.asp"-->
<!--#include virtual="/includes/dates.asp" -->

<%
dim id, nMeID, nCtID, nOdID 


nMeID = CLng("0" & session("meID"))
nCtID = CLng("0" & session("ctID"))
nOdID = CLng("0" & session("odID"))

if (nMeID = 0) or (nCtID = 0) then response.redirect "./"

'// lets update the cart with the member ID //
dim nErr        : nErr=0
dim aMember     : aMember=null
dim aCart       : aCart=null
dim aCartItems  : aCartItems=null
dim nPostZone   : nPostZone=0
dim bVAT        : bVAT=true
dim nPostage   : nPostage=0
dim blnPostZoneCovered : blnPostZoneCovered = false

dim sPostOpt,sGiftMsg,sDelMsg,bPartShip, nNoteID, nNote2ID, sVCHR
dim sX, sPostageMsg

nErr = loadMemberBespoke(nMeID, aMember)
if nErr > 0 then
    response.redirect "./error.asp"
else
    '// now lets calculate postage etc //
    if aMember(ME_USEALTADDRESS, 0) then
        nPostZone           = mid(aMember(ME_ALTADDRESS5, 0), 4, 1)
        blnPostZoneCovered  = aMember(ME_UBOUND+2,0)
        bVAT                = (mid(aMember(ME_ALTADDRESS5, 0), 6, 1) = "1")
    else
        nPostZone           = mid(aMember(ME_ADDRESS5, 0), 4, 1)
        blnPostZoneCovered  = aMember(ME_UBOUND+1,0)
        bVAT                = (mid(aMember(ME_ADDRESS5, 0), 6, 1) = "1")
    end if
    if blnPostZoneCovered = "" or len(blnPostZoneCovered) = 0 then
        blnPostZoneCovered = false
    end if
    'response.Write "<br />ME_USEALTADDRESS=" & aMember(ME_USEALTADDRESS, 0) 
    'response.Write "<br />ME_UBOUND 2=" & aMember(ME_UBOUND+2,0)
    'response.Write "<br />ME_UBOUND 1=" & aMember(ME_UBOUND+1,0) 
    'response.Write "<br />nPostZone=" & nPostZone 
    'response.Write "<br />blnPostZoneCovered=" & blnPostZoneCovered 
end if

nErr = loadCart(nCtID, aCart)
if nErr = 0 then
    aCart(CT_KEYFMEID, 0) = nMeID
    saveCart(aCart)    
    
    sSQL = "SELECT CI.*, PR.keyFpcID, PR.keyFwcID, PR.prPrice, PR.prPromoPrice, PR.prPromo FROM tblCartItem AS CI LEFT JOIN tblProduct AS PR ON CI.keyFprID=PR.prID WHERE CI.keyFctID=" & nCtID & " AND CI.keyFinID=" & INST_ID & ";"
    nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aCartItems)
    if nErr = 0 then
        'if blnPostZoneCovered then
            nErr = calcCartPostage(aCartItems, nPostZone, nPostage, sPostageMsg)
            if nErr > 0 then
                'response.redirect "./error.asp"
            end if
        'end if
    else
        '// cart is empty //
        response.redirect "./"
    end if
else
    'response.redirect "./error.asp"
end if



if request.form("submitted") = "yes" then
    sPostOpt = request.form("PostOpt")
    nNoteID = clng("0" & request.form("eax"))
    nNote2ID = clng("0" & request.form("aku"))
    sGiftMsg = trim("" & request.form("GiftMsg"))
    sDelMsg = trim("" & request.form("DelMsg"))
    bPartShip = (CInt("0" & request.form("partShip")) = 0)
    'sVCHR = trim("" & request.form("vchr"))
    'if len("" & sVCHR) > 0 then
    '    nErr = processVoucher(nMeID, sVCHR, sX)
    '    if nErr > 0 then
    '      session("VCHRCODE")=""
    '      session("VCHRDESC")=""
    '      session("VCHRDISC")=0
    '    end if
    'else
    '    session("VCHRCODE")=""
    '    session("VCHRDESC")=""
    '    session("VCHRDISC")=0
    'end if
    if nErr = 0 then
        nErr = setPostOptions(nCtID, sPostOpt, nNoteID, sGiftMsg, nNote2ID, sDelMsg, bPartShip)
        if nErr = 0 then
	        response.redirect("./confirm.asp")
        else
	        sX = "There was a problem saving your postage options, please try again."
        end if
    end if
else
    nErr = getPostOptions(nCtID, sPostOpt, nNoteID, sGiftMsg, nNote2ID, sDelMsg, bPartShip)
    sVchr = session("VCHRCODE")
end if

function processVoucher(nMeID, sVchr, sMsg)
  dim nErr:nErr=0
  dim dtNow:dtNow=now()
  dim dtStart:dtStart=null
  dim dtEnd:dtEnd=null
  dim sDesc:sDesc=""
  dim nDisc:nDisc=0
  nErr=1
  if len("" & sVchr) < 5 then
    nErr = 2  '// bad input parms //
    sMsg = "&nbsp;<br /><b>Sorry the voucher you entered is not valid.</b>" _
         & "<br />Please check that you have entered the details correctly.<br />"
  else
    select case sVchr
      case "ECAT1315"
        '// 15% off everything from jan to feb14th 2006 //
        dtStart = CDate("01/01/2006 00:00:01")
        dtEnd   = CDate("14/02/2006 23:59:59")
        if (dtStart > dtNow) then
          nErr = 1
          sMsg = "&nbsp;<br /><b>Sorry the voucher you entered is not currently valid.</b>" _
             & "<br />"
        elseif (dtEnd < dtNow) then
          nErr = 1
          sMsg = "&nbsp;<br /><b>Sorry the voucher you entered has expired.</b>" _
             & "<br />"
        else
          nErr = 0
          sDesc = "EW catalogue 15% off promotion."
          nDisc = 15
          session("VCHRCODE") = sVchr
          session("VCHRDESC") = sDesc
          session("VCHRDISC") = nDisc
        end if
      case else
        nErr = 1
        sMsg = "&nbsp;<br /><b>Sorry the voucher you entered is not valid.</b>" _
             & "<br />Please check that you have entered the details correctly.<br />"
    end select
  end if
  processVoucher = nErr
end function

%>
<!--#include virtual="/includes/doctype.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <%=GetDefaultEcommMetaTags("Delivery options - " & SHOP_NAME)%>  
    <!--#include virtual="/includes/mainCSS.asp" -->
	 <!--#include virtual="/includes/javascript.asp" -->

	  <script language="JavaScript" type="text/javascript">
        <!--
        function showTandC(){
	        var tandcWin;
	        tandcWin = window.open("/termsandconditions/index.asp","TandC","height=600,width=790,scrollbars,menubar,resizable");
	        return false;
        }
        //-->
    </script>
    <%=javaSetDropDown("registration")%>
</head>

<!--#include virtual="/includes/templateStart.asp" -->
<!--#include virtual="/includes/templateTopNav.asp" -->
<!--#include virtual="/includes/templateLeftNav.asp" -->

<!--#include virtual="/includes/templateBeginMainContent.asp" -->
  
<!-- Begin Content -->          
    <div class="contact_block"> 
      <%=nvCmsGetContentHTML(CMS_CONTENT_ID_CONTACT_SUMMARY)%>     
    </div> 
    
    
<!--#include virtual="/includes/templateBasketbreadCrumb.asp" -->  

<h2>Options</h2>
 
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="text">


<tr><td><img src="/graphics/clearpix.gif" alt="" width="1" height="1"></td>
    <td valign="top">
        <form name="options" action="./options.asp" method="post">
        <input type="hidden" name="submitted" value="yes" />
        <input type="hidden" name="eax" value="<%=nNoteID%>" />
        <input type="hidden" name="aku" value="<%=nNote2ID%>" />
        <!--<h3>&nbsp;<br />Please note: The last shipping date for Christmas delivery is 20th December 12:00pm.</h3>-->
        
<%
if not blnPostZoneCovered then
'// outside of uk and EU europe //
%>

<p class="red_text">Unfortunately the country you have chosen for your registered profile does not allow you to order online.
<br /><br />Please <a href="./register.asp" title="Edit My Profile">change your profile's delivery address</a> 
or <a href="/contactus/" title="contact us">contact us</a> for offline purchase and delivery price information.
<br /><br />We apologise for any inconvenience this may have caused.</p>

<%
else

    if VCHR_OPTS > 0 then
    %>
        <h3>Promotional Voucher</h3>
        <b>Enter any discount vouchers in the box below.</b><br />
        If you have a promotional voucher for <%=SHOP_NAME%> you can enter it here.<br />&nbsp;<br />
        Voucher: <input class="shopping_inputbox" type="text" name="vchr" id="vchr" value="<%=sVCHR%>"size="40" />
        <br /><%=sX%>&nbsp;<br />
    <%
    end if
    
    ' not implemented for this client
    if POST_OPTS > 0 then
    %>
        <h3>Delivery</h3>
        <input type="radio" name="postOpt" id="postSTD" value="STD" checked /><label for="postSTD"><b> postage &pound;4.90<%'=FormatNumber(nPostage,2)%>, FREE for orders over &pound;100</b></label><br />
        <input type="radio" name="postOpt" id="post24H" value="24H" <%if sPostOpt="24H" then response.write "checked "%>/>
        <label for="post24H"><b>Next day additional &pound;5.00</b></label>
        <br /><br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please read our <a href="\termsandconditions\index.asp" target="_blank" onclick="return showTandC();">Term &amp; Conditions</a> for full details<br />
        <br /><br />
    <%
    else
    %>		
        <h3>Delivery</h3>
        Your purchase will be sent via standard delivery.<br /><%=DELIVERY_MSG_VAT%><br /><br />
        <%
        ' sPostageMsg: This msg is determined in calling logic at top of page       
        response.Write sPostageMsg  
        %>
        <input type="hidden" name="postOpt" id="postOpt" value="STD" />
        <br />
    <% 
    end if
    %>
    <br />
    <input type="image" class="shopRight" src="/ecomm/buttons/button_proceedtocheckout.gif" name="submit" value="Update" />
    <br />
    <%=sX%>
    <br />
<%
end if  'nPostZone) <> "9"
%>
        </form>
    </td>
    <td><img src="/graphics/clearpix.gif" alt="" width="1" height="1"></td>
</tr>
</table>
<br /><br />
<!-- End Content -->
<!--#include virtual="/includes/templateEndMainContent.asp" -->


<!--#include virtual="/includes/templateRightColumn.asp" -->


<!--#include virtual="/includes/templateFooter.asp" -->
<!--#include virtual="/includes/templateEnd.asp" -->