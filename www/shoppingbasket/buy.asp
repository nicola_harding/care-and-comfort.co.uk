<!--#include virtual="/includes/careandcomfortAPI.asp"-->
<!--#include virtual="/ecomm/setup.asp"-->


<%

'Response.End()

IF SANDBOX AND (NVISAGE_IP <> REMOTE_HOST) Then
	Response.Redirect "./index.asp"
End IF
dim nErr : nErr=0
dim nMeID, nCtID, sColour, sSize
dim sRFR, sParms, nPrID, nQty 
dim aProd           : aProd = null
 
nMeID               = CLng("0" & session("meID"))
nCtID               = CLng("0" & session("ctID"))
sRFR                = request.servervariables("HTTP_REFERER")
session("BUY_RFR")  = sRFR
sParms              = request.querystring
nQty                = CLng("0" & request("qty"))
sNote               = "" & request("sNote")

If request("prID") = VBNullString Then
' // Picks up querystring and sets values into nCatID --> nPrdID
	call parseParms(request.querystring,"",nCatID,nRngID,nGrpID,nColID,nPrdID)
Else
	nPrID = CLng("0" & request("prID"))
End IF

if len("" & request("options")) > 0 then
    ' A child / colour item has been selected, extract the childs product id and details
    sNote   = ""    
    nPrID   = request("options")
    sSize   = request("child_size" & nPrID)
    sColour = request("child_colour" & nPrID)

    ' // if We are a child collect size and colour details
    if len("" & sSize) > 0 then
        if len(trim(sNote)) > 0 then
            sNote = sNote & " - "
        end if
	    sNote = sNote & sSize
	end if
    if len("" & sColour) > 0 then
        if len(trim(sNote)) > 0 then
            sNote = sNote & " - "
        end if
	    sNote = sNote & sColour
    end if    
end if

if nPrID = 0 or nPrID = "" then
    response.redirect sRFR
end if


if (nPrID = 0) or (nQty = 0) then
    response.redirect sRFR
else
    if len(trim(sNote)) > 0 then
        nNote = Add_Note(sNote)	
    end if

    nErr = addItemToCart(nMeID, nCtID, nPrID, nQty, nWlID, nNote, sNote)
	
	'Response.End
	
   response.redirect "./index.asp?" & buildParms("",nCatID,nRngID,nGrpID,nColID,nPrdID) 
end if

function Add_Note(sNote)
    dim aNote(2,2)
	if len(sNote) > 0 then
        aNote(0,0) = NT_ID
        aNote(1,0) = sNote
        aNote(2,0) = INST_ID
        nErr = saveNote(aNote)
        Add_Note = aNote(0,0)
	end if
end function

function addItemToCart(nMeID, nCtID, nPrID, nQty, nWlID, nNote, sNote)
  dim nErr:nErr=0
  dim nNtID:nNtID=0
  
  ' Response.Write "nCtID " & nCtID & "<br/>"

  if (nPrID > 0) and (nQty > 0) then
    '// does the customer have an existing shopping cart?//
    if nCtID = 0 then
      '// create a new cart //	   
      nErr = createCart(nMeID, nCtID, session.sessionID, request.servervariables("REMOTE_HOST"))
    end if
	
	'Response.Write "nErr " & nErr & "<br/>"
	
    if nErr = 0 then
      session("ctID") = nCtID

	'Response.Write "session(""ctID"") " &  session("ctID") & "<br/>"
      if not itemExistsInCart(nCtID, nPrID, sNote) then
        'if (nWlID > 0) then
          '// generate the note - WISHLIST:nWlID //
          'dim aWish:aWish=null
          'nErr = loadWishList(nWlID, aWish)
          'if nErr = 0 then
          '  nNtID = addNote("WISHLIST:" & nWlID & ":" & aWish(WL_NAME, 0))            
          'end if
          'aWish = null
        'end if
        
        nErr = addToCart(nCtID, nPrID, nQty, nNote)
	  Else
		nErr = itemUpdateQuantityInCart(nCtID, nPrID, nQty)
		
      end if
    end if
  else
    nErr = 1  '// bad input parms //
  end if
  addItemToCart = nErr
end function

function itemExistsInCart(nCtID, nPrID, sNote)
  dim bTmp:bTmp=false
  dim sSQL:sSQL=""
  dim aTmp:aTmp=null
  

  if (nPrID > 0) and (nCtID > 0) then
    'sSQL = "SELECT ciID FROM tblCartItem WHERE keyFctID=" & nCtID & " AND keyFprID=" & nPrID & " AND keyFinID=" & INST_ID & ";"
	sSQL = "SELECT CI.ciID,CI.keyFntID, NT.ntText FROM tblCartItem AS CI LEFT JOIN tblNote AS NT ON CI.keyFntID = NT.ntID WHERE CI.keyFctID=" & nCtID & " AND CI.keyFprID=" & nPrID & " AND CI.keyFinID=" & INST_ID & ";"
    bTmp = (getRowsBySQL(DSN_ECOM_DB, sSQL, aTmp) = 0)
	'Response.Write bTmp
    'aTmp = null
  end if
  itemExistsInCart = bTmp
end function

function itemUpdateQuantityInCart(nCtID, nPrID, nQty)
  dim bTmp:bTmp=false
  dim sSQL:sSQL=""
  dim aTmp:aTmp=null
  
 if (nCtID > 0) and (nCiID > 0) and (nQty > 0) then
 
    'sSQL = "SELECT ciID FROM tblCartItem WHERE keyFctID=" & nCtID & " AND keyFprID=" & nPrID & " AND keyFinID=" & INST_ID & ";"
		sSQL = "UPDATE tblCartItem SET ciQuantity = ciQuantity + " & nQty &  " WHERE keyFctID=" & nCtID & " AND keyFprID=" & nPrID & " AND keyFinID=" & INST_ID & ";"
   
		sqlExec DSN_ECOM_DB, sSQL 
	
	Else
		nErr = 1  '// bad input parms //
	end if
	
	itemUpdateQuantityInCart = bTmp
end function

function addToCart(nCtID, nPrID, nQty, nNtID)
  dim nErr:nErr=0
  dim aCartItem:aCartItem=null
  redim aCartItem(CI_UBOUND, 0)
  
  aCartItem(CI_KEYFCTID, 0) = nCtID
  aCartItem(CI_KEYFPRID, 0) = nPrID
  aCartItem(CI_QUANTITY, 0) = nQty
  aCartItem(CI_KEYFNTID, 0) = nNtID
  aCartItem(CI_KEYFINID, 0) = INST_ID
  nErr = saveCartItem(aCartItem)
    
  aCartItem = null
  addToCart = nErr
end function

function createCart(nMeID, nCtID, sSession, sIP)
  dim nErr:nErr=0
  dim aCart:aCart=null
  redim aCart(CT_UBOUND, 0)
  aCart(CT_KEYFMEID, 0) = nMeID
  aCart(CT_SESSIONID, 0) = sSession
  aCart(CT_IPADDRESS, 0) = sIP
  aCart(CT_KEYFINID, 0) = INST_ID
  'response.write "nMeID=" & nMeID & " sSession=" & sSession & "sIP=" & sIP & "INST_ID=" & INST_ID
  nErr = saveCart(aCart)
  
  if nErr = 0 then  	
    nCtID = aCart(CT_ID, 0)
  else
    nCtID = 0
  end if
  aCart = null
  createCart = nErr
end function
%>