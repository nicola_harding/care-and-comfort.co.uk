<!--#include virtual="/includes/careandcomfortAPI.asp"-->
<!--#include virtual="/ecomm/setup.asp"-->
<!--#include virtual="/ecomm/countryselector.asp"-->
<!--#include virtual="/cms/setup.asp"-->
<!--#include virtual="/shoppingbasket/protex_functions.asp"-->

<%
dim id, bVAT
dim nErr            : nErr=0
dim nOdID           : nOdID=0
dim nMeID           : nMeID=0
dim sHeader         : sHeader=""
dim aOrder          : aOrder=null
dim aOrderItems     : aOrderItems=null
dim bSuccess        : bSuccess=false
dim sProtxCrypt     : sProtxCrypt = ""
dim sProtxResult    : sProtxResult = ""
dim sProtxTxnId     : sProtxTxnId = ""
dim sBankAuthCode   : sBankAuthCode = ""
dim sPayType        : sPayType=trim("" & request.querystring("PAY"))
dim aMemberCompany              : aMemberCompany = null
dim blnIsPayByInvoicePermitted  : blnIsPayByInvoicePermitted = false

sProtxCrypt         = request.querystring("crypt")
nOdID               = unencodeID("" & request.querystring("e"))

'// build the receipt
if nOdID > 0 then
    nErr = getMemberFromOrderID(nOdID,aMember)
    
    '[hardin] logic below was looking at bVat but not setting it. Added this logic as seen in options and confirm steps
    if nErr > 0 then
        response.redirect "./error.asp"
    else
        '// Aid display of the applied vat for this order //
        if aMember(ME_USEALTADDRESS, 0) then
            bVAT = (mid(aMember(ME_ALTADDRESS5, 0), 6, 1) = "1")
        else
            bVAT = (mid(aMember(ME_ADDRESS5, 0), 6, 1) = "1")
        end if
        
        nMeID           = aMember(ME_ID, 0)
        session("MeID") = nMeID
    end if    
    
    if len(sProtxCrypt) > 0 then
        sProtxCrypt     = SimpleXor(Base64Decode(sProtxCrypt), EncryptionPassword)         
        ' // [hardin]  Retrieve the status of the transaction from Protx encrypted result info
        sProtxResult    = getToken(sProtxCrypt,"Status")
        sProtxTxnId     = getToken(sProtxCrypt,"VPSTxId")
        sBankAuthCode   = getToken(sProtxCrypt,"TxAuthNo")
        session("ProtxTxnId")   = sProtxTxnId
        session("BankAuthCode") = sBankAuthCode
    end if


    if len(session("isSuccessYes")) = 0 then
        ' interrogate the real protx result and save this to the db
        if lcase(sProtxResult) = "ok" then
            session("isSuccessYes") = "true"
			session("ctID") = 0
            session("odID") = 0
			
        else
            session("isSuccessYes") = "false"
        end if
    end if    
    
	
    nErr = loadOrder(nOdID, aOrder)
    if nErr = 0 then
        if session("isSuccessYes") = "true" then
            bSuccess = true
	        session("hasBeen") = "true"
	        ' // update orders table
	        nErr = makeCCPayment(aOrder, aMember, False, False, False, False, False, False, False, False, False)		
        else
	        bSuccess = false
        end if
    else
        ' // DO NOTHING? HUH
    end if
    
    if bSuccess then
        ' // these are set to nothing after they are populated into the upload forms (top and bottom)
        ' session("ctID") = 0
        ' session("odID") = 0
        
        'nErr = setPostOptions(0, "", 0, "", 0, "", false)
    end if
else
	'// the shopping basket is currently empty //
end if

'// Update stock levels - function located in /ecomm/set-up.asp
if bSuccess = true and LEN("" & session("stockDone")) = 0 then
    sSQL = "SELECT CI.*, PR.keyFpcID, PR.keyFwcID, PR.prPrice, PR.prPromoPrice, PR.prPromo, PR.prID, PR.prStockQty,CT.*  FROM ((tblCartItem AS CI  LEFT JOIN tblCart AS CT ON CI.keyFctID = CT.ctID)  LEFT JOIN tblProduct AS PR ON CI.keyFprID=PR.prID) WHERE CT.ctSessionID='"& aOrder(OD_SESSIONID, 0) & "' AND CI.keyFinID=" & INST_ID & ";"
    nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aCartItems)
    for intJ = 0 to ubound(aCartItems, 2)
        nErr = Update_Stock(aCartItems(CI_UBOUND+6,intJ),aCartItems(CI_UBOUND+7,intJ))
    next
    session("stockDone") = "Updated"
end if
%>

<!--#include virtual="/includes/doctype.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <%=GetDefaultEcommMetaTags("Order receipt " & SHOP_NAME)%>  
    <!--#include virtual="/includes/mainCSS.asp" -->
	 <!--#include virtual="/includes/javascript.asp" -->

	 
    <%=javaSetDropDown("registration")%>
</head>

<!--#include virtual="/includes/templateStart.asp" -->
<!--#include virtual="/includes/templateTopNav.asp" -->
<!--#include virtual="/includes/templateLeftNav.asp" -->


<!--#include virtual="/includes/templateBeginMainContent.asp" -->
    
  
<!-- Begin Content --> 
    
	<!--#include virtual="/includes/templateBasketbreadCrumb.asp" -->  
  


    <div class="contact_block"> 
      <%=nvCmsGetContentHTML(CMS_CONTENT_ID_CONTACT_SUMMARY)%>     
    </div> 
    
   
    <%
    if cbool(bSuccess) = false then
        session("odID") = nOdID   
        response.write "<h2>Payment Cancelled</h2>"
    %>
		<p class="text"><b>Attempt payment again?</b></p>
    
        <a href="./confirm.asp"><img src="/ecomm/buttons/button_yescontinue.gif" border="0" alt="Yes, Continue" /></a></p>
    <%    
    else
         response.write "<h2>Your Receipt" & sTraderProForma & "</h2>" 
         response.write showOrder(nOdId, aMember, BSKT_HDR_CLR, BSKT_ROW_CLR, BSKT_BGD_CLR)
    end if
    %>
<!-- End Content -->
<!--#include virtual="/includes/templateEndMainContent.asp" -->


<!--#include virtual="/includes/templateRightColumn.asp" -->


<!--#include virtual="/includes/templateFooter.asp" -->
<!--#include virtual="/includes/templateEnd.asp" -->
          
<%
function getMemberFromOrderID(nOdID,aMember)
  dim nErr:nErr=0
  dim sSQL:sSQL = ""
  if nOdID > 0 then
    sSQL = "SELECT ME.* FROM tblMember AS ME LEFT JOIN tblOrder AS OD ON OD.keyFmeID=ME.meID" _
        & " WHERE OD.odID=" & nOdID & " AND OD.keyFinID=" & INST_ID & " AND ME.keyFinID=" & INST_ID & ";"
    nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aMember)
  else
    nErr = 1  '// bad input parms //
  end if
  getMemberFromOrderID = nErr
end function

function showOrder(nOdID, aMember, sHedClr, sRowClr, sBkgClr)
dim nErr:nErr=0
dim sTmp:sTmp=""
dim aItems:aItems=null
dim nIdx:nIdx=0
dim sSQL:sSQL=""
dim nTotal, nSubTotal, nVat, nTotalVAT
dim sName, sLeadTime, nPrice, nQty
nIdx = 0
sTmp = ""
dim sMbrAdd, sDelAdd

    if nOdID > 0 then 

    sSQL = "SELECT OD.*,OI.* FROM tblOrder AS OD LEFT JOIN tblOrderItem AS OI ON OD.odID=OI.keyFodID" _
         & " WHERE OD.odID=" & nOdID & " AND OD.keyFinID= "& INST_ID & " AND OI.keyFinID=" & INST_ID & " ORDER BY OI.oiID ASC;"
    nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aItems)
    if nErr = 0 then        
        response.write UPLOAD_IMAGE_REMINDER_RECEIPT 
%>
  

    <h1><%=SHOP_NAME%></h1>

    <p class="text">&nbsp;<br /><b>Thank you for your order.</b></p>
    <p class="text">Confirmation details of this order have been sent to your email address.</p>

  <table align="center" class="cart_table" width="100%" border="0" bgcolor="<%=sBkgClr%>" cellspacing="0" cellpadding="0" class="text">
    <tr >
    <th>&nbsp;</th>
    <th>Description</th>
    <th>&nbsp;</th>
    <th align="center">Quantity</th>
    <th>&nbsp;</th>
    <th align="right">Price</th>
    <th>&nbsp;</th>
    <th align="right">Sub&nbsp;Total</th>
    <th>&nbsp;</th></tr>
<%
for nIdx=0 to UBound(aItems, 2)
%>
    <tr bgcolor="<%=sRowClr%>">
    <td>&nbsp;</td>                     
    <td valign="top"><%=aItems(OD_UBOUND + 1 + OI_DESCRIPTION, nIdx)%><!--<br><small>Delivery - <%=aItems(OD_UBOUND + 1 + OI_LEADTIMETEXT, nIdx)%></small>--></td>
    <td>&nbsp;</td>
    <td valign="top" align="center"><%=aItems(OD_UBOUND + 1 + OI_QUANTITY, nIdx)%></td>
    <td>&nbsp;</td>
    <td valign="top" align="right"><%=getPriceForexHTML(aItems(OD_UBOUND + 1 + OI_UNITCOST, nIdx), 2)%></td>
    <td>&nbsp;</td>
    <td valign="top" align="right"><%=getPriceForexHTML(aItems(OD_UBOUND + 1 + OI_UNITCOST, nIdx) * aItems(OD_UBOUND + 1 + OI_QUANTITY, nIdx), 2)%></td>
    <td>&nbsp;</td></tr>
<%next%>
<tr >
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th align="right">Sub&nbsp;Total</th>
<th>&nbsp;</th>
<th align="right"><%=getPriceForexHTML(aItems(OD_TOTALNOVAT, 0) + aItems(OD_VAT, 0), 2)%></th>
<th>&nbsp;</th></tr>

<%
' // Old logic from hen and mahhock: if 1 or bVAT or (VAT_OPTS = 0) then
if bVAT then
%>
    <tr bgcolor="<%=sRowClr%>">
    <td>&nbsp;</td>
    <td colspan="3">VAT Included</td>
    <td>&nbsp;</td>
    <td align="right">(<%=getPriceForexHTML(aItems(OD_VAT, 0), 2)%>)</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td></tr>
<%
else
%>
    <tr bgcolor="<%=sRowClr%>">
    <td>&nbsp;</td>
    <td colspan="3">VAT</td>
    <td>&nbsp;</td>
    <td align="right">-<%=getPriceForexHTML(aItems(OD_VAT, 0), 2)%></td>
    <td>&nbsp;</td>
    <td align="right">-<%=getPriceForexHTML(aItems(OD_VAT, 0), 2)%></td>
    <td>&nbsp;</td>
</tr>
<%end if%>
<%If False Then%>
<tr>
    <td>&nbsp;</td>
    <td colspan="3">Postage &amp; Packaging</td>
    <td>&nbsp;</td>
    <td align="right"><%=getPriceForexHTML(aItems(OD_POSTAGE, 0), 2)%></td>
    <td>&nbsp;</td>
    <td align="right"><%=getPriceForexHTML(aItems(OD_POSTAGE, 0), 2)%></td>
    <td>&nbsp;</td>
</tr>

<%
End IF
if len("" & session("promocode")) > 0 and (session("promocode") <> "") then
    ntempTotal          = formatnumber(aItems(OD_TOTALNOVAT, 0) + aItems(OD_VAT, 0), 4)
    nDiscountedTotal    = formatnumber((ntempTotal - aItems(OD_PROMODISC, 0)), 4)
    nDiscountedTotal    = formatnumber(nDiscountedTotal + aItems(OD_POSTAGE, 0), 4)
%>
    <tr bgcolor="<%=sRowClr%>">
    <th>&nbsp;</td>
    <th>Discount</td>
    <th>&nbsp;</td>
    <th>&nbsp;</td>
    <th>&nbsp;</td>
    <th align="right">&nbsp;</td>
    <th>&nbsp;</th>
    <th align="right">-<%=getPriceForexHTML(aItems(OD_PROMODISC, 0), 3)%></td>
    <th>&nbsp;</th></tr>

    <tr bgcolor="<%=sHedClr%>" class="cartHeaders">
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">Total</td>
    <td>&nbsp;</td>
    <td align="right"><%=getPriceForexHTML(nDiscountedTotal, 2)%></td>
    <td>&nbsp;</td></tr>
<%else%>
    <tr>
    <th>&nbsp;</th>
    <th>&nbsp;</th>
    <th>&nbsp;</th>
    <th>&nbsp;</th>
    <th>&nbsp;</th>
    <th align="right">Total</th>
    <th>&nbsp;</th>
    <th align="right"><%=getPriceForexHTML(aItems(OD_TOTALCHARGECALC, 0), 2)%></td>
    <th>&nbsp;</th></tr>
<%end if%>
</table>

<%
sMbrAdd=""
sDelAdd=""

sMbrAdd = sMbrAdd & "&nbsp;" & trim("" & aOrder(OD_TITLE, 0) & " " & aOrder(OD_FIRSTNAME, 0) & " " & aOrder(OD_SURNAME, 0)) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aOrder(OD_ADDRESS1, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aOrder(OD_ADDRESS2, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aOrder(OD_ADDRESS3, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aOrder(OD_ADDRESS4, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aOrder(OD_POSTCODE, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & getCountry(aOrder(OD_ADDRESS5, 0)) & "<br />" & vbcrlf

sDelAdd = sDelAdd & "&nbsp;" & trim("" & aOrder(OD_DELTITLE, 0) & " " & aOrder(OD_DELFIRSTNAME, 0) & " " & aOrder(OD_DELSURNAME, 0)) & "<br />" & vbcrlf
sDelAdd = sDelAdd & "&nbsp;" & aOrder(OD_DELADDRESS1, 0) & "<br />" & vbcrlf
sDelAdd = sDelAdd & "&nbsp;" & aOrder(OD_DELADDRESS2, 0) & "<br />" & vbcrlf
sDelAdd = sDelAdd & "&nbsp;" & aOrder(OD_DELADDRESS3, 0) & "<br />" & vbcrlf
sDelAdd = sDelAdd & "&nbsp;" & aOrder(OD_DELADDRESS4, 0) & "<br />" & vbcrlf
sDelAdd = sDelAdd & "&nbsp;" & aOrder(OD_DELPOSTCODE, 0) & "<br />" & vbcrlf
sDelAdd = sDelAdd & "&nbsp;" & getCountry(aOrder(OD_DELADDRESS5, 0)) & "<br />" & vbcrlf

sMbrAdd = replace(sMbrAdd, "&nbsp;<br />", "")
sDelAdd = replace(sDelAdd, "&nbsp;<br />", "")
%>

<table align="center" class="cart_table" width="100%" border="0" bgcolor="<%=sBkgClr%>" cellspacing="0" cellpadding="0" class="text">
    <tr><td width="45%">&nbsp;</td><td width="10%">&nbsp;</td><td width="45%">&nbsp;</td></tr>
    <tr>
        <th bgcolor="<%=sHedClr%>" class="cartHeaders">&nbsp;Billing Address&nbsp;</th>
        <td >&nbsp;</td>
        <th bgcolor="<%=sHedClr%>" class="cartHeaders">&nbsp;Delivery Address&nbsp;</th>
    </tr>
    <tr bgcolor="<%=sRowClr%>">
        <td valign="top">
            <%=sMbrAdd%>
        </td>
        <td>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td valign="top">
            <%=sDelAdd%>
        </td>
    </tr>
    <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
</table>

<% 
dim sPostOpt, nNoteID, sGiftMsg, nNote2ID, sDelMsg, bPartShip
'nErr = getPostOptions(nCtID, sPostOpt, nNoteID, sGiftMsg, nNote2ID, sDelMsg, bPartShip)
if nErr = 0 then
    'sTmp = sTmp & "<table width=""100%"" border=""0"" bgcolor=""" & sBkgClr & """ cellpadding=""0"" cellspacing=""0"" class=""text"">"
    'sTmp = sTmp & "<tr><td bgcolor=""" & sHedClr & """ class=""shopping_title"">&nbsp;Order Options&nbsp;</td></tr>"
    'if bPartShip then
    '    sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;Order items may be shipped seperately,&nbsp;</td></tr>"
    'else
    '    sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;Order items will be shipped as a single parcel,&nbsp;</td></tr>"
    'end if
    'if len("" & sPostOpt) = 0 then
    '    sPostOpt = request.querystring("postOpt")
    'end if
    'if sPostOpt = "24H" then
    '    sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;via special delivery.&nbsp;</td></tr>"
    'else
    '    sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;via standard delivery.&nbsp;</td></tr>"
    'end if
    
     if len(sDelMsg) > 0 then
        sTmp = sTmp & "<table width=""100%"" border=""0"" bgcolor=""" & sBkgClr & """ cellpadding=""2"" cellspacing=""0"" class=""text"">"
        sTmp = sTmp & "<tr><td bgcolor=""" & sHedClr & """ class=""cartHeaders"">&nbsp;Order Options&nbsp;</td></tr>"
        sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;Your order will contain the following delivery instruction:&nbsp;</td></tr>"
        sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td><i>" & replace(sDelMsg, vbcrlf, "&nbsp;<br />") & "</i></td></tr>"
    end if
    
    'if len(sGiftMsg) > 0 then
    '    sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;Your order will contain the following gift message:&nbsp;</td></tr>"
    '    sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td><i>" & replace(sGiftMsg, vbcrlf, "&nbsp;<br />") & "</i></td></tr>"
    'end if
    sTmp = sTmp & "<td>&nbsp;</td></tr></table>"
end if
response.write sTmp
sTmp = ""

    else
      sTmp = "&nbsp;<br /><div class='text'>No details to show.</div>"
    end if
  else
    sTmp = "&nbsp;<br /><div class='text'>No details to show.</div>"
  end if
%>  
        <p><%=SHOP_NAME%> order number: <%=aOrder(OD_ID,0)%></p>
        <p><%=SHOP_NAME%> member number: <%=aOrder(OD_KEYFMEID,0)%></p>
        <p>Order Date: <%=aOrder(OD_DATE,0)%></p>  
        <hr />
<% 
  response.write UPLOAD_IMAGE_REMINDER_RECEIPT 
%>
    
     <p class="text"><b><a href="./printreceipt.asp?e=<%=encodeID(nOdID)%>" target="_blank" onClick="return popReceipt('./printreceipt.asp?e=<%=encodeID(nOdID)%>')">Print this Receipt</a></b></p>

  <%
  
  showOrder = sTmp
end function

function makeCCPayment(aOrder, aMember, sCCName, sCCType, sCCNo, sCCExpMM, sCCExpYY, sCCIssMM, sCCIssYY, sCCIssNo, sCCChkNo)
    dim nErr:nErr = 0
    dim aItems:aItems = null
    dim sSQL:sSQL=""
    
    ' [hardin] added logic to allow traders to proceed to receipt page, bypassing protx if client has indicated they can order by pro-forma
    ' // do not mark the txn as completed if it is still in a pro-form invoice state
    ' -------------------------------------
    if session("ProtxTxnId") = "PROFORMA" then
        aOrder(OD_COMPLETEDALL, 0) = false
    else
        aOrder(OD_COMPLETEDALL, 0) = true   
    end if  
    aOrder(OD_TRANSSUC, 0) = cbool(session("isSuccessYes"))
    aOrder(OD_TRANSID, 0) = cstr(session("ProtxTxnId"))
    aOrder(OD_AUTHCODE, 0) = cstr(session("BankAuthCode"))
    
    ' dont update the txn time after its been entered
    if len("" & aOrder(OD_TRANSTIME, 0)) = 0 then
        aOrder(OD_TRANSTIME, 0) = now()
    end if
    session("ProtxTxnId") = ""
    session("BankAuthCode") = ""
    
    '// update dB with what has happened //
    nErr = saveOrder(aOrder)
    if nErr = 0 then
        sSQL = "SELECT OI.* FROM tblOrderItem AS OI" _
            & " WHERE OI.keyFodID=" & aOrder(OD_ID, 0) & " AND OI.keyFinID=" & INST_ID & " ORDER BY OI.oiID ASC;"
        nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aItems)
    else
        '// what?? //
        exit function
    end if
    if nErr = 0 then
        '// notifyShopOrderPlaced //
        nErr = notifyShopCCOrderPlaced(aOrder, aMember, aItems, sCCName, sCCType, sCCNo, sCCExpMM, sCCExpYY, sCCIssMM, sCCIssYY, sCCIssNo, sCCChkNo)
        '// notifyCustomerOrderConfirmed //
        nErr = notifyCustomerOrderConfirmed(aOrder, aMember, aItems)
        ' // set this session back from "proforma" back to true as the next payment in this browser session could be a protx order and not proforma
        if (len("" & session("trader")) > 0) then
            session("trader")       = true
        end if      
    end if
    
    makeCCPayment = nErr
end function

function notifyShopCCOrderPlaced(aOrder, aMember, aContents, sCCName, sCCType, sCCNo, sCCExpMM, sCCExpYY, sCCIssMM, sCCIssYY, sCCIssNo, sCCChkNo)
	dim nErr:nErr = 0
	dim sTo, sToName, sFrom, sFromName, sSubject, sSubjectWarning
	dim nPrice, nVATLineItem, nVatTotal     
	dim sChargeAdd,sDeliveryAdd,sCustName,sOrderDate,sOrderTime,sOrderMessage,sOrderInstruct,aTmp,i, sProdDesc
	dim sMsg:sMsg = ""
	if (not isNull(aOrder)) and (isArray(aOrder)) then
		if not isNull(aContents) then
			aTmp            = split(aOrder(OD_DATE, 0), " ")
			sOrderDate	    = aTmp(0)
			sOrderTime	    = aTmp(1)
			if UBound(aTmp) = 2 then
				'// also have a PM to add //
				sOrderTime = sOrderTime & " " & aTmp(2)
			end if
			sCustName	 = replace(trim(aOrder(OD_TITLE,0) & " " & aOrder(OD_FIRSTNAME,0) & " " & aOrder(OD_SURNAME,0)), "  ", " ")
			sChargeAdd	 = makeAddress(aOrder(OD_TITLE,0),aOrder(OD_FIRSTNAME,0),aOrder(OD_SURNAME,0) _
									  ,aOrder(OD_ADDRESS1,0),aOrder(OD_ADDRESS2,0),aOrder(OD_ADDRESS3,0) _
									  ,aOrder(OD_ADDRESS4,0),getCountry(aOrder(OD_ADDRESS5,0)),aOrder(OD_POSTCODE,0))
			sDeliveryAdd = makeAddress(aOrder(OD_DELTITLE,0),aOrder(OD_DELFIRSTNAME,0),aOrder(OD_DELSURNAME,0) _
									  ,aOrder(OD_DELADDRESS1,0),aOrder(OD_DELADDRESS2,0),aOrder(OD_DELADDRESS3,0) _
									  ,aOrder(OD_DELADDRESS4,0),getCountry(aOrder(OD_DELADDRESS5,0)),aOrder(OD_DELPOSTCODE,0))
			if aOrder(OD_KEYFN1ID,0) > 0 then
				sOrderMessage = getNote(aOrder(OD_KEYFN1ID,0))
			end if
			if aOrder(OD_KEYFN2ID,0) > 0 then
				sOrderInstruct = getNote(aOrder(OD_KEYFN2ID,0))
			end if
			
			'// Determine if vat is charged vat //
            if aMember(ME_USEALTADDRESS, 0) then
                bVAT = (mid(aMember(ME_ALTADDRESS5, 0), 6, 1))
            else
                bVAT = (mid(aMember(ME_ADDRESS5, 0), 6, 1))
            end if 		
			
			sMsg = sMsg & "You have received the following order. " & vbcrlf
            sMsg = sMsg & vbcrlf
            sMsg = sMsg & "ORDER DATE: "
            sMsg = sMsg & sOrderDate & " " & sOrderTime & vbcrlf
            sMsg = sMsg & vbcrlf
            sMsg = sMsg & "ONLINE ORDER REF: "
            sMsg = sMsg & aOrder(OD_ID, 0) & vbcrlf
            sMsg = sMsg & vbcrlf
            sMsg = sMsg & "FROM:  " & vbcrlf
            sMsg = sMsg & sChargeAdd & vbcrlf
            sMsg = sMsg & vbcrlf

            sMsg = sMsg & "EMAIL: "
            sMsg = sMsg & aOrder(OD_EMAIL, 0) & vbcrlf
            sMsg = sMsg & vbcrlf

            sMsg = sMsg & "TEL NO: "
            sMsg = sMsg & aOrder(OD_PHONE, 0) & vbcrlf
            sMsg = sMsg & vbcrlf

            sMsg = sMsg & "ORDER DETAILS: "	  
	        if (len("" & session("trader")) > 0) and (session("trader") = "PROFORMA") then
	            sMsg = sMsg & "PAYMENT BY PROFORMA INVOICE METHOD  " & vbcrlf & vbcrlf
	        else
	            sMsg = sMsg & "PAYMENT BY PROTX  " & vbcrlf & vbcrlf
	        end if
	        sMsg = sMsg & "PAYMENT BY PROTX  " & vbcrlf & vbcrlf
 
	        
	   '// Gift Aid Notification
	  if len(session("giftAid")) > 0 then
	  	sMsg = sMsg & vbcrlf & "I am a UK taxpayer and would like the " & SHOP_NAME & " to reclaim tax on all donations. "  & session("giftAid") & vbcrlf & vbcrlf
	  end if
	  
      '// loop items //
			for i = 0 to UBound(aContents,2)
				sProdDesc = aContents(OI_DESCRIPTION, i)
				if aContents(OI_KEYFN1ID, i) > 0 then
					sProdDesc = sProdDesc & " - " & getNote(aContents(OI_KEYFN1ID, i))
				end if

                ' // If bVAT is true, then the customer is being charged vat
				nVATLineItem    = aContents(OI_UNITVAT,i)
                if bVAT then
                    nPrice      = formatnumber(aContents(OI_UNITCOST,i),2)
                    nVATLineItem= formatnumber(nVATLineItem, 4)
                    nVatTotal   = formatnumber(aOrder(OD_VAT,0),2)
                else
                    nPrice      = formatnumber((aContents(OI_UNITCOST,i) - nVATLineItem),2)
                    nVATLineItem= ccur(0)
                    nVatTotal   = ccur(0)
                end if 
                
                sMsg = sMsg & "Order: " & aOrder(OD_ID, 0) & vbcrlf
                sMsg = sMsg & "ITEM CODE: " & aContents(OI_PRODUCTCODE, i) & vbcrlf
                sMsg = sMsg & "ITEM DESCRIPTION: " & replace(replace(replace(replace(sProdDesc,"<b>",""),"</b>",""),"<strong>",""),"</strong>","") & vbcrlf
                sMsg = sMsg & "PRICE: " & ECOM_CUR_ISO_POUND & nPrice 
                ' // Show corresponding forex if other that Uk pounds
                if len("" & session("Forex")) > 0 then
                    sMsg = sMsg & " (" & getPriceForexHTMLemail(nPrice, 2) & ")"
                end if
                sMsg = sMsg & vbcrlf
                                
                sMsg = sMsg & "QUANTITY: " & aContents(OI_QUANTITY, i) & vbcrlf
                
                sMsg = sMsg & "LINE VALUE: " & ECOM_CUR_ISO_POUND & formatnumber((nPrice * aContents(OI_QUANTITY, i)),2) 
                ' // Show corresponding forex if other that Uk pounds
                if len("" & session("Forex")) > 0 then
                    sMsg = sMsg & " (" & getPriceForexHTMLemail((nPrice * aContents(OI_QUANTITY, i)), 2) & ")"
                end if
                sMsg = sMsg & vbcrlf                
				sMsg = sMsg & vbcrlf
			next
						
			if len(session("promocode")) > 0 then
		  		sMsg = sMsg & "DISCOUNT: " & ECOM_CUR_ISO_POUND & formatnumber(aOrder(OD_PROMODISC,0), 4)
		  		' // Show corresponding forex if other that Uk pounds
                if len("" & session("Forex")) > 0 then
                    sMsg = sMsg & " (" & getPriceForexHTMLemail(aOrder(OD_PROMODISC,0), 4) & ")"
                end if
                sMsg = sMsg & vbcrlf  
			end if
			'response.Write "<br> nix OD_TOTALCHARGECALC: " & aOrder(OD_TOTALCHARGECALC,0)
			'response.Write "<br> nix OD_POSTAGE: " & aOrder(OD_POSTAGE,0)
			'response.Write "<br> nix OD_VAT: " & aOrder(OD_VAT,0)
			
			sMsg = sMsg & "ORDER VALUE " & ECOM_CUR_ISO_POUND & formatnumber(aOrder(OD_TOTALCHARGECALC,0) - (aOrder(OD_POSTAGE,0) + nVatTotal),2)
			' // Show corresponding forex if other that Uk pounds
            if len("" & session("Forex")) > 0 then
                sMsg = sMsg & " (" & getPriceForexHTMLemail(aOrder(OD_TOTALCHARGECALC,0) - (aOrder(OD_POSTAGE,0) + nVatTotal), 2) & ")"
            end if
            sMsg = sMsg & vbcrlf  
			
			'sMsg = sMsg & "POST & PACKAGING: " & ECOM_CUR_ISO_POUND & formatnumber(aOrder(OD_POSTAGE,0),2) 
			' // Show corresponding forex if other that Uk pounds
            if len("" & session("Forex")) > 0 then
                sMsg = sMsg & " (" & getPriceForexHTMLemail(aOrder(OD_POSTAGE,0), 2) & ")"
            end if
            sMsg = sMsg & vbcrlf 
			
			sMsg = sMsg & "VAT: " & ECOM_CUR_ISO_POUND & nVatTotal 
			' // Show corresponding forex if other that Uk pounds
            if len("" & session("Forex")) > 0 then
                sMsg = sMsg & " (" & getPriceForexHTMLemail(nVatTotal, 2) & ")"
            end if
            sMsg = sMsg & vbcrlf 
			
			
			if len(session("promocode")) > 0 then
                sMsg = sMsg & "TOTAL INVOICE VALUE: " & ECOM_CUR_ISO_POUND & FormatNumber(aOrder(OD_TOTALCHARGECALC,0) - aOrder(OD_PROMODISC,0), 2) 
                ' // Show corresponding forex if other that Uk pounds
                if len("" & session("Forex")) > 0 then
                    sMsg = sMsg & " (" & getPriceForexHTMLemail(aOrder(OD_TOTALCHARGECALC,0) - aOrder(OD_PROMODISC,0), 2) & ")"
                end if
                sMsg = sMsg & vbcrlf 
			else
			    sMsg = sMsg & "TOTAL INVOICE VALUE: " & ECOM_CUR_ISO_POUND & FormatNumber(aOrder(OD_TOTALCHARGECALC,0),2) 
			    ' // Show corresponding forex if other that Uk pounds
                if len("" & session("Forex")) > 0 then
                    sMsg = sMsg & " (" & getPriceForexHTMLemail(aOrder(OD_TOTALCHARGECALC,0), 2) & ")"
                end if
                sMsg = sMsg & vbcrlf 
			end if
			sMsg = sMsg & vbcrlf
           
            sMsg = sMsg & "DELIVER TO:  " & vbcrlf
            sMsg = sMsg & sDeliveryAdd & vbcrlf
            sMsg = sMsg & vbcrlf
      
			sMsg = sMsg & "ADDITIONAL INFORMATION:" & vbcrlf
			sMsg = sMsg & vbcrlf
			sMsg = sMsg & "MESSAGE TO RECIPIENT FROM CUSTOMER:" & vbcrlf
			sMsg = sMsg & sOrderMessage & vbcrlf
			sMsg = sMsg & vbcrlf
			sMsg = sMsg & "DELIVERY INSTRUCTIONS FROM CUSTOMER:" & vbcrlf
			sMsg = sMsg & sOrderInstruct & vbcrlf
			sMsg = sMsg & vbcrlf
			if aMember(ME_CONTACTBYUS, 0) then
				sMsg = sMsg & "MAY WE CONTACT THIS CUSTOMER:  Yes" & vbcrlf
			else
				sMsg = sMsg & "MAY WE CONTACT THIS CUSTOMER:  No" & vbcrlf
			end if
			sMsg = sMsg & vbcrlf
          if POST_OPTS > 0 then
		        if aOrder(OD_POSTTYPE,0) = "24H" then
			        sMsg = sMsg & "SPECIAL DELIVERY:  Yes" & vbcrlf
		        else
			        sMsg = sMsg & "SPECIAL DELIVERY:  No" & vbcrlf
		        end if
          end if
			if aOrder(OD_PARTSHIP,0) then
				sMsg = sMsg & "PARTIAL SHIPMENT ALLOWED:  Yes" & vbcrlf
			else
				sMsg = sMsg & "PARTIAL SHIPMENT ALLOWED:  No" & vbcrlf
			end if
			sMsg = sMsg & vbcrlf   
      
            sTo = EML_SHOPTOEMAIL_ECOMM
			sToName = EML_SHOPTONAME
			sFrom = EML_SHOPFROMEMAIL_ECOMM
			sFromName = EML_SHOPFROMNAME
			
			' Live / pre-live  / dev env?
            if IsEnvironmentLive(sHostHeader) then
                 sSubjectWarning = ""                 
            else
                if IsEnvironmentPreLive(sHostHeader) then
                     sSubjectWarning = "Please ignore (pre-live test): "
                else
                    ' dev env    
                    if IsEnvironmentDev(sHostHeader) then
                       sSubjectWarning = "Please ignore (dev test): "
                    else
                       sSubjectWarning = "(unknown env: "
                    end if
                end if 
            end if             
           
            sSubject = sSubjectWarning & SHOP_NAME & " Order (Order ref: " & aOrder(OD_ID,0) & ")"
			nErr = SendMailEnc(sTo, sToName, sFrom, sFromName, sSubject, sMsg, "", "", false, EMAIL_DEBUG)
		else
			nErr = 2
		end if
	else
		nErr = 1
end if
	notifyShopCCOrderPlaced = nErr
end function


function notifyCustomerOrderConfirmed(aOrder, aMember, aContents)
	dim nErr:nErr = 0
	dim sTo, sToName, sFrom, sFromName, sSubject, sSubjectWarning
	dim nPrice, nVATLineItem, nVatTotal     
	dim sChargeAdd,sDeliveryAdd,sCustName,sOrderDate,sOrderTime,sOrderMessage,sOrderInstruct,aTmp,i, sProdDesc
	dim sMsg:sMsg = ""
	if (not isNull(aOrder)) and (isArray(aOrder)) then
		if not isNull(aContents) then
			aTmp = split(aOrder(OD_DATE, 0), " ")
			sOrderDate	 = aTmp(0)
			sOrderTime	 = aTmp(1)
			if UBound(aTmp) = 2 then
				'// also have a PM to add //
				sOrderTime = sOrderTime & " " & aTmp(2)
			end if
			if len("" & aOrder(OD_FIRSTNAME,0)) > 0 then
				sCustName	 = aOrder(OD_FIRSTNAME,0)
			else
				sCustName	 = replace(trim(aOrder(OD_TITLE,0) & " " & aOrder(OD_FIRSTNAME,0) & " " & aOrder(OD_SURNAME,0)), "  ", " ")
			end if
			sChargeAdd	 = makeAddress(aOrder(OD_TITLE,0),aOrder(OD_FIRSTNAME,0),aOrder(OD_SURNAME,0) _
									  ,aOrder(OD_ADDRESS1,0),aOrder(OD_ADDRESS2,0),aOrder(OD_ADDRESS3,0) _
									  ,aOrder(OD_ADDRESS4,0),getCountry(aOrder(OD_ADDRESS5,0)),aOrder(OD_POSTCODE,0))
			sDeliveryAdd = makeAddress(aOrder(OD_DELTITLE,0),aOrder(OD_DELFIRSTNAME,0),aOrder(OD_DELSURNAME,0) _
									  ,aOrder(OD_DELADDRESS1,0),aOrder(OD_DELADDRESS2,0),aOrder(OD_DELADDRESS3,0) _
									  ,aOrder(OD_DELADDRESS4,0),getCountry(aOrder(OD_DELADDRESS5,0)),aOrder(OD_DELPOSTCODE,0))
			if aOrder(OD_KEYFNID,0) > 0 then
				sOrderMessage = getNote(aOrder(OD_KEYFNID,0))
			end if
			if aOrder(OD_KEYFN2ID,0) > 0 then
				sOrderInstruct = getNote(aOrder(OD_KEYFN2ID,0))
			end if
			
			'// Determine if vat is charged vat //
            if aMember(ME_USEALTADDRESS, 0) then
                bVAT = (mid(aMember(ME_ALTADDRESS5, 0), 6, 1))
            else
                bVAT = (mid(aMember(ME_ADDRESS5, 0), 6, 1))
            end if 	
			
			
            sMsg = sMsg & "Dear " & sCustName & "," & vbcrlf
            sMsg = sMsg & vbcrlf
            sMsg = sMsg & "Thank you for your order which will be processed as soon as possible. Your" _
                        & " order details are listed below - if there are any changes you need to make," _
                          & " please contact us without delay either by replying to this email or by" _
                          & " phoning us on  " & SHOP_TEL_NO & "." & vbcrlf
            sMsg = sMsg & vbcrlf & vbcrlf
            sMsg = sMsg & UPLOAD_IMAGE_REMINDER_RECEIPT_EMAIL 
            sMsg = sMsg & vbcrlf & vbcrlf
            sMsg = sMsg & "Your order details are as follows:" & vbcrlf
            sMsg = sMsg & vbcrlf
            
            sMsg = sMsg & "ORDER DATE: " 
	        sMsg = sMsg & sOrderDate & " " & sOrderTime & vbcrlf
            
            if (len("" & session("trader")) > 0) and (session("trader") = "PROFORMA") then
	            sMsg = sMsg & "PAYMENT BY PROFORMA INVOICE METHOD:  " & vbcrlf
	        else
	            sMsg = sMsg & "PAYMENT BY PROTX:  " & vbcrlf
	        end if  
	        
            '// Gift Aid Notification
            if len(session("giftAid")) > 0 then
                sMsg = sMsg & vbcrlf & "I am a UK taxpayer and would like the " & SHOP_NAME & " to reclaim tax on all donations. "  & session("giftAid") & vbcrlf & vbcrlf
            end if
	        
            sMsg = sMsg & vbcrlf
            sMsg = sMsg & "ONLINE ORDER REF: "
            sMsg = sMsg & aOrder(OD_ID, 0) & vbcrlf
                sMsg = sMsg & vbcrlf
                sMsg = sMsg & "FROM:  " & vbcrlf
            sMsg = sMsg & sChargeAdd & vbcrlf
            sMsg = sMsg & vbcrlf

                sMsg = sMsg & "EMAIL: "
            sMsg = sMsg & aOrder(OD_EMAIL, 0) & vbcrlf
            sMsg = sMsg & vbcrlf

                sMsg = sMsg & "TEL NO: "
            sMsg = sMsg & aOrder(OD_PHONE, 0) & vbcrlf
            sMsg = sMsg & vbcrlf

            sMsg = sMsg & "PRODUCT DETAILS:  " & vbcrlf
            '// loop items //
			for i = 0 to UBound(aContents,2)
				sProdDesc = aContents(OI_DESCRIPTION, i)
				if aContents(OI_KEYFN1ID, i) > 0 then
					sProdDesc = sProdDesc & " - " & getNote(aContents(OI_KEYFN1ID, i))
				end if				
				
				' // If bVAT is true, then the customer is being charged vat
				nVATLineItem    = aContents(OI_UNITVAT,i)
                if bVAT then
                    nPrice      = formatnumber(aContents(OI_UNITCOST,i),2)
                    nVATLineItem= formatnumber(nVATLineItem, 4)
                    nVatTotal   = formatnumber(aOrder(OD_VAT,0),2)
                else
                    nPrice      = formatnumber((aContents(OI_UNITCOST,i) - nVATLineItem),2)
                    nVATLineItem= ccur(0)
                    nVatTotal   = ccur(0)
                end if  
                
                sMsg = sMsg & "ITEM CODE: " & aContents(OI_PRODUCTCODE, i) & vbcrlf
                sMsg = sMsg & "ITEM DESCRIPTION: " & replace(replace(replace(replace(replace(replace(sProdDesc,"<b>",""),"</b>",""),"<strong>",""),"</strong>",""),"<i>",""),"</i>","") & vbcrlf
     
                sMsg = sMsg & "PRICE: " & ECOM_CUR_ISO_POUND & nPrice & vbcrlf
                ' // Show corresponding forex if other that Uk pounds
                if len("" & session("Forex")) > 0 then
                    sMsg = sMsg & " (" & getPriceForexHTMLemail(nPrice, 2) & ")"
                end if
                sMsg = sMsg & vbcrlf
                                
                sMsg = sMsg & "QUANTITY: " & aContents(OI_QUANTITY, i) & vbcrlf
                
                sMsg = sMsg & "LINE VALUE: " & ECOM_CUR_ISO_POUND & formatnumber((nPrice * aContents(OI_QUANTITY, i)),2) 
                ' // Show corresponding forex if other that Uk pounds
                if len("" & session("Forex")) > 0 then
                    sMsg = sMsg & " (" & getPriceForexHTMLemail((nPrice * aContents(OI_QUANTITY, i)), 2) & ")"
                end if
                sMsg = sMsg & vbcrlf
                sMsg = sMsg & vbcrlf
            next
            
            if len(session("promocode")) > 0 then
		  		sMsg = sMsg & "DISCOUNT: " & ECOM_CUR_ISO_POUND & formatnumber(aOrder(OD_PROMODISC,0), 4) 
		  		' // Show corresponding forex if other that Uk pounds
                if len("" & session("Forex")) > 0 then
                    sMsg = sMsg & " (" & getPriceForexHTMLemail(aOrder(OD_PROMODISC,0), 4) & ")"
                end if
                sMsg = sMsg & vbcrlf 
			end if
			
            sMsg = sMsg & "ORDER VALUE " & ECOM_CUR_ISO_POUND & FormatNumber(aOrder(OD_TOTALCHARGECALC,0) - (aOrder(OD_POSTAGE,0) + nVatTotal),2) 
            ' // Show corresponding forex if other that Uk pounds
            if len("" & session("Forex")) > 0 then
                sMsg = sMsg & " (" & getPriceForexHTMLemail(aOrder(OD_TOTALCHARGECALC,0) - (aOrder(OD_POSTAGE,0) + nVatTotal), 2) & ")"
            end if
            sMsg = sMsg & vbcrlf  
            
            'sMsg = sMsg & "POST & PACKAGING: " & ECOM_CUR_ISO_POUND & FormatNumber(aOrder(OD_POSTAGE,0),2) 
            ' // Show corresponding forex if other that Uk pounds
            if len("" & session("Forex")) > 0 then
                sMsg = sMsg & " (" & getPriceForexHTMLemail(aOrder(OD_POSTAGE,0), 2) & ")"
            end if
            sMsg = sMsg & vbcrlf 
            
            sMsg = sMsg & "VAT: " & ECOM_CUR_ISO_POUND & nVatTotal 
            ' // Show corresponding forex if other that Uk pounds
            if len("" & session("Forex")) > 0 then
                sMsg = sMsg & " (" & getPriceForexHTMLemail(nVatTotal, 2) & ")"
            end if
            sMsg = sMsg & vbcrlf 
                       
            if len(session("promocode")) > 0 then
                sMsg = sMsg & "TOTAL INVOICE VALUE: " & ECOM_CUR_ISO_POUND & FormatNumber(aOrder(OD_TOTALCHARGECALC,0) - aOrder(OD_PROMODISC,0), 2) 
                ' // Show corresponding forex if other that Uk pounds
                if len("" & session("Forex")) > 0 then
                    sMsg = sMsg & " (" & getPriceForexHTMLemail(aOrder(OD_TOTALCHARGECALC,0) - aOrder(OD_PROMODISC,0), 2) & ")"
                end if
                sMsg = sMsg & vbcrlf 
			else
			    sMsg = sMsg & "TOTAL INVOICE VALUE: " & ECOM_CUR_ISO_POUND & FormatNumber(aOrder(OD_TOTALCHARGECALC,0),2) 
			    ' // Show corresponding forex if other that Uk pounds
                if len("" & session("Forex")) > 0 then
                    sMsg = sMsg & " (" & getPriceForexHTMLemail(aOrder(OD_TOTALCHARGECALC,0), 2) & ")"
                end if
                sMsg = sMsg & vbcrlf 
			end if
			sMsg = sMsg & vbcrlf

            sMsg = sMsg & "DELIVER TO:  " & vbcrlf
            sMsg = sMsg & sDeliveryAdd & vbcrlf
            sMsg = sMsg & vbcrlf
      
			if len("" & sOrderMessage) then
				sMsg = sMsg & "Your items will be sent with the following gift message:" & vbcrlf
				sMsg = sMsg & sOrderMessage &  vbcrlf
				sMsg = sMsg & vbcrlf
			end if
			if len("" & sOrderInstruct) then
				sMsg = sMsg & "Your items will be sent with the following delivery instructions:" & vbcrlf
				sMsg = sMsg & sOrderInstruct &  vbcrlf
				sMsg = sMsg & vbcrlf
			end if
			sMsg = sMsg & vbcrlf & vbcrlf
            sMsg = sMsg & UPLOAD_IMAGE_REMINDER_RECEIPT_EMAIL 
            sMsg = sMsg & vbcrlf & vbcrlf
            sMsg = sMsg & "Thank you for shopping at " & SHOP_NAME & "." & vbcrlf & vbcrlf
            sMsg = sMsg & SHOP_NAME & vbcrlf
            sMsg = sMsg & "[W]: " & SITE_URL & vbcrlf
            sMsg = sMsg & "[E]: " & EML_SHOPTOEMAIL_ECOMM & vbcrlf
            sMsg = sMsg & "[T]: " & SHOP_TEL_NO & vbcrlf
            sMsg = sMsg & vbcrlf
            
            sTo = aOrder(OD_EMAIL,0)
            sToName = trim("" & aOrder(OD_TITLE,0) & " " & aOrder(OD_SURNAME,0) & " " & aOrder(OD_SURNAME,0))
            sFrom = EML_SHOPFROMEMAIL_ECOMM
            sFromName = EML_SHOPFROMNAME
            
            ' Live / pre-live  / dev env?
            if IsEnvironmentLive(sHostHeader) then
                 sSubjectWarning = ""                 
            else
                if IsEnvironmentPreLive(sHostHeader) then
                     sSubjectWarning = "Please ignore (pre-live test): "
                else
                    ' dev env    
                    if IsEnvironmentDev(sHostHeader) then
                       sSubjectWarning = "Please ignore (dev test): "
                    else
                       sSubjectWarning = "(unknown env: "
                    end if
                end if 
            end if             
           
            if (len("" & session("trader")) > 0) then
                sSubject = sSubjectWarning & SHOP_NAME & " Trader Order Confirmation (Order ref: " & aOrder(OD_ID,0) & ")"
            else
                sSubject = sSubjectWarning & SHOP_NAME & " Order Confirmation (Order ref: " & aOrder(OD_ID,0) & ")"
            end if
            nErr = SendMailEnc(sTo, sToName, sFrom, sFromName, sSubject, sMsg, "", "", false, EMAIL_DEBUG)
		else
			nErr = 2
		end if
	else
		nErr = 1
	end if
	notifyCustomerOrderConfirmed = nErr
end function

%>