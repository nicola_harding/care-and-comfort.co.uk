<!--#include virtual="/includes/careandcomfortAPI.asp"-->
<!--#include virtual="/ecomm/setup.asp"-->
<!--#include virtual="/ecomm/countryselector.asp"-->
<!--#include virtual="/includes/dates.asp" -->
<!--#include virtual="/cms/setup.asp"-->

<%
dim id  
dim nErr        : nErr=0
dim nOdID       : nOdID=0
dim nMeID       : nMeID=0
dim sHeader     : sHeader=""
dim aOrder      : aOrder=null
dim aOrderItems : aOrderItems=null
dim bSuccess    : bSuccess=false
dim sTraderProForma : sTraderProForma = ""

nOdID = unencodeID("" & request.querystring("e"))
'// build the receipt
if nOdID > 0 then
    nErr = getMemberFromOrderID(nOdID,aMember)   
    
    ' [hardin] logic below was looking at bVat but not setting it. Added this logic as seen in options and confirm steps
    if nErr > 0 then
        response.redirect "./error.asp"
    else
        '// Aid display of the applied vat for this order //
        if aMember(ME_USEALTADDRESS, 0) then
            bVAT = (mid(aMember(ME_ALTADDRESS5, 0), 6, 1) = "1")
        else
            bVAT = (mid(aMember(ME_ADDRESS5, 0), 6, 1) = "1")
        end if
        
        nMeID           = aMember(ME_ID, 0)
        session("MeID") = nMeID
    end if    
    
    nErr = loadOrder(nOdID, aOrder)
    if nErr = 0 then
        ' -------------------------
        if (len("" & session("trader")) > 0) and (cstr(aOrder(OD_TRANSID, 0)) = "N/A PROFORMA") then
            bSuccess = true
            sTraderProForma = " - Proforma invoicing applicable"
        else
            bSuccess = aOrder(OD_COMPLETEDALL, 0)
        end if
        ' -------------------------
    else
        ' // DO NOTHING? HUH
    end if
else
	'// the shopping basket is currently empty //
end if
%>

<!--#include virtual="/includes/doctype.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <%=GetDefaultEcommMetaTags("Print receipt " & SHOP_NAME)%>  
    <!--#include virtual="/includes/mainCSS.asp" -->
	 <!--#include virtual="/includes/javascript.asp" -->

	 
    <%=javaSetDropDown("registration")%>
</head>

<!--#include virtual="/includes/templateStart.asp" -->
<!--#include virtual="/includes/templateTopNav.asp" -->
<!--#include virtual="/includes/templateLeftNav.asp" -->


<!--#include virtual="/includes/templateBeginMainContent.asp" -->
<!-- Begin Content -->   
    <h2>
    <%
    if bSuccess then
        response.write "Your Receipt" & sTraderProForma
    else
        response.write "Payment Cancelled"
    end if
    %>
    </h2>
    <hr> 
    <%=showOrder(nOdId, aMember, BSKT_HDR_CLR_PRINT, BSKT_ROW_CLR, BSKT_BGD_CLR)%>

    <p><%=SHOP_NAME%> order number: <%=aOrder(OD_ID,0)%></p>
    <p><%=SHOP_NAME%> member number: <%=aOrder(OD_KEYFMEID,0)%></p>
    <p>Order Date: <%=aOrder(OD_DATE,0)%></p>  
    <hr>              
<!-- End Content -->
<!--#include virtual="/includes/templateEndMainContent.asp" -->


<!--#include virtual="/includes/templateRightColumn.asp" -->


<!--#include virtual="/includes/templateFooter.asp" -->
<!--#include virtual="/includes/templateEnd.asp" -->
<%
function getMemberFromOrderID(nOdID,aMember)
  dim nErr:nErr=0
  dim sSQL:sSQL = ""
  if nOdID > 0 then
    sSQL = "SELECT ME.* FROM tblMember AS ME LEFT JOIN tblOrder AS OD ON OD.keyFmeID=ME.meID" _
        & " WHERE OD.odID=" & nOdID & " AND OD.keyFinID=" & INST_ID & " AND ME.keyFinID=" & INST_ID & ";"
    nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aMember)
  else
    nErr = 1  '// bad input parms //
  end if
  getMemberFromOrderID = nErr
end function

function showOrder(nOdID, aMember, sHedClr, sRowClr, sBkgClr)
  dim nErr:nErr=0
  dim sTmp:sTmp=""
  dim aItems:aItems=null
  dim nIdx:nIdx=0
  dim sSQL:sSQL=""
  dim nTotal, nSubTotal, nVat, nTotalVAT, ntempTotal
  dim sName, sLeadTime, nPrice, nQty
  nIdx = 0
  sTmp = ""
  dim sMbrAdd, sDelAdd
  if nOdID > 0 then
    sSQL = "SELECT OD.*,OI.* FROM tblOrder AS OD LEFT JOIN tblOrderItem AS OI ON OD.odID=OI.keyFodID" _
        & " WHERE OD.odID=" & nOdID & " AND OD.keyFinID= "& INST_ID & " AND OI.keyFinID=" & INST_ID & " ORDER BY OI.oiID ASC;"
    nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aItems)
    if nErr = 0 then
%>
    
    <table width="100%" border="0" bgcolor="<%=sBkgClr%>" cellspacing="0" cellpadding="0" >
    <tr bgcolor="<%=sHedClr%>" class="cartHeadersPrint">
    <td>&nbsp;</td>
    <td>Description</td>
    <td>&nbsp;</td>
    <td align="center">Quantity</td>
    <td>&nbsp;</td>
    <td align="right">Price</td>
    <td>&nbsp;</td>
    <td align="right">Sub Total</td>
    <td>&nbsp;</td></tr>
<%
for nIdx=0 to UBound(aItems, 2)
%>
    <tr bgcolor="<%=sRowClr%>">
    <td>&nbsp;</td>
    <td valign="top"><%=aItems(OD_UBOUND + 1 + OI_DESCRIPTION, nIdx)%><br><small>Delivery - <%=aItems(OD_UBOUND + 1 + OI_LEADTIMETEXT, nIdx)%></small></td>
    <td>&nbsp;</td>
    <td valign="top" align="center"><%=aItems(OD_UBOUND + 1 + OI_QUANTITY, nIdx)%></td>
    <td>&nbsp;</td>
    <td valign="top" align="right"><%=getPriceForexHTML(aItems(OD_UBOUND + 1 + OI_UNITCOST, nIdx), 2)%></td>
    <td>&nbsp;</td>
    <td valign="top" align="right"><%=getPriceForexHTML(aItems(OD_UBOUND + 1 + OI_UNITCOST, nIdx) * aItems(OD_UBOUND + 1 + OI_QUANTITY, nIdx), 2)%></td>
    <td>&nbsp;</td></tr>
<%next%>
<tr bgcolor="<%=sHedClr%>" class="cartHeadersPrint">
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td align="right">Sub&nbsp;Total</td>
<td>&nbsp;</td>
<td align="right"><%=getPriceForexHTML(aItems(OD_TOTALNOVAT, 0) + aItems(OD_VAT, 0), 2)%></td>
<td>&nbsp;</td></tr>

<%
' // Old logic from hen and mahhock: if 1 or bVAT or (VAT_OPTS = 0) then
if bVAT then
%>
    <tr bgcolor="<%=sRowClr%>">
    <td>&nbsp;</td>
    <td colspan="3">VAT Included</td>
    <td>&nbsp;</td>
    <td align="right">(<%=getPriceForexHTML(aItems(OD_VAT, 0), 2)%>)</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>
<%
else
%>
    <tr bgcolor="<%=sRowClr%>">
    <td>&nbsp;</td>
    <td colspan="3">VAT</td>
    <td>&nbsp;</td>
    <td align="right">-<%=getPriceForexHTML(aItems(OD_VAT, 0), 2)%></td>
    <td>&nbsp;</td>
    <td align="right">-<%=getPriceForexHTML(aItems(OD_VAT, 0), 2)%></td>
    <td>&nbsp;</td>
</tr>
<%end if%>
<%IF False Then%>
<tr>
    <td>&nbsp;</td>
    <td colspan="3">Postage &amp; Packaging</td>
    <td>&nbsp;</td>
    <td align="right"><%=getPriceForexHTML(aItems(OD_POSTAGE, 0), 2)%></td>
    <td>&nbsp;</td>
    <td align="right"><%=getPriceForexHTML(aItems(OD_POSTAGE, 0), 2)%></td>
    <td>&nbsp;</td>
</tr>

<%
End IF
if len("" & session("promocode")) > 0 and (session("promocode") <> "") then
    ntempTotal          = formatnumber(aItems(OD_TOTALNOVAT, 0) + aItems(OD_VAT, 0), 4)
    nDiscountedTotal    = formatnumber((ntempTotal - aItems(OD_PROMODISC, 0)), 4)
    nDiscountedTotal    = formatnumber(nDiscountedTotal + aItems(OD_POSTAGE, 0), 4)
%>
    <tr bgcolor="<%=sRowClr%>">
    <td>&nbsp;</td>
    <td>Discount</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">-<%=getPriceForexHTML(aItems(OD_PROMODISC, 0), 3)%></td>
    <td>&nbsp;</td></tr>

    <tr bgcolor="<%=sHedClr%>" class="cartHeadersPrint">
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">Total</td>
    <td>&nbsp;</td>
    <td align="right"><%=getPriceForexHTML(nDiscountedTotal, 2)%></td>
    <td>&nbsp;</td></tr>
<%else%>
    <tr bgcolor="<%=sHedClr%>" class="cartHeadersPrint">
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">Total</td>
    <td>&nbsp;</td>
    <td align="right"><%=getPriceForexHTML(aItems(OD_TOTALCHARGECALC, 0), 2)%></td>
    <td>&nbsp;</td></tr>
<%end if%>
</table>

<%
sMbrAdd=""
sDelAdd=""

sMbrAdd = sMbrAdd & "&nbsp;" & trim("" & aOrder(OD_TITLE, 0) & " " & aOrder(OD_FIRSTNAME, 0) & " " & aOrder(OD_SURNAME, 0)) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aOrder(OD_ADDRESS1, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aOrder(OD_ADDRESS2, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aOrder(OD_ADDRESS3, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aOrder(OD_ADDRESS4, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aOrder(OD_POSTCODE, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & getCountry(aOrder(OD_ADDRESS5, 0)) & "<br />" & vbcrlf

sDelAdd = sDelAdd & "&nbsp;" & trim("" & aOrder(OD_DELTITLE, 0) & " " & aOrder(OD_DELFIRSTNAME, 0) & " " & aOrder(OD_DELSURNAME, 0)) & "<br />" & vbcrlf
sDelAdd = sDelAdd & "&nbsp;" & aOrder(OD_DELADDRESS1, 0) & "<br />" & vbcrlf
sDelAdd = sDelAdd & "&nbsp;" & aOrder(OD_DELADDRESS2, 0) & "<br />" & vbcrlf
sDelAdd = sDelAdd & "&nbsp;" & aOrder(OD_DELADDRESS3, 0) & "<br />" & vbcrlf
sDelAdd = sDelAdd & "&nbsp;" & aOrder(OD_DELADDRESS4, 0) & "<br />" & vbcrlf
sDelAdd = sDelAdd & "&nbsp;" & aOrder(OD_DELPOSTCODE, 0) & "<br />" & vbcrlf
sDelAdd = sDelAdd & "&nbsp;" & getCountry(aOrder(OD_DELADDRESS5, 0)) & "<br />" & vbcrlf

sMbrAdd = replace(sMbrAdd, "&nbsp;<br />", "")
sDelAdd = replace(sDelAdd, "&nbsp;<br />", "")
%>

<table align="center" width="100%" border="0" bgcolor="<%=sBkgClr%>" cellspacing="0" cellpadding="0" class="text">
    <tr><td width="45%">&nbsp;</td><td width="10%">&nbsp;</td><td width="45%">&nbsp;</td></tr>
    <tr>
        <td bgcolor="<%=sHedClr%>" class="cartHeadersPrint">&nbsp;Billing Address&nbsp;</td>
        <td >&nbsp;</td>
        <td bgcolor="<%=sHedClr%>" class="cartHeadersPrint">&nbsp;Delivery Address&nbsp;</td>
    </tr>
    <tr bgcolor="<%=sRowClr%>">
        <td valign="top">
            <p><%=sMbrAdd%></p>
        </td>
        <td>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td valign="top">
            <p><%=sDelAdd%></p>
        </td>
    </tr>
    <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
</table>

<%
dim sPostOpt, nNoteID, sGiftMsg, nNote2ID, sDelMsg, bPartShip
'nErr = getPostOptions(nCtID, sPostOpt, nNoteID, sGiftMsg, nNote2ID, sDelMsg, bPartShip)
if nErr = 0 then
    'sTmp = sTmp & "<table width=""100%"" border=""0"" bgcolor=""" & sBkgClr & """ cellpadding=""0"" cellspacing=""0"" class=""text"">"
    'sTmp = sTmp & "<tr><td bgcolor=""" & sHedClr & """ class=""shopping_title"">&nbsp;Order Options&nbsp;</td></tr>"
    'if bPartShip then
    '    sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;Order items may be shipped seperately,&nbsp;</td></tr>"
    'else
    '    sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;Order items will be shipped as a single parcel,&nbsp;</td></tr>"
    'end if
    'if len("" & sPostOpt) = 0 then
    '    sPostOpt = request.querystring("postOpt")
    'end if
    'if sPostOpt = "24H" then
    '    sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;via special delivery.&nbsp;</td></tr>"
    'else
    '    sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;via standard delivery.&nbsp;</td></tr>"
    'end if
    
     if len(sDelMsg) > 0 then
        sTmp = sTmp & "<table width=""100%"" border=""0"" bgcolor=""" & sBkgClr & """ cellpadding=""2"" cellspacing=""0"" class=""text"">"
        sTmp = sTmp & "<tr><td bgcolor=""" & sHedClr & """ class=""cartHeaders"">&nbsp;Order Options&nbsp;</td></tr>"
        sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;Your order will contain the following delivery instruction:&nbsp;</td></tr>"
        sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td><i>" & replace(sDelMsg, vbcrlf, "&nbsp;<br />") & "</i></td></tr>"
    end if
    
    'if len(sGiftMsg) > 0 then
    '    sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;Your order will contain the following gift message:&nbsp;</td></tr>"
    '    sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td><i>" & replace(sGiftMsg, vbcrlf, "&nbsp;<br />") & "</i></td></tr>"
    'end if
    sTmp = sTmp & "<td>&nbsp;</td></tr></table>"
end if
response.write sTmp
sTmp = ""

    else
      sTmp = "&nbsp;<br />No details to show."
    end if
  else
    sTmp = "&nbsp;<br />No details to show."
  end if
  showOrder = sTmp
end function
%>