<!--#include virtual="/ecomm/setup.asp"-->
<%
dim nErr:nErr=0
dim sOut:sOut=""
dim sTo, sToName, sFrom, sFromName, sSubject, sMsg, sFile1, sFile2, bEncrypted, bDebug

if request.form("submitted") = "yes" then
  sTo = trim("" & request.form("emailAdd"))
  if len("" & sTo) = 0 then
    sTo = "dru.moore@nvisage.co.uk"
  end if
  sToName = "dru"
  sFrom = "dru.moore@nvisage.co.uk"
  sFromName = "dru"
  sFile1 = ""
  sFile2 = ""
  bDebug = false
  if request.form("enc") = "encrypt" then
    sSubject = "Test encrypted"
    sMsg = "Test encrypted"
    bEncrypted = true
  elseif request.form("std") = "standard" then
    sSubject = "Test unencrypted"
    sMsg = "Test unencrypted"
    bEncrypted = false
  end if
  nErr = SendMailEnc(sTo, sToName, sFrom, sFromName, sSubject, sMsg, sFile1, sFile2, bEncrypted, bDebug)
  if nErr = 0 then
    sOut = "Email Successfully sent.<br />" & "<br>" & EMAIL_CERT_PATH
  else
    sOut = "Email not sent. Error code=" & nErr & ".<br />" & "<br>" & EMAIL_CERT_PATH
  end if
end if

%>
<html>
<head>
<title>Email Test</title>
</head>
<body>
<%=sOut%>
<form name="testEmail" action="./testemail.asp" method="POST">
<input type="hidden" name="submitted" value="yes" />
<input type="text" name="emailAdd" value="" size="32" />
<br />
<input type="submit" name="enc" value="encrypt" />
&nbsp;&nbsp;&nbsp;&nbsp;
<input type="submit" name="std" value="standard" />
</form>
</body>
</html>
