<!--#include virtual="/ecomm/setup.asp"-->
<!--#include virtual="/ecomm/countryselector.asp"-->
<!-- #include virtual="/cms300scripts/ektronAPI.asp" -->
<%
  Dim g_sFldr:g_sFldr="shop"
	Dim id, LHSid
  LHSid=14
  
dim nErr:nErr=0
dim nMeID, nCtID, nOdID
dim aMember:aMember=null
dim aOrder:aOrder=null

nMeID = CLng("0" & session("meID"))
nCtID = CLng("0" & session("ctID"))
nOdID = CLng("0" & session("odID"))
if (nOdID = 0) OR (nMeID = 0) then
	response.redirect "./error.asp?err=Session%20expired"
end if

dim nTotalNoVat,nPostage,nVat,nTotalChargeCalc, bVat:bVat=true
dim sCCName, sCCOrg, sCCAddress, sCCInvoice
', sCCType, sCCNo, sCCExpMM, sCCExpYY, sCCIssMM, sCCIssYY, sCCIssNo, sCCChkNo
dim dtTmp, dtNow, dtNowMM, dtNowYY, sMsg

nErr = loadMember(nMeID, aMember)
if nErr = 0 then
  if VAT_OPTS > 0 then
    if aMember(ME_USEALTADDRESS, 0) then
      bVAT = (mid(aMember(ME_ALTADDRESS5, 0), 6, 1) = "1")
    else
      bVAT = (mid(aMember(ME_ADDRESS5, 0), 6, 1) = "1")
    end if
  end if
  sCCName = trim("" & aMember(ME_TITLE, 0) & " " & aMember(ME_FIRSTNAME, 0) & " " & aMember(ME_SURNAME, 0))
  sCCOrg = trim("" & aMember(ME_COMPANY, 0))
  sCCAddress = aMember(ME_ADDRESS1, 0) & vbcrlf _
             & aMember(ME_ADDRESS2, 0) & vbcrlf _
             & aMember(ME_ADDRESS3, 0) & vbcrlf _
             & aMember(ME_ADDRESS4, 0) & vbcrlf _
             & aMember(ME_ADDRESS5, 0) & vbcrlf _
             & aMember(ME_POSTCODE, 0)
end if

nErr = loadOrder(nOdID, aOrder)
if nErr = 0 then
	nTotalNoVat = aOrder(OD_TOTALNOVAT, 0)
	nVat = aOrder(OD_VAT, 0)
	nPostage = aOrder(OD_POSTAGE, 0)
  if bVAT then
	  nTotalChargeCalc = nTotalNoVat + nVat + nPostage
  else
	  nTotalChargeCalc = nTotalNoVat + nPostage
  end if
else
	'// what now? no order details to show I guess //
	response.redirect "./error.asp?err=Session%20expired"
end if

if len("" & request.form("cancel.x")) > 0 then
  '// clear the cart //
  'session("OdID") = 0
  'session("CtID") = 0
  response.redirect("./receipt.asp?PAY=PRO&e=" & encodeID(nOdID))
elseif request.form("submitted") = "yes" then
	'// process the credit card stuff and send them on to receipt.asp //
  sCCName= Trim("" & request.form("ccName"))
  sCCOrg= Trim("" & request.form("ccOrg"))
  sCCAddress= Trim("" & request.form("ccAddress"))
  'sCCInvoice= Trim("" & request.form("ccInvoice"))
  
	'// okay we have all of the bits of the form we need just need to do some validation //
	'// do we have a card type? //
	if len("" & sCCName) < 3 then
		nErr = nErr + 1
		sMsg = sMsg & "Please enter the invoice name.<br />"
	end if
	if len("" & sCCAddress) < 12 then
		nErr = nErr + 1
		sMsg = sMsg & "Please enter the invoice address.<br />"
	end if
	'if len("" & sCCInvoice) < 3 then
	'	nErr = nErr + 1
	'	sMsg = sMsg & "Please enter the invoice number.<br />"
	'end if
  
	if nErr = 0 then
		'// make payment - do all of the dB updating and emailing that is required//
		nErr = makeProformaPayment(aOrder, aMember, sCCName, sCCOrg, sCCAddress)
		'// clear the cart //
		session("OdID") = 0
		session("CtID") = 0
		response.redirect("./receipt.asp?PAY=PRO&e=" & encodeID(nOdID))
	else
		'// fall through to displaying the message //
	end if
else
	'// just making the first pass nothing to do or see here //
end if


%>

<!--#include virtual="/includes/doctype.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Hen&amp;Hammock</title>
<!--#include virtual="/includes/mainCSS.asp" -->
<script language="JavaScript" type="text/javascript">
<!--
function showTandC(){
	var tandcWin;
	tandcWin = window.open("/termsandconditions/","TandC","height=600,width=680,scrollbars,menubar,resizable");
	return false;
}
//-->
</script>
<%=javaSetDropDown("registration")%>
</head>
<!--#include virtual="/includes/body.asp" -->
<!--#include virtual="/includes/templateStart.asp" -->
<!--#include virtual="/includes/templateTopNav.asp" -->

<div id="message_bar">
	<img src="/images/pictures/candles_message.jpg" alt="Shopping Basket" width="474" height="179" class="img_left" border="0" />
		<!-- this div has class of thin_intro --> 
		<div class="thin_intro">
			<h1 class="green">Shopping Basket</h1> 
        	<!-- additional classes of grey and intro_text applied to the p --> 
        	<p class="intro_text grey">Here is your shopping baskey</p>
		</div> 
</div> 

<!--#include virtual="/includes/templateBeginContent.asp" -->

<!-- Begin Content -->
          
					<h1 class="no_display">Shopping Basket</h1>
          
				<div id="cart_steps">Your Details &gt; Options &gt; Confirmation &gt; <strong>Payment</strong> &gt; Receipt</div>
					<div id="center_text_block">  
          
				<h2>Payment - Proforma Invoice</h2>
					
          
          
    <style type="text/css">
    .text {
      font-size: 95%;
      padding-top: 4px;
      padding-left: 2px;
      padding-right: 2px;
      padding-bottom: 2px;
    }
    .text .shopping_title {
      color: #ffffff;
      font-weight: bold;
      padding-top: 1px;
      padding-left: 2px;
      padding-right: 2px;
      padding-bottom: 1px;
    }
    .text input, .text select {
      font-size: 80%;
      height: 16px;
      /*width: 160px;*/
      border: 1px dotted #D97213;
      margin-top: 0px;
      margin-bottom: 0px;
    }
    input.form_button {
      border: 0;
      margin-top: 4px;
      margin-bottom: 4px;
    }
    .text small {
      font-size: 90%;
    }
    .error {
      color: #D97213;
      padding-bottom: 0px;
    }
    .text .form_title {
      background-color: <%=BSKT_HDR_CLR%>;
      color: #ffffff;
      font-weight: bold;
      padding-top: 1px;
      padding-left: 2px;
      padding-right: 2px;
      padding-bottom: 1px;
    }
    .text textarea {
	    font-family: Verdana, Arial, Helvetica, sans-serif;
      font-size: 90%;
      height: 100px;
      width: 200px;
      border: 1px dotted #D97213;
      margin-top: 0px;
      margin-bottom: 0px;
    }
    </style>
          
		
		<form name="payment" action="./paymentPRO.asp" method="POST">
		<input type="hidden" name="submitted" value="yes" />		
		<table bgcolor="<%=BSKT_BGD_CLR%>" border="0" cellpadding="0" cellspacing="2" class="text">
		<tr><td align="right" bgcolor="<%=BSKT_HDR_CLR%>" class="shopping_title">Total Items: </td>
			<td bgcolor="<%=BSKT_ROW_CLR%>">&pound;<%=FormatNumber(nTotalNoVat+nVat, 2)%></td></tr>
    <%if bVat OR (VAT_OPTS = 0) then%>
		<tr><td align="right" bgcolor="<%=BSKT_HDR_CLR%>" class="shopping_title">VAT included: </td>
			<td bgcolor="<%=BSKT_ROW_CLR%>">&pound;<%=FormatNumber(nVat, 2)%></td></tr>
    <%else%>
		<tr><td align="right" bgcolor="<%=BSKT_HDR_CLR%>" class="shopping_title">Less VAT: </td>
			<td bgcolor="<%=BSKT_ROW_CLR%>">&pound;<%=FormatNumber(nVat, 2)%></td></tr>
    <%end if%>
		<tr><td align="right" bgcolor="<%=BSKT_HDR_CLR%>" class="shopping_title">Post &amp; Packing: </td>
			<td bgcolor="<%=BSKT_ROW_CLR%>">&pound;<%=FormatNumber(nPostage, 2)%></td></tr>
		<tr><td align="right" bgcolor="<%=BSKT_HDR_CLR%>" class="shopping_title">Total To Pay: </td>
			<td bgcolor="<%=BSKT_ROW_CLR%>"><b>&pound;<%=FormatNumber(nTotalChargeCalc, 2)%></b></td></tr>
		<tr><td align="right">&nbsp;</td>
			<td>&nbsp;</td></tr>
		<%if len("" & sMsg) > 0 then
			response.write "<tr><td align=""right"">&nbsp;</td><td><p class=""error"">" & sMsg & "</p></td></tr>"
		end if%>
		<tr bgcolor="<%=BSKT_ROW_CLR%>"><td align="right" valign="top"><label for="ccName">Name: </label></td>
			<td valign="top"><input class="shopping_inputbox" type="text" name="ccName" id="ccName" size="21" value="<%=sCCName%>" /></td></tr>
		<tr bgcolor="<%=BSKT_ROW_CLR%>"><td align="right" valign="top"><label for="ccOrg">Organisation: </label></td>
			<td valign="top"><input class="shopping_inputbox" type="text" name="ccOrg" id="ccOrg" size="21" value="<%=sCCOrg%>" /></td></tr>
		<tr bgcolor="<%=BSKT_ROW_CLR%>"><td align="right" valign="top"><label for="ccAddress">Invoice Address: </label></td>
			<td valign="top"><textarea class="shopping_inputbox" name="ccAddress" id="ccAddress"><%=sCCAddress%></textarea></td></tr>
		  
		<tr><td align="right">&nbsp;</td>
			<td>&nbsp;</td></tr>
		<tr><td><input class="shopping_button no_border" type="image" src="/ecomm/buttons/button_cancelpayment.gif" name="cancel" alt="cancel payment" /></td>
			<td><input class="shopping_button no_border" type="image" src="/ecomm/buttons/button_submitpayment.gif" name="submit" alt="submit payment details" /></td></tr>
		</table>
		</form>
       
    <br />&nbsp;<br />
          <!-- End Content -->

<!--#include virtual="/includes/templateEndContent.asp" -->

<!-- LHS navigation -->
<!--#include virtual="/includes/templateLHSnav.asp" -->
<!--#include virtual="/includes/templateFooter.asp" -->

<!--#include virtual="/includes/templateEnd.asp" -->
          

<%
function makeProformaPayment(aOrder, aMember, sCCName, sCCOrg, sCCAddress)
	dim nErr:nErr = 0
	dim aItems:aItems = null
  dim sSQL:sSQL=""
	'// update dB with what has happened //
  aOrder(OD_COMPLETEDALL, 0) = true
  nErr = saveOrder(aOrder)
  if nErr = 0 then
    sSQL = "SELECT OI.* FROM tblOrderItem AS OI" _
        & " WHERE OI.keyFodID=" & aOrder(OD_ID, 0) & " AND OI.keyFinID=" & INST_ID & " ORDER BY OI.oiID ASC;"
    nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aItems)
  else
    '// what?? //
    exit function
  end if
  if nErr = 0 then
  	'// notifyShopOrderPlaced //
  	nErr = notifyShopProformaOrderPlaced(aOrder, aMember, aItems, sCCName, sCCOrg, sCCAddress)
  	'// notifyCustomerOrderConfirmed //
  	nErr = notifyCustomerOrderConfirmed(aOrder, aMember, aItems)
  else
  end if
	makeProformaPayment = nErr
end function

function notifyShopProformaOrderPlaced(aOrder, aMember, aContents, sCCName, sCCOrg, sCCAddress)
	dim nErr:nErr = 0
	dim sTo, sToName, sFrom, sFromName, sSubject
	dim sChargeAdd,sDeliveryAdd,sCustName,sOrderDate,sOrderTime,sOrderMessage,sOrderInstruct,aTmp,i, sProdDesc
	dim sMsg:sMsg = ""
	if (not isNull(aOrder)) and (isArray(aOrder)) then
		if not isNull(aContents) then
			aTmp = split(aOrder(OD_DATE, 0), " ")
			sOrderDate	 = aTmp(0)
			sOrderTime	 = aTmp(1)
			if UBound(aTmp) = 2 then
				'// also have a PM to add //
				sOrderTime = sOrderTime & " " & aTmp(2)
			end if
			sCustName	 = replace(trim(aOrder(OD_TITLE,0) & " " & aOrder(OD_FIRSTNAME,0) & " " & aOrder(OD_SURNAME,0)), "  ", " ")
			sChargeAdd	 = makeAddress(aOrder(OD_TITLE,0),aOrder(OD_FIRSTNAME,0),aOrder(OD_SURNAME,0) _
									  ,aOrder(OD_ADDRESS1,0),aOrder(OD_ADDRESS2,0),aOrder(OD_ADDRESS3,0) _
									  ,aOrder(OD_ADDRESS4,0),getCountry(aOrder(OD_ADDRESS5,0)),aOrder(OD_POSTCODE,0))
			sDeliveryAdd = makeAddress(aOrder(OD_DELTITLE,0),aOrder(OD_DELFIRSTNAME,0),aOrder(OD_DELSURNAME,0) _
									  ,aOrder(OD_DELADDRESS1,0),aOrder(OD_DELADDRESS2,0),aOrder(OD_DELADDRESS3,0) _
									  ,aOrder(OD_DELADDRESS4,0),getCountry(aOrder(OD_DELADDRESS5,0)),aOrder(OD_DELPOSTCODE,0))
			if aOrder(OD_KEYFN1ID,0) > 0 then
				sOrderMessage = getNote(aOrder(OD_KEYFN1ID,0))
			end if
			if aOrder(OD_KEYFN2ID,0) > 0 then
				sOrderInstruct = getNote(aOrder(OD_KEYFN2ID,0))
			end if
			sMsg = sMsg & "You have received the following order." & vbcrlf
			sMsg = sMsg & vbcrlf
			sMsg = sMsg & "ORDER DATE: "
      sMsg = sMsg & sOrderDate & " " & sOrderTime & vbcrlf
			sMsg = sMsg & vbcrlf
			sMsg = sMsg & "ONLINE ORDER REF: "
      sMsg = sMsg & aOrder(OD_ID, 0) & vbcrlf
			sMsg = sMsg & vbcrlf
			sMsg = sMsg & "FROM:  " & vbcrlf
      sMsg = sMsg & sChargeAdd & vbcrlf
      sMsg = sMsg & vbcrlf
      
			sMsg = sMsg & "EMAIL: "
      sMsg = sMsg & aOrder(OD_EMAIL, 0) & vbcrlf
      sMsg = sMsg & vbcrlf
      
			sMsg = sMsg & "TEL NO: "
      sMsg = sMsg & aOrder(OD_PHONE, 0) & vbcrlf
      sMsg = sMsg & vbcrlf
      
      sMsg = sMsg & "ORDER DETAILS:  " & vbcrlf
      '// loop items //
			for i = 0 to UBound(aContents,2)
				sProdDesc = aContents(OI_DESCRIPTION, i)
				if aContents(OI_KEYFN1ID, i) > 0 then
					sProdDesc = sProdDesc & " - " & getNote(aContents(OI_KEYFN1ID, i))
				end if
        'sMsg = sMsg & "Order: " & aOrder(OD_ID, 0) & vbcrlf
        sMsg = sMsg & "ITEM CODE: " & aContents(OI_PRODUCTCODE, i) & vbcrlf
        sMsg = sMsg & "ITEM DESCRIPTION: " & sProdDesc & vbcrlf
        sMsg = sMsg & "PRICE: �" & formatnumber(aContents(OI_UNITCOST,i),2) & vbcrlf
        sMsg = sMsg & "QUANTITY: " & aContents(OI_QUANTITY, i) & vbcrlf
        sMsg = sMsg & "LINE VALUE: �" & formatnumber((aContents(OI_UNITCOST,i) * aContents(OI_QUANTITY, i)),2) & vbcrlf
				sMsg = sMsg & vbcrlf
			next
			sMsg = sMsg & "ORDER VALUE � " & FormatNumber(aOrder(OD_TOTALCHARGECALC,0) - (aOrder(OD_POSTAGE,0) + aOrder(OD_VAT,0)),2) & vbcrlf
			sMsg = sMsg & "POST & PACKAGING: � " & FormatNumber(aOrder(OD_POSTAGE,0),2) & vbcrlf
			sMsg = sMsg & "VAT: � " & FormatNumber(aOrder(OD_VAT,0),2) & vbcrlf
			sMsg = sMsg & "TOTAL INVOICE VALUE: � " & FormatNumber(aOrder(OD_TOTALCHARGECALC,0),2) & vbcrlf
			sMsg = sMsg & vbcrlf
      
      
			sMsg = sMsg & "PLEASE SEND PROFORMA INVOICE TO."
			sMsg = sMsg & "INVOICE NAME    : " & sCCName & vbcrlf
			sMsg = sMsg & "ORGANISATION    : " & sCCOrg & vbcrlf
			sMsg = sMsg & "INVOICE ADDRESS : " & vbcrlf & sCCAddress & vbcrlf
			sMsg = sMsg & vbcrlf
      
			sMsg = sMsg & "DELIVER TO:  " & vbcrlf
      sMsg = sMsg & sDeliveryAdd & vbcrlf
      sMsg = sMsg & vbcrlf
      
			sMsg = sMsg & "ADDITIONAL INFORMATION:" & vbcrlf
			sMsg = sMsg & vbcrlf
			sMsg = sMsg & "MESSAGE TO RECIPIENT FROM CUSTOMER:" & vbcrlf
			sMsg = sMsg & sOrderMessage & vbcrlf
			sMsg = sMsg & vbcrlf
			sMsg = sMsg & "DELIVERY INSTRUCTIONS FROM CUSTOMER:" & vbcrlf
			sMsg = sMsg & sOrderInstruct & vbcrlf
			sMsg = sMsg & vbcrlf
			if aMember(ME_CONTACTBYUS, 0) then
				sMsg = sMsg & "MAY WE CONTACT THIS CUSTOMER:  Yes" & vbcrlf
			else
				sMsg = sMsg & "MAY WE CONTACT THIS CUSTOMER:  No" & vbcrlf
			end if
			sMsg = sMsg & vbcrlf
      if POST_OPTS > 0 then
  			if aOrder(OD_POSTTYPE,0) = "24H" then
  				sMsg = sMsg & "SPECIAL DELIVERY:  Yes" & vbcrlf
  			else
  				sMsg = sMsg & "SPECIAL DELIVERY:  No" & vbcrlf
  			end if
      end if
			if aOrder(OD_PARTSHIP,0) then
				sMsg = sMsg & "PARTIAL SHIPMENT ALLOWED:  Yes" & vbcrlf
			else
				sMsg = sMsg & "PARTIAL SHIPMENT ALLOWED:  No" & vbcrlf
			end if
			sMsg = sMsg & vbcrlf
      
			sTo = EML_SHOPTOEMAIL
			sToName = EML_SHOPTONAME
			sFrom = EML_SHOPFROMEMAIL
			sFromName = EML_SHOPFROMNAME
			sSubject = SHOP_NAME & " Order (Order ref: " & aOrder(OD_ID,0) & ")"
			nErr = SendMailEnc(sTo, sToName, sFrom, sFromName, sSubject, sMsg, "", "", true, false)
			'nErr = SendMailEnc(sTo, sToName, sFrom, sFromName, sSubject, sMsg, "", "", false, false)
		else
			nErr = 2
		end if
	else
		nErr = 1
	end if
	notifyShopProformaOrderPlaced = nErr
end function
%>