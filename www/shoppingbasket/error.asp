﻿<!--#include virtual="/includes/careandcomfortAPI.asp"-->
<!--#include virtual="/ecomm/setup.asp"-->
<!--#include virtual="/cms/setup.asp"-->
<%
dim nCtID
sBreadCrumbItems = "Shopping basket¦/shoppingbasket/" 

if len("" & request.querystring("id")) > 0 then
    nCntId = request.QueryString("id")
else
    nCntId = 44
end if
%>
<!--#include virtual="/includes/doctype.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title><%=SHOP_NAME%></title>
    <!--#include virtual="/includes/mainCSS.asp" -->
    <script language="JavaScript" type="text/javascript">
        <!--
        function showTandC(){
	        var tandcWin;
	        tandcWin = window.open("/termsandconditions/index.asp","TandC","height=600,width=680,scrollbars,menubar,resizable");
	        return false;
        }
        //-->
    </script>
</head>
<!--#include virtual="/includes/body.asp" -->
<!--#include virtual="/includes/templateStart.asp" -->
<!--#include virtual="/includes/templateTopNav.asp" -->
<!--#include virtual="/includes/templateBeginContent.asp" -->
<!-- Begin Content -->          
    <div class="contact_block"> 
      <%=nvCmsGetContentHTML(CMS_CONTENT_ID_CONTACT_SUMMARY)%>     
    </div> 
    
    <%=nvCmsGetContentHTML(nCntId)%>

<!-- End Content -->
<!--#include virtual="/includes/templateEndContent.asp" -->

<!-- LHS navigation -->
<!--#include virtual="/includes/templateLHSnav.asp" -->
<!-- RHS navigation -->
<!--#include virtual="/includes/templateRHSnav.asp" -->

<!--#include virtual="/includes/templateFooter.asp" -->
<!--#include virtual="/includes/templateEnd.asp" -->