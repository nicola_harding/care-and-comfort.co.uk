<!--#include virtual="/includes/careandcomfortAPI.asp"-->
<!--#include virtual="/ecomm/setup.asp"-->
<!--#include virtual="/ecomm/countryselector.asp"-->
<!--#include virtual="/cms/setup.asp"-->
<!--#include virtual="/includes/dates.asp" -->
<%
dim id, sUID, sPWD, nMID, sMsg
dim bGood, nOrderID, nMemberID, aCustomer, nPostalZone, bVat, bNewCustomer, bCustomer
dim nErr                : nErr = 0
dim aMember             : aMember = null
dim ME_EMAIL_CONFIRM    : ME_EMAIL_CONFIRM = "" 

nMID = CLng("0" & session("meID"))
bContactByUs = false
bContactByOther = false
bNewMember = false

if request.form("submitted") = "yes" then
	'// get the submitted values //
	if request.form("confirm") = "yes" then
		sMsg = sMsg & "<input class=""regForm"" type=""hidden"" name=""confirm"" value=""yes"" />"
	end if
    nErr = processFormSubmission(aMember, sMsg)
    aMember(ME_CUSTOMER, 0) = true
	if nErr = 0 then
        '// check to see if we already have this persons details //
        '// and if they made a purchase previously? //
        if not request.form("confirm") = "yes" then
            nMID = isAnExistingMember(aMember)
            if nMID > 0 then
                if (len(aMember(ME_UID, 0)) > 0) AND (len(aMember(ME_PWD, 0)) > 0) then
                    '// yes they are an existing customer //
                    '// what are we going to do then //
                    session("meID") = nMID
                    sendPasswordEmail(aMember)
                    nErr = 99
                    sMsg = "We appear to already have your details on record.<br />&nbsp;" _
	                    & "<br />We have sent you an email reminding you of your membership details.<br />&nbsp;" _
	                    & "<br />You can review your existing details by <a href=""./registerdetails.asp"" target=""_blank"">clicking here</a>" _
	                    & " (this will open in a new browser window so that you don't lose the information that you have just entered).<br />&nbsp;" _
	                    & "<br />If you are ready to login please <a href=""./login.asp"">click here</a>, you can always update your details later on.<br />&nbsp;" _
	                    & "<br />Finally if you click the [SUBMIT] button at the bottom of this page again," _
	                    & " we will update your details with the information that you have provided on this screen.<br />" _
	                    & "<input class=""regForm"" type=""hidden"" name=""confirm"" value=""yes"" />"
                else
                    '// email address & no UID & no PWD means no purchase but we have some of their details //
                      aMember(ME_UID, 0) = aMember(ME_EMAIL, 0)
                      aMember(ME_PWD, 0) = "" & aMember(ME_ID, 0)
                end if
	        else
	            '// create a new customer record
                nErr = saveMember(aMember)
                if nErr = 0 then
                    aMember(ME_UID, 0) = aMember(ME_EMAIL, 0)
                    aMember(ME_PWD, 0) = "" & aMember(ME_ID, 0)
                    nMID = aMember(ME_ID, 0)
                end if
				bNewMember = true
			end if      ' nMID > 0
		end if          ' not request.form("confirm") = "yes"
	end if              ' nErr = 0
	
	if (nErr = 0) AND (nMID > 0) then
		session("meID") = nMID
        nErr = saveMember(aMember)
	end if
	if nErr = 0 then
		if bNewMember = true then
			sendPasswordEmail(aMember)
		end if
		session("ShopLogged") = true
		response.redirect "./confirm.asp"
	else
		sMsg = "There was a problem submitting your details please check them and try again.<br />" & sMsg
	end if
	
elseif nMID > 0 then
    nOrderID = 0
    nPostalZone = 0
    bVat = true
    nErr = loadMember(nMID, aMember)
    sMsg = sMsg & "<input class=""regForm"" type=""hidden"" name=""confirm"" value=""yes"" />"
    if nErr > 0 then
        redim aMember(ME_UBOUND, 0)
        aMember(ME_USEALTADDRESS, 0) = false
        aMember(ME_CONTACTBYUS, 0) = true
        aMember(ME_CONTACTBYOTHER, 0) = false
        aMember(ME_CUSTOMER , 0) = false
        aMember(ME_SUPPLIER , 0) = false
        aMember(ME_SUBSCRIBER , 0) = false
    end if
else
    redim aMember(ME_UBOUND, 0)
    aMember(ME_USEALTADDRESS, 0) = false
    aMember(ME_CONTACTBYUS, 0) = true
    aMember(ME_CONTACTBYOTHER, 0) = false
    aMember(ME_CUSTOMER , 0) = false
    aMember(ME_SUPPLIER , 0) = false
    aMember(ME_SUBSCRIBER , 0) = false
end if

function isAnExistingMember(aMember)
    dim nErr:nErr=0
    dim nID:nID=0
    dim sSQL:sSQL=""
    dim aTmp:aTmp=null
    if aMember(ME_ID, 0) > 0 then
        nID = aMember(ME_ID, 0)
    elseif len("" & aMember(ME_EMAIL, 0)) > 0 then
        sSQL = "SELECT ME.* FROM tblMember AS ME WHERE ME.meEmail='" & SQLFixUp(aMember(ME_EMAIL, 0)) _
             & "' AND ME.meSurname='" & SQLFixUp(aMember(ME_SURNAME, 0)) & "';"
        nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aTmp)
        if nErr = 0 then
          aMember(ME_ID, 0)  = aTmp(ME_ID, 0)
          aMember(ME_UID, 0) = aTmp(ME_UID, 0)
          aMember(ME_PWD, 0) = aTmp(ME_PWD, 0)
          nID = aMember(ME_ID, 0)
        end if
        aTmp = null
    end if
    isAnExistingMember = nID
end function

function processFormSubmission(aMember, sMsg)
  dim nErr : nErr=0
  dim nID  : nID=0

  nID = CLng("0" & request.form("meID"))
  if nID > 0 then
    nErr = loadMember(nID, aMember)
  else
    redim aMember(ME_UBOUND, 0)
    aMember(ME_USEALTADDRESS, 0) = false
    aMember(ME_CONTACTBYUS, 0) = true
    aMember(ME_CONTACTBYOTHER, 0) = false
    aMember(ME_CUSTOMER , 0) = false
    aMember(ME_SUPPLIER , 0) = false
    aMember(ME_SUBSCRIBER , 0) = false
  end if
	aMember(ME_ID, 0)			     = nID
	aMember(ME_EMAIL, 0)		     = trim("" & request.form("meEmail"))
	ME_EMAIL_CONFIRM		         = trim("" & request.form("meEmailConfirm"))
	aMember(ME_TITLE, 0)			 = trim("" & request.form("meTitle"))
	aMember(ME_FIRSTNAME, 0)	     = trim("" & request.form("meFirstName"))
	aMember(ME_SURNAME, 0)		     = trim("" & request.form("meSurName"))
	aMember(ME_COMPANY, 0)		     = trim("" & request.form("meCompany"))
	aMember(ME_ADDRESS1, 0)		     = trim("" & request.form("meAddress1"))
	aMember(ME_ADDRESS2, 0)		     = trim("" & request.form("meAddress2"))
	aMember(ME_ADDRESS3, 0)		     = trim("" & request.form("meAddress3"))
	aMember(ME_ADDRESS4, 0)		     = trim("" & request.form("meAddress4"))
	aMember(ME_POSTCODE, 0)		     = trim("" & request.form("mePostCode"))
	aMember(ME_ADDRESS5, 0)		     = trim("" & request.form("meAddress5"))
	aMember(ME_TEL, 0)			     = trim("" & request.form("meTel"))
	aMember(ME_FAX, 0)			     = trim("" & request.form("meFax"))
	aMember(ME_MOB, 0)			     = trim("" & request.form("meMob"))
	aMember(ME_DATEREGISTERED, 0)	 = now()
	aMember(ME_CONTACTBYUS, 0)       = (CLng("0" & request.form("meContactByUs")) = 0)
	aMember(ME_CONTACTBYOTHER, 0)	 = (CLng("0" & request.form("meContactByOther")) > 0)
	aMember(ME_USEALTADDRESS, 0)	 = (CLng("0" & request.form("meUseAltAddress")) > 0)
	aMember(ME_ALTTITLE, 0)		     = trim("" & request.form("meAltTitle"))
	aMember(ME_ALTFIRSTNAME, 0)	     = trim("" & request.form("meAltFirstName"))
	aMember(ME_ALTSURNAME, 0)		 = trim("" & request.form("meAltSurName"))
	aMember(ME_ALTADDRESS1, 0)	   = trim("" & request.form("meAltAddress1"))
	aMember(ME_ALTADDRESS2, 0)	   = trim("" & request.form("meAltAddress2"))
	aMember(ME_ALTADDRESS3, 0)	   = trim("" & request.form("meAltAddress3"))
	aMember(ME_ALTADDRESS4, 0)	   = trim("" & request.form("meAltAddress4"))
	aMember(ME_ALTPOSTCODE, 0)	   = trim("" & request.form("meAltPostCode"))
	aMember(ME_ALTADDRESS5, 0)	   = trim("" & request.form("meAltAddress5"))
	'// now do the validation //
	if len(aMember(ME_EMAIL, 0)) > 0 then
		if not validateEmail(aMember(ME_EMAIL, 0)) then
			nErr = nErr + 1
			sMsg = sMsg & "Please enter a valid email address.<br />"
		end if
	else
		nErr = nErr + 1
		sMsg = sMsg & "Please enter your email address.<br />"
	end if
	if len(ME_EMAIL_CONFIRM) > 0 then
		if not validateEmail(ME_EMAIL_CONFIRM) then
			nErr = nErr + 1
			sMsg = sMsg & "Please enter confirm your email address.<br />"
		end if
	else
		nErr = nErr + 1
		sMsg = sMsg & "Please confirm your email address.<br />"
	end if
	' // Ensure both entered email addresses match
	if (len(aMember(ME_EMAIL, 0)) > 0) and (len(ME_EMAIL_CONFIRM) > 0) then
	    if lcase(aMember(ME_EMAIL, 0)) <> lcase(ME_EMAIL_CONFIRM) then
	        nErr = nErr + 1
		    sMsg = sMsg & "Email addresses do not match. <br />"
		end if
	end if
	if len(aMember(ME_TITLE, 0)) = 0 then
		nErr = nErr + 1
		sMsg = sMsg & "Please enter your title.<br />"
	end if
	if len(aMember(ME_FIRSTNAME, 0)) = 0 then
		nErr = nErr + 1
		sMsg = sMsg & "Please enter your first name<br />"
	end if
	if len(aMember(ME_SURNAME, 0)) = 0 then
		nErr = nErr + 1
		sMsg = sMsg & "Please enter your surname.<br />"
	end if
	if len(aMember(ME_ADDRESS1, 0)) = 0 then
		nErr = nErr + 1
		sMsg = sMsg & "Please enter the first line of your address.<br />"
	end if
	if len(aMember(ME_ADDRESS3, 0)) = 0 then
		nErr = nErr + 1
		sMsg = sMsg & "Please enter your town/city.<br />"
	end if
	if len(aMember(ME_ADDRESS5, 0)) = 0 then
		nErr = nErr + 1
		sMsg = sMsg & "Please enter your country.<br />"
	end if
	if len(aMember(ME_POSTCODE, 0)) = 0 then
		nErr = nErr + 1
		sMsg = sMsg & "Please enter your postal code.<br />"
	end if
	if len(aMember(ME_PHONE, 0)) = 0 then
		nErr = nErr + 1
		sMsg = sMsg & "Please enter your telephone number.<br />"
	end if
	
	'// do we validate the delivery details //
	if (aMember(ME_USEALTADDRESS, 0)) OR (len(aMember(ME_ALTTITLE, 0)) > 0) OR (len(aMember(ME_ALTFIRSTNAME, 0)) > 0) OR (len(aMember(ME_ALTSURNAME, 0)) > 0) _
		OR (len(aMember(ME_ALTADDRESS1, 0)) > 0) OR (len(aMember(ME_ALTADDRESS2, 0)) > 0) OR (len(aMember(ME_ALTADDRESS3, 0)) > 0) _
		OR (len(aMember(ME_ALTADDRESS4, 0)) > 0) OR (len(aMember(ME_ALTPOSTCODE, 0)) > 0) then ' OR (len(aMember(ME_ALTADDRESS5, 0)) > 0) 
		
		    if len(aMember(ME_ALTTITLE, 0)) = 0 then
				nErr = nErr + 1
				sMsg = sMsg & "Please enter your delivery title.<br />"
			end if
			if len(aMember(ME_ALTFIRSTNAME, 0)) = 0 then
				nErr = nErr + 1
				sMsg = sMsg & "Please enter your delivery first name<br />"
			end if
			if len(aMember(ME_ALTSURNAME, 0)) = 0 then
				nErr = nErr + 1
				sMsg = sMsg & "Please enter your delivery surname.<br />"
			end if
			if len(aMember(ME_ALTADDRESS1, 0)) = 0 then
				nErr = nErr + 1
				sMsg = sMsg & "Please enter the first line of your delivery address.<br />"
			end if
			if len(aMember(ME_ALTADDRESS3, 0)) = 0 then
				nErr = nErr + 1
				sMsg = sMsg & "Please enter your delivery town/city.<br />"
			end if
			if len("" & aMember(ME_USEALTADDRESS, 0)) > 0 then
			if len(aMember(ME_ALTADDRESS5, 0)) = 0 then
				nErr = nErr + 1
				sMsg = sMsg & "Please enter your delivery country.<br />"
			end if
			end if
			if len(aMember(ME_ALTPOSTCODE, 0)) = 0 then
				nErr = nErr + 1
				sMsg = sMsg & "Please enter your delivery postal code.<br />"
			end if
	end if
	'// if we are good to here then let's now... //
  if aMember(ME_ID, 0) = 0 then
	  aMember(ME_UID, 0) = aMember(ME_EMAIL, 0)
  end if
  aMember(ME_KEYFINID, 0) = INST_ID
  processFormSubmission = nErr
end function
%>

<!--#include virtual="/includes/doctype.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <%=GetDefaultEcommMetaTags("Registration " & SHOP_NAME)%>  
    <!--#include virtual="/includes/mainCSS.asp" -->
	 <!--#include virtual="/includes/javascript.asp" -->

	 
    <%=javaSetDropDown("registration")%>
</head>

<!--#include virtual="/includes/templateStart.asp" -->
<!--#include virtual="/includes/templateTopNav.asp" -->
<!--#include virtual="/includes/templateLeftNav.asp" -->


<!--#include virtual="/includes/templateBeginMainContent.asp" -->
    
  
<!-- Begin Content --> 


 	<!--#include virtual="/includes/templateBasketbreadCrumb.asp" -->  
     
    <div class="contact_block"> 
      <%=nvCmsGetContentHTML(CMS_CONTENT_ID_CONTACT_SUMMARY)%>     
    </div> 
    
     

    <h2>Register Your Details</h2>
	
<form action="./register.asp" name="registration" method="post" class="main_form">
<input class="regForm" type="hidden" name="submitted" value="yes" />
<input class="regForm" type="hidden" name="meID" value="<%=nMID%>" />
<%if len("" & sMsg) > 0 then%><div class="error"><%=sMsg%><br></div><%end if%>
						
<table width="420" border="0" cellpadding="0" cellspacing="0" class="register_table" >
<tr>
<td colspan="2" ><a name="Details"></a><div class="form_title">Please complete the form.</div></td></tr>
<tr>
<td >&nbsp;</td>
<td>&nbsp;</td></tr>
<tr>
<td colspan="2"><span class="error">Fields marked * are mandatory</span><br /><br /></td></tr>
<tr>
<td width="50%" valign="top"><label for="meEmail">Email Address:</label> <span class="error">*</span></td>
<td width="50%" valign="top"><span class="error"><input class="regForm" type="text" id="meEmail" name="meEmail" size="28" value="<%=aMember(ME_EMAIL, 0)%>" /></span></td></tr>
<tr>
<td width="50%" valign="top"><label for="meEmail">Confirm Email Address:</label> <span class="error">*</span></td>
<td width="50%" valign="top"><span class="error"><input class="regForm" type="text" id="meEmailConfirm" name="meEmailConfirm" size="28" value="<%=ME_EMAIL_CONFIRM%>" /></span></td></tr>
<tr>
<td valign="top"><label for="meTitle">Title (Mr/Ms/etc.):</label> <span class="error">*</span</td>
<td valign="top"><span class="error"><input class="regForm" type="text" id="meTitle" name="meTitle" size="4" value="<%=aMember(ME_TITLE, 0)%>" /></span></td></tr>
<tr>
<td valign="top"><label for="meFirstName">First Name:</label> <span class="error">*</span</td>
<td valign="top"><span class="error"><input class="regForm" type="text" id="meFirstName" name="meFirstName" size="28" value="<%=aMember(ME_FIRSTNAME, 0)%>" /></span></td></tr>
<tr>
<td valign="top"><label for="meSurName">Surname:</label><span class="error">*</span></td>
<td valign="top"><span class="error"><input class="regForm" type="text" id="meSurName" name="meSurName" size="28" value="<%=aMember(ME_SURNAME, 0)%>" /></span></td></tr>
<tr>
<td valign="top"><label for="meAddress1">Address Line 1:</label> <span class="error">*</span></td>
<td valign="top"><span class="error"><input class="regForm" type="text" id="meAddress1" name="meAddress1" size="28" value="<%=aMember(ME_ADDRESS1, 0)%>" /></span></td></tr>
<tr>
<td valign="top"><label for="meAddress2">Address Line 2:</label></td>
<td valign="top"><span class="error"><input class="regForm" type="text" id="meAddress2" name="meAddress2" size="28" value="<%=aMember(ME_ADDRESS2, 0)%>" /></span></td></tr>
<tr>
<td valign="top"><label for="meAddress3">Town/City:</label> <span class="error">*</span></td>
<td valign="top"><span class="error"><input class="regForm" type="text" id="meAddress3" name="meAddress3" size="28" value="<%=aMember(ME_ADDRESS3, 0)%>" /></span></td></tr>
<tr>
<td valign="top"><label for="meAddress4">County:</label></td> <!-- /State -->
<td valign="top"><span class="error"><input class="regForm" type="text" id="meAddress4" name="meAddress4" size="28" value="<%=aMember(ME_ADDRESS4, 0)%>" /></span></td></tr>
<tr>
<td valign="top"><label for="mePostCode">Postal Code:</label> <span class="error">*</span></td>
<td valign="top"><span class="error"><input class="regForm" type="text" id="mePostCode" name="mePostCode" size="28" value="<%=aMember(ME_POSTCODE, 0)%>" /></span></td></tr>
<tr>
    <td valign="top"><label for="meAddress5">Country:</label> <span class="error">*</span></td>
    <td valign="top">
    <span class="error">
        <%=countrySelectorDelivery("meAddress5", aMember(ME_ADDRESS5, 0))%>
        <%'=countrySelector("meAddress5")%>
    </span>   
    </td>
</tr>
<tr>
<td valign="top"><label for="meTel">Telephone Number:</label> <span class="error">*</span></td>
<td valign="top"><span class="error"><input class="regForm" type="text" id="meTel" name="meTel" size="28" value="<%=aMember(ME_TEL, 0)%>" /></span></td></tr>
<tr>
<td valign="top"><label for="meMob">Mobile Number:</label></td>
<td valign="top"><span class="error"><input class="regForm" type="text" id="meMob" name="meMob" size="28" value="<%=aMember(ME_MOB, 0)%>" /></span></td></tr>
<tr>
<td valign="top"><label for="meFax">Fax Number:</label></td>
<td valign="top"><span class="error"><input class="regForm" type="text" id="meFax" name="meFax" size="28" value="<%=aMember(ME_FAX, 0)%>" /></span></td></tr>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td></tr>
<tr>
<td align="right"></td>
<td align="left">
    <span class="error">
        <input class="regForm" type="radio" id="meUseAltAddressNo" name="meUseAltAddress" value="0" <%if not aMember(ME_USEALTADDRESS, 0) then response.write " checked"%>/>
    </span>
    <label for="meUseAltAddressNo">Use this address for delivery?</label>
    </td>
</tr>
<tr>
<td>&nbsp;</td>
<td align="center">&nbsp;</td></tr>
<tr>
<td colspan="2"><a name="Altdetails"></a><div class="form_title">Delivery Address if different</div></td></tr>
<tr>
<td>&nbsp;</td>
<td align="center">&nbsp;</td></tr>
<tr>
<td valign="top"><label for="meAltTitle">Title (Mr/Ms/etc.):</label> <span class="error">*</span></td>
<td valign="top"><span class="error"><input class="regForm" type="text" id="meAltTitle" name="meAltTitle" size="4" value="<%=aMember(ME_ALTTITLE, 0)%>" /></span></td></tr>
<tr>
<td valign="top"><label for="meAltFirstName">First Name:</label> <span class="error">*</span></td>
<td valign="top"><span class="error"><input class="regForm" type="text" id="meAltFirstName" name="meAltFirstName" size="28" value="<%=aMember(ME_ALTFIRSTNAME, 0)%>" /></span></td></tr>
<tr>
<td valign="top"><label for="meAltSurName">Surname:</label> <span class="error">*</span></td>
<td valign="top"><span class="error"><input class="regForm" type="text" id="meAltSurName" name="meAltSurName" size="28" value="<%=aMember(ME_ALTSURNAME, 0)%>" /></span></td></tr>
<tr>
<td valign="top"><label for="meAltAddress1">Address Line 1:</label> <span class="error">*</span></td>
<td valign="top"><span class="error"><input class="regForm" type="text" id="meAltAddress1" name="meAltAddress1" size="28" value="<%=aMember(ME_ALTADDRESS1, 0)%>" /></span></td></tr>
<tr>
<td valign="top"><label for="meAltAddress2">Address Line 2:</label></td>
<td valign="top"><span class="error"><input class="regForm" type="text" id="meAltAddress2" name="meAltAddress2" size="28" value="<%=aMember(ME_ALTADDRESS2, 0)%>" /></span></td></tr>
<tr>
<td valign="top"><label for="meAltAddress3">Town/City:</label> <span class="error">*</span></td>
<td valign="top"><span class="error"><input class="regForm" type="text" id="meAltAddress3" name="meAltAddress3" size="28" value="<%=aMember(ME_ALTADDRESS3, 0)%>" /></span></td></tr>
<tr>
<td valign="top"><label for="meAltAddress4">County:</label></td>
<td valign="top"><span class="error"><input class="regForm" type="text" id="meAltAddress4" name="meAltAddress4" size="28" value="<%=aMember(ME_ALTADDRESS4, 0)%>" /></span></td></tr>
<tr>
<td valign="top"><label for="meAltPostCode">Postal Code:</label> <span class="error">*</span></td><!-- /Zip-->
<td valign="top"><span class="error"><input class="regForm" type="text" id="meAltPostCode" name="meAltPostCode" size="28" value="<%=aMember(ME_ALTPOSTCODE, 0)%>" /></span></td></tr>
<tr>
    <td valign="top"><label for="meAddress5">Country:</label> <span class="error">*</span></td>
    <td valign="top">
    <span class="error">
        <%=countrySelectorDelivery("meAltAddress5", aMember(ME_ALTADDRESS5, 0))%>
        <%'=countrySelector("meAltAddress5")%>
    </span>   
    </td>
</tr>

<tr>
<td>&nbsp;</td>
<td>&nbsp;</td></tr>
<tr>
<td align="right"></td>
<td align="left">
    <span class="error"><input class="regForm" type="radio" id="meUseAltAddressYes" name="meUseAltAddress" value="1" <%if aMember(ME_USEALTADDRESS, 0) then response.write " checked"%>/></span>
    <label for="meUseAltAddressYes">Use this address for delivery?</label></td></tr>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td></tr>
<tr>
<td colspan="2"><a name="DataProtect"></a><div class="form_title">Privacy &amp; Data Protection</div></td></tr>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td></tr>
<tr>
<td colspan="2">
Occasionally we would like to let customers know about special or limited offers.  We limit these to about 5 per year.<br />
</td></tr>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td></tr>
<tr>
<td align="right" valign="top"><input class="regForm" type="checkbox" id="meContactByUs" name="meContactByUs" value="1"<%if not aMember(ME_CONTACTBYUS, 0) then response.write " checked"%> /></td>
<td align="left"><label for="meContactByUs">Please tick this if you do not want to be included:</label>
<br />All customer details are retained in strictest confidence, we do not share your details with any third party.<br />
<a href="/privacypolicy/window.asp" target="_blank" onClick="return showTandC();">Read our privacy policy here</a>.<br /></td>
</tr>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td></tr>
<!--<tr>
<td align="right"><input class="regForm" type="checkbox" id="meContactByOther" name="meContactByOther" value="1"<%if aMember(ME_CONTACTBYOTHER, 0) then response.write " checked"%> /></td>
<td align="left"><label for="meContactByOther">May our partner organisations contact you?</label></td></tr>-->
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td></tr>
<tr>
    <td colspan="2" align="right">
         <fieldset class="ecomm_buttons">
            <input type="image" src="/ecomm/buttons/button_submit.gif" name="submit" value="Update" tabindex="4" alt="Update details" />
         </fieldset>
    </td></tr>
</table>
</form>

<script language="JavaScript">
<!--
    // setDropDown('meAddress5','<%=aMember(ME_ADDRESS5, 0)%>');setDropDown('meAltAddress5','<%=aMember(ME_ALTADDRESS5, 0)%>');
//-->
</script>

<!-- End Content -->
<!--#include virtual="/includes/templateEndMainContent.asp" -->


<!--#include virtual="/includes/templateRightColumn.asp" -->


<!--#include virtual="/includes/templateFooter.asp" -->
<!--#include virtual="/includes/templateEnd.asp" -->