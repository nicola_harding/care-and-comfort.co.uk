<!--#include virtual="/includes/careandcomfortAPI.asp"-->
<!--#include virtual="/ecomm/setup.asp"-->
<!--#include virtual="/cms/setup.asp"-->
<!--#include virtual="/includes/dates.asp" -->

<%
dim nErr, sUID, sPWD, sEmail, aMember, sMsg1, sMsg2

if request.form("act") = "login" then
  sUID = trim("" & request.form("UID"))
  sPWD = trim("" & request.form("PWD"))
  if len(sUID) > 0 then
    if len(sPWD) > 0 then
      if validateEmail(sUID) then
        nErr = doLogin(sUID, sPWD, aMember)
        if nErr = 0 then
          response.redirect "./registerview.asp"
        else
          sMsg1 = sMsg1 & "<p class=""red_text"">Sorry, we were unable to validate your login information. Please check them and try again.</p>"
        end if
      else
        sMsg1 = sMsg1 & "<p class=""red_text"">Please enter a valid email address.</p>"
      end if
    else
      sMsg1 = sMsg1 & "<p class=""red_text"">Please enter your email address and customer number to log in.</p>"
    end if
  else
    sMsg1 = sMsg1 & "<p class=""red_text"">Please enter your email address and customer number to log in.</p>"
  end if
elseif request.form("act") = "remind" then
  sEmail = trim("" & request.form("email"))
  if len(sEmail) > 0 then
    if validateEmail(sEmail) then
        nErr = doLoginRemind(sEmail, aMember)
        if nErr = 0 then
          sMsg2 = sMsg2 & "<p class=""red_text"">Details of how to log in have been sent to your email address.</p>"
        else
          sMsg2 = sMsg2 & "<p class=""red_text"">Sorry, we were unable to find your email address in our records. Please check the address and try again.</p>"
        end if
    else
      sMsg2 = sMsg2 & "<p class=""red_text"">Please enter a valid email address.</p>"
    end if
  else
    sMsg2 = sMsg2 & "<p class=""red_text"">Please enter your email address.</p>"
  end if
else
  '// don't think we need to do anything at all. //
end if

function doLogin(sUID, sPWD, aMember)
  dim nErr:nErr=0
  dim sSQL:sSQL=""
  if (len("" & sUID) > 0) and (len("" & sPWD) > 0) then
    sSQL = "SELECT ME.* FROM tblMember AS ME WHERE ME.meUID='" & SQLFixUp(sUID) & "' AND ME.mePWD='" & SQLFixUp(sPWD) & "' AND ME.keyFinID=" & INST_ID & ";"
  
    nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aMember)
    if nErr = 0 then
      session("meID") = aMember(ME_ID, 0)
      aMember(ME_LASTLOGON, 0) = Cdate(now())
      aMember(ME_NUMBERVISITS, 0) = aMember(ME_NUMBERVISITS, 0) + 1
      nErr = saveMember(aMember)
    end if
  else
    nErr = 1  '// bad input parms //
    aMember = null
  end if
  doLogin = nErr
end function
%>

<!--#include virtual="/includes/doctype.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <%=getCMSMetaData(nCntId,"","","")%>  
    <!--#include virtual="/includes/mainCSS.asp" -->
	  <!--#include virtual="/includes/javascript.asp" -->
</head>

<!--#include virtual="/includes/templateStart.asp" -->
<!--#include virtual="/includes/templateTopNav.asp" -->
<!--#include virtual="/includes/templateLeftNav.asp" -->


<!--#include virtual="/includes/templateBeginMainContent.asp" -->
	
<!-- Begin Content -->
    
  	<!--#include virtual="/includes/templateBasketbreadCrumb.asp" -->  
<!-- Begin Content --> 
    
    

   
    <h2>Your Details</h2>				
      
<table class="login_table">
<tr>
    <td> 
 	<!--p style="color: #ff0000;">We are currently undergoing essential maintenance. </p>
        <p style="color: #ff0000;">The Order Placement Facility will be back online shortly after the scheduled slot. <br /><b>08:30am GMT Monday 26th August 2014</b></p-->



        <h3>Returning Customer</h3>
        If you are a returning customer, please enter your email and web member number below so we can retrieve your contact and delivery details.<br />&nbsp;<br />
        <%if len("" & sMsg1) > 0 then%>
            <span class="error"><%=sMsg1%></span>
        <%end if%>
        <form name="login" action="./login.asp" class="main_form" method="post">
        <fieldset class="fields"> 
        <input type="hidden" name="act" value="login">
        <table width="400"  border="0" cellpadding="2" cellspacing="0" summary="Fill in your registered email address and customer number to log in as an existing customer.">
        <caption style="display:none;">login box</caption>
          <tr>
            <td width="50%" align="right"><label for="UID" class="text2">email address:</label></td>
            <td width="50%"><input name="UID" type="text" id="UID" tabindex="3" size="30" maxlength="70" class="search_box_input" value="<%=sUID%>"></td>
          </tr>
          <tr>
            <td align="right"><label for="PWD" class="text2">web member number:</label></td>
            <td><input name="PWD" type="password" id="PWD" tabindex="4" size="30" maxlength="10" class="search_box_input" value="<%=sPWD%>"></td>
          </tr>
          <tr>
            <td align="right">&nbsp;</td>
            <td align="right">
                <input type="image" name="submit" src="/ecomm/buttons/button_login.gif" value="Login" tabindex="5" alt="login"  />
            </td>
          </tr>           
        </table>
            </fieldset>  
        </form>
		<br/>
        <h3>New Customer</h3>
        If you are a new customer please <a href="./register.asp" title="Register you details here">register you details here</a>.         
        <br /><br />
        <h3>Forgotten Your Details?</h3>
        If you have forgotten your web member number, please enter your email address and we will send it to you.<br />&nbsp;<br />
        <%if len("" & sMsg2) > 0 then%>
            <span class="error"><%=sMsg2%></span>
        <%end if%>
		<fieldset class="fields">
        <form name="remind" action="./login.asp" class="main_form" method="post">
        <input type="hidden" name="act" value="remind">
        <table width="400"  border="0" cellpadding="2" cellspacing="0" summary="Fill in your registered email address and we will send you a login reminder.">
        <caption style="display:none;">login reminder box</caption>
           
          <tr>
            <td width="50%" align="right"><label for="email" class="text">email address:</label></td>
            <td width="50%"><input name="email" type="text" class="search_box_input" id="email" tabindex="7" value="<%=sEmail%>" size="30" maxlength="70"></td>
          </tr>
         
          <tr>
            <td align="right">&nbsp;</td>
            <td align="right">
                 
                    <input type="image" name="submit" src="/ecomm/buttons/button_submit.gif" value="Email me" tabindex="8" alt="login"  />
                               
            </td>
          </tr>     
        </table>
        </form></fieldset>
    </td>
  </tr>
</table>
<br />
<!-- End Content -->
<!--#include virtual="/includes/templateEndMainContent.asp" -->

<!--#include virtual="/includes/templateRightColumn.asp" -->

<!--#include virtual="/includes/templateFooter.asp" -->

<!--#include virtual="/includes/templateEnd.asp" -->