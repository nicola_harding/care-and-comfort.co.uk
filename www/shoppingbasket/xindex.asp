<!--#include virtual="/includes/careandcomfortAPI.asp"-->
<!--#include virtual="/ecomm/setup.asp"-->
<!--#include virtual="/cms/setup.asp"-->
<!--#include virtual="/includes/dates.asp" -->

<%



		
'// work out what we are doing here //
dim nErr : nErr = 0
dim sAct, nItems, nCiID, ct, nTotQty, nMeID, nCtID, nViewProdId
dim sBreadCrumbItems
sBreadCrumbItems = "Shopping basket�/shoppingbasket/" 

if len("" & request.querystring("id")) > 0 then
    nCntId = request.QueryString("id")
else
    nCntId = 18
end if

nMeID   = CLng("0" & session("meID"))
nCtID   = CLng("0" & session("ctID"))
sAct    = request("act")

' // Picks up querystring and sets values into nCatID --> nPrdID
call parseParms(request.querystring,"",nCatID,nRngID,nGrpID,nColID,nPrdID)



if sAct = "upd" then
    '// process the quantity changes //
    nItems = CLng("0" & request.form("nItems"))
    for ct = 0 to nItems-1
        nCiID   = request.form("id-" & ct)
        nQty    = getNumberFromString(trim("" & request.form("qty-" & ct)))
        nTotQty = nTotQty + nQty
        if nQty > 0 then
	        nErr = updateCartItemQty(nCtID, nCiID, nQty)
        else
	        '// let them know?
        end if
    next
    'session("numItemsInCart") = nTotQty
elseif sAct = "rem" then
    '// remove the offending item //
    nCiID   = CLng("0" & request.querystring("ci"))
    nQty    = getNumberFromString(trim("" & request.querystring("qty")))
    'nTotQty = session("numItemsInCart")
    nTotQty = nTotQty - nQty
    if nTotQty < 0 then 
        nTotQty = 0
    end if
    nErr = removeCartItem(nCtID, nCiID)
   ' response.Write "<br />nErr=" & nErr
   ' response.Write "<br />nCiID=" & nCiID
   ' response.Write "<br />nQty=" & nQty
   ' response.Write "<br />nCtID=" & nCtID
    'session("numItemsInCart") = nTotQty
else
    ' // just been asked to display //
    if nRngID = 0 then  
        dim aRange  
        nErr = LoadProductRange(nPrdID, aRange)
		if nErr = 0 and (not isNull(aRange)) then
		    nRngID = aRange(RN_ID,0) 
		end if   		
	end if   
	if nCatID = 0 then	    
	    if nRngID > 0 then
	        nCatID = GetRangeCategory(nRngID)
		end if
	end if  
end if

function updateCartItemQty(nCtID, nCiID, nQty)
  dim nErr:nErr=0
  dim sSQL:sSQL=""
  
  if (nCtID > 0) and (nCiID > 0) and (nQty > 0) then
    sSQL = "UPDATE tblCartItem SET ciQuantity=" & nQty & " WHERE ciID=" & nCiID & " AND keyFctID=" & nCtID & " AND keyFinID=" & INST_ID & ";"
    call sqlExec(DSN_ECOM_DB, sSQL)
  else
    nErr = 1  '// bad input parms //
  end if
  updateCartItemQuantity = nErr
end function

function removeCartItem(nCtID, nCiID)
  dim nErr:nErr=0
  dim sSQL:sSQL=""
  if (nCtID > 0) and (nCiID > 0) then
    sSQL = "DELETE FROM tblCartItem WHERE ciID=" & nCiID & " AND keyFctID=" & nCtID & " AND keyFinID=" & INST_ID & ";"
    call sqlExec(DSN_ECOM_DB, sSQL)
  else
    nErr = 1  '// bad input parms //
  end if
  removeCartItem = nErr
end function


function showCart(nCtID, sHedClr, sRowClr, sBkgClr)
  dim nNtID, aWish, nWlID, sWishList, bHasWishItems, sTmpWL, aTmpWL
  dim nTotal, nSubTotal, sName, sLeadTime, nPrice, nQty, nTotQty, sImg, sShortCode, nCatID
  dim sTmp          : sTmp = ""
  dim aCartItems    : aCartItems=null
  dim nIdx          : nIdx=0
  dim sSQL          : sSQL=""
  dim sLinkCheckOut : sLinkCheckOut=""
  dim sLinkCont     : sLinkCont = session("BUY_RFR")
  dim aProduct(48)
  dim nFieldIdx
    
  if len("" & CHECKOUT_URL) > 0 then
    sLinkCheckOut = CHECKOUT_URL
  else
    sLinkCheckOut = "./checkout.asp"
  end if
  
  ' // legacy: Call getLastViewedItem(nIdx, sLinkCont, nIdx, sTmp)
  nIdx = 0
  sTmp = ""
  if len("" & sLinkCont) = 0 then
    sLinkCont = "/"
  end if
  if nCtID > 0 then
    sSQL = "SELECT PR.*, CI.ciQuantity, LT.ltText, CI.ciID, PT.ptName, NT.ntID, NT.ntText " ', PAR.prName"
    sSQL = sSQL & " FROM ((((tblCartItem AS CI LEFT JOIN tblProduct AS PR ON CI.keyFprID=PR.prID)"
    sSQL = sSQL & " LEFT JOIN tblLeadTime AS LT ON PR.keyFltID=LT.ltID)"
    sSQL = sSQL & " LEFT JOIN tblProductType AS PT ON PR.keyFptID = PT.ptID)"
    sSQL = sSQL & " LEFT JOIN tblNote as NT ON CI.keyFntID = NT.ntID)"
    'sSQL = sSQL & " LEFT JOIN tblProduct as PAR ON PR.prParentID = PAR.prID)"
    sSQL = sSQL & " WHERE CI.keyFctID=" & nCtID 
    sSQL = sSQL & " AND CI.keyFinID=" & INST_ID 
    sSQL = sSQL & " AND PR.keyFinID=" & INST_ID 
    sSQL = sSQL & " AND ( LT.keyFinID IS NULL OR LT.keyFinID=" & INST_ID & ")"
    sSQL = sSQL & " AND PR.prDisplay=1 AND PR.prOnSale=1"
    sSQl = sSQL & " ORDER BY CI.ciID ASC;"

	
    'sSQL = "SELECT PR.*, CI.ciQuantity, LT.ltText, CI.ciID, CI.keyFntID FROM (tblCartItem AS CI LEFT JOIN tblProduct AS PR ON CI.keyFprID=PR.prID) LEFT JOIN tblLeadTime AS LT ON PR.keyFltID=LT.ltID"
    'sSQL = sSQL & " WHERE CI.keyFctID=" & nCtID & " AND CI.keyFinID=" & INST_ID & " AND PR.keyFinID=" & INST_ID & " AND ( ISNULL(LT.keyFinID) OR LT.keyFinID=" & INST_ID & ")"
    'sSQL = sSQL & " AND PR.prDisplay=True AND PR.prOnSale=True"
    'sSQl = sSQL & " ORDER BY CI.ciID ASC;"
	'response.Write "sSQl=" & sSQl 
    nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aCartItems)
   

 
		
		
    if nErr = 0 then
 %>


	<form class="product_form" name="shoppingBasket" action="./index.asp?act=upd" method="POST">
	
        <table class="basket_table" summary="Your shopping basket">
        <tr class="header" >
            <th>&nbsp;Product</th>
           
            <th class="quantity">Qty</th>
           
            <th class="price">Price</th>
            
            <th class="sub_total">Sub&nbsp;Total</th>
           
            <th>&nbsp;</th>
        </tr>
<%
dim nMeID
for nIdx = 0 to UBound(aCartItems, 2)
    '// added for wishlist - Dru 14/09/2006 //
    sWishList = ""
    nNtID = aCartItems(PR_UBOUND + 5 , nIdx)
	
    if nNtID > 0 then
        sNote = getNote(nNtID)
        if (instr("" & sNote, "WISHLIST:")) then
            '// wishlist items //
            aTmpWL = split("" & sNote, ":")
            nWlID = clng("0" & aTmpWL(1))
            if nWlID > 0 then
                'nErr = loadWishList(nWlID, aWish)
                'if nErr = 0 then
                    bHasWishItems = true
                    sWishList = "<span class=""basket_wishlist_item"">*WISHLIST: " & aTmpWL(2) & "</span>"
                    'sWishList = "<span class=""basket_wishlist_item"">*WISHLIST: " & aWish(WL_NAME, 0) & "</span>"
                'end if
            end if
            aTmpWL = null
        end if
    end if   
    sNote = "" 
    aWish = null
    '// end of wishlist addition //
    
	nPrdID = aCartItems(PR_ID, nIdx)
	nCiID = aCartItems(PR_UBOUND + 3, nIdx)
    sName = "<strong>" & aCartItems(PR_NAME, nIdx) & "</strong>"  
    
   
     
   If CBool(aCartItems(PR_ISCHILD, nIdx)) Then
		
		sImg = "PR_" & aCartItems(PR_PARENTID, nIdx) & "_TH.jpg"
	
	Else
		 sImg = aCartItems(PR_IMAGETHUMB, nIdx)
   End If
    ' // This code adds a note but this clients product data will already contain the colour and size of the item so this is not needed
	'if len("" & aCartItems(PR_UBOUND + 6, nIdx)) > 0 then
	'	sName = sName & " " & aCartItems(PR_UBOUND + 6, nIdx)
	'end if
   
    if len("" & sImg) > 0 then
        sImg = "<img src=""/ecomm/graphics/" & sImg & """ width=""133"" alt=""" & aCartItems(PR_NAME, nIdx) & """ border=""0"" />"
    end if
    
    if len("" & aCartItems(PR_SIZE, nIdx)) > 0 then
        sName = sName & "<br />" & aCartItems(PR_SIZE, nIdx)
    end if
    if len("" & aCartItems(PR_COLOUR, nIdx)) > 0 then
        sName = sName & "<br />" & aCartItems(PR_COLOUR, nIdx)
    end if
    
    ' ++++++++++++ 
    ' if delivery restrictions apply and we can detect any member loggied in details at this point
    nMeID   = CLng("0" & session("meID"))
    if nMeID > 0 and aCartItems(PR_PASSPARMS,nIdx) then
        nErr = loadMember(nMeID, aMember)
        if nErr = 0 and (not isnull(aMember)) and (isarray(aMember)) then
            '// now lets calculate postage etc //
            if aMember(ME_USEALTADDRESS, 0) then
                nPostZone           = mid(aMember(ME_ALTADDRESS5, 0), 4, 1)
            else
                nPostZone           = mid(aMember(ME_ADDRESS5, 0), 4, 1)
            end if    
            
            ' and customer"s delivery zone falls outside the uk
            if nPostZone <> 9 then
                ' remove the invalid cart item and inform customer
                response.Redirect "./index.asp?act=rem&ci=" & nCiID & "&qty=" & nQty & "&prname=" & aCartItems(PR_NAME, nIdx)
            end if
        end if 
    else
        if aCartItems(PR_PASSPARMS,nIdx) then
            sName = sName & "<br /><small><strong>Delivery restrictions may apply</strong> - Please</small> <a href=""/shoppingbasket/login.asp"" title=""login"">login</a>, <small>for clarification based on your delivery address.</small>"
        end if
    end if     
    ' ++++++++++++  
   
    
    sLeadTime = aCartItems(PR_UBOUND + 2, nIdx)
    
    'if aCartItems(PR_STOCKQTY, nIdx) >= aCartItems(PR_STOCKTRIGGER, nIdx) then
    'if aCartItems(PR_STOCKQTY, nIdx) > aCartItems(PR_STOCKTRIGGER, nIdx) then
    '  sLeadTime = DFLT_LEADTIME_IS_ECOMM
    'elseif len("" & sLeadTime) = 0 then
    '  sLeadTime = DFLT_LEADTIME_OS
    'end if
    
    ' ================================================ 
    ' Put all the fields of the current array row into an array for the bespoke code to use
    for nFieldIdx = 0 to ubound(aCartItems, 1)
       ' Need this to stop population of aProduct when all the product db fields are retrieved from the aCartItems datasat
       if nFieldIdx <= ubound(aProduct) then
            aProduct(nFieldIdx) = aCartItems(nFieldIdx,nIdx)
       end if
    next
                    
    nErr = getAppropriatePrice(aProduct, nPrice, nOutputVAT, nOriginalPrice, nOriginalVAT)                    
   
    ' ================================================ 
    
    if nErr = 0 then
	
    nQty                        = aCartItems(PR_UBOUND + 1, nIdx)
    if isnull(nQty) then
    	nQty = 1
    end if
    
    sShortCode                  = aCartItems(PR_SHORTCODE, nIdx)
    nTotQty                     = nTotQty + nQty

    nSubTotal                   = nQty * nPrice
    'response.write("Example" & nQty)
    nTotal                      = nTotal + nSubTotal
    
else
'response.write("There was an error")
    end if

    'session("numItemsInCart")   = nTotQty
    'session("CartTotal")   = formatnumber(nTotal, 2)
    '??? dunno if it go here session("stockDone") = ""
%>
<tr >
    <td align="center" valign="top">
        
<%
if sImg = "" then
    if aCartItems(PR_ISCHILD, nIdx) then
        nViewProdId = aCartItems(PR_PARENTID, nIdx)
    else
        nViewProdId = nPrdID
    end if 
   ' response.Write "<br/><a href=""/products/product_detail.asp?" & buildParms("",nCatID,nRngID,nGrpID,nColID,nViewProdId) & """>" & sName & "</a>"
else
   
	
	'Response.Write aCartItems(PR_ISCHILD, nIdx) & "<br>"
	If CBool(aCartItems(PR_ISCHILD, nIdx)) Then
		
		response.Write "<a href=""/products/details/?" 
		'Response.Write aCartItems(PR_ID, nIdx)
		  sSQL = "SELECT     tblRangeToCategory.keyFcaID FROM tblRangeToCategory INNER JOIN " & _
		"tblProductToRange ON tblRangeToCategory.keyFrnID = tblProductToRange.keyFrnID " & _
		"WHERE     (tblProductToRange.keyFprID = " & aCartItems(PR_PARENTID, nIdx) & ");"
		'Response.Write sSQL & "<br/>"

		nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aCatId)

		response.Write buildParms("",aCatId(0,0) ,nRngID,nGrpID,nColID,aCartItems(PR_PARENTID, nIdx))
	
	Else
	
		response.Write "<a href=""/categories/?" 

		  sSQL = "SELECT     tblRangeToCategory.keyFcaID FROM tblRangeToCategory INNER JOIN " & _
		"tblProductToRange ON tblRangeToCategory.keyFrnID = tblProductToRange.keyFrnID " & _
		"WHERE     (tblProductToRange.keyFprID = " & aCartItems(PR_ID, nIdx) & ");"
		'Response.Write sql & "<br/>"

		nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aCatId)

		response.Write buildParms("",aCatId(0,0),nRngID,nGrpID,nColID,0)
	
	End If
	
	response.Write """>" & sImg & "</a><br />"
    response.Write sName
end if
if len("" & sShortCode) > 0 then
    response.Write "<br />Code: <strong>" & sShortCode & "</strong><br />"
end if

if len(aCartItems(PR_MISC1,nIdx)) > 0 then
	Response.Write  "&nbsp;-&nbsp;" & aCartItems(PR_MISC1,nIdx) & "" & vbcrlf
End If

' // dru - wishlist items //
if len("" & sWishList) > 0 then
    response.write sWishList
else
    'response.Write "<small>Delivery - " & sLeadTime & "</small>"
end if
%>
    </td>
   
    <td class="quantity"><br /><input size="3" type="text" name="qty-<%=nIdx%>" value="<%=nQty%>" /><input type="hidden" name="id-<%=nIdx%>" value="<%=session("ctID")%>" /></td>
  
    <td class="price"><br /><b><%=getPriceForexHTML(nPrice, 2)%></b></td>
   
    <td class="sub_total"><br /><b><%=getPriceForexHTML(nSubTotal, 2)%></b></td>
   
    <td align="right">
        <br /><a href="?act=rem&amp;ci=<%=nCiID%>&amp;qty=<%=nQty%>">
        <img src="/ecomm/buttons/button_remove.gif" border="0" alt="Remove this item from your shopping basket" />
    </a>
    </td>
</tr>
<%
next
%>
    <tr >
        <td>&nbsp;</td>
        <td>&nbsp;</td>
       
        <td  class="sub_total"><strong>Total</strong></td>
       
        <td class="sub_total"><strong><%=getPriceForexHTML(nTotal, 2)%></strong><input type="hidden" name="nItems" value="<%=nIdx%>" /></td>
      
    </tr>
      
</table>

<p class="help_text">
	<strong>Please update your order if you have amended any quantities.</strong>
</p>
<input type="image" src="/ecomm/buttons/button_update.gif" class="no_border" name="submit" value="Update" /> 
<a href="<%=sLinkCont%>"><img src="/ecomm/buttons/button_continueshopping.gif" alt="Continue shopping" class="no_border" /></a>
<a href="<%=sLinkCheckOut%>"><img src="/ecomm/buttons/button_proceedtocheckout.gif" border="0" alt="Proceed to checkout" class="no_border" /></a> 
</form>




<%
        else
            session("isSuccessYes") = ""
            session("hasBeen")      = ""
            sTmp = "<p class=""help_text""><strong>Your shopping basket is currently empty.</strong></p><a href=""" & sLinkCont & """><img src=""/ecomm/buttons/button_continueshopping.gif"" alt=""Continue shopping"" class=""no_border"" /></a>"
        end if
    else
        session("isSuccessYes") = ""
        session("hasBeen")      = ""
        sTmp = "<p class=""help_text""><strong>Your shopping basket is currently empty.</strong></p><a href=""" & sLinkCont & """><img src=""/ecomm/buttons/button_continueshopping.gif"" alt=""Continue shopping"" class=""no_border"" /></a>"
    end if
    showCart = sTmp
end function
%>

<%
'// cms content id setting for 1st content item that will hold the meta tags
Dim nCntId 

if len("" & request.querystring("id")) > 0 then
    nCntId = request.QueryString("id")
else
    nCntId = CMS_CONTENT_ID_HOME
end if
%>

<!--#include virtual="/includes/doctype.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
     <%=GetDefaultEcommMetaTags("Basket - " & SHOP_NAME)%>  
    <!--#include virtual="/includes/mainCSS.asp" -->
	  <!--#include virtual="/includes/javascript.asp" -->
</head>

<!--#include virtual="/includes/templateStart.asp" -->
<!--#include virtual="/includes/templateTopNav.asp" -->
<!--#include virtual="/includes/templateLeftNav.asp" -->


<!--#include virtual="/includes/templateBeginMainContent.asp" -->
	
<!-- Begin Content -->
    
   <div id="breadcrumb">
		You are in: <span class="coloured">Shopping basket</span>
	</div>
    <%
	IF SANDBOX AND (NVISAGE_IP <> REMOTE_HOST) Then
		%>
<br>		
	<p><strong>Sorry but the shopping cart is offline until the all the site's components are put in place. We hope to get this completed by the end of business on Monday 20th August .</strong></p>	
	<br>

	<p>We are still able to take orders directly over the phone or via email until this takes place.</p>
	
	<p>For any questions or technical help please ring Martin Ashmore - Mobile 07791-605722 Mon - Sat 9am until 9pm and Sun 1pm until 5pm.</p>

	<p>Or send us an email at <a href="mailto:info@care-and-comfort.co.uk" title="email us">info@care-and-comfort.co.uk</a>.</p>
	
	<p>Thank you , the Care and Comfort team.</p>
		<%   
	Else
		 
    
    
    dim sRemovedProduct : sRemovedProduct = request.QueryString("prname")
    if len("" & sRemovedProduct) > 0 then
        response.Write "<br /><p class=""red_text"">Item removed: " & sRemovedProduct & DELIVERY_AVAILABILITY_MSG & "<br /><br />"
    end if
    %>
     
    <%=showCart(nCtID, BSKT_HDR_CLR, BSKT_ROW_CLR, BSKT_BGD_CLR)%>
	<%
	End IF
	%>

<!-- End Content -->
<!--#include virtual="/includes/templateEndMainContent.asp" -->

<!--#include virtual="/includes/templateRightColumn.asp" -->

<!--#include virtual="/includes/templateFooter.asp" -->
<!--#include virtual="/includes/templateEnd.asp" -->


