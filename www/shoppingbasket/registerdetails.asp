<!--#include virtual="/includes/careandcomfortAPI.asp"-->
<!--#include virtual="/ecomm/setup.asp"-->
<!--#include virtual="/ecomm/countryselector.asp"-->
<!--#include virtual="/includes/dates.asp" -->
<%
dim nErr:nErr=0
dim nMeID, nCtID

nMeID = CLng("0" & session("meID"))
if nMeID = 0 then response.redirect "./login.asp"

nErr = loadMember(nMeID, aMember)
if nErr = 0 then
  '// check that we have enough details for ecomm - if not go back to register.asp //
  if  (len("" & aMember(ME_EMAIL, 0)) = 0) then
  	response.redirect "./register.asp"
  end if
else
  response.redirect "./login.asp"
end if
%>
<!--#include virtual="/includes/doctype.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title><%=SHOP_NAME%></title>
    <!--#include virtual="/includes/mainCSS.asp" -->
    <script language="JavaScript" type="text/javascript">
        <!--
        function showTandC(){
	        var tandcWin;
	        tandcWin = window.open("/termsandconditions/index.asp","TandC","height=600,width=790,scrollbars,menubar,resizable");
	        return false;
        }
        //-->
    </script>
    <%=javaSetDropDown("registration")%>
</head>
<!--#include virtual="/includes/body.asp" -->
<!--#include virtual="/includes/templateStart.asp" -->
<!--#include virtual="/includes/templateTopNav.asp" -->
<!--#include virtual="/includes/templateBeginContent.asp" -->
<!-- Begin Content --> 
    <div class="contact_block"> 
      <%=nvCmsGetContentHTML(CMS_CONTENT_ID_CONTACT_SUMMARY)%>     
    </div> 
    
    <!--#include virtual="/includes/templateBasketbreadCrumb.asp" -->  
    
<h1 class="no_display">Your Details</h1>

<div id="center_text_block">  
    <h2>Your Details</h2>

	<div class="form_indentation">
        <form name="Registration" id="Registration" method="POST" action="./register.asp">
        <input type="hidden" name="submitted" value="yes" />
        <input type="hidden" name="meID" value="<%=nMeID%>" />
        <table width="400" border="0" cellpadding="0" cellspacing="0" class="text">
        <tr>
        <td colspan="2"><a name="Details"></a><div class="form_title">&nbsp;Your details</div></td></tr>
        <tr>
        <td width="100">&nbsp;</td>
        <td>&nbsp;</td></tr>
        <tr>
        <td valign="top"><p>Email Address:</p></td>
        <td valign="top"><p><%=aMember(ME_EMAIL, 0)%></p></td></tr>
        <tr>
        <td valign="top"><p>Title (Mr/Ms/etc.):</p></td>
        <td valign="top"><p><%=aMember(ME_TITLE, 0)%></p></td></tr>
        <tr>
        <td valign="top"><p>First Name</p></td>
        <td valign="top"><p><%=aMember(ME_FIRSTNAME, 0)%></p></td></tr>
        <tr>
        <td valign="top"><p>Surname:</p></td>
        <td valign="top"><p><%=aMember(ME_SURNAME, 0)%></p></td></tr>
        <tr>
        <td valign="top"><p>Organisation:</p></td>
        <td valign="top"><p><%=aMember(ME_COMPANY, 0)%></p></td></tr>
        <tr>
        <td valign="top"><p>Address Line 1:</p></td>
        <td valign="top"><p><%=aMember(ME_ADDRESS1, 0)%></p></td></tr>
        <tr>
        <td valign="top"><p>Address Line 2:</p></td>
        <td valign="top"><p><%=aMember(ME_ADDRESS2, 0)%></p></td></tr>
        <tr>
        <td valign="top"><p>Town/City:</p></td>
        <td valign="top"><p><%=aMember(ME_ADDRESS3, 0)%></p></td></tr>
        <tr>
        <td valign="top"><p>County/State:</p></td>
        <td valign="top"><p><%=aMember(ME_ADDRESS4, 0)%></p></td></tr>
        <tr>
        <td valign="top"><p>Postal/Zip Code:</p></td>
        <td valign="top"><p><%=aMember(ME_POSTCODE, 0)%></p></td></tr>
        <tr>
            <td valign="top">Country:</td>
            <td valign="top">
                <%=getCountry(aMember(ME_ADDRESS5, 0))%>
            </td>
        </tr>      
        
        <tr>
        <td valign="top"><p>Telephone Number:</p></td>
        <td valign="top"><p><%=aMember(ME_TEL, 0)%></p></td></tr>
        <tr>
        <td valign="top"><p>Mobile Number:</p></td>
        <td valign="top"><p><%=aMember(ME_MOB, 0)%></p></td></tr>
        <tr>
        <td valign="top"><p>Fax Number:</p></td>
        <td valign="top"><p><%=aMember(ME_FAX, 0)%></p></td></tr>
        <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td></tr>
        <tr>
        <td align="right"><p><%=getTickCrossIcon(not aMember(ME_USEALTADDRESS, 0))%>&nbsp;&nbsp;</p></td>
        <td align="left"><p>Use this address for delivery?</p></td></tr>
        <td>&nbsp;</td>
        <td align="center">&nbsp;</td></tr>
        <tr>
        <td colspan="2"><a name="Altdetails"></a><div class="form_title">&nbsp;Delivery Address if different</div></td></tr>
        <tr>
        <td>&nbsp;</td>
        <td align="center">&nbsp;</td></tr>
        <tr>
        <td valign="top"><p>Title (Mr/Ms/etc.):</p></td>
        <td valign="top"><p><%=aMember(ME_ALTTITLE, 0)%></p></td></tr>
        <tr>
        <td valign="top"><p>First Name:</p></td>
        <td valign="top"><p><%=aMember(ME_ALTFIRSTNAME, 0)%></p></td></tr>
        <tr>
        <td valign="top"><p>Surname:</p></td>
        <td valign="top"><p><%=aMember(ME_ALTSURNAME, 0)%></p></td></tr>
        <tr>
        <td valign="top"><p>Address Line 1:</p></td>
        <td valign="top"><p><%=aMember(ME_ALTADDRESS1, 0)%></p></td></tr>
        <tr>
        <td valign="top"><p>Address Line 2:</p></td>
        <td valign="top"><p><%=aMember(ME_ALTADDRESS2, 0)%></p></td></tr>
        <tr>
        <td valign="top"><p>Town/City:</p></td>
        <td valign="top"><p><%=aMember(ME_ALTADDRESS3, 0)%></p></td></tr>
        <tr>
        <td valign="top"><p>County/State:</p></td>
        <td valign="top"><p><%=aMember(ME_ALTADDRESS4, 0)%></p></td></tr>
        <tr>
        <td valign="top"><p>Postal/Zip Code:</p></td>
        <td valign="top"><p><%=aMember(ME_ALTPOSTCODE, 0)%></p></td></tr>
                
        <tr>
            <td valign="top">Country:</td>
            <td valign="top">
                <%=getCountry(aMember(ME_ALTADDRESS5, 0))%>
            </td>
        </tr>    
        
        <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td></tr>
        <tr>
        <td align="right"><p><%=getTickCrossIcon(aMember(ME_USEALTADDRESS, 0))%>&nbsp;&nbsp;</p></td>
        <td align="left"><p>Use this address for delivery?</p></td></tr>
        <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td></tr>
        <tr>
        <td colspan="2"><a name="DataProtect"></a><div class="form_title">&nbsp;Privacy &amp; Data Protection</div></td></tr>
        <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td></tr>
        <tr>
        <td colspan="2">
        <p>Occasionally we would like to let customers know about special or limited offers.  We limit these to about 5 per year.<br /></p>
        </td></tr>
        <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td></tr>
        <tr>
        <td align="right" valign="top"><p><%=getTickCrossIcon(not aMember(ME_CONTACTBYUS, 0))%>&nbsp;&nbsp;</p></td>
        <td align="left"><p>Please tick this if you do not want to be included:</p>
        <p>All customer details are retained in strictest confidence, we do not share your details with any third party.<br />
        <a href="\termsandconditions\tandc.asp" target="_blank" onClick="return showTandC();">Read our privacy policy here</a>.</p></td>
        </tr>
        <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td></tr>
        <tr>
        <td align="right"><p><%=getTickCrossIcon(aMember(ME_CONTACTBYOTHER, 0))%>&nbsp;&nbsp;</p></td>
        <td align="left"><p>May our partner organisations contact you?</p></td></tr>
        <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td></tr>
        </table>
        </form>
    </div>
</div>
<!-- End Content -->
<!--#include virtual="/includes/templateEndContent.asp" -->

<!-- LHS navigation -->
<!--#include virtual="/includes/templateLHSnav.asp" -->
<!-- RHS navigation -->
<!--#in clude virtual="/includes/templateRHSnav.asp" -->

<!--#include virtual="/includes/templateFooter.asp" -->
<!--#include virtual="/includes/templateEnd.asp" -->