<!--#include virtual="/includes/careandcomfortAPI.asp"-->
<!--#include virtual="/ecomm/setup.asp"-->
<!--#include virtual="/ecomm/countryselector.asp"-->
<!--#include virtual="/includes/dates.asp" -->

<%
  Dim g_sFldr:g_sFldr="shop"
	Dim id, LHSid
  LHSid=14
  
dim nErr:nErr=0
dim nMeID, nCtID, nOdID

nMeID = CLng("0" & session("meID"))
nCtID = CLng("0" & session("ctID"))
nOdID = CLng("0" & session("odID"))
if (nMeID = 0) or (nCtID = 0) then response.redirect "./"

'// let's update the cart with the member ID //
dim aMember:aMember=null
dim aCart:aCart=null
dim aCartItems:aCartItems=null
dim sSQL:sSQL=""
dim nPostZone:nPostZone=0
dim bVAT:bVAT=true
dim nPostage:nPostage=0
nErr = loadMember(nMeID, aMember)
if nErr > 0 then
  response.redirect "./error.asp"
end if
nErr = loadCart(nCtID, aCart)
if nErr = 0 then
  aCart(CT_KEYFMEID, 0) = nMeID
  saveCart(aCart)
  '// now let's calculate postage, discounts etc //
  if aMember(ME_USEALTADDRESS, 0) then
    nPostZone = mid(aMember(ME_ALTADDRESS5, 0), 4, 1)
    bVAT = (mid(aMember(ME_ALTADDRESS5, 0), 6, 1) = "1")
  else
    nPostZone = mid(aMember(ME_ADDRESS5, 0), 4, 1)
    bVAT = (mid(aMember(ME_ADDRESS5, 0), 6, 1) = "1")
  end if
  sSQL = "SELECT CI.*, PR.keyFpcID, PR.keyFwcID, PR.prPrice, PR.prPromoPrice, PR.prPromo FROM tblCartItem AS CI LEFT JOIN tblProduct AS PR ON CI.keyFprID=PR.prID WHERE CI.keyFctID=" & nCtID & " AND CI.keyFinID=" & INST_ID & ";"
  nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aCartItems)
  if nErr = 0 then
    nErr = calcCartPostage(aCartItems, nPostZone, nPostage)
    if nErr > 0 then
      response.redirect "./error.asp"
    end if
  else
    '// cart is empty //
    response.redirect "./"
  end if
else
  response.redirect "./error.asp"
end if

dim sX
if sAct = "vchr" then
	processVoucherForm sX, nMemberID
end if

%>
<!--#include virtual="/includes/doctype.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title><%=SHOP_NAME%></title>
    <!--#include virtual="/includes/mainCSS.asp" -->
    <script language="JavaScript" type="text/javascript">
        <!--
        function showTandC(){
	        var tandcWin;
	        tandcWin = window.open("/termsandconditions/index.asp","TandC","height=600,width=680,scrollbars,menubar,resizable");
	        return false;
        }
        //-->
    </script>
    <%=javaSetDropDown("registration")%>
</head>
<!--#include virtual="/includes/body.asp" -->
<!--#include virtual="/includes/templateStart.asp" -->
<!--#include virtual="/includes/templateTopNav.asp" -->
<!--#include virtual="/includes/templateBeginContent.asp" -->
<!-- Begin Content --> 
          
<h1 class="no_display">Shopping Basket</h1>

<div id="cart_steps">Your Details &gt; Options &gt; <strong>Confirmation</strong> &gt; Payment &gt; Receipt</div>
<div id="center_text_block">  

<h2>&nbsp;<br />Confirmation<br />&nbsp;</h2>




<%=showCartRO(nCtID, aMember, BSKT_HDR_CLR, BSKT_ROW_CLR, BSKT_BGD_CLR, bVAT, nPostZone, nPostage)%>


<br />&nbsp;<br />
</div>
<!-- End Content -->
<!--#include virtual="/includes/templateEndContent.asp" -->

<!-- LHS navigation -->
<!--#include virtual="/includes/templateLHSnav.asp" -->
<!-- RHS navigation -->
<!--#in clude virtual="/includes/templateRHSnav.asp" -->

<!--#include virtual="/includes/templateFooter.asp" -->
<!--#include virtual="/includes/templateEnd.asp" -->
          
<%
function showCartRO(nCtID, aMember, sHedClr, sRowClr, sBkgClr, bVat, nPostZone, nPostage)
  dim nErr:nErr=0
  dim sTmp:sTmp=""
  dim aCartItems:aCartItems=null
  dim nIdx:nIdx=0
  dim sSQL:sSQL=""
  dim nTotal, nSubTotal, nVat, nTotalVAT
  dim sName, sLeadTime, nPrice, nQty
  dim sVchrDesc, sVchrCode, nVchrDisc, nDiscount
  nIdx = 0
  sTmp = ""
  if len("" & sLinkCont) = 0 then
    sLinkCont = "/shop/"
  end if
  dim sMbrAdd, sDelAdd
  if nCtID > 0 then
    sSQL = "SELECT PR.*, CI.ciQuantity, LT.ltText, CI.ciID, CI.keyFntID FROM (tblCartItem AS CI LEFT JOIN tblProduct AS PR ON CI.keyFprID=PR.prID) LEFT JOIN tblLeadTime AS LT ON PR.keyFltID=LT.ltID"
    sSQL = sSQL & " WHERE CI.keyFctID=" & nCtID & " AND CI.keyFinID=" & INST_ID & " AND PR.keyFinID=" & INST_ID & " AND ( ISNULL(LT.keyFinID) OR LT.keyFinID=" & INST_ID & ")"
    sSQL = sSQL & " AND PR.prDisplay=True AND PR.prOnSale=True"
    sSQl = sSQL & " ORDER BY CI.ciID ASC;"
    nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aCartItems)
    if nErr = 0 then
%>


    
    <table align="center" width="95%" border="0" bgcolor="<%=sBkgClr%>" cellspacing="0" cellpadding="0" class="text">
    <tr bgcolor="<%=sHedClr%>" class="shopping_title">
    <td>&nbsp;</td>
    <td>Description</td>
    <td>&nbsp;</td>
    <td align="center">Quantity</td>
    <td>&nbsp;</td>
    <td align="right">Price</td>
    <td>&nbsp;</td>
    <td align="right">Sub&nbsp;Total</td>
    <td>&nbsp;</td></tr>
<%
dim nNtID, aWish, nWlID, sWishList, bHasWishItems, bHasNormalItems, sTmpWL, aTmpWL
for nIdx=0 to UBound(aCartItems, 2)
    '// added for wishlist - Dru 14/09/2006 //
    sWishList = ""
    nNtID = aCartItems(PR_UBOUND + 4 , nIdx)
    if nNtID > 0 then
      sNote = getNote(nNtID)
      if (instr("" & sNote, "WISHLIST:")) then
        '// wishlist items //
        aTmpWL = split("" & sNote, ":")
        nWlID = clng("0" & aTmpWL(1))
        if nWlID > 0 then
          'nErr = loadWishList(nWlID, aWish)
          'if nErr = 0 then
            bHasWishItems = true
            sWishList = "<span class=""basket_wishlist_item"">*WISHLIST: " & aTmpWL(2) & "</span>"
            'sWishList = "<span class=""basket_wishlist_item"">*WISHLIST: " & aWish(WL_NAME, 0) & "</span>"
          'end if
        end if
        aTmpWL = null
      end if
    end if   
    '// was it a normal item? //
    if len("" & sWishList) = 0 then
      bHasNormalItems = true
    end if
    sNote = "" 
    aWish = null
    '// end of wishlist addition //
    nCiID = aCartItems(PR_UBOUND + 3, nIdx)
    sName = aCartItems(PR_NAME, nIdx)
    if aCartItems(PR_ISCHILD, nIdx) then
      sName = aCartItems(PR_NAME, nIdx) &  " " & aCartItems(PR_SIZE, nIdx) &  " " & aCartItems(PR_COLOUR, nIdx)
    end if
    sLeadTime = aCartItems(PR_UBOUND + 2, nIdx)
    if aCartItems(PR_STOCKQTY, nIdx) >= aCartItems(PR_STOCKTRIGGER, nIdx) then
      sLeadTime = DFLT_LEADTIME_IS
    elseif len("" & sLeadTime) = 0 then
      sLeadTime = DFLT_LEADTIME_OS
    end if
    if aCartItems(PR_PROMO, nIdx) then
      nPrice = aCartItems(PR_PROMOPRICE, nIdx)
      nVAT = aCartItems(PR_PROMOVAT, nIdx)
    else
      nPrice = aCartItems(PR_PRICE, nIdx)
      nVAT = aCartItems(PR_VAT, nIdx)
    end if
    nQty = aCartItems(PR_UBOUND + 1, nIdx)
    nSubTotal = nQty * nPrice
    nTotalVAT = nQty * nVAT
    nTotal = nTotal + nSubTotal
%>
<tr bgcolor="<%=sRowClr%>" class="cartBorder">
<td>&nbsp;</td>
<td valign="top"><%=sName%><br>
<%
'// dru - wishlist items //
if len("" & sWishList) > 0 then
  response.write sWishList
else%>
<small>Delivery - <%=sLeadTime%></small>
<%end if%>
</td>
<td>&nbsp;</td>
<td valign="top" align="center"><%=nQty%></td>
<td>&nbsp;</td>
<td valign="top" align="right">&pound;<%=formatnumber(nPrice, 2)%></td>
<td>&nbsp;</td>
<td valign="top" align="right">&pound;<%=formatnumber(nSubTotal, 2)%></td>
<td>&nbsp;</td></tr>
<%next%>
<%if len("" & session("VCHRCODE")) > 0 then
  sVchrDesc = session("VCHRDESC")
  sVchrCode = session("VCHRCODE")
  nVchrDisc = session("VCHRDISC")
  nDiscount = nTotal * (nVchrDisc / 100)
  nTotal = nTotal - nDiscount%>
<tr bgcolor="<%=sRowClr%>" class="cartBorder">
<td>&nbsp;</td>
<td valign="top"><%=sVchrDesc%><br /><small><%=sVchrCode%></small></td>
<td>&nbsp;</td>
<td valign="top" align="center">&nbsp;</td>
<td>&nbsp;</td>
<td valign="top" align="right"><%=formatnumber(nVchrDisc, 2)%>%</td>
<td>&nbsp;</td>
<td valign="top" align="right">-&pound;<%=formatnumber(nDiscount, 2)%></td>
<td>&nbsp;</td></tr>
<%end if%>
<tr bgcolor="<%=sHedClr%>" class="shopping_title">
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td align="right">Sub&nbsp;Total</td>
<td>&nbsp;</td>
<td align="right">&pound;<%=formatnumber(nTotal, 2)%></td>
<td>&nbsp;</td></tr>


<%if bVAT or (VAT_OPTS = 0) then
  nTotal = nTotal + nPostage
%>
<tr bgcolor="<%=sRowClr%>" class="cartBorder">
<td>&nbsp;</td>
<td colspan="3">VAT Included</td>
<td>&nbsp;</td>
<td align="right">(&pound;<%=formatnumber(nTotalVAT, 2)%>)</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td colspan="1">&nbsp;</td>
</tr>
<%else
  nTotal = nTotal - nTotalVAT + nPostage
%>
<tr bgcolor="<%=sRowClr%>" class="cartBorder">
<td>&nbsp;</td>
<td colspan="3">VAT</td>
<td>&nbsp;</td>
<td align="right">-&pound;<%=formatnumber(nTotalVAT, 2)%></td>
<td>&nbsp;</td>
<td align="right">-&pound;<%=formatnumber(nTotalVAT, 2)%></td>
<td colspan="1">&nbsp;</td>
</tr>
<%end if%>

<tr>
<td>&nbsp;</td>
<td colspan="3">Postage &amp; Packaging</td>
<td>&nbsp;</td>
<td align="right">&pound;<%=formatnumber(nPostage, 2)%></td>
<td>&nbsp;</td>
<td align="right">&pound;<%=formatnumber(nPostage, 2)%></td>
<td colspan="1">&nbsp;</td>
</tr>
<tr bgcolor="<%=sHedClr%>" class="shopping_title">
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td align="right">Total</td>
<td>&nbsp;</td>
<td align="right">&pound;<%=formatnumber(nTotal, 2)%></td>
<td colspan="1">&nbsp;</td>
</tr>

</table>

<%
sMbrAdd=""
sDelAdd=""

sMbrAdd = sMbrAdd & "&nbsp;" & trim("" & aMember(ME_TITLE, 0) & " " & aMember(ME_FIRSTNAME, 0) & " " & aMember(ME_SURNAME, 0)) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aMember(ME_ADDRESS1, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aMember(ME_ADDRESS2, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aMember(ME_ADDRESS3, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aMember(ME_ADDRESS4, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aMember(ME_POSTCODE, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & getCountry(aMember(ME_ADDRESS5, 0)) & "<br />" & vbcrlf
if aMember(ME_USEALTADDRESS, 0) then
  sDelAdd = sDelAdd & "&nbsp;" & trim("" & aMember(ME_ALTTITLE, 0) & " " & aMember(ME_ALTFIRSTNAME, 0) & " " & aMember(ME_ALTSURNAME, 0)) & "<br />" & vbcrlf
  sDelAdd = sDelAdd & "&nbsp;" & aMember(ME_ALTADDRESS1, 0) & "<br />" & vbcrlf
  sDelAdd = sDelAdd & "&nbsp;" & aMember(ME_ALTADDRESS2, 0) & "<br />" & vbcrlf
  sDelAdd = sDelAdd & "&nbsp;" & aMember(ME_ALTADDRESS3, 0) & "<br />" & vbcrlf
  sDelAdd = sDelAdd & "&nbsp;" & aMember(ME_ALTADDRESS4, 0) & "<br />" & vbcrlf
  sDelAdd = sDelAdd & "&nbsp;" & aMember(ME_ALTPOSTCODE, 0) & "<br />" & vbcrlf
  sDelAdd = sDelAdd & "&nbsp;" & getCountry(aMember(ME_ALTADDRESS5, 0)) & "<br />" & vbcrlf
else
  sDelAdd = sMbrAdd
end if
sMbrAdd = replace(sMbrAdd, "&nbsp;<br />", "")
sDelAdd = replace(sDelAdd, "&nbsp;<br />", "")
%>


<table align="center" width="95%" border="0" bgcolor="<%=sBkgClr%>" cellspacing="0" cellpadding="0" class="text">
<tr><td width="45%">&nbsp;</td><td width="10%">&nbsp;</td><td width="45%">&nbsp;</td></tr>
<tr><td bgcolor="<%=sHedClr%>" class="shopping_title">&nbsp;Billing Address&nbsp;</td>
<td class="shopping_title">&nbsp;</td><td bgcolor="<%=sHedClr%>" class="shopping_title">&nbsp;Delivery Address&nbsp;<%if bHasWishItems then response.write "<span class=""basket_wishlist_item"">*</span>"%></td></tr>
<tr bgcolor="<%=sRowClr%>"><td valign="top">
<%=sMbrAdd%>
</td><td>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign="top">
<%=sDelAdd%>
<br />
<%
if bHasWishItems then
  response.write "<span class=""basket_wishlist_item"">* Wishlist items will be delivered to&nbsp;<br />&nbsp;the address of the list owners choice.</span>"
end if
%>
</td></tr>
<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
</table>

<%
dim sPostOpt, nNoteID, sGiftMsg, nNote2ID, sDelMsg, bPartShip
nErr = getPostOptions(nCtID, sPostOpt, nNoteID, sGiftMsg, nNote2ID, sDelMsg, bPartShip)
if nErr = 0 then
sTmp = sTmp & "<table width=""95%"" border=""0"" bgcolor=""" & sBkgClr & """ cellpadding=""0"" cellspacing=""0"" class=""text"" align=""center"">"
sTmp = sTmp & "<tr><td bgcolor=""" & sHedClr & """ class=""shopping_title"">&nbsp;Order Options&nbsp;</td></tr>"
if bPartShip then
sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;Order items may be shipped seperately,&nbsp;</td></tr>"
else
sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;Order items will be shipped as a single parcel,&nbsp;</td></tr>"
end if
if sPostOpt = "24H" then
sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;via special delivery.&nbsp;</td></tr>"
else
sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;via standard delivery.&nbsp;</td></tr>"
end if
if len(sDelMsg) > 0 then
sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;Your order will contain the following delivery instruction:&nbsp;</td></tr>"
sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td><i>" & replace(sDelMsg, vbcrlf, "&nbsp;<br />") & "</i></td></tr>"
end if
if len(sGiftMsg) > 0 then
sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;Your order will contain the following gift message:&nbsp;</td></tr>"
sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td><i>" & replace(sGiftMsg, vbcrlf, "&nbsp;<br />") & "</i></td></tr>"
end if
sTmp = sTmp & "</table>"
end if
response.write sTmp
sTmp = ""
%>





  <%
  if nPostZone = 9 then
  '//only ship to UK//
   %>
		<p>&nbsp;&nbsp;&nbsp;&nbsp;Are these details correct?</p>
		<p>    
    <a href="./pay.asp?PAY=CC"><img src="/ecomm/buttons/button_yescontinue.gif" border="0" alt="Yes, Continue and Pay By Credit Card" class="shopRight" /></a><a href="./index.asp"><img src="/ecomm/buttons/button_noeditorder.gif" border="0" alt="No, Edit Order" class="shopLeft" style="margin-left: 8px; margin-right: 4px;" /></a><a href="./register.asp"><img src="/ecomm/buttons/button_noeditaddress.gif" border="0" alt="No, Edit Address" style="margin-left: 4px; margin-right: 8px;" /></a><a href="./options.asp"><img src="/ecomm/buttons/button_noeditoptions.gif" border="0" alt="No, Edit Delivery Options" /></a>
    </p>
    <!--<p style="margin-left: 12px;margin-right: 12px;"><b>Want to use an Earthworks discount voucher?</b> - these can be entered on the '<b>no, edit options</b>' screen.</p>
		<p style="margin-left: 12px;margin-right: 12px;">Please note that orders of standard postage rated items over &pound;40 are carriage free to UK mainland postcodes. 
		Orders to delivery address's outside the UK mainland will be charged at cost and you will be informed of the supplimental charge due before the goods are dispatched. 
		For details on postage rates please refer to our <a href="/howtoshop/" target="_blank">how to shop</a> page.</p>-->
  <%else %>
		<!-- <p style="margin-left: 12px;margin-right: 12px;"> Due to varying carriage costs when delivering to the EU and the Rest of the World, 
		we are unable, at present, to proceed with this purchase via the website. 
		Please email our customer service staff at the address below and we will contact you to arrange the completion of your purchase.</p>
                  					<table border="0" cellspacing="0" cellpadding="0" style="margin-left: 12px;margin-right: 12px;">
                    					<tr> 
                      						<td valign="top"><img src="/graphics/content_contactus_telephone.gif" width="20" height="21"></td>
                      						<td><img src="/graphics/clearpix.gif" width="10" height="10"></td>
                      						<td><p>Telephone: 01444 248855</p></td>
                    					</tr>
                    					<tr> 
                      						<td valign="top"><img src="/graphics/clearpix.gif" width="1" height="10"></td>
                      						<td><img src="/graphics/clearpix.gif" width="1" height="1"></td>
                      						<td><img src="/graphics/clearpix.gif" width="1" height="1"></td>
                    					</tr>
                    					<tr> 
                      						<td valign="top"><img src="/graphics/content_contactus_email.gif" width="22" height="22"></td>
                      						<td><img src="/graphics/clearpix.gif" width="1" height="1"></td>
                      						<td ><p>Email: <a href="mailto: info@earthworks.co.uk">info@earthworks.co.uk</a></p></td>
                    					</tr>
                    					<tr> 
                      						<td valign="top"><img src="/graphics/clearpix.gif" width="1" height="10"></td>
                      						<td><img src="/graphics/clearpix.gif" width="1" height="1"></td>
                      						<td><img src="/graphics/clearpix.gif" width="1" height="1"></td>
                    					</tr>
                    					<tr> 
                      						<td valign="top"><img src="/graphics/content_contactus_address.gif" width="22" height="22"></td>
                      						<td><img src="/graphics/clearpix.gif" width="1" height="1"></td>
                      						<td valign="top"><p>Earthworks Art &amp; Design Ltd <br>
                          						The Earthworks Building <br>
                          						Sheddingdean Business Park <br>
                          						Burgess Hill <br>
                          						West Sussex RH15 8QY</p>
                        					</td>
                    					</tr>
                    					<tr> 
                      						<td valign="top"><img src="/graphics/clearpix.gif" width="1" height="10"></td>
										  	<td><img src="/graphics/clearpix.gif" width="1" height="1"></td>
                      						<td><img src="/graphics/clearpix.gif" width="1" height="1"></td>
                    					</tr>
                    					<tr> 
                      						<td valign="top"><img src="/graphics/content_contactus_fax.gif" width="22" height="22"></td>
                      						<td><img src="/graphics/clearpix.gif" width="1" height="1"></td>
                      						<td><p>Fax: 01444 250341</p></td>
                    					</tr>
                    					<tr> 
                      						<td valign="top"><img src="/graphics/clearpix.gif" width="1" height="10"></td>
                      						<td><img src="/graphics/clearpix.gif" width="1" height="1"></td>
                      						<td><img src="/graphics/clearpix.gif" width="1" height="1"></td>
                    					</tr>
                  					</table>-->
   <%end if%>
        
<%
    else
      sTmp = "&nbsp;<br />Your shopping basket is currently empty."
    end if
  else
    sTmp = "&nbsp;<br />Your shopping basket is currently empty."
  end if
  showCartRO = sTmp
end function
%>