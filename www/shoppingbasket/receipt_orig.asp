<!--#include virtual="/includes/careandcomfortAPI.asp"-->
<!--#include virtual="/ecomm/setup.asp"-->
<!--#include virtual="/ecomm/countryselector.asp"-->
<!--#include virtual="/cms/setup.asp"-->
<!--#include virtual="/shoppingbasket/protex_functions.asp"-->

<%
dim id
dim nErr            : nErr=0
dim nOdID           : nOdID=0
dim nMeID           : nMeID=0
dim sHeader         : sHeader=""
dim aOrder          : aOrder=null
dim aOrderItems     : aOrderItems=null
dim bSuccess        : bSuccess=false
dim sProtxCrypt     : sProtxCrypt = ""
dim sProtxResult    : sProtxResult = ""
dim sPayType        : sPayType=trim("" & request.querystring("PAY"))

sProtxCrypt = request.querystring("crypt")

if len(sProtxCrypt) > 0 then
    sProtxCrypt = SimpleXor(Base64Decode(sProtxCrypt), EncryptionPassword) 
    'response.Write "<br>sProtxCrypt=" & sProtxCrypt 
    ' // Retrieve the status of the transaction from Protx encrypted result info
    sProtxResult = getToken(sProtxCrypt,"Status")
    'response.Write "<br>sProtxResult=" & sProtxResult 
end if

if len(session("isSuccessYes")) = 0 then
    session("isSuccessYes") = request.querystring("success")
end if

nOdID = unencodeID("" & request.querystring("e"))

'// build the receipt
if nOdID > 0 then
    nErr            = getMemberFromOrderID(nOdID,aMember)
    nMeID           = aMember(ME_ID, 0)
    session("MeID") = nMeID
    
    nErr = loadOrder(nOdID, aOrder)
    if nErr = 0 then
        'bSuccess = aOrder(OD_COMPLETEDALL, 0)
        'aOrder = 
        'if session("isSuccessYes") = "true" and len("" & session("hasBeen")) = 0 then
        if session("isSuccessYes") = "true" then
            bSuccess = true
	        session("hasBeen") = "true"
	        ' // update orders table
	        nErr = makeCCPayment(aOrder, aMember, False, False, False, False, False, False, False, False, False)		
        else
	        bSuccess = false
        end if
    else
        ' // DO NOTHING? HUH
    end if
    
    if bSuccess then
        ' // these are set to nothing after they are populated into the upload forms (top and bottom)
        ' session("ctID") = 0
        ' session("odID") = 0
        
        'nErr = setPostOptions(0, "", 0, "", 0, "", false)
    else
        '// not sure at the moment //
    end if
    '// display the order contents //
    'sOrderHTML = displayOrder(nOrderID,true,BASKET_HDR_CLR,BASKET_ROW_CLR,nItems)
else
	'// the shopping basket is currently empty //
	'sOrderHTML = "no order details to show."
end if

'// Update stock levels - function located in /ecomm/set-up.asp
if bSuccess = true and LEN("" & session("stockDone")) = 0 then
    sSQL = "SELECT CI.*, PR.keyFpcID, PR.keyFwcID, PR.prPrice, PR.prPromoPrice, PR.prPromo, PR.prID, PR.prStockQty,CT.*  FROM ((tblCartItem AS CI  LEFT JOIN tblCart AS CT ON CI.keyFctID = CT.ctID)  LEFT JOIN tblProduct AS PR ON CI.keyFprID=PR.prID) WHERE CT.ctSessionID='"& aOrder(OD_SESSIONID, 0) & "' AND CI.keyFinID=" & INST_ID & ";"
    nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aCartItems)
    for intJ = 0 to ubound(aCartItems, 2)
        nErr = Update_Stock(aCartItems(CI_UBOUND+6,intJ),aCartItems(CI_UBOUND+7,intJ))
    next
    session("stockDone") = "Updated"
end if
%>
<!--#include virtual="/includes/doctype.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title><%=SHOP_NAME%></title>
    <!--#include virtual="/includes/mainCSS.asp" -->
    <script language="JavaScript" type="text/javascript">
    <!--
    function showTandC(){
	    var tandcWin;
	    tandcWin = window.open("/termsandconditions/tandc.asp","TandC","height=600,width=520,scrollbars,menubar,resizable");
	    return false;
    }
    function popReceipt(sURL){
	    var tandcWin;
	    tandcWin = window.open(sURL,"Receipt","height=600,width=520,scrollbars,toolbar,menubar,resizable");
	    return false;
    }
    //-->
    </script>
</head>
<!--#include virtual="/includes/body.asp" -->
<!--#include virtual="/includes/templateStart.asp" -->
<!--#include virtual="/includes/templateTopNav.asp" -->
<!--#include virtual="/includes/templateBeginContent.asp" -->
<!-- Begin Content -->  
    <div class="contact_block"> 
      <%=nvCmsGetContentHTML(CMS_CONTENT_ID_CONTACT_SUMMARY)%>     
    </div> 
    
    <!--#include virtual="/includes/templateBasketbreadCrumb.asp" -->  
    
    <%
    if bSuccess then
        response.write "<h2>Your Receipt</h2>" 
    else
        response.write "<h2>Payment Cancelled</h2>"
    end if
    
    response.write showOrder(nOdId, aMember, BSKT_HDR_CLR, BSKT_ROW_CLR, BSKT_BGD_CLR)

    if NOT bSuccess then
        session("odID") = nOdID
    %>
		<p class="text"><b>Attempt payment again?</b></p>
    
        <a href="./confirm.asp"><img src="/ecomm/buttons/button_yescontinue.gif" border="0" alt="Yes, Continue" /></a></p>
    <%
    end if
    %>
<!-- End Content -->
<!--#include virtual="/includes/templateEndContent.asp" -->

<!-- LHS navigation -->
<!--#include virtual="/includes/templateLHSnav.asp" -->
<!-- RHS navigation -->
<!--#in clude virtual="/includes/templateRHSnav.asp" -->

<!--#include virtual="/includes/templateFooter.asp" -->
<!--#include virtual="/includes/templateEnd.asp" -->
          
<%
function getMemberFromOrderID(nOdID,aMember)
  dim nErr:nErr=0
  dim sSQL:sSQL = ""
  if nOdID > 0 then
    sSQL = "SELECT ME.* FROM tblMember AS ME LEFT JOIN tblOrder AS OD ON OD.keyFmeID=ME.meID" _
        & " WHERE OD.odID=" & nOdID & " AND OD.keyFinID=" & INST_ID & " AND ME.keyFinID=" & INST_ID & ";"
    nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aMember)
  else
    nErr = 1  '// bad input parms //
  end if
  getMemberFromOrderID = nErr
end function

function showOrder(nOdID, aMember, sHedClr, sRowClr, sBkgClr)
dim nErr:nErr=0
dim sTmp:sTmp=""
dim aItems:aItems=null
dim nIdx:nIdx=0
dim sSQL:sSQL=""
dim nTotal, nSubTotal, nVat, nTotalVAT
dim sName, sLeadTime, nPrice, nQty
nIdx = 0
sTmp = ""
dim sMbrAdd, sDelAdd

    if nOdID > 0 then 

    sSQL = "SELECT OD.*,OI.* FROM tblOrder AS OD LEFT JOIN tblOrderItem AS OI ON OD.odID=OI.keyFodID" _
         & " WHERE OD.odID=" & nOdID & " AND OD.keyFinID= "& INST_ID & " AND OI.keyFinID=" & INST_ID & " ORDER BY OI.oiID ASC;"
    nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aItems)
    if nErr = 0 then        
        response.write UPLOAD_IMAGE_REMINDER_RECEIPT 
%>
    <form name="frmUpload" action="/shoppingbasket/upload.asp" class="main_form" method="post">
         <input type="hidden" name="ordernumber" value="<%=encodeID(nOdId)%>">
         <input type="hidden" name="cartid" value="<%=session("ctID")%>">
         <fieldset class="ecomm_buttons">
             <input type="image" src="/ecomm/buttons/button_upload_images.gif" name="submit" value="Upload" tabindex="4" alt="Upload image(s)" />
         </fieldset>
     </form>  

    <h1><%=SHOP_NAME%></h1>

    <p class="text">&nbsp;<br /><b>Thank you for your order.</b></p>
    <p class="text">Confirmation details of this order have been sent to your email address.</p>

  <table align="center" width="100%" border="0" bgcolor="<%=sBkgClr%>" cellspacing="0" cellpadding="0" class="text">
    <tr bgcolor="<%=sHedClr%>" class="cartHeaders">
    <td>&nbsp;</td>
    <td>Description</td>
    <td>&nbsp;</td>
    <td align="center">Quantity</td>
    <td>&nbsp;</td>
    <td align="right">Price</td>
    <td>&nbsp;</td>
    <td align="right">Sub&nbsp;Total</td>
    <td>&nbsp;</td></tr>
<%
for nIdx=0 to UBound(aItems, 2)
%>
    <tr bgcolor="<%=sRowClr%>">
    <td>&nbsp;</td>
                     
    <td valign="top"><%=aItems(OD_UBOUND + 1 + OI_DESCRIPTION, nIdx)%><!--<br><small>Delivery - <%=aItems(OD_UBOUND + 1 + OI_LEADTIMETEXT, nIdx)%></small>--></td>
    <td>&nbsp;</td>
    <td valign="top" align="center"><%=aItems(OD_UBOUND + 1 + OI_QUANTITY, nIdx)%></td>
    <td>&nbsp;</td>
    <td valign="top" align="right">&pound;<%=formatnumber(aItems(OD_UBOUND + 1 + OI_UNITCOST, nIdx), 2)%></td>
    <td>&nbsp;</td>
    <td valign="top" align="right">&pound;<%=formatnumber(aItems(OD_UBOUND + 1 + OI_UNITCOST, nIdx) * aItems(OD_UBOUND + 1 + OI_QUANTITY, nIdx), 2)%></td>
    <td>&nbsp;</td></tr>
<%next%>
<tr bgcolor="<%=sHedClr%>" class="cartHeaders">
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td align="right">Sub&nbsp;Total</td>
<td>&nbsp;</td>
<td align="right">&pound;<%=formatnumber(aItems(OD_TOTALNOVAT, 0) + aItems(OD_VAT, 0), 2)%></td>
<td>&nbsp;</td></tr>


<%if 1 or bVAT or (VAT_OPTS = 0) then
%>
    <tr bgcolor="<%=sRowClr%>">
    <td>&nbsp;</td>
    <td colspan="3">VAT Included</td>
    <td>&nbsp;</td>
    <td align="right">(&pound;<%=formatnumber(aItems(OD_VAT, 0), 2)%>)</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td></tr>
<%else
%>
    <tr bgcolor="<%=sRowClr%>">
    <td>&nbsp;</td>
    <td colspan="3">VAT</td>
    <td>&nbsp;</td>
    <td align="right">-&pound;<%=formatnumber(aItems(OD_VAT, 0), 2)%></td>
    <td>&nbsp;</td>
    <td align="right">-&pound;<%=formatnumber(aItems(OD_VAT, 0), 2)%></td>
    <td>&nbsp;</td></tr>
<%end if%>

<tr>
<td>&nbsp;</td>
<td colspan="3">Postage &amp; Packaging</td>
<td>&nbsp;</td>
<td align="right">&pound;<%=formatnumber(aItems(OD_POSTAGE, 0), 2)%></td>
<td>&nbsp;</td>
<td align="right">&pound;<%=formatnumber(aItems(OD_POSTAGE, 0), 2)%></td>
<td>&nbsp;</td></tr>

<%
if len("" & session("promocode")) > 0 and (session("promocode") <> "") then
    nDiscountedTotal    = formatnumber((nTotal - aItems(OD_PROMODISC, 0)), 4)
    response.Write "nDiscountedTotal=" & nDiscountedTotal
%>
    <tr bgcolor="<%=sRowClr%>">
    <td>&nbsp;</td>
    <td>Discount</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">-&pound;<%=aItems(OD_PROMODISC, 0)%></td>
    <td>&nbsp;</td></tr>

    <tr bgcolor="<%=sHedClr%>" class="cartHeaders">
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">Total</td>
    <td>&nbsp;</td>
    <td align="right">&pound;<%=left(nDiscountedTotal, len(nDiscountedTotal)-2)%></td>
    <td>&nbsp;</td></tr>
<%else%>
    <tr bgcolor="<%=sHedClr%>" class="cartHeaders">
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">Total</td>
    <td>&nbsp;</td>
    <td align="right">&pound;<%=formatnumber(aItems(OD_TOTALCHARGECALC, 0), 2)%></td>
    <td>&nbsp;</td></tr>
<%end if%>
</table>

<%
sMbrAdd=""
sDelAdd=""

sMbrAdd = sMbrAdd & "&nbsp;" & trim("" & aOrder(OD_TITLE, 0) & " " & aOrder(OD_FIRSTNAME, 0) & " " & aOrder(OD_SURNAME, 0)) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aOrder(OD_ADDRESS1, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aOrder(OD_ADDRESS2, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aOrder(OD_ADDRESS3, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aOrder(OD_ADDRESS4, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & aOrder(OD_POSTCODE, 0) & "<br />" & vbcrlf
sMbrAdd = sMbrAdd & "&nbsp;" & getCountry(aOrder(OD_ADDRESS5, 0)) & "<br />" & vbcrlf

sDelAdd = sDelAdd & "&nbsp;" & trim("" & aOrder(OD_DELTITLE, 0) & " " & aOrder(OD_DELFIRSTNAME, 0) & " " & aOrder(OD_DELSURNAME, 0)) & "<br />" & vbcrlf
sDelAdd = sDelAdd & "&nbsp;" & aOrder(OD_DELADDRESS1, 0) & "<br />" & vbcrlf
sDelAdd = sDelAdd & "&nbsp;" & aOrder(OD_DELADDRESS2, 0) & "<br />" & vbcrlf
sDelAdd = sDelAdd & "&nbsp;" & aOrder(OD_DELADDRESS3, 0) & "<br />" & vbcrlf
sDelAdd = sDelAdd & "&nbsp;" & aOrder(OD_DELADDRESS4, 0) & "<br />" & vbcrlf
sDelAdd = sDelAdd & "&nbsp;" & aOrder(OD_DELPOSTCODE, 0) & "<br />" & vbcrlf
sDelAdd = sDelAdd & "&nbsp;" & getCountry(aOrder(OD_DELADDRESS5, 0)) & "<br />" & vbcrlf

sMbrAdd = replace(sMbrAdd, "&nbsp;<br />", "")
sDelAdd = replace(sDelAdd, "&nbsp;<br />", "")
%>

<table align="center" width="100%" border="0" bgcolor="<%=sBkgClr%>" cellspacing="0" cellpadding="0" class="text">
    <tr><td width="45%">&nbsp;</td><td width="10%">&nbsp;</td><td width="45%">&nbsp;</td></tr>
    <tr>
        <td bgcolor="<%=sHedClr%>" class="cartHeaders">&nbsp;Billing Address&nbsp;</td>
        <td >&nbsp;</td>
        <td bgcolor="<%=sHedClr%>" class="cartHeaders">&nbsp;Delivery Address&nbsp;</td>
    </tr>
    <tr bgcolor="<%=sRowClr%>">
        <td valign="top">
            <p><%=sMbrAdd%></p>
        </td>
        <td>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td valign="top">
            <p><%=sDelAdd%></p>
        </td>
    </tr>
    <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
</table>

<% 
dim sPostOpt, nNoteID, sGiftMsg, nNote2ID, sDelMsg, bPartShip
'nErr = getPostOptions(nCtID, sPostOpt, nNoteID, sGiftMsg, nNote2ID, sDelMsg, bPartShip)
if nErr = 0 then
    'sTmp = sTmp & "<table width=""100%"" border=""0"" bgcolor=""" & sBkgClr & """ cellpadding=""0"" cellspacing=""0"" class=""text"">"
    'sTmp = sTmp & "<tr><td bgcolor=""" & sHedClr & """ class=""shopping_title"">&nbsp;Order Options&nbsp;</td></tr>"
    'if bPartShip then
    '    sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;Order items may be shipped seperately,&nbsp;</td></tr>"
    'else
    '    sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;Order items will be shipped as a single parcel,&nbsp;</td></tr>"
    'end if
    'if len("" & sPostOpt) = 0 then
    '    sPostOpt = request.querystring("postOpt")
    'end if
    'if sPostOpt = "24H" then
    '    sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;via special delivery.&nbsp;</td></tr>"
    'else
    '    sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;via standard delivery.&nbsp;</td></tr>"
    'end if
    
     if len(sDelMsg) > 0 then
        sTmp = sTmp & "<table width=""100%"" border=""0"" bgcolor=""" & sBkgClr & """ cellpadding=""2"" cellspacing=""0"" class=""text"">"
        sTmp = sTmp & "<tr><td bgcolor=""" & sHedClr & """ class=""cartHeaders"">&nbsp;Order Options&nbsp;</td></tr>"
        sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;Your order will contain the following delivery instruction:&nbsp;</td></tr>"
        sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td><i>" & replace(sDelMsg, vbcrlf, "&nbsp;<br />") & "</i></td></tr>"
    end if
    
    'if len(sGiftMsg) > 0 then
    '    sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td>&nbsp;Your order will contain the following gift message:&nbsp;</td></tr>"
    '    sTmp = sTmp & "<tr bgcolor=""" & sRowClr & """><td><i>" & replace(sGiftMsg, vbcrlf, "&nbsp;<br />") & "</i></td></tr>"
    'end if
    sTmp = sTmp & "<td>&nbsp;</td></tr></table>"
end if
response.write sTmp
sTmp = ""

    else
      sTmp = "&nbsp;<br /><div class='text'>No details to show.</div>"
    end if
  else
    sTmp = "&nbsp;<br /><div class='text'>No details to show.</div>"
  end if
%>  
        <p><%=SHOP_NAME%> order number: <%=aOrder(OD_ID,0)%></p>
        <p><%=SHOP_NAME%> member number: <%=aOrder(OD_KEYFMEID,0)%></p>
        <p>Order Date: <%=aOrder(OD_DATE,0)%></p>  
<% 
  response.write UPLOAD_IMAGE_REMINDER_RECEIPT 
%>
      <form name="frmUploadBottom" action="/shoppingbasket/upload.asp" class="main_form" method="post">
         <input type="hidden" name="ordernumber" value="<%=encodeID(nOdId)%>">
         <input type="hidden" name="cartid" value="<%=session("ctID")%>">
        <%
        if bSuccess then
            session("ctID") = 0
            session("odID") = 0
        end if
         %>    
         <fieldset class="ecomm_buttons">
            <input type="image" src="/ecomm/buttons/button_upload_images.gif" name="submit" value="Upload" tabindex="5" alt="Upload image(s)" />
         </fieldset>
     </form>  
     <p class="text"><b><a href="./printreceipt.asp?e=<%=encodeID(nOdID)%>" target="_blank" onClick="return popReceipt('./printreceipt.asp?e=<%=encodeID(nOdID)%>')">Print this Receipt</a></b></p>

  <%
  
  showOrder = sTmp
end function

function makeCCPayment(aOrder, aMember, sCCName, sCCType, sCCNo, sCCExpMM, sCCExpYY, sCCIssMM, sCCIssYY, sCCIssNo, sCCChkNo)
    dim nErr:nErr = 0
    dim aItems:aItems = null
    dim sSQL:sSQL=""
    
    aOrder(OD_COMPLETEDALL, 0) = true
    
    '// update dB with what has happened //
    nErr = saveOrder(aOrder)
    if nErr = 0 then
        sSQL = "SELECT OI.* FROM tblOrderItem AS OI" _
            & " WHERE OI.keyFodID=" & aOrder(OD_ID, 0) & " AND OI.keyFinID=" & INST_ID & " ORDER BY OI.oiID ASC;"
        nErr = getRowsBySQL(DSN_ECOM_DB, sSQL, aItems)
    else
        '// what?? //
        exit function
    end if
    if nErr = 0 then
        '// notifyShopOrderPlaced //
        nErr = notifyShopCCOrderPlaced(aOrder, aMember, aItems, sCCName, sCCType, sCCNo, sCCExpMM, sCCExpYY, sCCIssMM, sCCIssYY, sCCIssNo, sCCChkNo)
        '// notifyCustomerOrderConfirmed //
        nErr = notifyCustomerOrderConfirmed(aOrder, aMember, aItems)
    else
    end if
    makeCCPayment = nErr
end function

function notifyShopCCOrderPlaced(aOrder, aMember, aContents, sCCName, sCCType, sCCNo, sCCExpMM, sCCExpYY, sCCIssMM, sCCIssYY, sCCIssNo, sCCChkNo)
	dim nErr:nErr = 0
	dim sTo, sToName, sFrom, sFromName, sSubject, sSubjectWarning
	dim sChargeAdd,sDeliveryAdd,sCustName,sOrderDate,sOrderTime,sOrderMessage,sOrderInstruct,aTmp,i, sProdDesc
	dim sMsg:sMsg = ""
	if (not isNull(aOrder)) and (isArray(aOrder)) then
		if not isNull(aContents) then
			aTmp            = split(aOrder(OD_DATE, 0), " ")
			sOrderDate	    = aTmp(0)
			sOrderTime	    = aTmp(1)
			if UBound(aTmp) = 2 then
				'// also have a PM to add //
				sOrderTime = sOrderTime & " " & aTmp(2)
			end if
			sCustName	 = replace(trim(aOrder(OD_TITLE,0) & " " & aOrder(OD_FIRSTNAME,0) & " " & aOrder(OD_SURNAME,0)), "  ", " ")
			sChargeAdd	 = makeAddress(aOrder(OD_TITLE,0),aOrder(OD_FIRSTNAME,0),aOrder(OD_SURNAME,0) _
									  ,aOrder(OD_ADDRESS1,0),aOrder(OD_ADDRESS2,0),aOrder(OD_ADDRESS3,0) _
									  ,aOrder(OD_ADDRESS4,0),getCountry(aOrder(OD_ADDRESS5,0)),aOrder(OD_POSTCODE,0))
			sDeliveryAdd = makeAddress(aOrder(OD_DELTITLE,0),aOrder(OD_DELFIRSTNAME,0),aOrder(OD_DELSURNAME,0) _
									  ,aOrder(OD_DELADDRESS1,0),aOrder(OD_DELADDRESS2,0),aOrder(OD_DELADDRESS3,0) _
									  ,aOrder(OD_DELADDRESS4,0),getCountry(aOrder(OD_DELADDRESS5,0)),aOrder(OD_DELPOSTCODE,0))
			if aOrder(OD_KEYFN1ID,0) > 0 then
				sOrderMessage = getNote(aOrder(OD_KEYFN1ID,0))
			end if
			if aOrder(OD_KEYFN2ID,0) > 0 then
				sOrderInstruct = getNote(aOrder(OD_KEYFN2ID,0))
			end if
			
            sMsg = sMsg & "You have received the following order. " & vbcrlf
            sMsg = sMsg & vbcrlf
            sMsg = sMsg & "ORDER DATE: "
            sMsg = sMsg & sOrderDate & " " & sOrderTime & vbcrlf
            sMsg = sMsg & vbcrlf
            sMsg = sMsg & "ONLINE ORDER REF: "
            sMsg = sMsg & aOrder(OD_ID, 0) & vbcrlf
            sMsg = sMsg & vbcrlf
            sMsg = sMsg & "FROM:  " & vbcrlf
            sMsg = sMsg & sChargeAdd & vbcrlf
            sMsg = sMsg & vbcrlf

            sMsg = sMsg & "EMAIL: "
            sMsg = sMsg & aOrder(OD_EMAIL, 0) & vbcrlf
            sMsg = sMsg & vbcrlf

            sMsg = sMsg & "TEL NO: "
            sMsg = sMsg & aOrder(OD_PHONE, 0) & vbcrlf
            sMsg = sMsg & vbcrlf

            sMsg = sMsg & "ORDER DETAILS:  " & vbcrlf
	  
	  '// Gift Aid Notification
	  if len(session("giftAid")) > 0 then
	  	sMsg = sMsg & vbcrlf & "I am a UK taxpayer and would like the London Philharmonic Orchestra to reclaim tax on all donations. "  & session("giftAid") & vbcrlf & vbcrlf
	  end if
	  
      '// loop items //
			for i = 0 to UBound(aContents,2)
				sProdDesc = aContents(OI_DESCRIPTION, i)
				if aContents(OI_KEYFN1ID, i) > 0 then
					sProdDesc = sProdDesc & " - " & getNote(aContents(OI_KEYFN1ID, i))
				end if
        sMsg = sMsg & "Order: " & aOrder(OD_ID, 0) & vbcrlf
        sMsg = sMsg & "ITEM CODE: " & aContents(OI_PRODUCTCODE, i) & vbcrlf
        sMsg = sMsg & "ITEM DESCRIPTION: " & replace(replace(replace(replace(sProdDesc,"<b>",""),"</b>",""),"<strong>",""),"</strong>","") & vbcrlf
        sMsg = sMsg & "PRICE: £" & formatnumber(aContents(OI_UNITCOST,i),2) & vbcrlf
        sMsg = sMsg & "QUANTITY: " & aContents(OI_QUANTITY, i) & vbcrlf
        sMsg = sMsg & "LINE VALUE: £" & formatnumber((aContents(OI_UNITCOST,i) * aContents(OI_QUANTITY, i)),2) & vbcrlf
				sMsg = sMsg & vbcrlf
			next
			if len(session("nPerfDisc")) > 0 then
		  		sMsg = sMsg & "DISCOUNT: " & formatnumber(session("nPerfDisc"), 2) & vbcrlf
			end if
			'response.Write "<br> nix OD_TOTALCHARGECALC: " & aOrder(OD_TOTALCHARGECALC,0)
			'response.Write "<br> nix OD_POSTAGE: " & aOrder(OD_POSTAGE,0)
			'response.Write "<br> nix OD_VAT: " & aOrder(OD_VAT,0)
			sMsg = sMsg & "ORDER VALUE £ " & formatnumber(aOrder(OD_TOTALCHARGECALC,0) - (aOrder(OD_POSTAGE,0) + aOrder(OD_VAT,0)),2) & vbcrlf
			sMsg = sMsg & "POST & PACKAGING: £ " & formatnumber(aOrder(OD_POSTAGE,0),2) & vbcrlf
			sMsg = sMsg & "VAT: £ " & FormatNumber(aOrder(OD_VAT,0),2) & vbcrlf
			if len(session("nPerfDisc")) > 0 then
			  sMsg = sMsg & "TOTAL INVOICE VALUE: £ " & FormatNumber(aOrder(OD_TOTALCHARGECALC,0) - session("nPerfDisc") ,2) & vbcrlf
			else
			  sMsg = sMsg & "TOTAL INVOICE VALUE: £ " & FormatNumber(aOrder(OD_TOTALCHARGECALC,0),2) & vbcrlf
			end if
			sMsg = sMsg & vbcrlf
      
			'sMsg = sMsg & "PLEASE PROCESS CARD PAYMENT" & vbcrlf
			'sMsg = sMsg & "CARD NO.    : " & sCCNo & vbcrlf
			'sMsg = sMsg & "CARD NAME   : " & sCCName & vbcrlf
			'sMsg = sMsg & "CARD TYPE   : " & sCCType & vbcrlf
			'sMsg = sMsg & "ISSUE DATE  : " & sCCIssMM & "/" & sCCIssYY & vbcrlf
			'sMsg = sMsg & "EXPIRY DATE : " & sCCExpMM & "/" & sCCExpYY & vbcrlf
			'sMsg = sMsg & "ISSUE NO.   : " & sCCIssNo & vbcrlf
			'sMsg = sMsg & "SECURITY NO.: " & sCCChkNo & vbcrlf
			'sMsg = sMsg & vbcrlf
      
	  sMsg = sMsg & "DELIVER TO:  " & vbcrlf
      sMsg = sMsg & sDeliveryAdd & vbcrlf
      sMsg = sMsg & vbcrlf
      
			sMsg = sMsg & "ADDITIONAL INFORMATION:" & vbcrlf
			sMsg = sMsg & vbcrlf
			sMsg = sMsg & "MESSAGE TO RECIPIENT FROM CUSTOMER:" & vbcrlf
			sMsg = sMsg & sOrderMessage & vbcrlf
			sMsg = sMsg & vbcrlf
			sMsg = sMsg & "DELIVERY INSTRUCTIONS FROM CUSTOMER:" & vbcrlf
			sMsg = sMsg & sOrderInstruct & vbcrlf
			sMsg = sMsg & vbcrlf
			if aMember(ME_CONTACTBYUS, 0) then
				sMsg = sMsg & "MAY WE CONTACT THIS CUSTOMER:  Yes" & vbcrlf
			else
				sMsg = sMsg & "MAY WE CONTACT THIS CUSTOMER:  No" & vbcrlf
			end if
			sMsg = sMsg & vbcrlf
          if POST_OPTS > 0 then
		        if aOrder(OD_POSTTYPE,0) = "24H" then
			        sMsg = sMsg & "SPECIAL DELIVERY:  Yes" & vbcrlf
		        else
			        sMsg = sMsg & "SPECIAL DELIVERY:  No" & vbcrlf
		        end if
          end if
			if aOrder(OD_PARTSHIP,0) then
				sMsg = sMsg & "PARTIAL SHIPMENT ALLOWED:  Yes" & vbcrlf
			else
				sMsg = sMsg & "PARTIAL SHIPMENT ALLOWED:  No" & vbcrlf
			end if
			sMsg = sMsg & vbcrlf   
      
            sTo = EML_SHOPTOEMAIL_ECOMM
			sToName = EML_SHOPTONAME
			sFrom = EML_SHOPFROMEMAIL_ECOMM
			sFromName = EML_SHOPFROMNAME
			
			' Live / pre-live  / dev env?
            if IsEnvironmentLive(sHostHeader) then
                 sSubjectWarning = ""                 
            else
                if IsEnvironmentPreLive(sHostHeader) then
                     sSubjectWarning = "Please ignore (pre-live test): "
                else
                    ' dev env    
                    if IsEnvironmentDev(sHostHeader) then
                       sSubjectWarning = "Please ignore (dev test): "
                    else
                       sSubjectWarning = "(unknown env: "
                    end if
                end if 
            end if             
           
            sSubject = sSubjectWarning & SHOP_NAME & " Order (Order ref: " & aOrder(OD_ID,0) & ")"
			nErr = SendMailEnc(sTo, sToName, sFrom, sFromName, sSubject, sMsg, "", "", false, false)
		else
			nErr = 2
		end if
	else
		nErr = 1
end if
	notifyShopCCOrderPlaced = nErr
end function


function notifyCustomerOrderConfirmed(aOrder, aMember, aContents)
	dim nErr:nErr = 0
	dim sTo, sToName, sFrom, sFromName, sSubject, sSubjectWarning
	dim sChargeAdd,sDeliveryAdd,sCustName,sOrderDate,sOrderTime,sOrderMessage,sOrderInstruct,aTmp,i, sProdDesc
	dim sMsg:sMsg = ""
	if (not isNull(aOrder)) and (isArray(aOrder)) then
		if not isNull(aContents) then
			aTmp = split(aOrder(OD_DATE, 0), " ")
			sOrderDate	 = aTmp(0)
			sOrderTime	 = aTmp(1)
			if UBound(aTmp) = 2 then
				'// also have a PM to add //
				sOrderTime = sOrderTime & " " & aTmp(2)
			end if
			if len("" & aOrder(OD_FIRSTNAME,0)) > 0 then
				sCustName	 = aOrder(OD_FIRSTNAME,0)
			else
				sCustName	 = replace(trim(aOrder(OD_TITLE,0) & " " & aOrder(OD_FIRSTNAME,0) & " " & aOrder(OD_SURNAME,0)), "  ", " ")
			end if
			sChargeAdd	 = makeAddress(aOrder(OD_TITLE,0),aOrder(OD_FIRSTNAME,0),aOrder(OD_SURNAME,0) _
									  ,aOrder(OD_ADDRESS1,0),aOrder(OD_ADDRESS2,0),aOrder(OD_ADDRESS3,0) _
									  ,aOrder(OD_ADDRESS4,0),getCountry(aOrder(OD_ADDRESS5,0)),aOrder(OD_POSTCODE,0))
			sDeliveryAdd = makeAddress(aOrder(OD_DELTITLE,0),aOrder(OD_DELFIRSTNAME,0),aOrder(OD_DELSURNAME,0) _
									  ,aOrder(OD_DELADDRESS1,0),aOrder(OD_DELADDRESS2,0),aOrder(OD_DELADDRESS3,0) _
									  ,aOrder(OD_DELADDRESS4,0),getCountry(aOrder(OD_DELADDRESS5,0)),aOrder(OD_DELPOSTCODE,0))
			if aOrder(OD_KEYFNID,0) > 0 then
				sOrderMessage = getNote(aOrder(OD_KEYFNID,0))
			end if
			if aOrder(OD_KEYFN2ID,0) > 0 then
				sOrderInstruct = getNote(aOrder(OD_KEYFN2ID,0))
			end if
            sMsg = sMsg & "Dear " & sCustName & "," & vbcrlf
            sMsg = sMsg & vbcrlf
            sMsg = sMsg & "Thank you for your order which will be processed as soon as possible. Your" _
                        & " order details are listed below - if there are any changes you need to make," _
                          & " please contact us without delay either by replying to this email or by" _
                          & " phoning us on  " & SHOP_TEL_NO & "." & vbcrlf
            sMsg = sMsg & vbcrlf & vbcrlf
            sMsg = sMsg & UPLOAD_IMAGE_REMINDER_RECEIPT_EMAIL 
            sMsg = sMsg & vbcrlf & vbcrlf
            sMsg = sMsg & "Your order details are as follows:" & vbcrlf
            sMsg = sMsg & vbcrlf
            sMsg = sMsg & "ORDER DATE: "
            sMsg = sMsg & sOrderDate & " " & sOrderTime & vbcrlf
                sMsg = sMsg & vbcrlf
                sMsg = sMsg & "ONLINE ORDER REF: "
            sMsg = sMsg & aOrder(OD_ID, 0) & vbcrlf
                sMsg = sMsg & vbcrlf
                sMsg = sMsg & "FROM:  " & vbcrlf
            sMsg = sMsg & sChargeAdd & vbcrlf
            sMsg = sMsg & vbcrlf

                sMsg = sMsg & "EMAIL: "
            sMsg = sMsg & aOrder(OD_EMAIL, 0) & vbcrlf
            sMsg = sMsg & vbcrlf

                sMsg = sMsg & "TEL NO: "
            sMsg = sMsg & aOrder(OD_PHONE, 0) & vbcrlf
            sMsg = sMsg & vbcrlf

            sMsg = sMsg & "PRODUCT DETAILS:  " & vbcrlf
            '// loop items //
			for i = 0 to UBound(aContents,2)
				sProdDesc = aContents(OI_DESCRIPTION, i)
				if aContents(OI_KEYFN1ID, i) > 0 then
					sProdDesc = sProdDesc & " - " & getNote(aContents(OI_KEYFN1ID, i))
				end if
                'sMsg = sMsg & "Order: " & aOrder(OD_ID, 0) & vbcrlf
                sMsg = sMsg & "ITEM CODE: " & aContents(OI_PRODUCTCODE, i) & vbcrlf
                sMsg = sMsg & "ITEM DESCRIPTION: " & replace(replace(replace(replace(replace(replace(sProdDesc,"<b>",""),"</b>",""),"<strong>",""),"</strong>",""),"<i>",""),"</i>","") & vbcrlf
                sMsg = sMsg & "PRICE: £" & formatnumber(aContents(OI_UNITCOST,i),2) & vbcrlf
                sMsg = sMsg & "QUANTITY: " & aContents(OI_QUANTITY, i) & vbcrlf
                sMsg = sMsg & "LINE VALUE: £" & formatnumber((aContents(OI_UNITCOST,i) * aContents(OI_QUANTITY, i)),2) & vbcrlf
	            sMsg = sMsg & vbcrlf
            next
            'sMsg = sMsg & "DISCOUNT: " & formatnumber(session("nPerfDisc"), 2) & vbcrlf
            sMsg = sMsg & "ORDER VALUE £ " & FormatNumber(aOrder(OD_TOTALCHARGECALC,0) - (aOrder(OD_POSTAGE,0) + aOrder(OD_VAT,0)),2) & vbcrlf
            sMsg = sMsg & "POST & PACKAGING: £ " & FormatNumber(aOrder(OD_POSTAGE,0),2) & vbcrlf
            sMsg = sMsg & "VAT: £ " & FormatNumber(aOrder(OD_VAT,0),2) & vbcrlf
            sMsg = sMsg & "TOTAL INVOICE VALUE: £ " & FormatNumber(aOrder(OD_TOTALCHARGECALC,0),2) & vbcrlf
                       
            'if len(session("nPerfDisc")) > 0 then
			'  sMsg = sMsg & "TOTAL INVOICE VALUE: £ " & FormatNumber(aOrder(OD_TOTALCHARGECALC,0) - session("nPerfDisc") ,2) & vbcrlf
			'else
			'  sMsg = sMsg & "TOTAL INVOICE VALUE: £ " & FormatNumber(aOrder(OD_TOTALCHARGECALC,0),2) & vbcrlf
			'end if
            
            sMsg = sMsg & vbcrlf

            sMsg = sMsg & "DELIVER TO:  " & vbcrlf
            sMsg = sMsg & sDeliveryAdd & vbcrlf
            sMsg = sMsg & vbcrlf
      
			if len("" & sOrderMessage) then
				sMsg = sMsg & "Your items will be sent with the following gift message:" & vbcrlf
				sMsg = sMsg & sOrderMessage &  vbcrlf
				sMsg = sMsg & vbcrlf
			end if
			if len("" & sOrderInstruct) then
				sMsg = sMsg & "Your items will be sent with the following delivery instructions:" & vbcrlf
				sMsg = sMsg & sOrderInstruct &  vbcrlf
				sMsg = sMsg & vbcrlf
			end if
			sMsg = sMsg & vbcrlf & vbcrlf
            sMsg = sMsg & UPLOAD_IMAGE_REMINDER_RECEIPT_EMAIL 
            sMsg = sMsg & vbcrlf & vbcrlf
            sMsg = sMsg & "Thank you for shopping at " & SHOP_NAME & "." & vbcrlf & vbcrlf
            sMsg = sMsg & SHOP_NAME & vbcrlf
            sMsg = sMsg & "[W]: " & SITE_URL & vbcrlf
            sMsg = sMsg & "[E]: " & SHOP_TOEMAIL & vbcrlf
            sMsg = sMsg & "[T]: " & SHOP_TEL_NO & vbcrlf
            sMsg = sMsg & vbcrlf
            
            sTo = aOrder(OD_EMAIL,0)
            sToName = trim("" & aOrder(OD_TITLE,0) & " " & aOrder(OD_SURNAME,0) & " " & aOrder(OD_SURNAME,0))
            sFrom = EML_SHOPFROMEMAIL_ECOMM
            sFromName = EML_SHOPFROMNAME
            
            ' Live / pre-live  / dev env?
            if IsEnvironmentLive(sHostHeader) then
                 sSubjectWarning = ""                 
            else
                if IsEnvironmentPreLive(sHostHeader) then
                     sSubjectWarning = "Please ignore (pre-live test): "
                else
                    ' dev env    
                    if IsEnvironmentDev(sHostHeader) then
                       sSubjectWarning = "Please ignore (dev test): "
                    else
                       sSubjectWarning = "(unknown env: "
                    end if
                end if 
            end if             
           
            sSubject = sSubjectWarning & SHOP_NAME & " Order Confirmation (Order ref: " & aOrder(OD_ID,0) & ")"
            nErr = SendMailEnc(sTo, sToName, sFrom, sFromName, sSubject, sMsg, "", "", false, false)
		else
			nErr = 2
		end if
	else
		nErr = 1
	end if
	notifyCustomerOrderConfirmed = nErr
end function

%>