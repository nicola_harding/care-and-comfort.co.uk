<%' {Any /{cmsscripts or ecommscripts folder}/setup.asp file will always need the matching {ini_..}.asp include statement, paired with it%>
<!--#include virtual="/includes/ini_cms.asp"-->

<!--#include virtual="/admin/cmsscripts/setup.asp"-->

<%
dim sBreadLeftCrumbHtml     : sBreadLeftCrumbHtml = ""
dim sBreadTopCrumbHtml      : sBreadTopCrumbHtml = ""
dim sBreadFooterCrumbHtml   : sBreadFooterCrumbHtml = ""
dim sLeftNavHtml            : sLeftNavHtml = ""
dim sTopNavHtml             : sTopNavHtml = ""
dim sBottomNavHtml          : sBottomNavHtml = ""
dim sMainNavHtml            : sMainNavHtml = ""

sTopNavHtml     = nvCmsGetNavigationForDisplay(2, "top", nCntId, sBreadTopCrumbHtml)
sMainNavHtml    = nvCmsGetNavigationForDisplay(4, "main", nCntId, sBreadTopCrumbHtml)
sLeftNavHtml    = nvCmsGetNavigationForDisplay(1, "left", nCntId, sBreadLeftCrumbHtml)
sFooterNavHtml  = nvCmsGetNavigationForDisplay(3, "footer", nCntId, sBreadFooterCrumbHtml)

if len(sBreadTopCrumbHtml) > 0 then
    sBreadCrumbHtml = sBreadTopCrumbHtml
end if
if len(sBreadLeftCrumbHtml) > 0 then
    sBreadCrumbHtml = sBreadLeftCrumbHtml
end if
if len(sBreadFooterCrumbHtml) > 0 then
    sBreadCrumbHtml = sBreadFooterCrumbHtml
end if


' +++++++++++++++++++++++++++++++++++++++++++++++
 
function getBespokeSubMenuLeft(aFields, pintCurrentBrowsedContentId, pblnIsPreviousParent, pblnIsNextParent, pblnIsBrowsedContentWithinCurrentBranch, pblnIsLastItem, sOut, sBreadCrumbOut)
dim nErr:nErr=0
dim aContentInfo:aContentInfo=null
dim sContentTitle, sContentURL
dim nContentId, sNavTitle, sLastItemFrig
dim sCssStyle:sCssStyle = ""

nContentId = aFields(NAVI_NVI_KEYFCNTID)
sNavTitle = aFields(NAVI_NVI_TITLE)

    if (not isnull(nContentId)) and (len(nContentId) > 0) then        
        nErr = loadContentForNav(cint(nContentId), aContentInfo)
        if nErr = 0 and (not isNull(aContentInfo)) then
            sContentTitle = aContentInfo(0,0)
            sContentURL   = aContentInfo(1,0)
        end if
    else
        nErr = 5 ' Content not configured correctly
    end if    
    if nErr = 0 then
        if cbool(pblnIsBrowsedContentWithinCurrentBranch) = cbool(1) then
            sBreadCrumbOut = sBreadCrumbOut & sNavTitle & "|" & sContentURL & "index.asp?id=" & nContentId & "<>"
        end if        
        if pintCurrentBrowsedContentId = nContentId then
            sCssStyle = " class=""selected"""
        end if   
        if cbool(pblnIsLastItem) = cbool(1) then
            sLastItemFrig = "<br />"
        else   
            sLastItemFrig = ""
        end if
            
        if cbool(pblnIsPreviousParent) = cbool(1) then
            sOut = sOut & "</li>" 
        end if     
        sOut = sOut & "<li" & sCssStyle & "><a href=""" & sContentURL & "index.asp?id=" & nContentId & """ title=""Navigate to: " & sContentTitle & """>" & sNavTitle & sLastItemFrig & "</a>"  
    end if
	sOut = VBNullString
    getBespokeSubMenuLeft = nErr
end function


' -------------------------------------------------------

function getBespokeContentMenuTop(aFields, pintCurrentBrowsedContentId, pblnIsPreviousParent, pblnIsNextParent, pblnIsBrowsedContentWithinCurrentBranch, pblnIsLastItem, sOut, sBreadCrumbOut)
dim nErr:nErr=0
dim aContentInfo:aContentInfo=null
dim sContentTitle, sContentURL
dim nContentId, sNavTitle
dim sCssStyle:sCssStyle = ""
dim nDisplayOrder:nDisplayOrder = 0
	
	'Response.Write UBOUND(aFields) & "<br>"
	'Response.Write pintCurrentBrowsedContentId & "<br>"
	'Response.Write pblnIsPreviousParent & "<br>"
	'Response.Write pblnIsNextParent & "<br>"
	'Response.Write pblnIsBrowsedContentWithinCurrentBranch & "<br>"
	'Response.Write pblnIsLastItem & "<br>"
	'Response.Write sOut & "<br>"
	'Response.Write sBreadCrumbOut & "<br>"
	
    nContentId      = aFields(NAVI_NVI_KEYFCNTID)
    sNavTitle       = aFields(NAVI_NVI_TITLE)
    nDisplayOrder   = aFields(NAVI_NVI_DISPLAYORDER)
    if (not isnull(nContentId)) and (len(nContentId) > 0) then
    
        nErr = loadContentForNav(cint(nContentId), aContentInfo)
        if nErr = 0 and (not isNull(aContentInfo)) then
            sContentTitle = aContentInfo(0,0)
            sContentURL   = aContentInfo(1,0)
        end if       
      
    else
        nErr = 5 ' Content not configured correctly
    end if

	
    'if nErr = 0 then
        if cint(pintCurrentBrowsedContentId) = cint(nContentId) then
            sCssStyle = sCssStyle & " class=""selected"""
            sBreadCrumbOut = sBreadCrumbOut & sNavTitle & "|" & sContentURL & "index.asp?id=" & nContentId & "<>"
        else
            sCssStyle = sCssStyle & ""
        end if  
        sOut = sOut & "<li" & sCssStyle & "><a href=""" & sContentURL & "index.asp?id=" & nContentId & """ title=""Navigate to: " & sContentTitle & """>" & sNavTitle & "</a><div class=""tl""></div><div class=""tr""></div></li>"  
    'end if 
  'Response.Write sOut & "<br>"
    getBespokeContentMenuTop = nErr
end function

' -------------------------------------------------------

function getBespokeContentMenuMain(aFields, pintCurrentBrowsedContentId, pblnIsPreviousParent, pblnIsNextParent, pblnIsBrowsedContentWithinCurrentBranch, pblnIsLastItem, sOut, sBreadCrumbOut)
dim nErr:nErr=0
dim aContentInfo:aContentInfo=null
dim sContentTitle, sContentURL
dim nContentId, sNavTitle
dim sCssStyle:sCssStyle = ""
dim nDisplayOrder:nDisplayOrder = 0
	
	'Response.Write sOut & "<br>"
	'Response.Write pintCurrentBrowsedContentId & "<br>"
	'Response.Write pblnIsPreviousParent & "<br>"
	'Response.Write pblnIsNextParent & "<br>"
	'Response.Write pblnIsBrowsedContentWithinCurrentBranch & "<br>"
	'Response.Write pblnIsLastItem & "<br>"
	'Response.Write sOut & "<br>"
	'Response.Write sBreadCrumbOut & "<br>"

    nContentId      = aFields(NAVI_NVI_KEYFCNTID)
    sNavTitle       = aFields(NAVI_NVI_TITLE)
    nDisplayOrder   = aFields(NAVI_NVI_DISPLAYORDER)
    if (not isnull(nContentId)) and (len(nContentId) > 0) then
    
        nErr = loadContentForNav(cint(nContentId), aContentInfo)
        if nErr = 0 and (not isNull(aContentInfo)) then
            sContentTitle = aContentInfo(0,0)
            sContentURL   = aContentInfo(1,0)
        end if       
      
    else
        nErr = 5 ' Content not configured correctly
    end if
    if nErr = 0 then
        

		'deal with sub sections
		Select Case cint(nContentId)
			'entertainement
			Case 30:
				If cint(pintCurrentBrowsedContentId) = 30 OR  cint(pintCurrentBrowsedContentId) = 31 OR  cint(pintCurrentBrowsedContentId) = 32 OR  cint(pintCurrentBrowsedContentId) = 32 OR  cint(pintCurrentBrowsedContentId) = 38 OR  cint(pintCurrentBrowsedContentId) = 37 Then
					sCssStyle = sCssStyle & " class=""selected"""
				Else
					 sCssStyle = sCssStyle & ""
				End If
			'fashion
			Case 16:
				If cint(pintCurrentBrowsedContentId) = 16 OR  cint(pintCurrentBrowsedContentId) = 34 OR  cint(pintCurrentBrowsedContentId) = 35  OR  cint(pintCurrentBrowsedContentId)  = 40 Then
					sCssStyle = sCssStyle & " class=""selected"""
				Else
					 sCssStyle = sCssStyle & ""
				End If
			'gifts
			Case 20:
				If cint(pintCurrentBrowsedContentId) = 20 OR  cint(pintCurrentBrowsedContentId) = 36 Then
					sCssStyle = sCssStyle & " class=""selected"""
				Else
					 sCssStyle = sCssStyle & ""
				End If
			'accessories
			Case 12:
				If cint(pintCurrentBrowsedContentId) = 12 OR  cint(pintCurrentBrowsedContentId) = 42 OR  cint(pintCurrentBrowsedContentId) = 43  OR cint(pintCurrentBrowsedContentId) = 41    OR  cint(pintCurrentBrowsedContentId) = 47  Then
					sCssStyle = sCssStyle & " class=""selected"""
				Else
					 sCssStyle = sCssStyle & ""
				End If

			Case Else:
				if cint(pintCurrentBrowsedContentId) = cint(nContentId) then
					sCssStyle = sCssStyle & " class=""selected"""
					sBreadCrumbOut = sBreadCrumbOut & sNavTitle & "|" & sContentURL & "index.asp?id=" & nContentId & "<>"
				else
					sCssStyle = sCssStyle & ""
				end if 
		
		End Select

        sOut = sOut & "<li"  
		
		If nContentId = 12 OR nContentId = 20 Then
		
			If Len(Trim(sCssStyle)) = 0 Then
				sCssStyle = sCssStyle & " class=""start"""
			Else
				sCssStyle = sCssStyle & " class=""selected_start"""
			End If
		End If
		If nContentId = 22 Then
			sOut = sOut & " id=""food_wine"""
			
		End If
		
		sOut = sOut & sCssStyle & "><a href=""" & sContentURL & "index.asp?id=" & nContentId & """ title=""Navigate to: " & sContentTitle & """>" & sNavTitle & "</a></li>"  
    end if 
  'Response.Write sOut & "<br>"
    getBespokeContentMenuMain = nErr
end function

function getBespokeContentMenuLeft(aFields, pintCurrentBrowsedContentId, pblnIsPreviousParent, pblnIsNextParent, pblnIsBrowsedContentWithinCurrentBranch, pblnIsLastItem, sOut, sBreadCrumbOut)
dim nErr:nErr=0
dim aContentInfo:aContentInfo=null
dim sContentTitle, sContentURL
dim nContentId, sNavTitle
dim sCssStyle:sCssStyle = ""

    ' Only both showing the navigation branch if the page currently being browsed falls within this nav item branch
    if True then

        nContentId = aFields(NAVI_NVI_KEYFCNTID)
        sNavTitle = aFields(NAVI_NVI_TITLE)
        if (not isnull(nContentId)) and (len(nContentId) > 0) then
            nErr = loadContentForNav(cint(nContentId), aContentInfo)
            if nErr = 0 and (not isNull(aContentInfo)) then
                sContentTitle = aContentInfo(0,0)
                sContentURL   = aContentInfo(1,0)
            end if
        else
            nErr = 5 ' Content not configured correctly
        end if
        if nErr = 0 then            
            if cint(pintCurrentBrowsedContentId) = cint(nContentId) then
                sCssStyle = " class=""selected"""
                sBreadCrumbOut = sBreadCrumbOut & sNavTitle & "|" & sContentURL & "index.asp?id=" & nContentId & "<>"
            end if   
            if cbool(pblnIsLastItem) = cbool(1) then
                sLastItemFrig = "<br />"
            else   
                sLastItemFrig = ""
            end if        
                 
            if cbool(pblnIsPreviousParent) = cbool(1) then
                sOut = sOut & "<ul class=""sub_nav"">"     
            end if
            sOut = sOut & "<li" & sCssStyle & "><a href=""" & sContentURL & "index.asp?id=" & nContentId & """ title=""Navigate to: " & sContentTitle & """>" & sNavTitle & sLastItemFrig & "</a></li>"  
            if cbool(pblnIsNextParent) = cbool(1) then
                'sOut = sOut & "</ul>" 
            end if 
        end if 
    else
        ' if this is not the current nav branch being browsed, and the next item is a parent then we need to include the parent nav items missing tags
        if cbool(pblnIsNextParent) = cbool(1) then
            sOut = sOut & "</li>" 
        end if 
    end if
    getBespokeContentMenuLeft = nErr
end function


function getBespokeContentMenuFooter(aFields, pintCurrentBrowsedContentId, pblnIsPreviousParent, pblnIsNextParent, pblnIsBrowsedContentWithinCurrentBranch, pblnIsLastItem, sOut, sBreadCrumbOut)
dim nErr:nErr=0
dim aContentInfo:aContentInfo=null
dim sContentTitle, sContentURL
dim nContentId, sNavTitle
dim sCssStyle:sCssStyle = ""
dim nDisplayOrder:nDisplayOrder = 0

    nContentId      = aFields(NAVI_NVI_KEYFCNTID)
    sNavTitle       = aFields(NAVI_NVI_TITLE)
    nDisplayOrder   = aFields(NAVI_NVI_DISPLAYORDER)
    
    sContentTitle = sNavTitle
    sContentURL   = aFields(NAVI_NVI_URL)
    
    if (not isnull(nContentId)) and (len(nContentId) > 0) then
        nErr = loadContentForNav(cint(nContentId), aContentInfo)
        if nErr = 0 and (not isNull(aContentInfo)) then
            sContentTitle = aContentInfo(0,0)
            sContentURL   = aContentInfo(1,0)
        end if
    else
        nErr = 5 ' Content not configured correctly
    end if
    
    
    if (isnull(nContentId)) and (len(nContentId) > 0) then
        nErr = 5 ' Content not configured correctly
    end if           
    if nErr = 0 then

        if cint(pintCurrentBrowsedContentId) = cint(nContentId) then
            'sBreadCrumbOut = sBreadCrumbOut & sNavTitle & "|" & sContentURL & "index.asp?id=" & nContentId & "<>"
        end if 
        
        sOut = sOut & "<a href=""" & sContentURL & "index.asp?id=" & nContentId & """  title=""Navigate to: " & sContentTitle & """>" & sNavTitle & "</a>&nbsp;&nbsp;"
        if cbool(pblnIsLastItem) = cbool(1) then
            sOut = sOut & "<br />" 
        else
            select case nDisplayOrder
                case 7
                   ' sOut = sOut & "<br />"  
            end select
        end if
    end if 
  
    getBespokeContentMenuFooter = nErr
end function

function getBespokeSubMenuFooter(aFields, pintCurrentBrowsedContentId, pblnIsPreviousParent, pblnIsNextParent, pblnIsBrowsedContentWithinCurrentBranch, pblnIsLastItem, sOut, sBreadCrumbOut)

end function

' -----------------------------------------------


' *************************************************

function getBespokeURLMenuTop(aFields, pintCurrentBrowsedContentId, pblnIsPreviousParent, pblnIsNextParent, pblnIsBrowsedContentWithinCurrentBranch, pblnIsLastItem, sOut, sBreadCrumbOut)
dim nErr:nErr=0
dim aContentInfo:aContentInfo=null
dim sContentTitle, sContentURL
dim nContentId, sNavTitle
dim sCssStyle:sCssStyle = ""
dim nDisplayOrder:nDisplayOrder = 0

    nContentId      = aFields(NAVI_NVI_KEYFCNTID)
    sNavTitle       = aFields(NAVI_NVI_TITLE)
    nDisplayOrder   = aFields(NAVI_NVI_DISPLAYORDER)
    
    sContentTitle = sNavTitle
    sContentURL   = aFields(NAVI_NVI_URL)
    if (not isnull(nContentId)) or (len(sContentURL) = 0) then
        nErr = 5 ' Content not configured correctly
    end if
    select case nDisplayOrder
        case 1
            sCssStyle = " class=""first"
        case 2
            sCssStyle = " class=""second"
        case 3
            sCssStyle = " class=""third"
        case 4
            sCssStyle = " class=""fourth"
        case 5
            sCssStyle = " class=""fifth"
        case 6
            sCssStyle = " class=""sixth"
        case else
            nErr = 5 ' Content not configured correctly
    end select   
           
    if nErr = 0 then
        if instr(request.ServerVariables("URL") & "?" & request.QueryString, sContentURL) then
            sCssStyle = sCssStyle & " selected"""
            sBreadCrumbOut = sBreadCrumbOut & sNavTitle & "|" & sContentURL & "index.asp?id=" & nContentId & "<>"
        else
            sCssStyle = sCssStyle & """"
        end if      
        sOut = sOut & "<li" & sCssStyle & "><a href=""" & sContentURL & """ title=""Navigate to: " & sContentTitle & """>" & sNavTitle & sLastItemFrig & "</a><div class=""tl""></div><div class=""tr""></div></li>"  
    end if 
  
    getBespokeURLMenuTop = nErr
end function

function getBespokeURLMenuLeft(aFields, pintCurrentBrowsedContentId, pblnIsPreviousParent, pblnIsNextParent, pblnIsBrowsedContentWithinCurrentBranch, pblnIsLastItem, sOut, sBreadCrumbOut)
dim nErr:nErr=0
dim aContentInfo:aContentInfo=null
dim sContentTitle, sContentURL
dim nContentId, sNavTitle
dim sCssStyle:sCssStyle = ""

    ' Only both showing the navigation branch if the page currently being browsed falls within this nav item branch
    if True then
        sNavTitle = aFields(NAVI_NVI_TITLE)
        sContentTitle = sNavTitle
        sContentURL   = aFields(NAVI_NVI_URL)
        if (not isnull(nContentId)) or (len(sContentURL) = 0) then
            nErr = 5 ' Content not configured correctly
        end if
       if nErr = 0 then            
            if pintCurrentBrowsedContentId = nContentId then
                sCssStyle = " class=""selected"""
                sBreadCrumbOut = sBreadCrumbOut & sNavTitle & "|" & sContentURL & "index.asp?id=" & nContentId & "<>"
            end if   
            if cbool(pblnIsLastItem) = cbool(1) then
                sLastItemFrig = "<br />"
            else   
                sLastItemFrig = ""
            end if        
                 
            if cbool(pblnIsPreviousParent) = cbool(1) then
                sOut = sOut & "<ul class=""sub_nav"">"     
            end if
            sOut = sOut & "<li " & sCssStyle & "><a href=""" & sContentURL & """ title=""Navigate to: " & sContentTitle & """>" & sNavTitle & sLastItemFrig & "</a></li>"  
            if cbool(pblnIsNextParent) = cbool(1) then
                sOut = sOut & "</ul></li>" 
            end if 
        end if 
    end if 
    getBespokeURLMenuLeft = nErr
end function

function getBespokeURLMenuFooter(aFields, pintCurrentBrowsedContentId, pblnIsPreviousParent, pblnIsNextParent, pblnIsBrowsedContentWithinCurrentBranch, pblnIsLastItem, sOut, sBreadCrumbOut)
dim nErr:nErr=0
dim aContentInfo:aContentInfo=null
dim sContentTitle, sContentURL
dim nContentId, sNavTitle
dim sCssStyle:sCssStyle = ""
dim nDisplayOrder:nDisplayOrder = 0

    nContentId      = aFields(NAVI_NVI_KEYFCNTID)
    sNavTitle       = aFields(NAVI_NVI_TITLE)
    nDisplayOrder   = aFields(NAVI_NVI_DISPLAYORDER)
    
    sContentTitle = sNavTitle
    sContentURL   = aFields(NAVI_NVI_URL)
    if (not isnull(nContentId)) or (len(sContentURL) = 0) then
        nErr = 5 ' Content not configured correctly
    end if   
    if nErr = 0 then
        select case nDisplayOrder
            case 1, 4, 8, 12
                sCssStyle = " class=""first"""
            case else
                sCssStyle = ""
        end select  
        if instr(request.ServerVariables("URL") & "?" & request.QueryString, sContentURL) then
            sBreadCrumbOut = sBreadCrumbOut & sNavTitle & "|" & sContentURL & "index.asp?id=" & nContentId & "<>"
        end if         
           
        sOut = sOut & "<li " & sCssStyle & "><a href=""" & sContentURL & """ title=""Navigate to: " & sContentTitle & """>" & sNavTitle & sLastItemFrig & "</a>&nbsp;"
        if cbool(pblnIsLastItem) = cbool(1) then
            sOut = sOut & "<br />" 
        else
            select case nDisplayOrder
                case 3, 7, 11
                    sOut = sOut & "<br />"  
            end select
        end if
        sOut = sOut & "</li>"  
    end if 
  
    getBespokeURLMenuFooter = nErr
end function

' *******************************************************


function getBespokeMenuCompleted(pblnIsPreviousParent, sOut)
dim nErr:nErr=0
    getBespokeMenuCompleted = nErr
end function

%>