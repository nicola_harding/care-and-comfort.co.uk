<!--#include virtual="/admin/FCKeditor/fckeditor.asp"-->
<%
sub showEditor(pre, el, html)
'// this file will be included from the generic adminscripts //
'// 3 globals are exposed from that file :- //
'// 'pre' is a boolean value for preview mode (1 preview, 0 edit) //
'// 'el' the name of the form element used by the calling page, this should simply be passively passed back in querystring //
'// 'html' this is the html to be edited and should be posted back by the form //
'// form names should persist as 'el','html' //
'// hidden element 'submitted' value = 'yes' is also assumed to be present //

if pre then
  response.write html & "&nbsp;<br />&nbsp;<br /><a href=""#"" onClick=""window.self.close();"">Close window</a>"
else
%>
<form name="htmlEditor" action="<%=request.servervariables("SCRIPT_NAME")%>?el=<%=el%>" method="post">
  <input type="hidden" name="submitted" value="yes" />
  
<%
  Dim oFCKeditor  
  Set oFCKeditor = New FCKeditor
  oFCKeditor.BasePath = "/admin/FCKeditor/"
  oFCKeditor.ToolbarSet = "Nvisage"
  oFCKeditor.Value = html
  oFCKeditor.Create "html"
%>
      <br>
      <input type="submit" value="SAVE your changes" />
      &nbsp;&nbsp;&nbsp;&nbsp;
      <input type="button" value="CANCEL your changes" onClick="window.self.close();" />

</form>
<p><b>Quick help</b></p>
<p>The editor below has a similar interface to many word processors you may be familiar with, but below is a key to the most important buttons to get you started.</p>
<p>
<img src="/admin/FCKeditor/editor/skins/default/toolbar/save.gif" alt="Save Icon" border="0" />
<b>Save</b> - Click this to publish any changes you may have made.
<br />

<img src="/admin/FCKeditor/editor/skins/default/toolbar/preview.gif" alt="Preview Icon" border="0" />
<b>Preview</b> - Click this to view your page without the editor around it. 
<br />

<img src="/admin/FCKeditor/editor/skins/default/toolbar/templates.gif" alt="Templates Icon" border="0" />
<b>Templates</b> - Click here to select from the available page templates to get you started.
<br />
&nbsp;<br />
<a href="/admin/FCKeditor/editorhelp.asp" target="_blank">Click here for more detailed instructions</a>. 
<br />

</p>
<%
end if

end sub

function showCMSEditor(pre, el, html)
'// this file will be included from the generic adminscripts //
'// 3 globals are exposed from that file :- //
'// 'pre' is a boolean value for preview mode (1 preview, 0 edit) //
'// 'el' the name of the form element used by the calling page, this should simply be passively passed back in querystring //
'// 'html' this is the html to be edited and should be posted back by the form //
'// form names should persist as 'el','html' //
'// hidden element 'submitted' value = 'yes' is also assumed to be present //
dim sTmp:sTmp = ""
if pre then
  sTmp = sTmp &  html & "&nbsp;<br />&nbsp;<br /><a href=""#"" onClick=""window.self.close();"">Close window</a>"
else
  sTmp = sTmp & ""
  Dim oFCKeditor  
  Set oFCKeditor = New FCKeditor
  oFCKeditor.BasePath = "/admin/FCKeditor/"
  oFCKeditor.ToolbarSet = "Nvisage"
  oFCKeditor.Value = html
  '// this outputs directly to response object //
  oFCKeditor.Create el
  sTmp = sTmp & "<p><b>Quick help</b></p>"
  sTmp = sTmp & "<p>The editor below has a similar interface to many word processors you may be familiar with, but below is a key to the most important buttons to get you started.</p>"
  sTmp = sTmp & "<p><img src=""/admin/FCKeditor/editor/skins/default/toolbar/save.gif"" alt=""Save Icon"" border=""0"" />"
  sTmp = sTmp & "<b>Save</b> - Click this to save any changes you may have made.<br />"
  'sTmp = sTmp & "<p><img src=""/admin/FCKeditor/editor/skins/default/toolbar/publish.gif"" alt=""Publish Icon"" border=""0"" />"
  'sTmp = sTmp & "<b>Save</b> - Click this to publish any changes you may have made to the website.<br />"
  sTmp = sTmp & "<img src=""/admin/FCKeditor/editor/skins/default/toolbar/preview.gif"" alt=""Preview Icon"" border=""0"" />"
  sTmp = sTmp & "<b>Preview</b> - Click this to view your page without the editor around it.<br />"
  sTmp = sTmp & "<img src=""/admin/FCKeditor/editor/skins/default/toolbar/templates.gif"" alt=""Templates Icon"" border=""0"" />"
  sTmp = sTmp & "<b>Templates</b> - Click here to select from the available page templates to get you started.<br />&nbsp;<br />"
  sTmp = sTmp & "<a href=""/admin/FCKeditor/editorhelp.asp"" target=""_blank"">Click here for more detailed instructions</a>.<br /></p>"
end if
  showCMSEditor = sTmp
end function
%>