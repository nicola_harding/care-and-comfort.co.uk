<!--#include virtual="/scripts/aspstart.asp"-->
<!--#include virtual="/scripts/adovbs.asp"-->
<!--#include file="./setup.asp"-->
<!--#include virtual="/includes/partnersAPI.asp"-->

<%
dim nPerms, sUT
'sUT = SESSION("USER_TYPE")
'If sUT = "SUR" Then
'	nPerms = PERM_ALL
'Else
'	nPerms = GetPermissions(SESSION("PERMISSIONS"), AREA_DIST)
'End If

'if (nPerms and PERM_READ) = 0 then response.redirect("./")

dim nMemberID, sOperatorCode,sMsg,sUsername
dim nOrder, nMaxCount, pageCur, nTotalPages, thisPage, sControls, pageCount, sWhere
dim aFields
dim nErr:nErr = 0
thisPage = Request.ServerVariables("SCRIPT_NAME")
nErr = 0
nRows = 15
pageSize = CInt("0" & Request.Querystring("sz"))
pageCur = CInt("0" & Request.QueryString("pg"))
newSize = CInt("0" & Request.Querystring("newsz"))
nOrder = CInt("0" & Request.Querystring("o"))
nSup = CLng("0" & Request.Querystring("sup"))
sMsg = ""
if newSize >  0 Then
	pageSize = newSize
End If
If pageSize = 0 Then
	pageSize = 20
End If

dim sTown, sCountry, sCompany
If Request.Form("submitted") = "yes" Then
	'// we are building a new query //
	strSQL = ""
	strSQLWhere = " "
	sUsername = trim("" & request.form("sUsername"))
	pageSize = Request.Form("sz")
	'// start the logic //
	Dim bWhere
	bWhere = false
	If sLocation <> "" Then
		If bWhere = False Then
			strSQLWhere = strSQLWhere & " WHERE PA.paUsername LIKE '" & SQLFixUP(sUsername) & "%'"
		Else
			strSQLWhere = strSQLWhere & " AND PA.paUsername LIKE '" & SQLFixUP(sUsername) & "%'"
		End if
		bWhere = True
	End If
	
	strSQL = "SELECT PA.*, (PA.paFirstName + ' ' + PA.paLastName) AS paFullname" & _
			" FROM tblPartners AS PA" & strSQLWhere & strSQLOrder & ";"
ElseIf Request.QueryString("sql") = "y" Then
	'// see what we have in the session variable SQL //
	strSQLWhere = SESSION("SUB_SQL_WHERE")
	if strSQLWhere = "" Then
		strSQLWhere = " "
	End If
	strSQLOrder = SESSION("SUB_SQL_ORDER")
	if request.querystring("order") <> "" Then
		strSQLOrder = " ORDER BY " & request.querystring("order")
	End If
	if strSQLOrder = "" Then
		strSQLOrder = " ORDER BY PA.paUsername ASC"
	end if
	strSQL = "SELECT PA.*, (PA.paFirstName + ' ' + PA.paLastName) AS paFullname" & _
			" FROM tblPartners AS PA" & strSQLWhere & strSQLOrder & ";"
End If
If Not Len(strSQL) > 0 then
	'// use the default sql string //
	'strSQLWhere = " WHERE VC.vcStatus=True"
	strSQLOrder = " ORDER BY PA.paUsername ASC"
	strSQL = "SELECT PA.*, (PA.paFirstName + ' ' + PA.paLastName) AS paFullname" & _
			" FROM tblPartners AS PA " & strSQLWhere & strSQLOrder & ";"
End If
'// store it back in the session
SESSION("SUB_SQL_WHERE") = strSQLWhere
SESSION("SUB_SQL_ORDER") = strSQLOrder
SESSION("SUB_SQL") = strSQL
sParms = "?sql=y"
sParms = sParms & "&sz=" & pageSize

	Call adminHeader
	response.write "<b>Reviewing Partners.</b><br />&nbsp;<br />"
	if len(sMsg) > 0 then response.write "<p><b>" & sMsg & "</b></p>"
	nErr = getRowsPageBySQL(DSN_PARTNERS, pageSize, pageCur, nTotalPages, strSQL, aFields)
	call buildControls
	call displayProducts
	Call adminFooter

'// okay the first step is to create an order number and fill in any details that we can //

'// next we add all of the items required //

'// then we collect any more order data that may be appropriate //


sub buildControls
	'// records per page //
	sControls = "<table border=""0"" cellspacing=""0"" cellpadding=""0""><tr><td colspan=""4"" align=""left"">" & _
			"[&nbsp;<a href=""" & thisPage & sParms & "&newsz=10&pg=1" & """>10</a>&nbsp;]&nbsp;" & _
			"[&nbsp;<a href=""" & thisPage & sParms & "&newsz=20&pg=1" & """>20</a>&nbsp;]&nbsp;" & _
			"[&nbsp;<a href=""" & thisPage & sParms & "&newsz=50&pg=1" & """>50</a>&nbsp;]&nbsp;" & _
			"[&nbsp;<a href=""" & thisPage & sParms & "&newsz=100&pg=1" & """>100</a>&nbsp;]&nbsp;" & _
			"<small>per page.</small>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & vbcrlf
	'// show first page button? //
	if pageCur > 1 then
		sControls = sControls & "<a href=""" & thisPage & sParms & "&pg=1" & _
				"""><img src=""icon_first.gif"" width=""14"" height=""10"" alt=""first page"" border=""0""></a>"
	else
		sControls = sControls & "<img src=""icon_first_off.gif"" width=""14"" height=""10"" alt="""" border=""0"">"
	end if
	sControls = sControls & "" & VbCrlf
	'// show previous 10 page button? //
	if pageCur > 10 then
		sControls = sControls & "<a href=""" & thisPage & sParms & "&pg=" & pageCur-10 & _
				"""><img src=""icon_prev10.gif"" width=""14"" height=""10"" alt=""back 10 pages"" border=""0""></a>"
	else
		sControls = sControls & "<img src=""icon_prev10_off.gif"" width=""14"" height=""10"" alt="""" border=""0"">"
	end if
	sControls = sControls & "" & VbCrlf
	'// show previous page button? //
	if pageCur > 1 then
		sControls = sControls & "<a href=""" & thisPage & sParms & "&pg=" & pageCur-1 & _
				"""><img src=""icon_prev.gif"" width=""14"" height=""10"" alt=""previous page"" border=""0""></a>"
	else
		sControls = sControls & "<img src=""icon_prev_off.gif"" width=""14"" height=""10"" alt="""" border=""0"">"
	end if
	sControls = sControls & "&nbsp;&nbsp;<small>" & pageCur & " of " & nTotalPages & "</small>&nbsp;&nbsp;" & VbCrlf
	'// show next page button? //
	if pageCur < nTotalPages then
		sControls = sControls & "<a href=""" & thisPage & sParms & "&pg=" & pageCur+1 & _
				"""><img src=""icon_next.gif"" width=""14"" height=""10"" alt=""next page"" border=""0""></a>"
	else
		sControls = sControls & "<img src=""icon_next_off.gif"" width=""14"" height=""10"" alt="""" border=""0"">"
	end if
	sControls = sControls & "" & VbCrlf
	'// show next 10 page button? //
	if pageCur < nTotalPages - 9 then
		sControls = sControls & "<a href=""" & thisPage & sParms & "&pg=" & pageCur+10 & _
				"""><img src=""icon_next10.gif"" width=""14"" height=""10"" alt=""forward 10 pages"" border=""0""></a>"
	else
		sControls = sControls & "<img src=""icon_next10_off.gif"" width=""14"" height=""10"" alt="""" border=""0"">"
	end if
	sControls = sControls & "" & VbCrlf
	'// show last page button? //
	if pageCur < nTotalPages then
		sControls = sControls & "<a href=""" & thisPage & sParms & "&pg=" & nTotalPages & _
				"""><img src=""icon_last.gif"" width=""14"" height=""10"" alt=""last page"" border=""0""></a>"
	else
		sControls = sControls & "<img src=""icon_last_off.gif"" width=""14"" height=""10"" alt="""" border=""0"">"
	end if
	'// do table headers //
	sControls = sControls & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>" & VbCrlf & "<td>&nbsp;&nbsp;"
	
		sControls = sControls & "&nbsp;[&nbsp;<a href=""./pa_ledit.asp?pa=0"">Add New</a>&nbsp;]"
	
	sControls = sControls & "&nbsp;</td></tr></table>"
End sub

sub displayProducts()
	dim i,j, sTmp
	dim nErr:nErr = 0
	response.write sControls
%>
<table cellspacing="0" cellpadding="0" border="0" width="100%" bgcolor="<%=TBL_BG_CLR%>">
<tr bgcolor="<%=HDR_BG_CLR%>">
<td>&nbsp;<%=makeColHdrCtrl(thispage & sParms,"ID","PA.ID")%>&nbsp;</td>
<td>&nbsp;<%=makeColHdrCtrl(thispage & sParms,"Usermame","PA.paUsername")%>&nbsp;</td>
<td>&nbsp;<%=makeColHdrCtrl(thispage & sParms,"First Name","PA.paFirstName")%>&nbsp;</td>
<td>&nbsp;<%=makeColHdrCtrl(thispage & sParms,"Last Name","PA.paLastName")%>&nbsp;</td>
<td>&nbsp;<%=makeColHdrCtrl(thispage & sParms,"Company","PA.paCompanyName")%>&nbsp;</td>
<td>&nbsp;<b class="adminTitle">Options</b>&nbsp;</td>
<%
	if nErr = 0 and not isNull(aFields) then
		for i = 0 to UBound(aFields, 2)
			sTmp = sTmp & "<tr bgcolor=""" & g_aROW_CLR(i mod 2) & """>" & vbcrlf
			sTmp = sTmp & "<td align=""left"" valign=""top"">&nbsp;" & aFields(PA_ID,i) & "&nbsp;</td>" & vbcrlf
			sTmp = sTmp & "<td align=""left"" valign=""top"">&nbsp;" & aFields(PA_USERNAME,i) & "&nbsp;</td>" & vbcrlf
			sTmp = sTmp & "<td align=""left"" valign=""top"">&nbsp;" & aFields(PA_FIRSTNAME ,i) & "&nbsp;</td>" & vbcrlf
			sTmp = sTmp & "<td align=""left"" valign=""top"">&nbsp;" & aFields(PA_LASTNAME ,i) & "&nbsp;</td>" & vbcrlf
			sTmp = sTmp & "<td align=""left"" valign=""top"">&nbsp;" & aFields(PA_COMPANYNAME,i) & "&nbsp;</td>" & vbcrlf
			sTmp = sTmp & "<td align=""left"" valign=""top"">"
			'if nPerms and PERM_READ then
				sTmp = sTmp & "&nbsp;[&nbsp;<a href=""./pa_lview.asp?pa=" & aFields(PA_ID,i) & """>View</a>&nbsp;]"
			'end if
			'if nPerms and PERM_MODIFY then
				sTmp = sTmp & "&nbsp;[&nbsp;<a href=""./pa_ledit.asp?pa=" & aFields(PA_ID,i) & """>Edit</a>&nbsp;]"
			'end if
			'if nPerms and PERM_DELETE then
				sTmp = sTmp & "&nbsp;[&nbsp;<a href=""./pa_ldelete.asp?pa=" & aFields(PA_ID,i) & """>Delete</a>&nbsp;]"
			'end if
			sTmp = sTmp & "&nbsp;</td>" & vbcrlf
			sTmp = sTmp & "</tr>"
		next
	else
		'// no details to show //
	end if
	response.write sTmp
%>
</tr></table><%=sControls%><br>&nbsp;<br>&nbsp;<br>
<%
	call doSearchForm()
end sub


sub doSearchForm
%>
	
<table border="0" bgcolor="<%=TBL_BG_CLR%>">
  <form name="searchForm" action="<%=thisPage & sParms%>" method="POST">
    <input type="hidden" name="submitted" value="yes" />
    <tr bgcolor="<%=HDR_BG_CLR%>"> 
      <td colspan="6"> <b class="adminTitle">Search for... (use % as a wildcard)</b></td>
    </tr>
    <tr> 
      <td align="right"> Store Name</td>
      <td><input name="sUsername" type="text" id="sUsername" value="<%=sUsername%>" size="12" /></td>
      <td align="right">&nbsp;</td>
      <td colspan="2" rowspan="2">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td align="right">&nbsp;</td>
      <td>&nbsp;</td>
      <td align="right">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="6"><hr width="100%" color="<%=HR_CLR%>" /></td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td colspan="2"> Results per page:&nbsp;&nbsp; <select name="sz">
          <option value="10"<%if pageSize=10 then response.write " selected"%>>10</option>
          <option value="20"<%if pageSize=20 then response.write " selected"%>>20</option>
          <option value="50"<%if pageSize=50 then response.write " selected"%>>50</option>
          <option value="100"<%if pageSize=100 then response.write " selected"%>>100</option>
          <option value="250"<%if pageSize=250 then response.write " selected"%>>250</option>
          <option value="500"<%if pageSize=500 then response.write " selected"%>>500</option>
        </select></td>
      <td colspan="2"><input type="submit" value="Run This Search &gt;&gt;" /></td>
      <td>&nbsp;</td>
    </tr>
    <tr bgcolor="<%=HDR_BG_CLR%>"> 
      <td colspan="6">&nbsp;</td>
    </tr>
  </form>
</table>
&nbsp;<br />
<%
end sub
%>
