<!--#include virtual="/ecomm/setup.asp"-->
<!-- #include virtual="/includes/ewAPI.asp" -->
<%
Server.ScriptTimeout = 600
'on error resume next
'/// Import Application vn0.01 \\\
'---------------------------------
CONST DSN_SOURCE = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=C:\inetpub\wwwroot\www.earthworks.co.uk\data\EWproducts.mdb"

'// old ew constants req'd //
'// for product table //
const P_ID		 	 = 0
const P_ISPARENT	 = 1
const P_ISCHILD		 = 2
const P_CODE		 = 3
const P_SHORTCODE	 = 4
const P_BARCODE		 = 5
const P_NAME		 = 6
const P_DESCSHORT	 = 7
const P_DESCLONG	 = 8
const P_KEYWORDS	 = 9
const P_IMAGETHUMB	 = 10
const P_IMAGEMAIN	 = 11
const P_IMAGELARGE	 = 12
const P_STOCKQTY	 = 13
const P_STOCKTRIGGER = 14
const P_PRICE		 = 15
const P_VAT			 = 16
const P_COLOUR		 = 17
const P_SIZE		 = 18
const P_SPECIALREQS	 = 19
const P_REQSTEXT	 = 20
const P_WRAPPABLE	 = 21
const P_DISPLAY		 = 22
const P_ONSALE		 = 23
const P_PROMO		 = 24
const P_PROMOPRICE	 = 25
const P_PROMOVAT	 = 26
const P_KEYFPTID	 = 27
const P_KEYFMID		 = 28
const P_KEYFPCID	 = 29
const P_KEYFRCID	 = 30
const P_KEYFSCID	 = 31
const P_KEYFWCID	 = 32
const P_KEYFLTID	 = 33
const P_METATITLE	 = 34
const P_METADESC	 = 35
const P_UBOUND		 = 35


Dim intX : intX = 0
Dim intIDX : intIDX = 0
Dim intJDX : intJDX = 0
Dim intKDX : intKDX = 0
Dim aPrdOut:aPrdOut=Null

'// Select Range source table //
'sSql = "SELECT * FROM tblRange"
'getRowsFromSourceBySQL(DSN_SOURCE, sSQL, aRNsrc)
	'//IF WE LOOP AN INSERT TO DEST TABLE, UPDATE SRC TBL WITH NEW ID

'// Select Group source table //
'sSql = "SELECT * FROM tblGroup"
'getRowsFromSourceBySQL(DSN_SOURCE, sSQL, aGRsrc)
	'//IF WE LOOP AN INSERT TO DEST TABLE, UPDATE SRC TBL WITH NEW ID

'// Select RangeToGroup source table //
'sSql = "SELECT * FROM tblRangeToGroup"
'getRowsFromSourceBySQL(DSN_SOURCE, sSQL, aR2Gsrc)
	'//loop through, select the FrangeID, inner join to the range table select new range ID, inner join select FgroupID_
	'// and select new group ID,
	'// Insert new Range and group ID's into the Dest GroupToRange tbl
	

'// Select  product source table //
'The following tables I have manually imported the data:
'	- tblLeadTime
'	- tblManufacturer
'	- tblPostageCategory
'	- tblProductType
'	- tblSizeCategory
'	- tblWeightCategory
'	- tblWrappingCategory
'// First loop through find get Orphans

call importOrphans

call importParentsAndChildren



sub importParentsAndChildren()
  sSql = "SELECT * FROM EWproducts AS PR WHERE (PR.Field1='YES') ORDER BY PR.Field4 ASC;"
  call getRowsFromSourceBySQL(DSN_SOURCE, sSQL, aPRsrc) 
  For intIDX = 0 to ubound(aPRsrc, 2)
  	aFields = null
		redim aFields(PR_UBOUND,0)
    aFields(PR_ID, 0) = 0
		aFields(PR_KEYFINID,0) = INST_ID
		aFields(PR_DISPLAY, 0) = true
		aFields(PR_ONSALE, 0) = false
		aFields(PR_PROMO, 0) = false
		aFields(PR_PASSPARMS, 0) = true
		aFields(PR_ISPARENT, 0) = true
		aFields(PR_ISCHILD, 0) = false
		aFields(PR_SPECIALREQS, 0) = false
		aFields(PR_WRAPPABLE, 0) = true
    aFields(PR_DATE1, 0) = null
    aFields(PR_DATE2, 0) = null
   '// now get the real values from the recordset //
   
	aFields(PR_ID, 0) = 0 'aPRsrc(P_ID  , intIDX)
	aFields(PR_NAME, 0) = aPRsrc(7  , intIDX)
	aFields(PR_DESCSHORT, 0) = aPRsrc(8  , intIDX)
	aFields(PR_DESCLONG, 0) = aPRsrc(9  , intIDX)
	aFields(PR_FURTHERINFO, 0) = "" 'aPRsrc(P_  , intIDX)
	aFields(PR_MISC1, 0) = "" 'aPRsrc(P_  , intIDX)
	aFields(PR_MISC2, 0) = "" 'aPRsrc(P_  , intIDX)
	aFields(PR_IMAGETHUMB, 0) = replace("" & aPRsrc(11  , intIDX), "T01", "S01")
  if len("" & aFields(PR_IMAGETHUMB, 0)) > 0 then
    aFields(PR_IMAGETHUMB, 0) = aFields(PR_IMAGETHUMB, 0) & ".jpg"
  end if
	aFields(PR_IMAGEMAIN, 0) = aPRsrc(12  , intIDX)
  if len("" & aFields(PR_IMAGEMAIN, 0)) > 0 then
    aFields(PR_IMAGEMAIN, 0) = aFields(PR_IMAGEMAIN, 0) & ".jpg"
  end if
	aFields(PR_IMAGELARGE, 0) = aPRsrc(13  , intIDX)
  if len("" & aFields(PR_IMAGELARGE, 0)) > 0 then
    aFields(PR_IMAGELARGE, 0) = aFields(PR_IMAGELARGE, 0) & ".jpg"
  end if
	aFields(PR_FILE1, 0) = aPRsrc(14  , intIDX) '// large image //
  if len("" & aFields(PR_FILE1, 0)) > 0 then
    aFields(PR_FILE1, 0) = aFields(PR_FILE1, 0) & ".jpg"
  end if
	aFields(PR_FILE2, 0) = "" 'aPRsrc(P_  , intIDX)
	aFields(PR_FILE3, 0) = "" 'aPRsrc(P_  , intIDX)
  aFields(PR_DATE1, 0) = null
  aFields(PR_DATE2, 0) = null
	aFields(PR_HTMLPAGE, 0) = "" 'aPRsrc(P_  , intIDX)
	aFields(PR_BUYPAGE, 0) = "" 'aPRsrc(P_  , intIDX)
	aFields(PR_PASSPARMS, 0) = true 'aPRsrc(PR_WRAPPABLE  , intIDX) '// reusing for Wrappability 'true 'aPRsrc(P_  , intIDX)
	aFields(PR_ISPARENT, 0) = true 'aPRsrc(P_ISPARENT  , intIDX)
	aFields(PR_ISCHILD, 0) = false 'aPRsrc(P_ISCHILD  , intIDX)
	aFields(PR_SPECIALREQS, 0) = false 'aPRsrc(P_SPECIALREQS  , intIDX)
	aFields(PR_REQSTEXT, 0) = "" 'aPRsrc(PR_REQSTEXT  , intIDX)
	aFields(PR_DISPLAY, 0) = ("" & aPRsrc(23  , intIDX) = "YES")
	aFields(PR_ONSALE, 0) = ("" & aPRsrc(24  , intIDX) = "YES")
	aFields(PR_PROMO, 0) = ("" & aPRsrc(25  , intIDX) = "YES")
	aFields(PR_KEYWORDS, 0) = aPRsrc(10  , intIDX)
	aFields(PR_METATITLE, 0) = aPRsrc(34  , intIDX)
	aFields(PR_METADESC, 0) = aPRsrc(35  , intIDX)
	aFields(PR_COLOUR, 0) = left("" & aPRsrc(19  , intIDX), 128)
	aFields(PR_SIZE, 0) = left("" & aPRsrc(20  , intIDX), 128)
	aFields(PR_CODE, 0) = aPRsrc(4  , intIDX)
	aFields(PR_SHORTCODE, 0) = right("00000" & aPRsrc(5 , intIDX), 5)
	aFields(PR_BARCODE, 0) = "" 'aPRsrc(PR_BARCODE  , intIDX)
	aFields(PR_KEYFPTID, 0) = 0 ' Clng("0" & aPRsrc(PR_KEYFPTID  , intIDX))
	aFields(PR_KEYFMFID, 0) = Clng("0" & aPRsrc(29  , intIDX))
	aFields(PR_KEYFPCID, 0) = 0 ' Clng("0" & aPRsrc(PR_KEYFPCID  , intIDX))
	aFields(PR_KEYFRCID, 0) = Clng("0" & aPRsrc(30  , intIDX))
	aFields(PR_KEYFSZID, 0) = Clng("0" & aPRsrc(31  , intIDX))
	aFields(PR_KEYFWCID, 0) = Clng("0" & aPRsrc(32  , intIDX))
	aFields(PR_KEYFLTID, 0) = Clng("0" & aPRsrc(33  , intIDX))
	aFields(PR_PARENTID, 0) = 0 'aPRsrc(P_  , intIDX)
	aFields(PR_PRICE, 0) = CDbl("0" & aPRsrc(17  , intIDX))
  aFields(PR_VAT, 0) = CDbl("0" & aPRsrc(18  , intIDX))
	aFields(PR_PROMOPRICE, 0) = CDbl("0" & aPRsrc(26  , intIDX))
  aFields(PR_PROMOVAT, 0) = CDbl("0" & aPRsrc(27  , intIDX))
	aFields(PR_STOCKQTY, 0) = Clng(aPRsrc(15  , intIDX))
	aFields(PR_STOCKTRIGGER, 0) = Clng(aPRsrc(16  , intIDX))
   '// now just save the bugger //
  	nErr = saveProduct(aFields)
    if nErr = 0 then
      'call updateSourceProduct(aPRsrc(P_ID  , intIDX), aFields(PR_ID, 0))
      intJDX = intJDX + doKids(aFields(PR_CODE,0), aFields(PR_ID, 0))
    end if
    response.write " x . ":response.flush
  Next 
  response.write "<br />imported " & intIDX + 1 & " parents."
  response.write "<br />imported " & intJDX + 1 & " children."
end sub

function doKids(sCode, nParID)
  dim aTmp1:aTmp1=null
  dim aTmp2:aTmp2=null
  intKDX = 0
  sSql = "SELECT PR.* FROM EWproducts AS PR WHERE (PR.Field2='YES') AND PR.Field3='" & sCode & "' ORDER BY PR.Field4 ASC;"
  nErr = getRowsFromSourceBySQL(DSN_SOURCE, sSQL, aTmp2) 
  response.write sSQL
  if nErr=0 then
  For intKDX = 0 to ubound(aTmp2, 2)
  	aTmp1 = null
		redim aTmp1(PR_UBOUND,0)
    aTmp1(PR_ID, 0) = 0
		aTmp1(PR_KEYFINID,0) = INST_ID
		aTmp1(PR_DISPLAY, 0) = false
		aTmp1(PR_ONSALE, 0) = false
		aTmp1(PR_PROMO, 0) = false
		aTmp1(PR_PASSPARMS, 0) = true
		aTmp1(PR_ISPARENT, 0) = false
		aTmp1(PR_ISCHILD, 0) = true
		aTmp1(PR_SPECIALREQS, 0) = false
		aTmp1(PR_WRAPPABLE, 0) = true
    aTmp1(PR_DATE1, 0) = null
    aTmp1(PR_DATE2, 0) = null
   '// now get the real values from the recordset //
   
	aTmp1(PR_ID, 0) = 0 'aTmp2(P_ID  , intKDX)
	aTmp1(PR_NAME, 0) = aTmp2(7  , intKDX)
	aTmp1(PR_DESCSHORT, 0) = aTmp2(8  , intKDX)
	aTmp1(PR_DESCLONG, 0) = aTmp2(9  , intKDX)
	aTmp1(PR_FURTHERINFO, 0) = "" 'aTmp2(P_  , intKDX)
	aTmp1(PR_MISC1, 0) = "" 'aTmp2(P_  , intKDX)
	aTmp1(PR_MISC2, 0) = "" 'aTmp2(P_  , intKDX)
	aTmp1(PR_IMAGETHUMB, 0) = replace("" & aTmp2(11  , intKDX), "T01", "S01")
  if len("" & aTmp1(PR_IMAGETHUMB, 0)) > 0 then
    aTmp1(PR_IMAGETHUMB, 0) = aTmp1(PR_IMAGETHUMB, 0) & ".jpg"
  end if
	aTmp1(PR_IMAGEMAIN, 0) = aTmp2(12  , intKDX)
  if len("" & aTmp1(PR_IMAGEMAIN, 0)) > 0 then
    aTmp1(PR_IMAGEMAIN, 0) = aTmp1(PR_IMAGEMAIN, 0) & ".jpg"
  end if
	aTmp1(PR_IMAGELARGE, 0) = aTmp2(13  , intKDX)
  if len("" & aTmp1(PR_IMAGELARGE, 0)) > 0 then
    aTmp1(PR_IMAGELARGE, 0) = aTmp1(PR_IMAGELARGE, 0) & ".jpg"
  end if
	aTmp1(PR_FILE1, 0) = aTmp2(14  , intKDX) '// large image //
  if len("" & aTmp1(PR_FILE1, 0)) > 0 then
    aTmp1(PR_FILE1, 0) = aTmp1(PR_FILE1, 0) & ".jpg"
  end if
	aTmp1(PR_FILE2, 0) = "" 'aTmp2(P_  , intKDX)
	aTmp1(PR_FILE3, 0) = "" 'aTmp2(P_  , intKDX)
  aTmp1(PR_DATE1, 0) = null
  aTmp1(PR_DATE2, 0) = null
	aTmp1(PR_HTMLPAGE, 0) = "" 'aTmp2(P_  , intKDX)
	aTmp1(PR_BUYPAGE, 0) = "" 'aTmp2(P_  , intKDX)
	aTmp1(PR_PASSPARMS, 0) = true 'aTmp2(PR_WRAPPABLE  , intKDX) '// reusing for Wrappability 'true 'aTmp2(P_  , intKDX)
	aTmp1(PR_ISPARENT, 0) = false 'aTmp2(P_ISPARENT  , intKDX)
	aTmp1(PR_ISCHILD, 0) = true 'aTmp2(P_ISCHILD  , intKDX)
	aTmp1(PR_SPECIALREQS, 0) = false 'aTmp2(P_SPECIALREQS  , intKDX)
	aTmp1(PR_REQSTEXT, 0) = "" 'aTmp2(PR_REQSTEXT  , intKDX)
	aTmp1(PR_DISPLAY, 0) = ("" & aTmp2(23  , intKDX) = "YES")
	aTmp1(PR_ONSALE, 0) = ("" & aTmp2(24  , intKDX) = "YES")
	aTmp1(PR_PROMO, 0) = ("" & aTmp2(25  , intKDX) = "YES")
	aTmp1(PR_KEYWORDS, 0) = aTmp2(10  , intKDX)
	aTmp1(PR_METATITLE, 0) = aTmp2(34  , intKDX)
	aTmp1(PR_METADESC, 0) = aTmp2(35  , intKDX)
	aTmp1(PR_COLOUR, 0) = left("" & aTmp2(19  , intKDX), 128)
	aTmp1(PR_SIZE, 0) = left("" & aTmp2(20  , intKDX), 128)
	aTmp1(PR_CODE, 0) = aTmp2(4  , intKDX)
	aTmp1(PR_SHORTCODE, 0) = right("00000" & aTmp2(5 , intKDX), 5)
	aTmp1(PR_BARCODE, 0) = "" 'aTmp2(PR_BARCODE  , intKDX)
	aTmp1(PR_KEYFPTID, 0) = 0 ' Clng("0" & aTmp2(PR_KEYFPTID  , intKDX))
	aTmp1(PR_KEYFMFID, 0) = Clng("0" & aTmp2(29  , intKDX))
	aTmp1(PR_KEYFPCID, 0) = 0 ' Clng("0" & aTmp2(PR_KEYFPCID  , intKDX))
	aTmp1(PR_KEYFRCID, 0) = Clng("0" & aTmp2(30  , intKDX))
	aTmp1(PR_KEYFSZID, 0) = Clng("0" & aTmp2(31  , intKDX))
	aTmp1(PR_KEYFWCID, 0) = Clng("0" & aTmp2(32  , intKDX))
	aTmp1(PR_KEYFLTID, 0) = Clng("0" & aTmp2(33  , intKDX))
	aTmp1(PR_PARENTID, 0) = nParId 'aTmp2(P_  , intKDX)
	aTmp1(PR_PRICE, 0) = CDbl("0" & aTmp2(17  , intKDX))
  aTmp1(PR_VAT, 0) = CDbl("0" & aTmp2(18  , intKDX))
	aTmp1(PR_PROMOPRICE, 0) = CDbl("0" & aTmp2(26  , intKDX))
  aTmp1(PR_PROMOVAT, 0) = CDbl("0" & aTmp2(27  , intKDX))
	aTmp1(PR_STOCKQTY, 0) = Clng(aTmp2(15  , intKDX))
	aTmp1(PR_STOCKTRIGGER, 0) = Clng(aTmp2(16  , intKDX))
   '// now just save the bugger //
  	nErr = saveProduct(aTmp1)
    'if nErr = 0 then
    '  call updateSourceProduct(aTmp2(P_ID  , intKDX), aTmp1(PR_ID, 0))
    'end if
    response.write " x . ":response.flush
  Next 
  end if
  doKids = intKDX
end function

sub importOrphans()
  sSql = "SELECT PR.* FROM EWproducts AS PR WHERE ((NOT PR.Field1='YES' OR ISNULL(Field1))) AND ((NOT PR.Field2='YES' OR ISNULL(field2))) ORDER BY PR.Field4 ASC;"
  nErr = getRowsFromSourceBySQL(DSN_SOURCE, sSQL, aPRsrc) 
  response.write sSQL
  For intIDX = 0 to ubound(aPRsrc, 2)
  	aFields = null
		redim aFields(PR_UBOUND,0)
    aFields(PR_ID, 0) = 0
		aFields(PR_KEYFINID,0) = INST_ID
		aFields(PR_DISPLAY, 0) = false
		aFields(PR_ONSALE, 0) = false
		aFields(PR_PROMO, 0) = false
		aFields(PR_PASSPARMS, 0) = true
		aFields(PR_ISPARENT, 0) = false
		aFields(PR_ISCHILD, 0) = false
		aFields(PR_SPECIALREQS, 0) = false
		aFields(PR_WRAPPABLE, 0) = true
    aFields(PR_DATE1, 0) = null
    aFields(PR_DATE2, 0) = null
   '// now get the real values from the recordset //
   
	aFields(PR_ID, 0) = 0 'aPRsrc(P_ID  , intIDX)
	aFields(PR_NAME, 0) = aPRsrc(7  , intIDX)
	aFields(PR_DESCSHORT, 0) = aPRsrc(8  , intIDX)
	aFields(PR_DESCLONG, 0) = aPRsrc(9  , intIDX)
	aFields(PR_FURTHERINFO, 0) = "" 'aPRsrc(P_  , intIDX)
	aFields(PR_MISC1, 0) = "" 'aPRsrc(P_  , intIDX)
	aFields(PR_MISC2, 0) = "" 'aPRsrc(P_  , intIDX)
	aFields(PR_IMAGETHUMB, 0) = replace("" & aPRsrc(11  , intIDX), "T01", "S01")
  if len("" & aFields(PR_IMAGETHUMB, 0)) > 0 then
    aFields(PR_IMAGETHUMB, 0) = aFields(PR_IMAGETHUMB, 0) & ".jpg"
  end if
	aFields(PR_IMAGEMAIN, 0) = aPRsrc(12  , intIDX)
  if len("" & aFields(PR_IMAGEMAIN, 0)) > 0 then
    aFields(PR_IMAGEMAIN, 0) = aFields(PR_IMAGEMAIN, 0) & ".jpg"
  end if
	aFields(PR_IMAGELARGE, 0) = aPRsrc(13  , intIDX)
  if len("" & aFields(PR_IMAGELARGE, 0)) > 0 then
    aFields(PR_IMAGELARGE, 0) = aFields(PR_IMAGELARGE, 0) & ".jpg"
  end if
	aFields(PR_FILE1, 0) = aPRsrc(14  , intIDX) '// large image //
  if len("" & aFields(PR_FILE1, 0)) > 0 then
    aFields(PR_FILE1, 0) = aFields(PR_FILE1, 0) & ".jpg"
  end if
	'aFields(PR_IMAGETHUMB, 0) = replace("" & aPRsrc(11  , intIDX), "T01", "S01")
	'aFields(PR_IMAGEMAIN, 0) = aPRsrc(12  , intIDX)
	'aFields(PR_IMAGELARGE, 0) = aPRsrc(13  , intIDX)
	'aFields(PR_FILE1, 0) = aPRsrc(14  , intIDX) '// large image //
	aFields(PR_FILE2, 0) = "" 'aPRsrc(P_  , intIDX)
	aFields(PR_FILE3, 0) = "" 'aPRsrc(P_  , intIDX)
  aFields(PR_DATE1, 0) = null
  aFields(PR_DATE2, 0) = null
	aFields(PR_HTMLPAGE, 0) = "" 'aPRsrc(P_  , intIDX)
	aFields(PR_BUYPAGE, 0) = "" 'aPRsrc(P_  , intIDX)
	aFields(PR_PASSPARMS, 0) = true 'aPRsrc(PR_WRAPPABLE  , intIDX) '// reusing for Wrappability 'true 'aPRsrc(P_  , intIDX)
	aFields(PR_ISPARENT, 0) = false 'aPRsrc(P_ISPARENT  , intIDX)
	aFields(PR_ISCHILD, 0) = false 'aPRsrc(P_ISCHILD  , intIDX)
	aFields(PR_SPECIALREQS, 0) = false 'aPRsrc(P_SPECIALREQS  , intIDX)
	aFields(PR_REQSTEXT, 0) = "" 'aPRsrc(PR_REQSTEXT  , intIDX)
	aFields(PR_DISPLAY, 0) = ("" & aPRsrc(23  , intIDX) = "YES")
	aFields(PR_ONSALE, 0) = ("" & aPRsrc(24  , intIDX) = "YES")
	aFields(PR_PROMO, 0) = ("" & aPRsrc(25  , intIDX) = "YES")
	aFields(PR_KEYWORDS, 0) = aPRsrc( 10  , intIDX)
	aFields(PR_METATITLE, 0) = aPRsrc(34  , intIDX)
	aFields(PR_METADESC, 0) = aPRsrc(35  , intIDX)
	aFields(PR_COLOUR, 0) = left("" & aPRsrc(19  , intIDX), 128)
	aFields(PR_SIZE, 0) = left("" & aPRsrc(20  , intIDX), 128)
	aFields(PR_CODE, 0) = aPRsrc(4  , intIDX)
	aFields(PR_SHORTCODE, 0) = right("00000" & aPRsrc(5 , intIDX), 5)
	aFields(PR_BARCODE, 0) = "" 'aPRsrc(PR_BARCODE  , intIDX)
	aFields(PR_KEYFPTID, 0) = 0 ' Clng("0" & aPRsrc(PR_KEYFPTID  , intIDX))
	aFields(PR_KEYFMFID, 0) = Clng("0" & aPRsrc(29  , intIDX))
	aFields(PR_KEYFPCID, 0) = 0 ' Clng("0" & aPRsrc(PR_KEYFPCID  , intIDX))
	aFields(PR_KEYFRCID, 0) = Clng("0" & aPRsrc(30  , intIDX))
	aFields(PR_KEYFSZID, 0) = Clng("0" & aPRsrc(31  , intIDX))
	aFields(PR_KEYFWCID, 0) = Clng("0" & aPRsrc(32  , intIDX))
	aFields(PR_KEYFLTID, 0) = Clng("0" & aPRsrc(33  , intIDX))
	aFields(PR_PARENTID, 0) = 0 'aPRsrc(P_  , intIDX)
	aFields(PR_PRICE, 0) = CDbl("0" & aPRsrc(17  , intIDX))
  aFields(PR_VAT, 0) = CDbl("0" & aPRsrc(18  , intIDX))
	aFields(PR_PROMOPRICE, 0) = CDbl("0" & aPRsrc(26  , intIDX))
  aFields(PR_PROMOVAT, 0) = CDbl("0" & aPRsrc(27  , intIDX))
	aFields(PR_STOCKQTY, 0) = Clng(aPRsrc(15  , intIDX))
	aFields(PR_STOCKTRIGGER, 0) = Clng(aPRsrc(16  , intIDX))
   '// now just save the bugger //
  	nErr = saveProduct(aFields)
    'if nErr = 0 then
    '  call updateSourceProduct(aPRsrc(P_ID  , intIDX), aFields(PR_ID, 0))
    'end if
    response.write " x . ":response.flush
  Next 
  response.write "<br />imported " & intIDX + 1 & " orphans."
end sub

function updateSourceProduct(nOldID, nNewID)
  dim sSQL:sSQL="UPDATE tblProduct SET TempNID=" & nNewID & " WHERE pID=" & nOldId & ";"
  sqlexec DSN_SOURCE, sSQL
  updateSourceProduct = 0
end function


Function InsertID(sDSN,sSQL)
	'Insert in to new prod table
	dim oConn
	if len("" & sSQL) > 0 then
		set oConn = server.createobject("ADODB.Connection")
		oConn.open sDSN
		oConn.execute sSQL
		oConn.close
		set oConn = nothing
	end if
	'select top 1 from table just inserted into ordered by ID DESC
	sSQL = "SELECT TOP 1 PR.prID FROM tblProduct AS PR ORDER BY PR.prID DESC"
	call getRowsBySQL(DSN_ECOM_DB, sSQL, aNewID)
	'update source table with new ID
End Function


'// generic row fetcher for source DB //
function getRowsFromSourceBySQL(sDSN, sSQL, aFields)
	dim nErr:nErr = 0
	dim oConn, oRS
	if len("" & sSQL) > 0 then
		set oConn = server.createobject("ADODB.Connection")
		oConn.open sDSN
		set oRS = oConn.execute(sSQL)
		if not oRS.eof then
			aFields = oRS.getRows
		else
			'// no record found //
			nErr = 2
			aFields = null
		end if
		oRS.close
		set oRS = nothing
		oConn.close
		set oConn = nothing
	else
		'// bad parms //
		nErr = 1
		aFields = null
	end if
	getRowsFromSourceBySQL = nErr
end function
%>