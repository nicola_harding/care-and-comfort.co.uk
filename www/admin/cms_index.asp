<!--#include virtual="/scripts/aspstart.asp"-->
<!--#include virtual="/scripts/adovbs.asp"-->
<!--#include file="./setup.asp"-->
<%
'// main entry point to the page //
call subMain
sub subMain
	call adminHeader
	call showAdminOptions
	call adminFooter
end sub

sub showAdminOptions
%>
<b>CMS Administration Home</b><br />&nbsp;<br />
<table width="400" border="0" cellpadding="2" cellspacing="2" bgcolor="<%=TBL_BG_CLR%>">
<%
if SESSION("ISROOT") then
	'call showRootOptions
end if

Select Case SESSION("USER_TYPE")
	Case "SUR"
		call showCMSOptions
	Case "ADM","MBR"
		If Len(SESSION("PERMISSIONS")) > 0 Then
			arrPerms = Split(SESSION("PERMISSIONS"), ",")
			For i=0 To UBound(arrPerms)
				arrTemp = Split(arrPerms(i), "#")
				nID = CLng("0" & arrTemp(0))
				If 	nID = AREA_CMS Then
					call showCMSOptions
				End If
			Next
		Else
			Response.Write "<tr><td colspan=""2"">You currently do not have access to CMS functionality.</td></tr>" & VbCrLf
		End if
End Select
%>
</table>
<br />
<%
end sub

sub showCMSOptions
%>
	<tr bgcolor="<%=HDR_BG_CLR%>"><td colspan="2">&nbsp;<b class="adminTitle">Web Site Content Management</b>&nbsp;</td></tr>
  <%if SESSION("USER_TYPE") = "SUR" then %>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
    <tr><td>&nbsp;CMS Settings.&nbsp;</td><td align="center">&nbsp;[ <a href="./cms_settings.asp">Click here</a> ]</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
    <tr><td>&nbsp;Work with the CMS contents.&nbsp;</td><td align="center">&nbsp;[ <a href="./cms_contents.asp">Click here</a> ]</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
    <tr><td>&nbsp;Work with the CMS folders.&nbsp;</td><td align="center">&nbsp;[ <a href="./cms_folders.asp">Click here</a> ]</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
    <tr><td>&nbsp;Work with the CMS templates.&nbsp;</td><td align="center">&nbsp;[ <a href="./cms_templates.asp">Click here</a> ]</td></tr>
  <%end if %>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
    <tr><td>&nbsp;Work with your CMS content.&nbsp;</td><td align="center">&nbsp;[ <a href="./cms_browser.asp" target="cmsBrowser">Click here</a> ]</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td colspan="2">&nbsp;</td></tr>
<%
end sub
%>