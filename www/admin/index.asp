<!--#include virtual="/scripts/aspstart.asp"-->
<!--#include virtual="/scripts/adovbs.asp"-->
<!--#include file="./setup.asp"-->
<%
'// main entry point to the page //
call subMain
sub subMain
	call adminHeader
	call showAdminOptions
	call adminFooter
end sub

sub showAdminOptions
%>
<b>Administration Home</b><br />&nbsp;<br />
<table width="400" border="0" cellpadding="2" cellspacing="2" bgcolor="<%=TBL_BG_CLR%>">
<%
if SESSION("ISROOT") then
	call showRootOptions
end if
call showRootOptions
Select Case SESSION("USER_TYPE")
	Case "SUR"
		
		Call showCMSOptions
		call showEcommerceOptions
		call showOrderOptions
		'Call showOrderOptions
		'Call showCustomerOptions
		Call showUserOptions
		
		'Call showPartnerOptions
		call showStats
		'Call newsletterOptions
		'Call newsletterConfigOptions
		'Call subscriberOptions
		'call pollOptions
	Case "ADM","MBR"
		If Len(SESSION("PERMISSIONS")) > 0 Then
			arrPerms = Split(SESSION("PERMISSIONS"), ",")
			For i=0 To UBound(arrPerms)
				arrTemp = Split(arrPerms(i), "#")
				nID = CLng("0" & arrTemp(0))
				If 	nID = AREA_USERS Then
					Call showUserOptions
					'Call showPartnerOptions
				ElseIf 	nID = AREA_STAT Then
					Call showStats
				ElseIf 	nID = AREA_CMS Then
					Call showCMSOptions
				ElseIf 	nID = AREA_NEWS Then
					'Call newsletterOptions
					'Call newsletterConfigOptions
					'Call subscriberOptions
				ElseIf 	nID = AREA_NL Then
					'Call newsletterOptions
					'Call newsletterConfigOptions
				ElseIf 	nID = AREA_SUBS Then
					'Call subscriberOptions
				ElseIf 	nID = AREA_ECOM Then
					Call showEcommerceOptions
					Call showOrderOptions
					Call showCustomerOptions
					'call pollOptions
				End If
			Next
		Else
			Response.Write "<tr><td colspan=""2"">You do not currently have access to any options.</td></tr>" & VbCrLf
		End if
End Select
%>
</table>
<br>&nbsp;<br>
<%
end sub



Sub showCustomerOptions
%>
	<tr bgcolor="<%=HDR_BG_CLR%>"><td colspan="2">&nbsp;<b class="adminTitle">Customer Options</b>&nbsp;</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td>&nbsp;Work with Customers.&nbsp;</td><td>&nbsp;[ <a href="/admin/ecommscripts/ecm_members.asp">Search</a> ]&nbsp;</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td>&nbsp;Manually add a Customer.&nbsp;</td><td>&nbsp;[ <a href="/admin/ecommscripts/ecm_meedit.asp?me=0">Add</a> ]&nbsp;</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td colspan="2">&nbsp;</td></tr>
	<%
End Sub



sub showEcommerceOptions
%>
	<tr bgcolor="<%=HDR_BG_CLR%>"><td colspan="2">&nbsp;<b class="adminTitle">e-Commerce Administration</b>&nbsp;</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td>&nbsp;Manage Your eCommerce.&nbsp;</td><td>&nbsp;[ <a href="./ecommscripts/">Click here</a> ]</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td colspan="2">&nbsp;</td></tr>
<%
end sub

Sub showOrderOptions
%>
	<tr bgcolor="<%=HDR_BG_CLR%>"><td colspan="2">&nbsp;<b class="adminTitle">Order Options</b>&nbsp;</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td>&nbsp;Work with Orders.&nbsp;</td><td>&nbsp;[ <a href="/admin/ecommscripts/ecm_orders.asp">Search</a> ]&nbsp;</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td colspan="2">&nbsp;</td></tr>
	<%
End Sub

sub showUserOptions
%>
	<tr bgcolor="<%=HDR_BG_CLR%>"><td colspan="2">&nbsp;<b class="adminTitle">Administrative Users</b>&nbsp;</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td>&nbsp;User Administration.&nbsp;</td><td>&nbsp;[ <a href="/admin/userscripts/">Click here</a> ]</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td colspan="2">&nbsp;</td></tr>
	
	
<%
end sub

sub showPartnerOptions
%>
	<tr bgcolor="<%=HDR_BG_CLR%>"><td colspan="2">&nbsp;<b class="adminTitle">Manage Partners</b>&nbsp;</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td>&nbsp;Partner Administration.&nbsp;</td><td>&nbsp;[ <a href="/admin/partners.asp">Click here</a> ]</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td colspan="2">&nbsp;</td></tr>
<%
end sub

sub showStats
%>
	<tr bgcolor="<%=HDR_BG_CLR%>"><td colspan="2">&nbsp;<b class="adminTitle">Site statistics</b>&nbsp;</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td>&nbsp;View Site Statistics.&nbsp;</td><td>&nbsp;[ <a href="/admin/webstats/">Click here</a> ]</td></tr>

	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<!-- 	<tr><td>&nbsp;Last 30 days.&nbsp;</td><td>&nbsp;[ <a href="/admin/stats/last30days/">Click here</a> ]</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr> -->
		<!-- <tr><td>&nbsp;2005.&nbsp;</td><td>&nbsp;[ <a href="/admin/stats/2005/">Click here</a> ]</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr> -->
	<tr><td colspan="2">&nbsp;</td></tr>
<%
end sub

sub showCMSOptions
%>
	<tr bgcolor="<%=HDR_BG_CLR%>"><td colspan="2">&nbsp;<b class="adminTitle">CMS Administration</b>&nbsp;</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td>&nbsp;Manage Your CMS.&nbsp;</td><td>&nbsp;[ <a href="./cmsscripts/">Click here</a> ]</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td colspan="2">&nbsp;</td></tr>
<%
end sub

sub showRootOptions
%>
<!--<tr><td>&nbsp;Run Product IMport / Update.&nbsp;</td><td>&nbsp;[ <a href="/admin/DRUimportP2GNew.asp">Click here</a> ]</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>-->
	<tr bgcolor="<%=HDR_BG_CLR%>"><td colspan="2">&nbsp;<b class="adminTitle">sysadm</b>&nbsp;</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td>&nbsp;System Administration.&nbsp;</td><td>&nbsp;[ <a href="/admin/rootscripts/">Click here</a> ]</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td colspan="2">&nbsp;</td></tr>
<%
end sub

Sub newsletterOptions
%>
	<tr bgcolor="<%=HDR_BG_CLR%>"><td colspan="2">&nbsp;<b class="adminTitle">Newsletter Editing</b>&nbsp;</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td>&nbsp;Newsletter Editing.&nbsp;</td><td>&nbsp;[ <a href="/admin/newletterscripts/">Options</a> ]&nbsp;</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td colspan="2">&nbsp;</td></tr>
	<%
End Sub
%>