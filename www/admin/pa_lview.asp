<!--#include virtual="/scripts/aspstart.asp"-->
<!--#include virtual="/scripts/adovbs.asp"-->
<!--#include file="./setup.asp"-->
<!--#include virtual="/includes/partnersAPI.asp"-->
<%
'//////////////////////////////////
'// Main entry point to the page //
dim nPerms, sUT
sUT = SESSION("USER_TYPE")
If sUT = "SUR" Then
	nPerms = PERM_ALL
Else
	nPerms = GetPermissions(SESSION("PERMISSIONS"), AREA_DIST)
End If

'if (nPerms and PERM_READ) = 0 then response.redirect("./")

dim aFields, nID
dim nErr:nErr = 0

nID = clng("0" & request.querystring("pa"))

nErr = loadPartner(nID, aFields)

if nErr = 0 then
	Call adminHeader
	Call displayRecord(aFields)
	Call adminFooter
else
	response.redirect "./"
end if
'// End main entry point to page //
'//////////////////////////////////

'// sub to display new newsletter form //
Sub displayRecord(aFields)
	dim nErr:nErr = 0
  dim nIdx:nIdx=0
  dim x:x=0
%>
<b>Partner Administration<br />&nbsp;</b>
<br />
<%
	'if nPerms And PERM_MODIFY then
	  response.write "[&nbsp;<a href=""pa_ledit.asp?pa=" & aFields(pa_ID, 0) & """>Edit</a>&nbsp;]&nbsp;"
	'end if
	'if nPerms And PERM_DELETE then
	  response.write "[&nbsp;<a href=""pa_ldelete.asp?pa=" & aFields(pa_ID, 0) & """>Delete</a>&nbsp;]&nbsp;"
	'end if
	'if nPerms And PERM_WRITE then
	  response.write "[&nbsp;<a href=""pa_ledit.asp?pa=0"">Add New</a>&nbsp;]&nbsp;"
	'end if
%>&nbsp;<br />&nbsp;
	<table border="0" cellpadding="0" cellspacing="0"><tr><td valign="top">

  
<table cellspacing="0" cellpadding="0" border="0" width="100%">
<tr bgcolor="<%=HDR_BG_CLR%>"><td>&nbsp;<b class="adminTitle">Username</b>&nbsp;</td></tr>
<tr bgcolor="<%=ROW_CLR_0%>"><td>&nbsp;<%=aFields(PA_USERNAME, 0)%>&nbsp;</td></tr>
<tr bgcolor="<%=HDR_BG_CLR%>"><td>&nbsp;<b class="adminTitle">Password</b>&nbsp;</td></tr>
<tr bgcolor="<%=ROW_CLR_0%>"><td>&nbsp;<%=aFields(PA_PASSWORD, 0)%>&nbsp;</td></tr>
<tr bgcolor="<%=HDR_BG_CLR%>"><td>&nbsp;<b class="adminTitle">First Name</b>&nbsp;</td></tr>
<tr bgcolor="<%=ROW_CLR_0%>"><td>&nbsp;<%=aFields(PA_FIRSTNAME, 0)%>&nbsp;</td></tr>
<tr bgcolor="<%=HDR_BG_CLR%>"><td>&nbsp;<b class="adminTitle">Last Name</b>&nbsp;</td></tr>
<tr bgcolor="<%=ROW_CLR_0%>"><td>&nbsp;<%=aFields(PA_LASTNAME, 0)%>&nbsp;</td></tr>
<tr bgcolor="<%=HDR_BG_CLR%>"><td>&nbsp;<b class="adminTitle">Company Name</b>&nbsp;</td></tr>
<tr bgcolor="<%=ROW_CLR_0%>"><td>&nbsp;<%=aFields(PA_COMPANYNAME, 0)%>&nbsp;</td></tr>
<tr bgcolor="<%=HDR_BG_CLR%>"><td>&nbsp;<b class="adminTitle">Email</b>&nbsp;</td></tr>
<tr bgcolor="<%=ROW_CLR_0%>"><td>&nbsp;<%=aFields(PA_EMAIL, 0)%>&nbsp;</td></tr>

<tr bgcolor="<%=HDR_BG_CLR%>"><td>&nbsp;<b class="adminTitle">Level</b>&nbsp;</td></tr>
<tr bgcolor="<%=ROW_CLR_0%>"><td>&nbsp;<%=aFields(PA_KEYFLVLID, 0)%>&nbsp;</td></tr>

<tr bgcolor="<%=HDR_BG_CLR%>"><td>&nbsp;<b class="adminTitle">Last Login</b>&nbsp;</td></tr>
<tr bgcolor="<%=ROW_CLR_0%>"><td>&nbsp;<%=aFields(PA_LASTLOGIN, 0)%>&nbsp;</td></tr>

</table>

	

	
	</td></tr></table>
	&nbsp;<br />
	<a href="partners.asp?sql=y">Partners</a>&nbsp;<br />
<%
End Sub
%>
