<!--#include virtual="/scripts/aspstart.asp"-->
<!--#include virtual="/scripts/adovbs.asp"-->
<!--#include file="./setup.asp"-->
<!--#include virtual="/includes/partnersAPI.asp"-->
<%
'//////////////////////////////////
'// Main entry point to the page //
dim nPerms, sUT
sUT = SESSION("USER_TYPE")
If sUT = "SUR" Then
	nPerms = PERM_ALL
Else
	nPerms = GetPermissions(SESSION("PERMISSIONS"), AREA_DIST)
End If

dim aFields, nID, sMsg
dim nErr:nErr = 0

nID = clng("0" & request.querystring("pa"))
if nID = 0 then
'	if (nPerms and PERM_WRITE) = 0 then response.redirect("./")
else
	'if (nPerms and PERM_MODIFY) = 0 then response.redirect("./")
end if

if request.form("submitted") = "yes" then
	'// hoof up the form variables //
	nErr = getFormSubmission(aFields, sMsg)
	if nErr = 0 then
		nErr = savePartner(aFields)
		if nErr = 0 then
			response.redirect "pa_lview.asp?pa=" & aFields(PA_ID,0)
		else
			Call adminHeader
			Call displayEditForm(aFields, "There was a problem saving the record details! Please try again.<br />")
			Call adminFooter
		end if
	else
		Call adminHeader
		Call displayEditForm(aFields, sMsg)
		Call adminFooter
	end if
elseif nID > 0 then
	'// load up the vars from the dB //
	nErr = loadPartner(nID, aFields)
	if nErr = 0 then
		Call adminHeader
		Call displayEditForm(aFields, "")
		Call adminFooter
	else
		response.redirect "./"
	end if
else
	'// create blank array //
	redim aFields(PA_UBOUND,0)
	Call adminHeader
	Call displayEditForm(aFields, "")
	Call adminFooter
end if

'// End main entry point to page //
'//////////////////////////////////

function getFormSubmission(aFields, sMsg)
	dim nErr:nErr = 0
	redim aFields(PA_UBOUND,0)
	'// let's validate as we go //
	
	aFields(PA_ID, 0) = Clng("0" & request.form("paID"))
	aFields(PA_USERNAME, 0) = left(Trim("" & request.form("username")), 255)
	aFields(PA_PASSWORD, 0) = request.form("password")
	aFields(PA_FIRSTNAME, 0) = request.form("firstname")
	aFields(PA_LASTNAME, 0) = request.form("lastname")
	aFields(PA_COMPANYNAME, 0) = request.form("companyname")
	aFields(PA_EMAIL, 0) = left(Trim("" & request.form("email")), 255) 
	aFields(PA_KEYFLVLID, 0) = request.form("level")
	aFields(PA_LASTLOGIN, 0) = Trim("" & request.form("lastlogin"))
	
	getFormSubmission = nErr
end function

'// sub to display new newsletter form //
Sub displayEditForm(aFields, sMsg)
	dim nErr:nErr = 0
%>
<b>Partner Administration<br />&nbsp;</b>
<br />
<%
	'if nPerms And PERM_READ then
	  response.write "[&nbsp;<a href=""pa_lview.asp?pa=" & aFields(pa_ID, 0) & """>View</a>&nbsp;]&nbsp;"
	'end if
	'if nPerms And PERM_DELETE then
	  response.write "[&nbsp;<a href=""pa_ldelete.asp?pa=" & aFields(pa_ID, 0) & """>Delete</a>&nbsp;]&nbsp;"
	'end if
	'if nPerms And PERM_WRITE then
	  response.write "[&nbsp;<a href=""pa_ledit.asp?pa=0"">Add New</a>&nbsp;]&nbsp;"
	'end if
%>&nbsp;<br />
<%
if len("" & sMsg) > 0 then
	response.write "There was a problem with the data.<br /><b>" & sMsg & "</b>"
end if
%>&nbsp;

<form name="editRecord" action="<%=request.servervariables("SCRIPT_NAME") & "?pa=" & aFields(pa_ID, 0)%>" method="POST">
<input type="hidden" name="submitted" value="yes" />
<input type="hidden" name="paID" value="<%=aFields(pa_ID,0)%>" />


	
  <table border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td valign="top"> 
	  	<table cellspaing="0" cellpadding="0" border="0" width="100%">
          <tr bgcolor="<%=HDR_BG_CLR%>"> 
            <td>&nbsp;<b class="adminTitle">Username</b>&nbsp;</td>
          </tr>
          <tr bgcolor="<%=ROW_CLR_0%>">
            <td><input type="text" name="username" value="<%=aFields(PA_USERNAME, 0)%>" size="48" /></td>
          </tr>
          <tr bgcolor="<%=HDR_BG_CLR%>"> 
            <td>&nbsp;<b class="adminTitle">Password</b></td>
          </tr>
          <tr bgcolor="<%=ROW_CLR_0%>">
            <td><input type="text" name="password" value="<%=aFields(PA_PASSWORD, 0)%>" size="48" /></td>
          </tr>
          <tr bgcolor="<%=HDR_BG_CLR%>"> 
            <td>&nbsp;<b class="adminTitle">First Name</b></td>
          </tr>
          <tr bgcolor="<%=ROW_CLR_0%>">
            <td><input type="text" name="firstname" value="<%=aFields(PA_FIRSTNAME, 0)%>" size="48" /></td>
          </tr>
          <tr bgcolor="<%=HDR_BG_CLR%>"> 
            <td>&nbsp;<b class="adminTitle">Last Name</b></td>
          </tr>
          <tr bgcolor="<%=ROW_CLR_0%>">
            <td><input type="text" name="lastname" value="<%=aFields(PA_LASTNAME, 0)%>" size="48" /></td>
          </tr>
 		  <tr bgcolor="<%=HDR_BG_CLR%>"> 
            <td>&nbsp;<b class="adminTitle">Company Name</b></td>
          </tr>
          <tr bgcolor="<%=ROW_CLR_0%>">
            <td><input type="text" name="companyname" value="<%=aFields(PA_COMPANYNAME, 0)%>" size="48" /></td>
          </tr>
        <tr bgcolor="<%=HDR_BG_CLR%>"> 
            <td>&nbsp;<b class="adminTitle">Email</b></td>
          </tr>
          <tr bgcolor="<%=ROW_CLR_0%>">
            <td><input type="text" name="email" value="<%=aFields(PA_EMAIL, 0)%>" size="48" /></td>
          </tr>
		  <tr bgcolor="<%=HDR_BG_CLR%>"> 
            <td>&nbsp;<b class="adminTitle">Security level</b></td>
          </tr>

		 <tr bgcolor="<%=ROW_CLR_0%>">
            <td>
			<%=genericDropDownFromDB(DSN_PARTNERS, "SELECT LV.* FROM tblPartnerLevels AS LV ORDER BY LV.lvName ASC;", "level", aFields(PA_KEYFLVLID, 0))%> 
			<!--<textarea name="level" cols="52" rows="5"><%'=aFields(PA_KEYFLVLID, 0)%></textarea>--></td>
          </tr>
		  <tr bgcolor="<%=HDR_BG_CLR%>"> 
            <td>&nbsp;<b class="adminTitle">Last Login</b></td>
          </tr>
		   <tr bgcolor="<%=ROW_CLR_0%>">
		   <%
		   if len("" & aFields(PA_LASTLOGIN, 0)) = 0 then
		   	aFields(PA_LASTLOGIN, 0) = now()	
		   end if
		   %>
            <td><input type="text" name="lastlogin" value="<%=aFields(PA_LASTLOGIN, 0)%>" size="48" /></td>
          </tr>
		 
		  </table>
		 </td> 
    <tr>
      <td colspan="3">&nbsp;<br /> </td>
    </tr>
  </table>
	
<input type="submit" name="submit" value="save changes" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type="reset" name="reset" value="reset changes" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type="button" name="cancel" value="cancel" onClick="window.location.href='Partners.asp?sql=y';" />
</form>

	&nbsp;<br />
	<a href="Partners.asp?sql=y">Partners</a>&nbsp;<br />
<%
End Sub
%>
