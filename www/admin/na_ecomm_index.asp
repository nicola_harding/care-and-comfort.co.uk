<!--#include virtual="/scripts/aspstart.asp"-->
<!--#include virtual="/scripts/adovbs.asp"-->
<!--#include file="./setup.asp"-->
<%
'// main entry point to the page //
call subMain
sub subMain
	call adminHeader
	call showAdminOptions
	call adminFooter
end sub

sub showAdminOptions
%>
<b>Ecommerce Administration Home</b><br />&nbsp;<br />
<table width="400" border="0" cellpadding="2" cellspacing="2" bgcolor="<%=TBL_BG_CLR%>">
<%
if SESSION("ISROOT") then
	'call showRootOptions
end if

Select Case SESSION("USER_TYPE")
	Case "SUR"
		call showEcommerceOptions
		Call showOrderOptions
		Call showCustomerOptions
		Call showReportingOptions
	Case "ADM","MBR"
		If Len(SESSION("PERMISSIONS")) > 0 Then
			arrPerms = Split(SESSION("PERMISSIONS"), ",")
			For i=0 To UBound(arrPerms)
				arrTemp = Split(arrPerms(i), "#")
				nID = CLng("0" & arrTemp(0))
				If 	nID = AREA_ECOM Then
					Call showEcommerceOptions
					Call showOrderOptions
					Call showCustomerOptions
					Call showReportingOptions
				End If
			Next
		Else
			Response.Write "<tr><td colspan=""2"">You do not currently have access to any options.</td></tr>" & VbCrLf
		End if
End Select
%>
</table>
<br />
<%
end sub

Sub showCustomerOptions
%>
	<tr bgcolor="<%=HDR_BG_CLR%>"><td colspan="2">&nbsp;<b class="adminTitle">Customer Options</b>&nbsp;</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td>&nbsp;Work with Customers.&nbsp;</td><td>&nbsp;[ <a href="./ecm_members.asp">Search</a> ]&nbsp;</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td>&nbsp;Manually add a Customer.&nbsp;</td><td>&nbsp;[ <a href="./ecm_meedit.asp?me=0">Add</a> ]&nbsp;</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td colspan="2">&nbsp;</td></tr>
	<%
End Sub

Sub showOrderOptions
%>
	<tr bgcolor="<%=HDR_BG_CLR%>"><td colspan="2">&nbsp;<b class="adminTitle">Order Options</b>&nbsp;</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td>&nbsp;Work with Orders.&nbsp;</td><td>&nbsp;[ <a href="./ecm_orders.asp">Search</a> ]&nbsp;</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td colspan="2">&nbsp;</td></tr>
<%
End Sub

sub showEcommerceOptions
%>
	<tr bgcolor="<%=HDR_BG_CLR%>"><td colspan="2">&nbsp;<b class="adminTitle">e-Commerce Administration</b>&nbsp;</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td>&nbsp;Edit Categories.&nbsp;(Primary Navigation)&nbsp;</td><td>&nbsp;[ <a href="./ecm_categories.asp">Click here</a> ]</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td>&nbsp;Edit Ranges.&nbsp;(Secondary Navigation)&nbsp;</td><td>&nbsp;[ <a href="./ecm_ranges.asp">Click here</a> ]</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td>&nbsp;Work with Products.&nbsp;</td><td>&nbsp;[ <a href="./ecm_products_1.asp?order=PR.prCODE">Click here</a> ]</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<!--tr><td>&nbsp;Edit Groups.&nbsp;</td><td>&nbsp;[ <a href="./ecm_groups.asp">Click here</a> ]</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td>&nbsp;Work with manufacturers.&nbsp;</td><td>&nbsp;[ <a href="./ecm_manufacturers.asp">Click here</a> ]</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr-->
	<tr><td>&nbsp;Work with lead times.&nbsp;</td><td>&nbsp;[ <a href="./ecm_leadtimes.asp">Click here</a> ]</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td>&nbsp;Work with postage categories.&nbsp;</td><td>&nbsp;[ <a href="./ecm_postagecategories.asp">Click here</a> ]</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<!--tr><td>&nbsp;Work with weight categories.&nbsp;</td><td>&nbsp;[ <a href="./ecm_weightcategories.asp">Click here</a> ]</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td>&nbsp;Work with wrapping categories.&nbsp;</td><td>&nbsp;[ <a href="./ecm_wrappingcategories.asp">Click here</a> ]</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td>&nbsp;Work with collections.&nbsp;</td><td>&nbsp;[ <a href="./ecm_collections.asp">Click here</a> ]</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td>&nbsp;Work with product types.&nbsp;</td><td>&nbsp;[ <a href="./ecm_producttypes.asp">Click here</a> ]</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr-->
	<!--tr><td>&nbsp;Work with product categories.&nbsp;</td><td>&nbsp;[ <a href="./ecm_categories.asp">Click here</a> ]</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td>&nbsp;Work with ranges of products.&nbsp;</td><td>&nbsp;[ <a href="./ecm_ranges.asp">Click here</a> ]</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td>&nbsp;Work with groups of products.&nbsp;</td><td>&nbsp;[ <a href="./ecm_groups.asp">Click here</a> ]</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr-->
	<tr><td>&nbsp;Work with collections of products.&nbsp;</td><td>&nbsp;[ <a href="./ecm_collections.asp">Click here</a> ]</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<!--<tr><td>&nbsp;Export Data.&nbsp;</td><td>&nbsp;[ <a href="./ecm_export.asp">Click here</a> ]</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>-->
	<tr><td colspan="2">&nbsp;</td></tr>
<%
end sub

sub showReportingOptions
%>
	<tr bgcolor="<%=HDR_BG_CLR%>"><td colspan="2">&nbsp;<b class="adminTitle">e-Commerce Reporting</b>&nbsp;</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td>&nbsp;Top Selling Products.&nbsp;</td><td>&nbsp;[ <a href="./ecm_rptbestsellers.asp">Click here</a> ]</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td>&nbsp;Per Product Reporting.&nbsp;</td><td>&nbsp;[ <a href="./ecm_rptproducts.asp">Click here</a> ]</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td>&nbsp;Order Values Reporting.&nbsp;</td><td>&nbsp;[ <a href="./ecm_rptordervalues.asp">Click here</a> ]</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td>&nbsp;Voucher Use Reporting.&nbsp;</td><td>&nbsp;[ <a href="./ecm_rptvouchervalues.asp">Click here</a> ]</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<!--tr><td>&nbsp;Per Organisation Order Reporting.&nbsp;</td><td>&nbsp;[ <a href="./ecm_rptorderorgs.asp">Click here</a> ]</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr-->
	<tr><td>&nbsp;Member Registrations.&nbsp;</td><td>&nbsp;[ <a href="./ecm_rptmemberregs.asp">Click here</a> ]</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td>&nbsp;Member Activity.&nbsp;</td><td>&nbsp;[ <a href="./ecm_rptmemberlogins.asp">Click here</a> ]</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td>&nbsp;Top Downloads.&nbsp;</td><td>&nbsp;[ <a href="./ecm_rpttopdownloads.asp">Click here</a> ]</td></tr>
	<tr><td colspan="2"><hr width="100%" color="<%=HR_CLR%>" /></td></tr>
	<tr><td colspan="2">&nbsp;</td></tr>
<%
end sub
%>