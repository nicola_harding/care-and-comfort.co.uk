<!-- #include virtual="/includes/careandcomfortAPI.asp" --><!--#include virtual="/ecomm/setup.asp"-->
<%
Server.ScriptTimeout = 600
'/// Import Application vn0.01 \\\
'---------------------------------
dim EXEC_DIR:EXEC_DIR		  = replace(Server.MapPath("/"), "\www", "\data")
EXEC_DIR = replace(EXEC_DIR, "dataroot", "wwwroot")
EXEC_DIR = replace(EXEC_DIR, "data.", "www.")
dim DSN_SOURCE:DSN_SOURCE = "FileDSN=" & EXEC_DIR & "\csv.dsn" & _
       ";DefaultDir=" & EXEC_DIR & _
       ";DBQ=" & EXEC_DIR & ";"

'// old ew constants req'd //
'// for product table //
const P_ID		 	 = 0
const P_ISPARENT	 = 1
const P_ISCHILD		 = 2
const P_CODE		 = 3
const P_SHORTCODE	 = 4
const P_BARCODE		 = 5
const P_NAME		 = 6
const P_DESCSHORT	 = 7
const P_DESCLONG	 = 8
const P_KEYWORDS	 = 9
const P_IMAGETHUMB	 = 10
const P_IMAGEMAIN	 = 11
const P_IMAGELARGE	 = 12
const P_STOCKQTY	 = 13
const P_STOCKTRIGGER = 14
const P_PRICE		 = 15
const P_VAT			 = 16
const P_COLOUR		 = 17
const P_SIZE		 = 18
const P_SPECIALREQS	 = 19
const P_REQSTEXT	 = 20
const P_WRAPPABLE	 = 21
const P_DISPLAY		 = 22
const P_ONSALE		 = 23
const P_PROMO		 = 24
const P_PROMOPRICE	 = 25
const P_PROMOVAT	 = 26
const P_KEYFPTID	 = 27
const P_KEYFMID		 = 28
const P_KEYFPCID	 = 29
const P_KEYFRCID	 = 30
const P_KEYFSCID	 = 31
const P_KEYFWCID	 = 32
const P_KEYFLTID	 = 33
const P_METATITLE	 = 34
const P_METADESC	 = 35
const P_UBOUND		 = 35


Dim intX : intX = 0
Dim intIDX : intIDX = 0
Dim intJDX : intJDX = 0
Dim intKDX : intKDX = 0
Dim aPrdOut:aPrdOut=Null

 
'// Select Range source table //
'sSql = "SELECT * FROM tblRange"
'getRowsFromSourceBySQL(DSN_SOURCE, sSQL, aRNsrc)
	'//IF WE LOOP AN INSERT TO DEST TABLE, UPDATE SRC TBL WITH NEW ID

'// Select Group source table //
'sSql = "SELECT * FROM tblGroup"
'getRowsFromSourceBySQL(DSN_SOURCE, sSQL, aGRsrc)
	'//IF WE LOOP AN INSERT TO DEST TABLE, UPDATE SRC TBL WITH NEW ID

'// Select RangeToGroup source table //
'sSql = "SELECT * FROM tblRangeToGroup"
'getRowsFromSourceBySQL(DSN_SOURCE, sSQL, aR2Gsrc)
	'//loop through, select the FrangeID, inner join to the range table select new range ID, inner join select FgroupID_
	'// and select new group ID,
	'// Insert new Range and group ID's into the Dest GroupToRange tbl
	

'// Select  product source table //
'The following tables I have manually imported the data:
'	- tblLeadTime
'	- tblManufacturer
'	- tblPostageCategory
'	- tblProductType
'	- tblSizeCategory
'	- tblWeightCategory
'	- tblWrappingCategory
'// First loop through find get Orphans


call importProducts2Groups("Products_all.csv")

'response.write DSN_SOURCE:response.end

sub importProducts2Groups(sFileIn)
  dim oConn,oRS,sSQL,x,y
  dim nGRIP, nPRID, nRNID
  dim nIdx:nIdx=0
  x = 0
  y = 0
  '// empty it out first of all //
  'sSQL = "DELETE * FROM tblProductToGroup WHERE keyFinID=" & INST_ID & ";"
  'call sqlExec(DSN_ECOM_DB, sSQL)
  sSQL = "SELECT * FROM " & sFileIn & ";"
  set oConn = server.createobject("ADODB.Connection")
  response.write DSN_SOURCE
  oConn.open DSN_SOURCE

  set oEcomConn = server.createobject("ADODB.Connection")

  oEcomConn.Open DSN_ECOM_DB

  set oRS = oConn.execute(sSQL)
  
  if not oRS.eof then
    while not oRS.eof
     
		'check to see if a pillow or a topper

		If oRS(6) = 0 OR oRS(6) = 1 Then
		
			sSQL = "INSERT INTO tblProduct (prIsChild, prDisplay, prOnSale,prPassParms,prIsParent,prSpecialReqs, prWrappable,prPromo,prCode,prName,prMisc1,prPrice,prVAT,prParentID,keyFinID) VALUES (1,1,1,0,0,0,0,0,'" & oRS(1)  & "','"  &  oRS(2) & "','"  &  oRS(3) & "'," &  oRS(5) & "," & oRS(4) & ",0," & INST_ID & ");"
			
			oEcomConn.execute(sSQL)
			Response.Write sSQL & "<br/>"

			sSQLSelect = "SELECT TOP 1 prID FROM tblProduct ORDER BY prID DESC"
			
			set oRSSelect = oEcomConn.execute(sSQLSelect)
			
			If NOT oRSSelect.EOF Then
				iNewProductId = oRSSelect("prID")
			End IF
			
			set oRSSelect = Nothing
			'if topper
			If oRS(6) = 0 Then
				sSQLRel = "INSERT INTO tblProductToRange (keyFrnID,keyFprID,p2rDisplay,keyFinID) VALUES (63," & iNewProductId & ",0," & INST_ID & ");"
			End If
			'if pillow
			If oRS(6) = 1 Then
				sSQLRel = "INSERT INTO tblProductToRange (keyFrnID,keyFprID,p2rDisplay,keyFinID) VALUES (60," & iNewProductId & ",0," & INST_ID & ");"
			End If
			
			oEcomConn.execute(sSQLRel)

			Response.Write sSQLRel & "<br/>"

		Else

			sSQL = "INSERT INTO tblProduct (prIsChild, prDisplay, prOnSale,prPassParms,prIsParent,prSpecialReqs, prWrappable,prPromo,prCode,prName,prMisc1,prPrice,prVAT,prParentID,keyFinID) VALUES (1,1,1,0,0,0,0,0,'" & oRS(1)  & "','"  &  oRS(2) & "','"  &  oRS(3) & "'," &  oRS(5) & "," & oRS(4) & "," & oRS(6) & "," & INST_ID & ");"
		
			Response.Write sSQL & "<br/>"
			
			oEcomConn.execute(sSQL)
		End If
		
		
		 oRS.moveNext
	Wend	
		response.write "<br />imported " & x & " lines. failed on " & y & " lines."
  else
    response.write "file empty"
  end if
  oEcomConn.Close
	set oEcomConn = nothing
  oRS.close
  set oRS = nothing
  oConn.close
  set oConn = nothing
end sub

function getProductID(sCode)
  dim oConn, oRS, sSQL, nID:nID=0
  sSQL = "SELECT prID FROM tblProduct WHERE prCode='" & SQLFixUp(sCode) & "' AND NOT prIsChild=True;"
  set oConn = server.createobject("ADODB.Connection")
  oConn.open DSN_ECOM_DB
  set oRS = oConn.execute(sSQL)
  if not oRS.eof then
    nID = oRS(0)
  end if
  oRS.close
  set oRS = nothing
  oConn.close
  set oConn = nothing
  getProductID = nID
end function


function getGroupID(sCode)
  dim oConn, oRS, sSQL, nID:nID=0
  sSQL = "SELECT grID FROM tblGroup WHERE grCode='" & SQLFixUp(sCode) & "';"
  set oConn = server.createobject("ADODB.Connection")
  oConn.open DSN_ECOM_DB
  set oRS = oConn.execute(sSQL)
  if not oRS.eof then
    nID = oRS(0)
  end if
  oRS.close
  set oRS = nothing
  oConn.close
  set oConn = nothing
  getGroupID = nID
end function

function getRangeID(sRng)
  dim nID:nID=0
  select case sRng
    case "Baby"
      nID =	7
    case "Furniture"
      nID =	2
    case "Giving"
      nID =	3
    case "Indulge"
      nID =	8
    case "Kitchen"
      nID =	4
    case "Living"
      nID =	5
    case "Outdoor Living", "Outdoor living"
      nID =	13
    case "Paper"
      nID =	10
    case "Showroom"
      nID =	6
    case "Special"
      nID =	1
    case "Wedding"
      nID =	9
    case else
      nID = 0
  end select
  getRangeID = nID
end function

function xgetGroupID(sGrp, nRNID)
  dim nID:nID=0
  select case nRNID
    case 1
      select case sGrp
        case "End of Line"
          nID =	5
        case "Gifts"
          nID =	10
        case "Offers"
          nID =	4
        case "Other"
          nID =	3
        case else
          nId = 0
      end select
    case 2
      select case sGrp
        case "Beds"
          nID =	6
        case "Chairs & Sofas"
          nID =	7
        case "Storage"
          nID =	8
        case "Tables"
          nID =	9
        case else
          nId = 0
      end select
    case 3
      select case sGrp
        case "Gifts"
          nID =	49
        case "Gifts �10-�25"
          nID =	14
        case "Gifts �25-�50"
          nID =	15
        case "Gifts for Girls", "Gifts for girls"
          nID =	11
        case "Gifts for Men", "Gifts for men"
          nID =	12
        case "Gifts over �50"
          nID =	16
        case "Gifts under �10"
          nID =	13
        case else
          nId = 0
      end select
    case 4
      select case sGrp
        case "Caf�", "Cafe"
          nID =	17
        case "Entertaining"
          nID =	59
        case "Espresso Cups"
          nID =	19
        case "Favourites"
          nID =	20
        case "Kitchen Accessory", "Kichen accessory"
          nID =	21
        case "Napkins"
          nID =	22
        case "Nigella Lawson"
          nID =	23
        case else
          nId = 0
      end select
    case 5
      select case sGrp
        case "Accessories"
          nID =	24
        case "Baskets/Bags", "Baskets and Bags", "Baskets/bags"
          nID =	26
        case "Bed & Bath", "Bed and Bath", "Bed and bath", "Bed & bath"
          nID =	27
        case "Candles"
          nID =	28
        case "Entertaining"
          nID =	18
        case "Lighting"
          nID =	30
        case "Rugs"
          nID =	25
        case "Slippers/Shoes", "Slippers and Shoes", "Slippers/shoes"
          nID =	31
        case "Throws/Blankets", "Throws and Blankets", "Throws/blankets"
          nID =	32
        case "Vases/Containers", "Vases and Containers", "Vases/containers"
          nID =	33
        case else
          nId = 0
      end select
    case 6
      select case sGrp
        case "End of line"
          nID =	35
        case "Events"
          nID =	34
        case "Gallery"
          nID =	38
        case "Offers"
          nID =	36
        case "What's new"
          nID =	37
        case else
          nId = 0
      end select
    case 7
      select case sGrp
        case "Accessories", "Accessory"
          nID =	58
        case "Furniture"
          nID =	41
        case "Gifts"
          nID =	49
        case "Gifts for Mum"
          nID =	44
        case "Shoes/Clothes", "Shoes and Clothes", "Baby shoes"
          nID =	39
        case "Stationery"
          nID =	50
        case "Toys"
          nID =	60
        case else
          nId = 0
      end select
    case 8
      select case sGrp
        case "Accessories", "Accessory"
          nID =	42
        case "Girl Treats", "Girl treats"
          nID =	45
        case "Jewellery"
          nID =	46
        case "Pamper"
          nID =	47
        case else
          nId = 0
      end select
    case 9
      select case sGrp
        case "Confetti"
          nID =	51
        case "Gifts"
          nID =	49
        case "Stationery", "Albums and Jounals"
          nID =	50
        case else
          nId = 0
      end select
    case 10
      select case sGrp
        case "Albums/Journals", "Albums and Journals", "Albums and Jounals"
          nID =	54
        case "Cards"
          nID =	52
        case "Giftwrap/Tags", "Giftwrap and Tags"
          nID =	55
        case "Stationery"
          nID =	53
        case else
          nId = 0
      end select
    case 13
      select case sGrp
        case "Accessories", "Accessory"
          nID =	48
        case "Days Out"
          nID =	56
        case "Entertaining"
          nID =	29
        case "Lights/Lanterns", "Lights and Lanterns"
          nID =57
        case else
          nId = 0
      end select
    case else
      nID = 0
  end select
  getGroupID = nID
end function

sub importOrphans()
  sSql = "SELECT PR.* FROM tblProduct AS PR WHERE PR.pIsParent=False AND PR.pIsChild=False ORDER BY PR.pShortCode ASC;"
  call getRowsFromSourceBySQL(DSN_SOURCE, sSQL, aPRsrc) 
  For intIDX = 0 to ubound(aPRsrc, 2)
  	aFields = null
		redim aFields(PR_UBOUND,0)
    aFields(PR_ID, 0) = 0
		aFields(PR_KEYFINID,0) = INST_ID
		aFields(PR_DISPLAY, 0) = false
		aFields(PR_ONSALE, 0) = false
		aFields(PR_PROMO, 0) = false
		aFields(PR_PASSPARMS, 0) = true
		aFields(PR_ISPARENT, 0) = false
		aFields(PR_ISCHILD, 0) = false
		aFields(PR_SPECIALREQS, 0) = false
		aFields(PR_WRAPPABLE, 0) = true
    aFields(PR_DATE1, 0) = null
    aFields(PR_DATE2, 0) = null
   '// now get the real values from the recordset //
   
	aFields(PR_ID, 0) = 0 'aPRsrc(P_ID  , intIDX)
	aFields(PR_NAME, 0) = aPRsrc(P_NAME  , intIDX)
	aFields(PR_DESCSHORT, 0) = aPRsrc(P_DESCSHORT  , intIDX)
	aFields(PR_DESCLONG, 0) = aPRsrc(P_DESCLONG  , intIDX)
	aFields(PR_FURTHERINFO, 0) = "" 'aPRsrc(P_  , intIDX)
	aFields(PR_MISC1, 0) = "" 'aPRsrc(P_  , intIDX)
	aFields(PR_MISC2, 0) = "" 'aPRsrc(P_  , intIDX)
	aFields(PR_IMAGETHUMB, 0) = replace("" & aPRsrc(P_IMAGETHUMB  , intIDX), "T01", "S01")
	aFields(PR_IMAGEMAIN, 0) = aPRsrc(P_IMAGETHUMB  , intIDX)
	aFields(PR_IMAGELARGE, 0) = aPRsrc(P_IMAGEMAIN  , intIDX)
	aFields(PR_FILE1, 0) = aPRsrc(P_IMAGELARGE  , intIDX) '// large image //
	aFields(PR_FILE2, 0) = "" 'aPRsrc(P_  , intIDX)
	aFields(PR_FILE3, 0) = "" 'aPRsrc(P_  , intIDX)
  aFields(PR_DATE1, 0) = null
  aFields(PR_DATE2, 0) = null
	aFields(PR_HTMLPAGE, 0) = "" 'aPRsrc(P_  , intIDX)
	aFields(PR_BUYPAGE, 0) = "" 'aPRsrc(P_  , intIDX)
	aFields(PR_PASSPARMS, 0) = aPRsrc(P_WRAPPABLE  , intIDX) '// reusing for Wrappability 'true 'aPRsrc(P_  , intIDX)
	aFields(PR_ISPARENT, 0) = false 'aPRsrc(P_ISPARENT  , intIDX)
	aFields(PR_ISCHILD, 0) = false 'aPRsrc(P_ISCHILD  , intIDX)
	aFields(PR_SPECIALREQS, 0) = aPRsrc(P_SPECIALREQS  , intIDX)
	aFields(PR_REQSTEXT, 0) = aPRsrc(P_REQSTEXT  , intIDX)
	aFields(PR_DISPLAY, 0) = aPRsrc(P_DISPLAY  , intIDX)
	aFields(PR_ONSALE, 0) = aPRsrc(P_ONSALE  , intIDX)
	aFields(PR_PROMO, 0) = aPRsrc(P_PROMO  , intIDX)
	aFields(PR_KEYWORDS, 0) = aPRsrc(P_KEYWORDS  , intIDX)
	aFields(PR_METATITLE, 0) = aPRsrc(P_METATITLE  , intIDX)
	aFields(PR_METADESC, 0) = aPRsrc(P_METADESC  , intIDX)
	aFields(PR_COLOUR, 0) = aPRsrc(P_COLOUR  , intIDX)
	aFields(PR_SIZE, 0) = aPRsrc(P_SIZE  , intIDX)
	aFields(PR_CODE, 0) = aPRsrc(P_CODE  , intIDX)
	aFields(PR_SHORTCODE, 0) = aPRsrc(P_SHORTCODE  , intIDX)
	aFields(PR_BARCODE, 0) = aPRsrc(P_BARCODE  , intIDX)
	aFields(PR_KEYFPTID, 0) = aPRsrc(P_KEYFPTID  , intIDX)
	aFields(PR_KEYFMFID, 0) = aPRsrc(P_KEYFMID  , intIDX)
	aFields(PR_KEYFPCID, 0) = aPRsrc(P_KEYFPCID  , intIDX)
	aFields(PR_KEYFRCID, 0) = aPRsrc(P_KEYFRCID  , intIDX)
	aFields(PR_KEYFSZID, 0) = aPRsrc(P_KEYFSCID  , intIDX)
	aFields(PR_KEYFWCID, 0) = aPRsrc(P_KEYFWCID  , intIDX)
	aFields(PR_KEYFLTID, 0) = aPRsrc(P_KEYFLTID  , intIDX)
	aFields(PR_PARENTID, 0) = 0 'aPRsrc(P_  , intIDX)
	aFields(PR_PRICE, 0) = aPRsrc(P_PRICE  , intIDX)
  aFields(PR_VAT, 0) = aPRsrc(P_VAT  , intIDX)
	aFields(PR_PROMOPRICE, 0) = aPRsrc(P_PROMOPRICE  , intIDX)
  aFields(PR_PROMOVAT, 0) = aPRsrc(P_PROMOVAT  , intIDX)
	aFields(PR_STOCKQTY, 0) = aPRsrc(P_STOCKQTY  , intIDX)
	aFields(PR_STOCKTRIGGER, 0) = aPRsrc(P_STOCKTRIGGER  , intIDX)
   '// now just save the bugger //
  	nErr = saveProduct(aFields)
    if nErr = 0 then
      call updateSourceProduct(aPRsrc(P_ID  , intIDX), aFields(PR_ID, 0))
    end if
    response.write " x . ":response.flush
  Next 
  response.write "<br />imported " & intIDX + 1 & " orphans."
end sub

function updateSourceProduct(nOldID, nNewID)
  dim sSQL:sSQL="UPDATE tblProduct SET TempNID=" & nNewID & " WHERE pID=" & nOldId & ";"
  sqlexec DSN_SOURCE, sSQL
  updateSourceProduct = 0
end function


Function InsertID(sDSN,sSQL)
	'Insert in to new prod table
	dim oConn
	if len("" & sSQL) > 0 then
		set oConn = server.createobject("ADODB.Connection")
		oConn.open sDSN
		oConn.execute sSQL
		oConn.close
		set oConn = nothing
	end if
	'select top 1 from table just inserted into ordered by ID DESC
	sSQL = "SELECT TOP 1 PR.prID FROM tblProduct AS PR ORDER BY PR.prID DESC"
	call getRowsBySQL(DSN_ECOM_DB, sSQL, aNewID)
	'update source table with new ID
End Function


'// generic row fetcher for source DB //
function getRowsFromSourceBySQL(sDSN, sSQL, aFields)
	dim nErr:nErr = 0
	dim oConn, oRS
	if len("" & sSQL) > 0 then
		set oConn = server.createobject("ADODB.Connection")
		oConn.open sDSN
		set oRS = oConn.execute(sSQL)
		if not oRS.eof then
			aFields = oRS.getRows
		else
			'// no record found //
			nErr = 2
			aFields = null
		end if
		oRS.close
		set oRS = nothing
		oConn.close
		set oConn = nothing
	else
		'// bad parms //
		nErr = 1
		aFields = null
	end if
	getRowsFromSourceBySQL = nErr
end function
%>