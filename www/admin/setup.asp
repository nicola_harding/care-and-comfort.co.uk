<!--#include virtual="/admin/userscripts/setup.asp"-->

<%' ini_common is mandatory - must come before the ini_cms or ini_ecomm.%>
<!--include virtual="/includes/ini_common.asp"-->
<!--#include virtual="/includes/ini_cms.asp"-->
<!--#include virtual="/includes/ini_ecomm.asp"-->

<!--#include virtual="/admin/rootscripts/setup.asp"-->
<!--#include virtual="/admin/cmsscripts/setup.asp"-->
<!--#include virtual="/admin/ecommscripts/setup.asp"-->
<%
'// field names //
'const ADM_PR_MISC1 = "Authors"
'const ADM_PR_MISC2 = "Contributors"
'const ADM_PR_FILE1 = "Summary File"
'const ADM_PR_FILE2 = "Full Report"
'const ADM_PR_FILE3 = "File 3"
'const ADM_PR_DATE1 = "Publication Date"
'const ADM_PR_DATE2 = "Date 2"
'// colors //
Const HR_CLR		  = "#B2B1B1" '#010066
Const TBL_BG_CLR	  = "#FFFFFF"
Const HDR_BG_CLR	  = "#CC99CC" 'xxx
Const ROW_CLR_0 	  = "#FCFCFC"
Const ROW_CLR_1 	  = "#F6F6F6"
Const BDY_BG_CLR 	  = "#FFFFFF"
Dim g_aROW_CLR:g_aROW_CLR = Array(ROW_CLR_0,ROW_CLR_1)

Sub adminHeader
%>
	<html>
	<head>
	<meta http-equiv="Content-Language" content="en-gb">
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
	<title>Admin</title>
	<script language="JavaScript" type="Text/JavaScript">
	<!--
	//function PopUpWindow (url, hWind, nWidth, nHeight, nScroll, nResize) {
	//	var cToolBar = 'toolbar=0,location=0,directories=0,status=' + nResize + ',menubar=0,scrollbars=' + nScroll + ',resizable=' + nResize + ',width=' + nWidth + ',height=' + nHeight;
	//	var popupwin = window.open(url, hWind, cToolBar);
	//	return popupwin;
	//}
	//-->
	</script>
	<style type="text/css">
	<!--
	body, p, td, tr, form, input, table, select, submit, button, textarea {
		font-family: arial, verdana, helvetica, sans-serif;
		font-size: 9pt;
		color: #333333;
	}
	body, html {
		background-color: <%=BDY_BG_CLR%>;
		margin:  0;
	    padding: 0;
	    border:  0 inherit;
	}
	b.adminHeader {
		color: #ffffff;
	}
	a {
		font-family: arial, verdana, helvetica, sans-serif;
		font-size: 8pt;
		font-weight: bold;
	}
	a:link {		
		color: #0068B4;
		text-decoration: none;
	}
	a:visited {		
		color: #0068B4;
		text-decoration: none;
	}
	a:hover {		
		color: #ff0000;
		text-decoration: underline;
	}
	input, select, textarea, button, submit {
		color: #000000;
		background-color: #ffffff;
	}
	h4 {
		font-family: arial, verdana, helvetica, sans-serif;
		font-size: 13pt;
		color: #010066;
	}
	.adminTitle {
		color: #ffffff;
		font-weight: bold;
	}
	a.adminTitle:visited, a.adminTitle:link {
		color: #ffffff;
		font-weight: bold;
	}
	//-->
	</style>
	</head>
	
	<body>
	<table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="#ffffff"><tr><td><br /><br />&nbsp;&nbsp;&nbsp;<img src="/admin/admin_header.gif" border="0"></td></tr></table>
	

<br />&nbsp;&nbsp;
	[ <a href="./">Section Admin Home</a> ] - [ <a href="<%=ADMIN_URL%>">Root Admin Home</a> ] - [ <a href="<%=ADMIN_URL%>/userscripts/logout.asp">Log Out</a>]
	
<table><tr><td>&nbsp;</td><td>
	<h4>Administration</h4>
<%
End Sub

Sub adminFooter
%>

	[ <a href="./">Section Admin Home</a> ] - [ <a href="<%=ADMIN_URL%>">Root Admin Home</a> ] - [ <a href="<%=ADMIN_URL%>/userscripts/logout.asp">Log Out</a>]
	
	</td></tr></table>
	</body>
	</html>
<%
End Sub
%>