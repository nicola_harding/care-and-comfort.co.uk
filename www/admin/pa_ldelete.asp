<!--#include virtual="/scripts/aspstart.asp"-->
<!--#include virtual="/scripts/adovbs.asp"-->
<!--#include file="./setup.asp"-->
<!--#include virtual="/includes/partnersAPI.asp"-->
<%
'//////////////////////////////////
'// Main entry point to the page //
dim nPerms, sUT
sUT = SESSION("USER_TYPE")
If sUT = "SUR" Then
	nPerms = PERM_ALL
Else
	nPerms = GetPermissions(SESSION("PERMISSIONS"), AREA_DIST)
End If

dim aFields, nID, sMsg, bStatus
dim nErr:nErr = 0

nID = clng("0" & request.querystring("pa"))
'if (nPerms and PERM_MODIFY) = 0 then response.redirect("./")

if request.form("submitted") = "yes" then
	'// hoof up the form variables //
	nID = clng("0" & request.form("paID"))
	nErr = nErr + deletePartner(nID)
	if nErr = 0 then
		response.redirect "Partners.asp?sql=y"
	else
		nErr = loadPartner(nID, aFields)
		Call adminHeader
		Call displayDeleteForm(aFields, "There was a problem saving the record details! Please try again.<br />")
		Call adminFooter
	end if
elseif nID > 0 then
	'// load up the vars from the dB //
	nErr = loadPartner(nID, aFields)
	if nErr = 0 then
		Call adminHeader
		Call displayDeleteForm(aFields, "")
		Call adminFooter
	else
		response.redirect "./Partners.asp?sql=y"
	end if
else
	response.redirect "./Partners.asp?sql=y"
end if

'// End main entry point to page //
'//////////////////////////////////

'// sub to display new newsletter form //
Sub displayDeleteForm(aFields, sMsg)
	dim nErr:nErr = 0
  dim nIdx:nIdx=0
  dim x:x=0
	response.write javaConfirmDelete()
%>
<b>Partner Administration<br />&nbsp;</b>
<br />
<%
	'if nPerms And PERM_READ then
	  response.write "[&nbsp;<a href=""pa_lview.asp?pa=" & aFields(pa_ID, 0) & """>View</a>&nbsp;]&nbsp;"
	'end if
	'if nPerms And PERM_MODIFY then
	  response.write "[&nbsp;<a href=""pa_ledit.asp?pa=" & aFields(pa_ID, 0) & """>Edit</a>&nbsp;]&nbsp;"
	'end if
	'if nPerms And PERM_DELETE then
	  response.write "[&nbsp;<a href=""pa_ldelete.asp?pa=" & aFields(pa_ID, 0) & """>Delete</a>&nbsp;]&nbsp;"
	'end if
	'if nPerms And PERM_WRITE then
	  response.write "[&nbsp;<a href=""pa_ledit.asp?pa=0"">Add New</a>&nbsp;]&nbsp;"
	'end if
%>&nbsp;<br />
<%
if len("" & sMsg) > 0 then
	response.write "There was a problem with the data.<br /><b>" & sMsg & "</b>"
end if
%>&nbsp;
<p><b>Please confirm that you wish to delete the following record.<br />
Deletion is PERMANENT and CANNOT be UNDONE.<br />
Remember there may be other records which are related to this one or link to it's information.<br />
If you are unsure please click 'Cancel', you could always just archive the record instead.
</b></p>
<form name="editStatus" action="<%=request.servervariables("SCRIPT_NAME") & "?pa=" & aFields(pa_ID, 0)%>" method="POST">
<input type="hidden" name="submitted" value="yes" />
<input type="hidden" name="paID" value="<%=aFields(pa_ID, 0)%>" />

	<table border="0" cellpadding="0" cellspacing="0"><tr><td valign="top">

<table cellspacing="0" cellpadding="0" border="0" width="100%">
<tr bgcolor="<%=HDR_BG_CLR%>"><td>&nbsp;<b class="adminTitle">Username</b>&nbsp;</td></tr>
<tr bgcolor="<%=ROW_CLR_0%>"><td>&nbsp;<%=aFields(PA_USERNAME, 0)%>&nbsp;</td></tr>
<tr bgcolor="<%=HDR_BG_CLR%>"><td>&nbsp;<b class="adminTitle">Password</b>&nbsp;</td></tr>
<tr bgcolor="<%=ROW_CLR_0%>"><td>&nbsp;<%=aFields(PA_PASSWORD, 0)%>&nbsp;</td></tr>
<tr bgcolor="<%=HDR_BG_CLR%>"><td>&nbsp;<b class="adminTitle">First Name</b>&nbsp;</td></tr>
<tr bgcolor="<%=ROW_CLR_0%>"><td>&nbsp;<%=aFields(PA_FIRSTNAME, 0)%>&nbsp;</td></tr>
<tr bgcolor="<%=HDR_BG_CLR%>"><td>&nbsp;<b class="adminTitle">Last Name</b>&nbsp;</td></tr>
<tr bgcolor="<%=ROW_CLR_0%>"><td>&nbsp;<%=aFields(PA_LASTNAME, 0)%>&nbsp;</td></tr>
<tr bgcolor="<%=HDR_BG_CLR%>"><td>&nbsp;<b class="adminTitle">Company Name</b>&nbsp;</td></tr>
<tr bgcolor="<%=ROW_CLR_0%>"><td>&nbsp;<%=aFields(PA_COMPANYNAME, 0)%>&nbsp;</td></tr>
<tr bgcolor="<%=HDR_BG_CLR%>"><td>&nbsp;<b class="adminTitle">Email</b>&nbsp;</td></tr>
<tr bgcolor="<%=ROW_CLR_0%>"><td>&nbsp;<%=aFields(PA_EMAIL, 0)%>&nbsp;</td></tr>

<tr bgcolor="<%=HDR_BG_CLR%>"><td>&nbsp;<b class="adminTitle">Level</b>&nbsp;</td></tr>
<tr bgcolor="<%=ROW_CLR_0%>"><td>&nbsp;<%=aFields(PA_KEYFLVLID, 0)%>&nbsp;</td></tr>

<tr bgcolor="<%=HDR_BG_CLR%>"><td>&nbsp;<b class="adminTitle">Last Login</b>&nbsp;</td></tr>
<tr bgcolor="<%=ROW_CLR_0%>"><td>&nbsp;<%=aFields(PA_LASTLOGIN, 0)%>&nbsp;</td></tr>

</table>

	
	</td></tr>
	<tr><td colspan="3">&nbsp;<br />
	
<input type="submit" name="submit" value="delete record" onClick="return confirmDeletion();" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type="button" name="cancel" value="cancel" onClick="window.location.href='Partners.asp?sql=y';" />
	
	</td></tr>
	</table>
	
</form>

	&nbsp;<br />
	<a href="Partners.asp?sql=y">Partners</a>&nbsp;<br />
<%
End Sub
%>
